<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Wakuliner</title>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="images/favicon.ico">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <style type="text/css">
    </style>
	<style>
	#main-slider .item3 {
	  height: 260px;
	  background-repeat: no-repeat;
	  background-position: 80% 0;
	  background-size: cover;
	}
	.media.service-box {
  margin: 25px 0;
}

.media.service-box .pull-left {

  margin-left: -25px;

}

.media.service-box .pull-left > i {
  font-size: 24px;
  height: 64px;
  line-height: 64px;
  text-align: center;
  width: 64px;
  border-radius: 100%;
  color: #45aed6;
  box-shadow: inset 0 0 0 1px #d7d7d7;
  -webkit-box-shadow: inset 0 0 0 1px #d7d7d7;
  transition: background-color 400ms, background-color 400ms;
  position: relative;
}
.media.service-box .pull-left > i:after {
  content: "";
  position: absolute;
  width: 20px;
  height: 20px;
  top: 50%;
  margin-top: -10px;
  right: -10px;
  border: 4px solid #fff;
  border-radius: 20px;
  background: #45aed6;
}
	</style>

</head><!--/head-->

<?
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
	$mobile = 1;
} else {
	$mobile = 0;
}
?>

<style>
<? if($mobile == 1) { ?>
#img_m {
	width:50%;
	margin-left:20px;
}
<? } ?>
</style>

<body id="home" class="homepage">

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
					<?
						if($mobile == 1) {
							echo " <a class='navbar-brand' href='index.php'><img height='57px' src='images/logo.png' alt='logo'></a>";
						} else {
							echo " <a class='navbar-brand' href='index.php'><img height='57px' src='images/logo.png' alt='logo'></a>";
						}
					?>

                </div>

                <div class="collapse navbar-collapse navbar-right">
				<? if($mobile == 0) {
					$br = '<br>';
				} else {
					$br = '';
				}
				?>
                    <ul class="nav navbar-nav">
                        <li class="scroll"><a class='menu-wrap' href="index.php">Apa itu <?= $br; ?>WAKULINER</a></li>
                        <li class="scroll"><a class='menu-wrap' href="index.php#features">Cara <?= $br; ?>Ber-WAKULINER</a></li>
                        <li class="scroll"><a class='menu-wrap' href="index.php#cta2">Mengapa <?= $br; ?>Ber-WAKULINER</a></li>
						<?
						if($mobile == 1) {
							echo "<li class='scroll'><a class='menu-wrap' href='joinus.php'>Join Us</a></li>";
						} else { ?>
							<li><a class='menu-wrap' href="joinus.php"><img class='img-responsive' width='50%' style='margin-top:-10px;' src='images/btnjoin.png'></a></li>
						<?
						}
                        ?>
                    </ul>
                 </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->

    <section id="features">
        <div class="container">
            <div class="section-header">
                <h2 class="title text-center wow fadeInDown" style='font-family:Arista light,sans-serif;'>Cara Menggunakan Wakuliner</h2>
            </div>

			<p align="center">
				<div align="center">
					<div style='width:75%' >
						1. Gunakan <b> WAKU-ANTAR</b> jika Anda ingin pesan makanan atau snack/oleh-oleh, dan menerima antaran di lokasi Anda atau orang kesayangan Anda<br><br>
						2. Gunakan <b>WAKU-WIKU</b> jika Anda ingin makan di luar atau menikmati destinasi-destinasi kuliner yang legendaris dan populer<br><br>
						Untuk lebih detil nya, silahkan mengikuti panduan-panduan dibawah ini<br>
					</div>
				</div>
			</p><br><br>

			<div class="col-sm-10 col-sm-offset-31 text-center" align="center">
			<? if($mobile == 1) { ?>

				<div onclick='wakuantar();' id='wakuantar' style='display:block;float:left;background-color:#d7fad9;width:30%;height:50px;margin-left:10px;cursor:pointer;border-style:groove;'>
					<img src='images/wakuantar.png' style='width:90%;margin-top:8px;'>
				</div>
				<div onclick='wakuantar();' id='wakuantar2' style='display:none;float:left;background-color:#f6f6f6;width:30%;height:50px;margin-left:10px;cursor:pointer;border-style:groove;'>
					<img src='images/wakuantar.png' style='width:90%;margin-top:8px;'>
				</div>

				<div onclick='wakuwiku();' id='wakuwiku' style='float:left;display:none;background-color:#d7fad9;width:30%;height:50px;margin-left:10px;border-style:groove;'>
					<img src='images/wakuwikuhorisontal.png' style='width:90%;margin-top:8px;cursor:pointer;'>
				</div>
				<div onclick='wakuwiku();'id='wakuwiku2' style='display:block;float:left;background-color:#f6f6f6;width:30%;height:50px;margin-left:10px;border-style:groove;'>
					<img src='images/wakuwikuhorisontal.png' style='width:90%;margin-top:8px;'>
				</div>

				<!--
				<div onclick='panduan_cms();' id='panduan_cms' style='display:none;float:left;background-color:#d7fad9;width:30%;height:60px;margin-left:10px;cursor:pointer;'>
					<img src='images/panduan_cms.png' style='width:90%;margin-top:8px;cursor:pointer;'>
				</div>
				<div onclick='panduan_cms();' id='panduan_cms2' style='display:block;float:left;width:30%;height:60px;margin-left:10px;cursor:pointer;'>
					<img src='images/panduan_cms.png' style='width:90%;margin-top:8px;cursor:pointer;'>
				</div>
				-->

			<? } else { ?>

                <div class="col-sm-3 col-md-30" style='margin-left:50px;'>
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="team-img" id='waku-hov'>
							<div onclick='wakuantar();' id='wakuantar' style='display:block;background-color:#d7fad9;height:60px;cursor:pointer;border-style:groove;'>
								<img src='images/wakuantar.png' style='margin-top:8px;'>
							</div>
							<div onclick='wakuantar();' id='wakuantar2' style='display:none;background-color:#f6f6f6;height:60px;cursor:pointer;border-style:groove;'>
								<img src='images/wakuantar.png' style='margin-top:8px;'>
							</div>
						</div>
                    </div>
                </div>

				<div class="col-sm-3 col-md-30">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
                        <div class="team-img" id='waku-hov'>
							<div onclick='wakuwiku();' id='wakuwiku' style='display:none;background-color:#d7fad9;height:60px;border-style:groove;'>
								<img src='images/wakuwikuhorisontal.png' style='margin-top:8px;cursor:pointer;'>
							</div>

							<div onclick='wakuwiku();'id='wakuwiku2' style='display:block;background-color:#f6f6f6;height:60px;cursor:pointer;border-style:groove;'>
								<img src='images/wakuwikuhorisontal.png' style='margin-top:8px;'>
							</div>
                        </div>
                    </div>
                </div>

				<!--
                <div class="col-sm-3 col-md-30" >
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">
                        <div class="team-img" id='waku-hov'>
							<div onclick='panduan_cms();' id='panduan_cms' style='display:none;background-color:#d7fad9;height:60px;'>
								<img src='images/panduan_cms.png' style='margin-top:8px;cursor:pointer;'>
							</div>
							<div onclick='panduan_cms();' id='panduan_cms2' style='display:block;height:60px;'>
								<img src='images/panduan_cms.png' style='margin-top:8px;cursor:pointer;'>
							</div>
                        </div>
                    </div>
                </div>
				-->

			<? } ?>
			</div>

			<script>

				function wakuantar() {
					document.getElementById('wakuwiku_content').style.display = 'none';
					document.getElementById('wakuwiku').style.display = 'none';
					document.getElementById('wakuwiku2').style.display = 'block';

					document.getElementById('wakuantar_content').style.display = 'block';
					document.getElementById('wakuantar2').style.display = 'none';
					document.getElementById('wakuantar').style.display = 'block';

					<!--
					document.getElementById('cms_content').style.display = 'none';
					document.getElementById('panduan_cms2').style.display = 'block';
					document.getElementById('panduan_cms').style.display = 'none';
					-->
				}

				function wakuwiku() {
					document.getElementById('wakuwiku_content').style.display = 'block';
					document.getElementById('wakuwiku').style.display = 'block';
					document.getElementById('wakuwiku2').style.display = 'none';

					document.getElementById('wakuantar_content').style.display = 'none';
					document.getElementById('wakuantar2').style.display = 'block';
					document.getElementById('wakuantar').style.display = 'none';

					<!--
					document.getElementById('cms_content').style.display = 'none';
					document.getElementById('panduan_cms2').style.display = 'block';
					document.getElementById('panduan_cms').style.display = 'none';
					-->
				}

				<!--
				function panduan_cms() {
					document.getElementById('wakuwiku_content').style.display = 'none';
					document.getElementById('wakuwiku').style.display = 'none';
					document.getElementById('wakuwiku2').style.display = 'block';

					document.getElementById('wakuantar_content').style.display = 'none';
					document.getElementById('wakuantar2').style.display = 'block';
					document.getElementById('wakuantar').style.display = 'none';

					document.getElementById('cms_content').style.display = 'block';
					document.getElementById('panduan_cms2').style.display = 'none';
					document.getElementById('panduan_cms').style.display = 'block';

					document.getElementById('bg-feat').style.display = 'none';
				}
				-->
			</script>

			<style>

			#waku-hov:hover {
				background-color: #b8f6bc;
			}

			#waku-hov2:hover {
				/*
				border-style: solid;border-color:#fffbb4 #fffbb4 #11e91c #fffbb4;
				*/
				background-color: #b8f6bc;
			}

			.tab_jual {
				border: 4px solid #fffbcb;
				padding: 10px 40px;
				background: #fffbcb;
				width: 250px;
				height:85px;
				color:#28b328;
				border-radius: 10px;
			}

			.tab_active {
				border: 4px solid #00aa0b;
				padding: 10px 40px;
				background: #00aa0b;
				width: 250px;
				height:85px;
				color:#fff;
				border-radius: 10px;
			}

			/* Style the tab content */
			.tabcontent {
				display: none;
				padding: 6px 12px;
				border: 1px solid #ccc;
				border-top: none;
			}
			</style>

			<div id='wakuantar_content'>
				<div class="col-sm-10 col-sm-offset-1 text-center" style='margin-bottom:20px; margin-top:50px;'>
					<div class="team-info" style='text-align:center;' style='width:75%'>
						<p align="left">
							<div align="left">
								<div style='width:90%' >

									<img src='images/panduan-pelanggan/PanduanAntar1.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanAntar2.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanAntar3.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanAntar4.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanAntar5.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanAntar6.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanAntar7.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanAntar8.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanAntar9.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanAntar10a.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanAntar10b.png' style='width:100%;height:100%;' align='left'/> <br><br>

								</div>
							</div>
						</p>
					</div>
				</div>
			</div>

			<div id='wakuwiku_content' style='display:none;'>
				<div class="col-sm-10 col-sm-offset-1 text-center" style='margin-bottom:20px; margin-top:50px;'>
					<div class="team-info" style='text-align:center;'>
						<p align="left">
							<div align="left">
								<div style='width:90%' >
									<img src='images/panduan-pelanggan/PanduanWiku1.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanWiku2.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanWiku3.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanWiku4.png' style='width:100%;height:100%;' align='left'/> <br><br>

									<img src='images/panduan-pelanggan/PanduanWiku5.png' style='width:100%;height:100%;' align='left'/> <br><br>

								</div>
							</div>
						</p>
					</div>
				</div>
			</div>

			<!--
			<div id='cms_content' style='display:none;'>
				<div class="col-sm-10 col-sm-offset-1 text-center" style='margin-bottom:20px; margin-top:50px;'>
					<div class="team-info" style='text-align:center;'>
						<p align="left">
							<div align="left">
								<div style='width:90%' >
									<img src='images/panduan-pelanggan/PanduanCMS1.png' style='width:100%;height:100%;' align='left'/> <br>
									Login menggunakan alamat email dan password yang Anda gunakan waktu mendaftarkan Toko Anda di aplikasi. Gunakan Google Chrome / Firefox sebagai browser Anda. Jangan menggunakan Internet Explorer sebagai browser untuk membuka Wakuliner.<br>
								</div>
							</div>
						</p>
					</div>
				</div>
			</div>
			-->

        </div>
    </section>


    <section id="testimonial">
        <div class="container">
            <div class="section-header">
                <h2 class="title text-center wow fadeInDown" style='font-family:Arista light,sans-serif;'>Download Merchant App <br> Sekarang Juga!</h2>
            </div>
			<?
			if($mobile == 1) { ?>
				<a href='https://play.google.com/store/apps/details?id=com.bigit.wakuliner.merchant' target='_blank'>
					<img class='img-responsive' style='width:40%; float:left; margin-left:5%; margin-right:10%;' src='images/play1.png'>
				</a>
				<a href='https://itunes.apple.com/us/app/renilukaw/id1074598371?mt=8' target='_blank'>
					<img class='img-responsive' style='width:40%;' src='images/appstore2.png'>
				</a>

			<?
			} else {
			?>
			<div class="col-sm-3 col-sm-offset-3">
				<div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
				<a href='https://play.google.com/store/apps/details?id=com.bigit.wakuliner.merchant' target='_blank'>
					<div class="team-img">
						<img class='img-responsive' style='width:90%;' src='images/play1.png'>
					</div>
				</a>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
				<a href='https://itunes.apple.com/us/app/renilukaw/id1074598371?mt=8' target='_blank'>
					<div class="team-img">
						<img class='img-responsive' <? if($mobile == 1) { echo "style='width:80%;'"; } else { echo "style='width:95%;'"; } ?>  src='images/appstore2.png'>
					</div>
				</a>
				</div>
			</div>
			<? } ?>

        </div>
<?
		/*
		if($mobile == 0) {
			echo "<div style='margin-bottom:350px;'></div>";
		}
		else {
			echo "<div style='margin-bottom:150px;'></div>";
		}
		*/
		?>
<style>


.marginbottom {
		margin-bottom:350px;
}

@media screen and (orientation:landscape) {
	.marginbottom {
		margin-bottom:350px;
	}
}

@media screen and (orientation:portrait) {
	.marginbottom {
		margin-bottom:150px;
	}
}
</style>
		<div class='marginbottom'></div>
		<img id='foot' src="images/wakutunggu.png" alt=""/>


    </section><!--/#testimonial-->


    <footer id="footer">
        <div class="container">
            <div class="row" style='margin-bottom:20px;'>
			<? if($mobile == 1) { ?>
                <div class="text-center col-sm-3" style='font-size:10px;'>
                    &copy; 2017 Wakuliner. All rights reserved
                </div>
                <div class="text-center col-sm-6" style='font-size:5px;'>
                    <ul class="social-icons">
                        <li><a href="index.php#home">Apa Itu Wakuliner</a></li>
                        <li><a href="index.php#features">Cara Ber-WAKULINER</a></li>
                        <li><a href="index.php#cta2">Mengapa Ber-WAKULINER</a></li>
                        <li><a href="joinus.php">Join Us</a></li>
                    </ul>
                </div>
            </div>

			<div class="text-center col-sm-5 col-sm-offset-2">
				<ul class="social-icons">
					<li><a href="http://www.facebook.com/wakuliner/" target='_blank'><img src='images/fb.png'></a></li>
					<!--
					<li><a href="#"><img src='images/twit.png'></a></li>
					!-->
					<li><a href="http://www.instagram.com/wakuliner/" target='_blank'><img src='images/ig.png'></a></li>
				</ul>
			</div>
			<? } else { ?>
				<div class="col-sm-3">
                    &copy; 2017 Wakuliner. All rights reserved
                </div>
                <div class="col-sm-9">
                    <ul class="social-icons">
                        <li><a href="index.php#home">Apa Itu Wakuliner</a></li>
                        <li><a href="index.php#features">Cara Ber-WAKULINER</a></li>
                        <li><a href="index.php#cta2">Mengapa Ber-WAKULINER</a></li>
                        <li><a href="joinus.php">Join Us</a></li>
                    </ul>
                </div>
            </div>

			<div class="col-sm-5 col-sm-offset-2">
				<ul class="social-icons">
					<li><a href="http://www.facebook.com/wakuliner/" target='_blank'><img src='images/fb.png'></a></li>
					<!--
					<li><a href="#"><img src='images/twit.png'></a></li>
					!-->
					<li><a href="http://www.instagram.com/wakuliner/" target='_blank'><img src='images/ig.png'></a></li>
				</ul>
			</div>
			<? } ?>
        </div>
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mousescroll.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>