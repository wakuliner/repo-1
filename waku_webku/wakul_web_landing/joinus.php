<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Wakuliner</title>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="images/favicon.ico">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

</head><!--/head-->

<style>

.form_join {
    border: 4px solid #fff;
    padding: 10px 40px;
    background: #fff;
    width: 500px;
    border-radius: 30px;
}

#contactLink {
position:absolute;
	background-color:#006306;
	margin-left:-38px;
	margin-top:20px;
    height: 50px;
    width: 488px;
	/*
	background-image: url(images/send.png);
	*/
    display: block;
    cursor: pointer;
	border-bottom-right-radius:2em;
	border-bottom-left-radius:2em;
}

#bukadaftar {
    display: block;
	background-color:#1eb528;
    cursor: pointer;
	border-radius:30px;
	height: 50px;
	color:#fff;
	width:30%;
}

#bukadaftar2 {
    display: block;
	background-color:#1eb528;
    cursor: pointer;
	border-radius:30px;
	height: 50px;
	color:#fff;
	width:100%;
}

.form_join2 {
	margin-top:30px;
    border: 4px solid #fff;
    padding: 10px 40px;
    background: #fff;
    width: 100%;
    border-radius: 30px;
}

#contactLink2 {
position:absolute;
	background-color:#006306;
	margin-left:-38px;
	margin-top:20px;
    height: 50px;
    width: 95%;
	/*
	background-image: url(images/send.png);
	*/
    display: block;
    cursor: pointer;
	border-bottom-right-radius:2em;
	border-bottom-left-radius:2em;
}
</style>

<?
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
	$mobile = 1;
} else {
	$mobile = 0;
}
?>

<body id="home" class="homepage">

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?
						if($mobile == 1) {
							echo " <a class='navbar-brand' href='index.php'><img height='57px' src='images/logo.png' alt='logo'></a>";
						} else {
							echo " <a class='navbar-brand' href='index.php'><img height='57px' src='images/logo.png' alt='logo'></a>";
						}
					?>
                </div>

				<? if($mobile == 0) {
					$br = '<br>';
				} else {
					$br = '';
				}
				?>

                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="active"><a class='menu-wrap' onclick="location.href='index.php#home'" href="#">Apa itu <?= $br; ?>WAKULINER</a></li>
                        <li><a class='menu-wrap' onclick="location.href='index.php#features'" href="#">Cara <?= $br; ?>Ber-WAKULINER</a></li>
                        <li><a class='menu-wrap' onclick="location.href='index.php#cta2'" href="#">Mengapa <?= $br; ?>Ber-WAKULINER</a></li>
						<?
						if($mobile == 1) {
							echo "<li><a class='menu-wrap' href='joinus.php'>Join Us</a></li>";
						} else { ?>
							<li><a class='menu-wrap' href="joinus.php"><img class='img-responsive' width='50%' style='margin-top:-10px;' src='images/btnjoin.png'></a></li>
						<?
						}
                        ?>
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->

    <section id="cta3" class="wow fadeIn">
        <div class="container">
            <div class="section-header">
                <h2 class="title text-center wow fadeInDown" style='color:#fff;font-weight:bold;font-family:Arista light,sans-serif;'>                Daftarkan Toko Anda dan Langsung Mulai Jualan di Wakuliner!</h2>
            </div>

        </div>
    </section><!--/#cta-->

    <section id="features" style="padding:50px 0;">
        <div class="container">
			<div class="sub-header">
                <p class="sub-title col-sm-offset-1"
                	style='<? if($mobile == 1)
                				{ echo "font-size:15px;"; } else { echo "font-size:15px;"; }
                			?> line-height:30px;'>
                Ayo daftarkan toko Anda dan mulai berjualan! Ga pakek lama, ga pake ribet! Gratis!<br><br>
                Anda punya toko online atau usaha rumahan di bidang kuliner, restoran, depot, atau cafe? Mau punya <b>toko kuliner online</b> dan kendali penuh terhadap produk dan pemesanan Anda? Yuk jadi Waku-Merchant di Wakuliner, Wadah kuliner pertama di Indonesia.<br><br>

				Syarat untuk menjadi Waku-Merchant:<br><br>
				1. Punya smartphone, untuk download aplikasi Merchant (Android atau iOS).<br>
				*Kenapa harus ada smartphone?*<br>
				Pesanan dari pelanggan akan langsung masuk ke dalam aplikasi di smartphone, yang telah kami <b>design secara khusus untuk kuliner</b>, sehingga Anda dapat melayani pesanan dengan cepat dan akurat. <br><br>
				2. Punya logo tempat usaha<br><br>
				3. Punya foto menu<br><br>
				4. Bisa antar pesanan dengan JNE atau kurir sendiri <i>(Wakuliner tidak menyediakan kurir pengantaran)</i><br><br>
				5. Punya rekening bank (untuk menerima pembayaran online)<br><br>

				Ayo segera download <b>Wakuliner Merchant app</b> dari Google Play Store / App Store dan langsung daftarkan toko Anda dan langsung mulai berjualan.<br><br>
				Kunjungi <a href="http://wakuliner.com/panduan-merchant.php" target='_blank'>halaman panduan-merchant</a> untuk detil cara daftar, cara terima pesanan, dan kelola toko.<br><br>
				Selamat berjualan!<br><br>
				<i>(Klik dibawah ini untuk download Wakuliner Merchant)</i> <br>
				<div>
					<?
					if($mobile == 1) { ?>
						<a href='https://play.google.com/store/apps/details?id=com.bigit.wakuliner.merchant' target='_blank'>
							<img class='img-responsive' style='width:40%; float:left; margin-left:5%; margin-right:10%;' src='images/play1.png'>
						</a>
						<a href='https://itunes.apple.com/us/app/renilukaw/id1074598371?mt=8' target='_blank'>
							<img class='img-responsive' style='width:40%;' src='images/appstore2.png'>
						</a>

					<?
					} else {
					?>
					<div class="col-sm-3 col-sm-offset-3">
						<div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
						<a href='https://play.google.com/store/apps/details?id=com.bigit.wakuliner.merchant' target='_blank'>
							<div class="team-img">
								<img class='img-responsive' style='width:90%;' src='images/play1.png'>
							</div>
						</a>
						</div>
					</div>

					<div class="col-sm-3">
						<div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
						<a href='https://itunes.apple.com/us/app/renilukaw/id1074598371?mt=8' target='_blank'>
							<div class="team-img">
								<img class='img-responsive' <? if($mobile == 1) { echo "style='width:80%;'"; } else { echo "style='width:90%;'"; } ?>  src='images/appstore2.png'>
							</div>
						</a>
						</div>
					</div>
					<? } ?>
				</div> <br>
</span></p>
            </div>
<!--
			 <button type="button" <? if($mobile == 1) { echo "id='bukadaftar2'"; } else { echo "id='bukadaftar'"; } ?> style='font-weight:bold;' class='col-sm-offset-4' data-toggle="modal" data-target="#myModal">
				ISI FORMULIR PENDAFTARAN
			 </button>
			<? if($mobile == 0) { ?>
			<div style='margin-bottom:30px;'></div>
			<? } ?>
-->
        </div>
    </section>

    <section id="joinus" <? if( $mobile == 1) { echo "style='margin-top:-10px;'"; } else { echo "style='margin-top:-20px;'"; } ?> >
		<?

		if($mobile == 0) { ?>
		<div class="col-sm-offset-4 text-center" style='margin-top:-50px;'>

			<div class='form_join'>
			<img src='images/joinus2.png' style='position:absolute;margin-left:-500px; margin-top:-70px;'>
				<h1 style='font-size:28px; color:#006306; font-weight:500px;'>Ada pertanyaan atau masukan untuk kami?</h1>
				<p style='color:#000;line-height:15px;'>Silahkan isi form dibawah ini.<br> Kami akan menghubungi Anda</p>
				<div style='margin-bottom:40px;'></div>
				<form id="main-contact-form" name="contact-form" method="post" action="sendemail2.php">
					<div class="form-group">
						<input type="text" name="name" class="form-control" placeholder="Name" required>
					</div>
					<div class="form-group">
						<input type="email" name="email" class="form-control" placeholder="Email" required>
					</div>
					<div class="form-group">
						<input type="text" name="telp" class="form-control" placeholder="No Telp" required>
					</div>
					<div class="form-group">
						<input type="text" name="tanya" class="form-control" placeholder="Pertanyaan / Masukan" required>
					</div>
					<button id='contactLink'><p style='font-size:20px;margin-top:10px;'>SUBMIT</p></button>
					<div style='margin-bottom:75px;'></div>

				</form>

			</div>
		</div>
		<? } else { ?>
		<div class="col-sm-offset-4 text-center" style='margin-top:-50px;'>
			<div class='form_join2'>
				<h1 style='font-size:28px; color:#006306; font-weight:500px;'>Ada pertanyaan atau masukan untuk kami?</h1>
				<p style='color:#000;line-height:15px;'>Silahkan isi form dibawah ini.<br> Kami akan menghubungi Anda</p>
				<form id="main-contact-form" name="contact-form" method="post" action="sendemail2.php">
					<div class="form-group">
						<input type="text" name="name" class="form-control" placeholder="Name" required>
					</div>
					<div class="form-group">
						<input type="email" name="email" class="form-control" placeholder="Email" required>
					</div>
					<div class="form-group">
						<input type="text" name="telp" class="form-control" placeholder="No Telp" required>
					</div>
					<div class="form-group">
						<input type="text" name="tanya" class="form-control" placeholder="Pertanyaan / Masukan" required>
					</div>
					<button id='contactLink2'><p style='font-size:20px;margin-top:10px;'>SUBMIT</p></button>
					<div style='margin-bottom:75px;'></div>

				</form>
			</div>
		</div>
		<?
		}
		?>


		<div style='margin-bottom:30px;'></div>
    </section><!--/#testimonial-->




  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
		<?
		if($mobile == 1) { $marg = '40px'; } else { $marg = '100px'; }
		?>
        <div class="modal-body">
          <div class="form-title">Terima kasih Anda telah mau bergabung dengan Wakuliner<br>
Sebelum mendaftar, pastikan bahwa resto atau toko Anda sudah memiliki:</div>
		  <div style='margin-top:20px;clear:both;'></div>
		  <div class='col-sm-12' style='margin-bottom:30px;'>
		    <img src='images/R2.png' style='width:20%;float:left;'>
			<img src='images/antar2.png' style='width:20%; margin-left:<?= $marg; ?>;float:left;'>
			<img src='images/rp2.png' style='width:20%; margin-left:<?= $marg; ?>;'>
		  </div>
			<div style='margin-top:50px;clear:both;'></div>
		   @include('content_join')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
	</div>


    <footer id="footer">
        <div class="container">
            <div class="row" style='margin-bottom:20px;'>
			<? if($mobile == 1) { ?>
                <div class="text-center col-sm-3" style='font-size:10px;'>
                    &copy; 2017 Wakuliner. All rights reserved
                </div>
                <div class="text-center col-sm-6" style='font-size:5px;'>
                    <ul class="social-icons">
                        <li><a href="index.php#home">Apa Itu Wakuliner</a></li>
                        <li><a href="index.php#features">Cara Ber-WAKULINER</a></li>
                        <li><a href="index.php#cta2">Mengapa Ber-WAKULINER</a></li>
                        <li><a href="joinus.php">Join Us</a></li>
                    </ul>
                </div>
            </div>

			<div class="text-center col-sm-5 col-sm-offset-2">
				<ul class="social-icons">
					<li><a href="http://www.facebook.com/wakuliner/" target='_blank'><img src='images/fb.png'></a></li>
					<!--
					<li><a href="#"><img src='images/twit.png'></a></li>
					!-->
					<li><a href="http://www.instagram.com/wakuliner/" target='_blank'><img src='images/ig.png'></a></li>
				</ul>
			</div>
			<? } else { ?>
				<div class="col-sm-3">
                    &copy; 2017 Wakuliner. All rights reserved
                </div>
                <div class="col-sm-9">
                    <ul class="social-icons">
                        <li><a href="index.php#home">Apa Itu Wakuliner</a></li>
                        <li><a href="index.php#features">Cara Ber-WAKULINER</a></li>
                        <li><a href="index.php#cta2">Mengapa Ber-WAKULINER</a></li>
                        <li><a href="joinus.php">Join Us</a></li>
                    </ul>
                </div>
            </div>

			<div class="col-sm-5 col-sm-offset-2">
				<ul class="social-icons">
					<li><a href="http://www.facebook.com/wakuliner/" target='_blank'><img src='images/fb.png'></a></li>
					<!--
					<li><a href="#"><img src='images/twit.png'></a></li>
					!-->
					<li><a href="http://www.instagram.com/wakuliner/" target='_blank'><img src='images/ig.png'></a></li>
				</ul>
			</div>
			<? } ?>
        </div>
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mousescroll.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>