<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Wakuliner</title>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="images/favicon.ico">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <style type="text/css">
    </style>
	<style>
	#main-slider .item3 {
	  height: 260px;
	  background-repeat: no-repeat;
	  background-position: 80% 0;
	  background-size: cover;
	}
	.media.service-box {
  margin: 25px 0;
}

.media.service-box .pull-left {

  margin-left: -25px;

}

.media.service-box .pull-left > i {
  font-size: 24px;
  height: 64px;
  line-height: 64px;
  text-align: center;
  width: 64px;
  border-radius: 100%;
  color: #45aed6;
  box-shadow: inset 0 0 0 1px #d7d7d7;
  -webkit-box-shadow: inset 0 0 0 1px #d7d7d7;
  transition: background-color 400ms, background-color 400ms;
  position: relative;
}
.media.service-box .pull-left > i:after {
  content: "";
  position: absolute;
  width: 20px;
  height: 20px;
  top: 50%;
  margin-top: -10px;
  right: -10px;
  border: 4px solid #fff;
  border-radius: 20px;
  background: #45aed6;
}
	</style>

</head><!--/head-->

<?
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
	$mobile = 1;
} else {
	$mobile = 0;
}
?>

<style>
<? if($mobile == 1) { ?>
#img_m {
	width:50%;
	margin-left:20px;
}
<? } ?>
</style>

<body id="home" class="homepage">

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
					<?
						if($mobile == 1) {
							echo " <a class='navbar-brand' href='index.php'><img height='57px' src='images/logo.png' alt='logo'></a>";
						} else {
							echo " <a class='navbar-brand' href='index.php'><img height='57px' src='images/logo.png' alt='logo'></a>";
						}
					?>

                </div>

                <div class="collapse navbar-collapse navbar-right">
				<? if($mobile == 0) {
					$br = '<br>';
				} else {
					$br = '';
				}
				?>
                    <ul class="nav navbar-nav">
                        <li class="scroll active"><a class='menu-wrap' href="#home">Apa itu <?= $br; ?>WAKULINER</a></li>
                        <li class="scroll"><a class='menu-wrap' href="#features">Cara <?= $br; ?>Ber-WAKULINER</a></li>
                        <li class="scroll"><a class='menu-wrap' href="#cta2">Mengapa <?= $br; ?>Ber-WAKULINER</a></li>
						<?
						if($mobile == 1) {
							echo "<li class='scroll'><a class='menu-wrap' href='joinus.php'>Join Us</a></li>";
						} else { ?>
							<li><a class='menu-wrap' href="joinus.php"><img class='img-responsive' width='50%' style='margin-top:-10px;' src='images/btnjoin.png'></a></li>
						<?
						}
                        ?>
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->
<style>
@media screen and (orientation:landscape) {
	.item3 {
		background-image: url(images/slider/slide_mobi.jpg);
		background-size: cover;
		background-position:80%
	}


	#main-slider .item3 {
	  height: 580px;
	  background-repeat: no-repeat;
	  background-position: 80% 0;
	  background-size: cover;
	}
}
</style>
	<a href='http://wakuliner.com/undian.php'>
    <section id="main-slider">
	<?
	if($mobile == 1) {
		$wakuwiku = 'wakuwikumob.png';
		$wakuantar = 'wakuantarmob.png';
		$wakukatring = 'wakukateringmob.png';
		 //echo '<div class="item3" style="background-image: url(images/slider/slide_mobi.jpg);background-size: 100% 100%;">';
		 echo '<div class="item3" style="background-image: url(images/slider/slide_mobi.jpg);background-size: 100% 100%;">';
	} else {
		$wakuwiku = 'wakuwikuhorisontal.png';
		$wakuantar = 'wakuantar.png';
		$wakukatring = 'wakukatering.png';
        echo '<div class="item" style="background-image: url(images/slider/slide.jpg);background-size: 100% 100%;height: 400px;">';
	}
	?>
<!--
				<div class="slider-inner">
                    <div class="container">
                        <div class="row">
                                <div class="carousel-content">
                                    <h2 style='text-align:center;font-family:Arista light,sans-serif;'>Apakah Wakuliner itu?</h2>
                                </div>
                        </div>
                    </div>
                </div>
				!-->
            </div><!--/.item-->
    </section><!--/#main-slider-->
	</a>

    <section id="cta" class="wow fadeIn">
	<?
	if($mobile == 0) { ?>
		<!-- <img src='' style='background-color:#87c03b;width:100%;height:25px;position:absolute;margin-left;margin-top:755px;'> -->
	<?
	}
	?>
	 <div class="container">
			<div class="section-header">
                <h2 class="title text-center wow fadeInDown" style='font-size:55px;font-family:Arista light,sans-serif;'>Apa itu Wakuliner?</h2>
            </div>

		<div class="row">
		<div class="col-md-6 col-md-offset-3">
		     <p style='text-align:center;font-size:20px;line-height:25px;font-weight:600;color:#000;font-family:Quicksand-Regular,sans-serif;'>
		     Wadah kuliner untuk semua jenis kuliner. Pertama di Indonesia.
		     </p>
		     <br>
			<!-- <div class="tengah"> -->
				<div class="col-md-3 'fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>">
					<a class="thumbnail" style="background: transparent;border: none">
						<img width="110px" height="110px" src="images/a/mob2.png" alt="">
			       	</a>

			    </div>
				<div class="col-md-8">
					<p></p>
					<p style="color: #000; text-align: center;">Waku-Antar memberikan jasa pesan-antar tercepat di Indonesia, dengan 2.850 merchants di 30 kota. Pesan makanan langsung diantar oleh penjual ke lokasi Anda atau orang kesayangan Anda. </p><br>
				</div>



		</div>
		</div>

		<div class="row">
		<div class="col-md-6 col-md-offset-3">

				<div class="col-md-3  'fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>">
					<a class="thumbnail" style="background: transparent;border: none">
						<img width="110px" height="110px" src="images/a/mob1.png" alt="">
			       	</a>

			    </div>
				<div class="col-md-8 ">
				<p></p>
				<p style="color: #000; text-align: center;">Temukan restoran & tempat makan populer yang legendaris di kota-kota kesayangan Anda. Dapatkan info restoran, menu, foto-foto menu dan lokasi, & petunjuk arah kesana.</p><br>

				</div>

		</div>
		</div>

		<div class="row">
		<div class="col-md-6 col-md-offset-3">

				<div class="col-md-3 'fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>">
					<a class="thumbnail" style="background: transparent;border: none">
						<img width="110px" height="110px" src="images/a/mob3.png" alt="">
			       	</a>


			    </div>
				<div class="col-md-8">

					<p style="color: #000;font-size: 20px;text-align: center;"><strong> Coming Soon!</strong></p>
					<p style="color: #000; text-align: center;">Pesan katering untuk acara spesial Anda, ataupun langganan katering harian untuk rumah atau kantor Anda.</p><br>
				</div>

		</div>
		</div>


			<div class="col-sm-9 col-sm-offset-3" style='margin-top:120px;'>
			<!--
				<img class='img-responsive' src='images/iconmudah.png'>
				!-->

			</div>

	</div>
<div class="row">
  <div class="col-sm-12">
    <div class="row">
      <div >
        <img src='' style='background-color:#87c03b;width:100%;height:25px;position:absolute;margin-right;margin-top:51px;'>
      </div>
      <div>
        <img src='' style='background-color:#87c03b;width:100%;height:25px;position:absolute;margin-right;margin-top:51px;'>
      </div>
    </div>
  </div>
</div>

			<div class="col-sm-9 col-sm-offset-3">
			<? if($mobile == 0) { ?>
				<img class='img-responsive' src='images/iconmudah.png' style='position:absolute;width:46%;margin-top:-80px;'>
			<? } else {
				echo "<img src='images/iconmudah.png' style=' display: block;
    max-width: 100%;
    position: absolute;
    bottom:0;
    left:50%;
	top:-40px;
    transform:translateX(-50%);' />";
			} ?>
			</div>
    </section><!--/#cta-->

    <section id="features">
	<?
	if($mobile == 0) { ?>
		<img id='bg-feat' src='' style='background-color:#d5fad8;width:100%;height:100px;position:absolute;margin-left;margin-top:330px;'>
	<?
	}
	?>

        <div class="container">
            <div class="section-header">
                <h2 class="title text-center wow fadeInDown" style='font-family:Arista light,sans-serif;'>Bagaimana cara kerja Wakuliner</h2>
            </div>
			<div class="col-sm-10 col-sm-offset-31 text-center">
			<? if($mobile == 1) { ?>

				<div onclick='wakuantar();' id='wakuantar' style='display:block;float:left;background-color:#d7fad9;width:30%;height:60px;margin-left:10px;cursor:pointer;'>
					<img src='images/<?= $wakuantar ?>' style='width:70%;margin-top:8px;'>
				</div>
				<div onclick='wakuantar();' id='wakuantar2' style='display:none;float:left;width:30%;margin-left:10px;cursor:pointer;'>
					<img src='images/<?= $wakuantar ?>' style='width:70%;margin-top:8px;'>
				</div>

				<div onclick='wakuwiku();' id='wakuwiku' style='float:left;display:none;background-color:#d7fad9;width:30%;height:60px;'>
					<img src='images/<?= $wakuwiku ?>' style='width:60%;margin-top:8px;cursor:pointer;'>
				</div>
				<div onclick='wakuwiku();'id='wakuwiku2' style='display:block;float:left;width:30%;'>
					<img src='images/<?= $wakuwiku ?>' style='width:60%;margin-top:8px;'>
				</div>




				<div onclick='wakukatring();' id='wakukatring' style='display:none;float:left;background-color:#d7fad9;width:30%;height:60px;margin-left:10px;cursor:pointer;'>
					<img src='images/<?= $wakukatring ?>' style='width:70%;margin-top:8px;cursor:pointer;'>
				</div>
				<div onclick='wakukatring();' id='wakukatring2' style='display:block;float:left;width:30%;margin-left:10px;cursor:pointer;'>
					<img src='images/<?= $wakukatring ?>' style='width:70%;margin-top:8px;cursor:pointer;'>
				</div>
			<? } else { ?>

                <div class="col-sm-3 col-md-30" style='margin-left:50px; margin-right:50px;'>
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="team-img" id='waku-hov'>
							<div onclick='wakuantar();' id='wakuantar' style='display:block;background-color:#d7fad9;height:60px;cursor:pointer;'>
								<img src='images/<?= $wakuantar ?>' style='margin-top:8px;'>
							</div>
							<div onclick='wakuantar();' id='wakuantar2' style='display:none;height:60px;cursor:pointer;'>
								<img src='images/<?= $wakuantar ?>' style='margin-top:8px;'>
							</div>
						</div>
                    </div>
                </div>

				<div class="col-sm-3 col-md-30">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
                        <div class="team-img" id='waku-hov'>
							<div onclick='wakuwiku();' id='wakuwiku' style='display:none;background-color:#d7fad9;height:60px;'>
								<img src='images/<?= $wakuwiku ?>' style='margin-top:8px;cursor:pointer;'>
							</div>

							<div onclick='wakuwiku();'id='wakuwiku2' style='display:block;height:60px;cursor:pointer;'>
								<img src='images/<?= $wakuwiku ?>' style='margin-top:8px;'>
							</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-md-30" >
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">
                        <div class="team-img" id='waku-hov'>
							<div id='wakukatring' style='display:none;background-color:#d7fad9;height:60px;'>
								<img onclick='wakukatring();' src='images/<?= $wakukatring ?>' style='width:90%;margin-top:8px;cursor:pointer;'>
							</div>
							<div id='wakukatring2' style='display:block;height:60px;'>
								<img onclick='wakukatring();' src='images/<?= $wakukatring ?>' style='width:90%;margin-top:8px;cursor:pointer;'>
							</div>
							<!--
							<div id='wakukatring2' style='display:block;height:60px;'>
								<img onclick='wakukatring();' src='images/<?= $wakukatring ?>' style='height:50px;margin-top:8px;cursor:pointer;'>
							</div>
							!-->
                        </div>
                    </div>
                </div>
			<? } ?>
			</div>

			<script>

				function wakuantar() {
					document.getElementById('wakuwiku_content').style.display = 'none';
					document.getElementById('wakuwiku').style.display = 'none';
					document.getElementById('wakuwiku2').style.display = 'block';

					document.getElementById('wakuantar_content').style.display = 'block';
					document.getElementById('wakuantar2').style.display = 'none';
					document.getElementById('wakuantar').style.display = 'block';

					document.getElementById('wakukatring_content').style.display = 'none';
					document.getElementById('wakukatring2').style.display = 'block';
					document.getElementById('wakukatring').style.display = 'none';
				}

				function wakuwiku() {
					document.getElementById('wakuwiku_content').style.display = 'block';
					document.getElementById('wakuwiku').style.display = 'block';
					document.getElementById('wakuwiku2').style.display = 'none';

					document.getElementById('wakuantar_content').style.display = 'none';
					document.getElementById('wakuantar2').style.display = 'block';
					document.getElementById('wakuantar').style.display = 'none';

					document.getElementById('wakukatring_content').style.display = 'none';
					document.getElementById('wakukatring2').style.display = 'block';
					document.getElementById('wakukatring').style.display = 'none';
				}


				function wakukatring() {
					document.getElementById('wakuwiku_content').style.display = 'none';
					document.getElementById('wakuwiku').style.display = 'none';
					document.getElementById('wakuwiku2').style.display = 'block';

					document.getElementById('wakuantar_content').style.display = 'none';
					document.getElementById('wakuantar2').style.display = 'block';
					document.getElementById('wakuantar').style.display = 'none';

					document.getElementById('wakukatring_content').style.display = 'block';
					document.getElementById('wakukatring2').style.display = 'none';
					document.getElementById('wakukatring').style.display = 'block';

					document.getElementById('bg-feat').style.display = 'none';
				}
			</script>

			<div id='wakuantar_content'>
				<div class="col-sm-10 col-sm-offset-30 text-center" style='margin-bottom:50px; margin-top:100px;'>
					<div class="col-sm-3 col-md-30">
						<div class="team-member">
							<div class="team-info" style='text-align:center;'>
								<img src='images/del1big.png' <? if($mobile == 1) { echo "style='width:30%;'"; } ?> >
							</div>
							<!--
							<div class="team-img">
								<img class="img-responsive" src="images/del1big.png" alt="">
							</div>
							!-->
							<div class="team-info">
								<span>Cari / Masukan Lokasi Anda</span>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-md-30">
						<div class="team-member">
							<div class="team-info" style='text-align:center;'>
								<img src="images/dine2big.png" <? if($mobile == 1) { echo "style='width:30%;'"; } ?> alt="">
							</div>
							<div class="team-info">
								<span>Pilih Tempat Makan yang Anda Mau</span>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-md-30">
						<div class="team-member">
							<div class="team-info" style='text-align:center;'>
								<img src="images/dine3big.png" <? if($mobile == 1) { echo "style='width:30%;'"; } ?> alt="">
							</div>
							<div class="team-info">
								<span>Tempat Makan Mengantar Pesanan Langsung ke Anda, dengan waktu-pengiriman tercepat untuk Anda</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id='wakuwiku_content' style='display:none;'>
				<div class="col-sm-10 col-sm-offset-30 text-center" style='margin-bottom:50px; margin-top:100px;'>
					<div class="col-sm-3 col-md-30">
						<div class="team-member">
							<div class="team-info" style='text-align:center;'>
								<img src="images/del1big.png" <? if($mobile == 1) { echo "style='width:30%;'"; } ?> alt="">
							</div>
							<div class="team-info">
								<span>Cari / Masukan Lokasi Anda</span>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-md-30">
						<div class="team-member">
							<div class="team-info" style='text-align:center;'>
								<img src="images/del2big.png" <? if($mobile == 1) { echo "style='width:30%;'"; } else { echo "height='166px'"; } ?> alt="">
							</div>
							<div class="team-info">
								<span>Jelajahi Tempat Makan Populer dan Legendaris di Kota Anda</span>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-md-30">
						<div class="team-member">
							<div class="team-info" style='text-align:center;'>
								<img src="images/del3big.png" <? if($mobile == 1) { echo "style='width:30%;'"; } else { echo "height='166px'"; } ?> alt="">
							</div>
							<div class="team-info">
								<span>Dapatkan Info Lokasi, Petunjuk Arah, Menu, Foto-foto makanan dan lokasi, Review Pelanggan, dan Info Promo</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id='wakukatring_content' style='display:none;'>
				<div class="col-sm-10 col-sm-offset-1 text-center" style='margin-bottom:50px; margin-top:100px;'>
					<div class="team-info" style='text-align:center;'>
						<h2 style='color:#f8892d;'>Coming Soon !</h2>
						<p>Anda akan segera dapat menikmati pelayanan katering (event atau harian). Tunggu ya.</p>
						<!--
						<img <? if($mobile == 1) { echo "style='width:80%;'"; } ?> src="images/soon.png" alt="">
						!-->
					</div>
					<? /*
					<div class="col-sm-3 col-md-30">
						<div class="team-member">
							<div class="team-info" style='text-align:center;'>
								<img src="images/del1big.png" alt="">
							</div>
							<!--
							<div class="team-info">
								<span>Masukkan <br> Lokasi Anda</span>
							</div>
							!-->
						</div>
					</div>
					<div class="col-sm-3 col-md-30">
						<div class="team-member">
							<div class="team-info" style='text-align:center;'>
								<img src="images/del2big.png" alt="">
							</div>
							<!--
							<div class="team-info">
								<span>Pilih Restoran <br> yang Anda Mau</span>
							</div>
							!-->
						</div>
					</div>
					<div class="col-sm-3 col-md-30">
						<div class="team-member">
							<div class="team-info" style='text-align:center;'>
								<img src="images/del3big.png" alt="">
							</div>
							<!--
							<div class="team-info">
								<span>Pesan Antar Makanan <br> Tiba di Piring Anda</span>
							</div>
							!-->
						</div>
					</div>
					*/ ?>
				</div>
			</div>

        </div>
    </section>

    <section id="cta2">
        <div class="container">
             <div class="section-header">
                <h2 class="title text-center wow fadeInDown" style='font-family:Arista light,sans-serif;'>Mengapa Berjualan di Wakuliner</h2>
				<h2 class="sub-title text-center" style='font-size:27px; font-weight:300;'>Jelajahi 3 Fitur Wakuliner Yang Dapat Anda Gunakan</h2>
            </div>

			<div class="row">
			<? if($mobile == 1) { ?>
			<div class="col-sm-10 col-sm-offset-31 text-center">

				<div onclick='wakuantar2();' id='wakuantar3' style='display:block;float:left;background-color:#d7fad9;width:30%;height:60px;margin-left:10px;cursor:pointer;'>
					<img src='images/<?= $wakuantar ?>' style='width:70%;margin-top:8px;'>
				</div>
				<div onclick='wakuantar2();' id='wakuantar4' style='display:none;float:left;width:30%;margin-left:10px;cursor:pointer;'>
					<img src='images/<?= $wakuantar ?>' style='width:70%;margin-top:8px;'>
				</div>




				<div onclick='wakuwiku2();' id='wakuwiku3' style='float:left;display:none;background-color:#d7fad9;width:30%;height:60px;'>
					<img src='images/<?= $wakuwiku ?>' style='width:60%;margin-top:8px;cursor:pointer;'>
				</div>
				<div onclick='wakuwiku2();'id='wakuwiku4' style='display:block;float:left;width:30%;'>
					<img src='images/<?= $wakuwiku ?>' style='width:60%;margin-top:8px;'>
				</div>




				<div onclick='wakukatring2();' id='wakukatring3' style='display:none;float:left;background-color:#d7fad9;width:30%;height:60px;margin-left:10px;cursor:pointer;'>
					<img src='images/<?= $wakukatring ?>' style='width:70%;margin-top:8px;cursor:pointer;'>
				</div>
				<div onclick='wakukatring2();' id='wakukatring4' style='display:block;float:left;width:30%;margin-left:10px;cursor:pointer;'>
					<img src='images/<?= $wakukatring ?>' style='width:70%;margin-top:8px;cursor:pointer;'>
				</div>
			</div>

			<? } else { ?>

			<div class="col-sm-10 col-sm-offset-31 text-center">

                <div class="col-sm-3 col-md-30" style='margin-left:50px; margin-right:50px;'>
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
                        <div class="team-img" id='waku-hov2'>
							<div onclick='wakuantar2();' id='wakuantar3' style='display:block;border-style: solid;border-color:#fffbb4 #fffbb4 #0eae16 #fffbb4;height:60px;cursor:pointer;'>
								<img src='images/wakuantar.png' style='margin-top:8px;'>
							</div>
							<div onclick='wakuantar2();' id='wakuantar4' style='display:none;height:60px;cursor:pointer;'>
								<img src='images/wakuantar.png' style='margin-top:8px;'>
							</div>
						</div>
                    </div>
                </div>


				<div class="col-sm-3 col-md-30">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="team-img" id='waku-hov2'>
							<div onclick='wakuwiku2();' id='wakuwiku3' style='display:none;border-style: solid;border-color:#fffbb4 #fffbb4 #0eae16 #fffbb4;height:60px;'>
								<img src='images/wakuwikuhorisontal.png' style='margin-top:3px;cursor:pointer;'>
							</div>

							<div onclick='wakuwiku2();'id='wakuwiku4' style='display:block;height:60px;cursor:pointer;'>
								<img src='images/wakuwikuhorisontal.png' style='margin-top:8px;'>
							</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-md-30" >
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">
                        <div class="team-img" id='waku-hov2'>
							<div id='wakukatring3' style='display:none;border-style: solid;border-color:#fffbb4 #fffbb4 #0eae16 #fffbb4;height:60px;'>
								<img onclick='wakukatring2();' src='images/<?= $wakukatring; ?>' style='width:90%;margin-top:8px;cursor:pointer;'>
							</div>
							<div id='wakukatring4' style='display:block;height:60px;'>
								<img onclick='wakukatring2();' src='images/<?= $wakukatring; ?>' style='width:90%;margin-top:8px;cursor:pointer;'>
							</div>
                        </div>
                    </div>
                </div>
			</div>

			<? } ?>

<script>
				function wakuwiku2() {
					document.getElementById('wakuwiku_content2').style.display = 'block';
					document.getElementById('wakuwiku3').style.display = 'block';
					document.getElementById('wakuwiku4').style.display = 'none';

					document.getElementById('wakuantar_content2').style.display = 'none';
					document.getElementById('wakuantar4').style.display = 'block';
					document.getElementById('wakuantar3').style.display = 'none';

					document.getElementById('wakukatring_content2').style.display = 'none';
					document.getElementById('wakukatring4').style.display = 'block';
					document.getElementById('wakukatring3').style.display = 'none';
				}

				function wakuantar2() {
					document.getElementById('wakuwiku_content2').style.display = 'none';
					document.getElementById('wakuwiku3').style.display = 'none';
					document.getElementById('wakuwiku4').style.display = 'block';

					document.getElementById('wakuantar_content2').style.display = 'block';
					document.getElementById('wakuantar4').style.display = 'none';
					document.getElementById('wakuantar3').style.display = 'block';

					document.getElementById('wakukatring_content2').style.display = 'none';
					document.getElementById('wakukatring4').style.display = 'block';
					document.getElementById('wakukatring3').style.display = 'none';
				}

				function wakukatring2() {
					document.getElementById('wakuwiku_content2').style.display = 'none';
					document.getElementById('wakuwiku3').style.display = 'none';
					document.getElementById('wakuwiku4').style.display = 'block';

					document.getElementById('wakuantar_content2').style.display = 'none';
					document.getElementById('wakuantar4').style.display = 'block';
					document.getElementById('wakuantar3').style.display = 'none';

					document.getElementById('wakukatring_content2').style.display = 'block';
					document.getElementById('wakukatring4').style.display = 'none';
					document.getElementById('wakukatring3').style.display = 'block';
				}
</script>
<style>

#waku-hov:hover {
	background-color: #b8f6bc;
}

#waku-hov2:hover {
	/*
	border-style: solid;border-color:#fffbb4 #fffbb4 #11e91c #fffbb4;
	*/
	background-color: #b8f6bc;
}

.tab_jual {
	border: 4px solid #fffbcb;
    padding: 10px 40px;
    background: #fffbcb;
    width: 250px;
	height:85px;
	color:#28b328;
    border-radius: 10px;
}

.tab_active {
	border: 4px solid #00aa0b;
    padding: 10px 40px;
    background: #00aa0b;
    width: 250px;
	height:85px;
	color:#fff;
    border-radius: 10px;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
</style>
			<div style='margin-bottom:20px;clear:both;'></div>
			<div id='wakukatring_content2' style='display:none;'>
				<div class="col-sm-12" style='margin-top:60px;margin-bottom:90px;'>
					<div class="team-info" style='text-align:center;'>
					<!--
						<img <? if($mobile == 1) { echo "style='width:80%;'"; } ?>  src="images/soon.png" alt="">
					!-->
						<h2 style='color:#f8892d;'>Coming Soon !</h2>
						<p style='color:#000;'>Jika Anda adalah pengusaha katering, sebentar lagi dapat berjualan juga lho di Wakuliner. <br>Silahkan mengirimkan informasi Anda (nama, nomer HP, alamat email, nama usaha katering, jenis katering) lewat email ke merchants@wakuliner.com atau WhatsApp 0821-1456-5292.</p>
					</div>
				<?
				/*
                    <div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' src='images/16.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">MENCARI RESTORAN TOP</h4>
                                <div class='fitur-sub col-md-12'>
									Aplikasi smartphone GRATIS (Android & IOS),<br> untuk terima pesananan, kontrol menu, dan buka/tutup toko.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' src='images/17.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">INFO DETAIL RESTORAN</h4>
                                <div class='fitur-sub col-md-12'>
									Merchant dapat menjual menu set / paket, berupa kombinasi atau kotak bento. Konsumen dapat memilih pilihan
									 dan isi menu set.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

					<div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' src='images/15.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">LENGKAP DENGAN FOTO & MENU</h4>
                                <div class='fitur-sub col-md-12'>
									Merchant dapat memasang diskon, promosi, dan banner marketing dengan GRATIS
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' src='images/18.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">INFO ARAH DAN LOKASI RESTO</h4>
                                <div class='fitur-sub col-md-12'>
									Konsumen dan merchant harus menilai dan me-review masing-masing pihak. Pengalaman kuliner yang aman & terpercaya.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->


*/ ?>
                </div>
			</div>

			<?
				$mrg2 = '';
				$ftr = 'col-md-12';
				if($mobile == 1) { $mrg2 = "style='margin-left:-30px;'"; $ftr = ''; }
			?>

			<div id='wakuantar_content2'>

				<div class="col-sm-12" style='margin-top:60px;margin-bottom:90px;'>
                    <div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/1.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?> >GRATIS MOBILE APPS</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>>
									Aplikasi smartphone GRATIS (Android & IOS), untuk terima pesanan, kontrol menu, dan buka /tutup toko
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/7.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?> >MENU PAKET & COMBO</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?> >
									Merchant dapat menjual menu set/ paket, berupa kombinasi atau kotak bento. Konsumen dapat memilih pilihan dan isi menu set.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

					<div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/2.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?>>GRATIS MARKETING & PROMO</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>>
									Merchant dapat memasang diskon, promosi dan banner marketing dengan GRATIS.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/8.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?>>SISTEM PENILAIAN 2 ARAH</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?> >
									Konsumen dan merchant harus menilai dan me-review masing-masing pihak. Pengalaman kuliner yang aman & terpercaya.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

					<div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/3.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?> >GRATIS WEB ADMIN</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>>
									Merchant mendapat website admin GRATIS untuk ubah dan upload menu makanan, area kirim & ongkir, dan hari/jam kerja.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/10.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?> >LAPORAN & ANALYTICS</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>>
									Laporan dan analytics GRATIS bagi merchant untuk mengembangkan usaha.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

					<div class="col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/4.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?> >KUANTITAS MIN & MAX</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>>
									Merchant dapat menentukan kuantitas minimum dan maksimum menu untuk dijual.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/11.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?> >PEMBAYARAN ONLINE</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?> >
									Pilihan untuk pembayaran kartu kredit untuk kenyamanan konsumen.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

					<div class="col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/5.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?>>GRATIS BAGI KONSUMEN</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>>
									GRATIS bagi konsumen, tanpa biaya membership.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/12.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?>>REWARD BAGI KONSUMEN</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>>
									Poin reward untuk konsumen dari setiap pembelian.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

					<div class="col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/6.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?>>VERIFIKASI KONSUMEN</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>>
									Konsumen harus punya akun untuk memesan, untuk perlindungan merchant dan konsumen sendiri.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/13.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?>>PILIHAN LENGKAP</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?> >
									Tersedia semua ragam usaha & produk kuliner (resto,depot,cafe, toko online, food court), di lokal, inter-lokal, maupun luar pulau.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->


                </div>
			</div>

			<div id='wakuwiku_content2' style="display: none;">
                <div class="col-sm-12" style='margin-top:60px;margin-bottom:90px;'>
                    <div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/16.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?> >PELANGGAN BARU</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>>
									Dapatkan pelanggan baru, makan di tempat makan Anda.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/17.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?>>PASANG INFO LENGKAP</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?> >
									Pasang alamat, nomer telepon, hari dan jam kerja, kisaran harga, website / sosmed, foto-foto, tipe pelayanan, dan informasi promosi di tempat makan Anda.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

					<div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/15.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?>>GRATIS MARKETING & PROMO</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>>
									Pasang banyak foto lokasi, foto semua menu & makanan, beserta promosi.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/18.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?>>GPS PETUNJUK JALAN</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>>
									Pelanggan akan kami beri petunjuk jalan ke tempat makan Anda.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->

					<div class="col-md-6 col-sm-6">
                        <div class="media service-box">
                            <div class="pull-left">
                                <img class='img-responsive' id='img_m' src='images/14.png'>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading" <?= $mrg2; ?>>KUMPULAN RATING</h4>
                                <div class='fitur-sub <?= $ftr; ?>' <?= $mrg2; ?>>
									Kumpulkan rating dan penilaian dari pelanggan-pelanggan Anda.
								</div>
                            </div>
                        </div>
                    </div><!--/.col-md-4-->




                </div>
			</div>
            </div><!--/.row-->

        </div>
    </section>

	<style>
	img.ri
	{
		position: absolute;
		max-width: 80%;
		top: 10%;
		left: 10%;
		border-radius: 3px;
		box-shadow: 0 3px 6px rgba(0,0,0,0.9);
	}
	</style>

    <section id="testimonial">
        <div class="container">
            <div class="section-header">
                <h2 class="title text-center wow fadeInDown" style='font-family:Arista light,sans-serif;'>Download App nya <br> Sekarang Juga!</h2>
            </div>
			<?
			if($mobile == 1) { ?>
				<a href='https://play.google.com/store/apps/details?id=com.bigit.wakuliner.customer' target='_blank'>
					<img class='img-responsive' style='width:40%; float:left; margin-left:5%; margin-right:10%;' src='images/play1.png'>
				</a>
				<a href='https://itunes.apple.com/us/app/wakuliner-food-delivery-order/id1070066694?ls=1&mt=8' target='_blank'>
					<img class='img-responsive' style='width:40%;' src='images/appstore2.png'>
				</a>

			<?
			} else {
			?>
			<div class="col-sm-3 col-sm-offset-3">
				<div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
				<a href='https://play.google.com/store/apps/details?id=com.bigit.wakuliner.customer' target='_blank'>
					<div class="team-img">
						<img class='img-responsive' style='width:90%;' src='images/play1.png'>
					</div>
				</a>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
				<a href='https://itunes.apple.com/us/app/wakuliner-food-delivery-order/id1070066694?ls=1&mt=8' target='_blank'>
					<div class="team-img">
						<img class='img-responsive' <? if($mobile == 1) { echo "style='width:80%;'"; } else { echo "style='width:95%;'"; } ?>  src='images/appstore2.png'>
					</div>
				</a>
				</div>
			</div>
			<? } ?>

			<!--
			<div class="col-sm-10 col-sm-offset-2 text-center">
				<div class="col-sm-3 col-md-30">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
					<a href='https://play.google.com/store/apps/details?id=com.bigit.wakuliner.customer' target='_blank'>
                        <div class="team-img">
                            <img class='img-responsive' src='images/play1.png'>
                        </div>
					</a>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 col-sm-offset-2">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
					<a href='https://itunes.apple.com/us/app/wakuliner-food-delivery-order/id1070066694?ls=1&mt=8' target='_blank'>
                        <div class="team-img">
                            <img class='img-responsive' src='images/appstore2.png'>
                        </div>
					</a>
                    </div>
                </div>


			</div>
			!-->

        </div>
<?
		/*
		if($mobile == 0) {
			echo "<div style='margin-bottom:350px;'></div>";
		}
		else {
			echo "<div style='margin-bottom:150px;'></div>";
		}
		*/
		?>
<style>


.marginbottom {
		margin-bottom:350px;
}

@media screen and (orientation:landscape) {
	.marginbottom {
		margin-bottom:350px;
	}
}

@media screen and (orientation:portrait) {
	.marginbottom {
		margin-bottom:150px;
	}
}
</style>
		<div class='marginbottom'></div>
		<img id='foot' src="images/wakutunggu.png" alt=""/>


    </section><!--/#testimonial-->


    <footer id="footer">
        <div class="container">
            <div class="row" style='margin-bottom:20px;'>
			<? if($mobile == 1) { ?>
                <div class="text-center col-sm-3" style='font-size:10px;'>
                    &copy; 2017 Wakuliner. All rights reserved
                </div>
                <div class="text-center col-sm-6" style='font-size:5px;'>
                    <ul class="social-icons">
                        <li><a href="#home">Apa Itu Wakuliner</a></li>
                        <li><a href="#features">Cara Ber-WAKULINER</a></li>
                        <li><a href="#cta2">Mengapa Ber-WAKULINER</a></li>
                        <li><a href="joinus.php">Join Us</a></li>
                    </ul>
                </div>
            </div>

			<div class="text-center col-sm-5 col-sm-offset-2">
				<ul class="social-icons">
					<li><a href="http://www.facebook.com/wakuliner/" target='_blank'><img src='images/fb.png'></a></li>
					<!--
					<li><a href="#"><img src='images/twit.png'></a></li>
					!-->
					<li><a href="http://www.instagram.com/wakuliner/" target='_blank'><img src='images/ig.png'></a></li>
				</ul>
			</div>
			<? } else { ?>
				<div class="col-sm-3">
                    &copy; 2017 Wakuliner. All rights reserved
                </div>
                <div class="col-sm-9">
                    <ul class="social-icons">
                        <li><a href="#home">Apa Itu Wakuliner</a></li>
                        <li><a href="#features">Cara Ber-WAKULINER</a></li>
                        <li><a href="#cta2">Mengapa Ber-WAKULINER</a></li>
                        <li><a href="joinus.php">Join Us</a></li>
                    </ul>
                </div>
            </div>

			<div class="col-sm-5 col-sm-offset-2">
				<ul class="social-icons">
					<li><a href="http://www.facebook.com/wakuliner/" target='_blank'><img src='images/fb.png'></a></li>
					<!--
					<li><a href="#"><img src='images/twit.png'></a></li>
					!-->
					<li><a href="http://www.instagram.com/wakuliner/" target='_blank'><img src='images/ig.png'></a></li>
				</ul>
			</div>
			<? } ?>
        </div>
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mousescroll.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>