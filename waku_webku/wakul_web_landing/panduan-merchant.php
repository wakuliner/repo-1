<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Wakuliner</title>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="images/favicon.ico">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <style type="text/css">
    </style>
	<style>
	#main-slider .item3 {
	  height: 260px;
	  background-repeat: no-repeat;
	  background-position: 80% 0;
	  background-size: cover;
	}
	.media.service-box {
  margin: 25px 0;
}

.media.service-box .pull-left {

  margin-left: -25px;

}

.media.service-box .pull-left > i {
  font-size: 24px;
  height: 64px;
  line-height: 64px;
  text-align: center;
  width: 64px;
  border-radius: 100%;
  color: #45aed6;
  box-shadow: inset 0 0 0 1px #d7d7d7;
  -webkit-box-shadow: inset 0 0 0 1px #d7d7d7;
  transition: background-color 400ms, background-color 400ms;
  position: relative;
}
.media.service-box .pull-left > i:after {
  content: "";
  position: absolute;
  width: 20px;
  height: 20px;
  top: 50%;
  margin-top: -10px;
  right: -10px;
  border: 4px solid #fff;
  border-radius: 20px;
  background: #45aed6;
}
	</style>

</head><!--/head-->

<?
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
	$mobile = 1;
} else {
	$mobile = 0;
}
?>

<style>
<? if($mobile == 1) { ?>
#img_m {
	width:50%;
	margin-left:20px;
}
<? } ?>
</style>

<body id="home" class="homepage">

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
					<?
						if($mobile == 1) {
							echo " <a class='navbar-brand' href='index.php'><img height='57px' src='images/logo.png' alt='logo'></a>";
						} else {
							echo " <a class='navbar-brand' href='index.php'><img height='57px' src='images/logo.png' alt='logo'></a>";
						}
					?>

                </div>

                <div class="collapse navbar-collapse navbar-right">
				<? if($mobile == 0) {
					$br = '<br>';
				} else {
					$br = '';
				}
				?>
                    <ul class="nav navbar-nav">
                        <li class="scroll"><a class='menu-wrap' href="index.php">Apa itu <?= $br; ?>WAKULINER</a></li>
                        <li class="scroll"><a class='menu-wrap' href="index.php#features">Cara <?= $br; ?>Ber-WAKULINER</a></li>
                        <li class="scroll"><a class='menu-wrap' href="index.php#cta2">Mengapa <?= $br; ?>Ber-WAKULINER</a></li>
						<?
						if($mobile == 1) {
							echo "<li class='scroll'><a class='menu-wrap' href='joinus.php'>Join Us</a></li>";
						} else { ?>
							<li><a class='menu-wrap' href="joinus.php"><img class='img-responsive' width='50%' style='margin-top:-10px;' src='images/btnjoin.png'></a></li>
						<?
						}
                        ?>
                    </ul>
                 </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->

    <section id="features">
        <div class="container">
            <div class="section-header">
                <h2 class="title text-center wow fadeInDown" style='font-family:Arista light,sans-serif;'>Cara Berjualan di Wakuliner</h2>
            </div>

			<p align="center">
				<h2 class="sub-title text-center" style='font-size:27px; font-weight:300;'>Ikuti 3 panduan merchant dibawah ini untuk mendaftarkan toko Anda, menerima pesanan, dan mengelola toko Anda.</h2>
			</p><br><br>

			<div class="col-sm-10 col-sm-offset-31 text-center" align="center">
			<? if($mobile == 1) { ?>

				<div onclick='daftartoko();' id='daftartoko' style='display:block;float:left;background-color:#d7fad9;width:30%;height:50px;margin-left:10px;cursor:pointer;border-style:groove;'>
					<img src='images/daftartoko.png' style='width:90%;margin-top:8px;'>
				</div>
				<div onclick='daftartoko();' id='daftartoko2' style='display:none;float:left;background-color:#f6f6f6;width:30%;height:50px;margin-left:10px;cursor:pointer;border-style:groove;'>
					<img src='images/daftartoko.png' style='width:90%;margin-top:8px;'>
				</div>

				<div onclick='terimaorder();' id='terimaorder' style='float:left;display:none;background-color:#d7fad9;width:30%;height:50px;margin-left:10px;border-style:groove;'>
					<img src='images/terimaorder.png' style='width:90%;margin-top:8px;cursor:pointer;'>
				</div>
				<div onclick='terimaorder();'id='terimaorder2' style='display:block;float:left;background-color:#f6f6f6;width:30%;height:50px;margin-left:10px;border-style:groove;'>
					<img src='images/terimaorder.png' style='width:90%;margin-top:8px;'>
				</div>

				<div onclick='panduan_cms();' id='panduan_cms' style='display:none;float:left;background-color:#d7fad9;width:30%;height:50px;margin-left:10px;cursor:pointer;border-style:groove;'>
					<img src='images/panduan_cms.png' style='width:90%;margin-top:8px;cursor:pointer;'>
				</div>
				<div onclick='panduan_cms();' id='panduan_cms2' style='display:block;float:left;background-color:#f6f6f6;width:30%;height:50px;margin-left:10px;cursor:pointer;border-style:groove;'>
					<img src='images/panduan_cms.png' style='width:90%;margin-top:8px;cursor:pointer;'>
				</div>
			<? } else { ?>

                <div class="col-sm-3 col-md-30" style='margin-left:50px;'>
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="team-img" id='waku-hov'>
							<div onclick='daftartoko();' id='daftartoko' style='display:block;background-color:#d7fad9;height:60px;cursor:pointer;border-style:groove;'>
								<img src='images/daftartoko.png' style='margin-top:8px;'>
							</div>
							<div onclick='daftartoko();' id='daftartoko2' style='display:none;background-color:#f6f6f6;height:60px;cursor:pointer;border-style:groove;'>
								<img src='images/daftartoko.png' style='margin-top:8px;'>
							</div>
						</div>
                    </div>
                </div>

				<div class="col-sm-3 col-md-30">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
                        <div class="team-img" id='waku-hov'>
							<div onclick='terimaorder();' id='terimaorder' style='display:none;background-color:#d7fad9;height:60px;border-style:groove;'>
								<img src='images/terimaorder.png' style='margin-top:8px;cursor:pointer;'>
							</div>

							<div onclick='terimaorder();'id='terimaorder2' style='display:block;background-color:#f6f6f6;height:60px;cursor:pointer;border-style:groove;'>
								<img src='images/terimaorder.png' style='margin-top:8px;'>
							</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-md-30" >
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">
                        <div class="team-img" id='waku-hov'>
							<div onclick='panduan_cms();' id='panduan_cms' style='display:none;background-color:#d7fad9;height:60px;border-style:groove;'>
								<img src='images/panduan_cms.png' style='margin-top:8px;cursor:pointer;'>
							</div>
							<div onclick='panduan_cms();' id='panduan_cms2' style='display:block;background-color:#f6f6f6;height:60px;border-style:groove;'>
								<img src='images/panduan_cms.png' style='margin-top:8px;cursor:pointer;'>
							</div>
                        </div>
                    </div>
                </div>
			<? } ?>
			</div>

			<script>

				function daftartoko() {
					document.getElementById('terimaorder_content').style.display = 'none';
					document.getElementById('terimaorder').style.display = 'none';
					document.getElementById('terimaorder2').style.display = 'block';

					document.getElementById('daftartoko_content').style.display = 'block';
					document.getElementById('daftartoko2').style.display = 'none';
					document.getElementById('daftartoko').style.display = 'block';

					document.getElementById('cms_content').style.display = 'none';
					document.getElementById('panduan_cms2').style.display = 'block';
					document.getElementById('panduan_cms').style.display = 'none';
				}

				function terimaorder() {
					document.getElementById('terimaorder_content').style.display = 'block';
					document.getElementById('terimaorder').style.display = 'block';
					document.getElementById('terimaorder2').style.display = 'none';

					document.getElementById('daftartoko_content').style.display = 'none';
					document.getElementById('daftartoko2').style.display = 'block';
					document.getElementById('daftartoko').style.display = 'none';

					document.getElementById('cms_content').style.display = 'none';
					document.getElementById('panduan_cms2').style.display = 'block';
					document.getElementById('panduan_cms').style.display = 'none';
				}


				function panduan_cms() {
					document.getElementById('terimaorder_content').style.display = 'none';
					document.getElementById('terimaorder').style.display = 'none';
					document.getElementById('terimaorder2').style.display = 'block';

					document.getElementById('daftartoko_content').style.display = 'none';
					document.getElementById('daftartoko2').style.display = 'block';
					document.getElementById('daftartoko').style.display = 'none';

					document.getElementById('cms_content').style.display = 'block';
					document.getElementById('panduan_cms2').style.display = 'none';
					document.getElementById('panduan_cms').style.display = 'block';

					document.getElementById('bg-feat').style.display = 'none';
				}
			</script>

			<style>

			#waku-hov:hover {
				background-color: #b8f6bc;
			}

			#waku-hov2:hover {
				/*
				border-style: solid;border-color:#fffbb4 #fffbb4 #11e91c #fffbb4;
				*/
				background-color: #b8f6bc;
			}

			.tab_jual {
				border: 4px solid #fffbcb;
				padding: 10px 40px;
				background: #fffbcb;
				width: 250px;
				height:85px;
				color:#28b328;
				border-radius: 10px;
			}

			.tab_active {
				border: 4px solid #00aa0b;
				padding: 10px 40px;
				background: #00aa0b;
				width: 250px;
				height:85px;
				color:#fff;
				border-radius: 10px;
			}

			/* Style the tab content */
			.tabcontent {
				display: none;
				padding: 6px 12px;
				border: 1px solid #ccc;
				border-top: none;
			}
			</style>

			<div id='daftartoko_content'>
				<div class="col-sm-10 col-sm-offset-1 text-center" style='margin-bottom:20px; margin-top:50px;'>
					<div class="team-info" style='text-align:center;' style='width:75%'>
						<p align="left">
							<div align="left">
								<div style='width:90%' >

									<img src='images/panduan-merchant/DaftarToko1.png' style='width:100%;height:100%;' align='left'/> <br>
									Sebelumnya, pastikan Anda sudah mendownload aplikasi Wakuliner <b>Merchant</b> dari Play Store / App Store, dan menyalakan push notif dan ringtone Anda.<br><br>
									Buka aplikasi Wakuliner Merchant di smartphone Anda, klik tombol <b>DAFTAR</b>, pastikan toko Anda sudah memiliki semua keperluan untuk mendaftar di Wakuliner.<br><br>

									<img src='images/panduan-merchant/DaftarToko2.png' style='width:100%;height:100%;' align='left'/> <br>
									Jika Anda mengirimkan pesanan dengan JNE / POS, pilih tipe toko <b>TOKO ONLINE</b><br>
									Jika Anda merupakan restoran dan mengirimkan pesanan dengan kurir sendiri, pilih tipe toko <b>RESTORAN</b><br><br>
									Isi semua data toko yang diperlukan dengan lengkap dan akurat, seperti:<br>
									- Logo toko<br>
									- Informasi toko<br>
									- Informasi pemilik toko<br>
									- Rekening bank<br>
									<b>Ingat / catat password Anda</b><br><br>
									Klik tombol <b>LANJUT</b><br><br>

									<img src='images/panduan-merchant/DaftarToko3.png' style='width:100%;height:100%;' align='left'/> <br>
									Jika toko Anda tipe <b>TOKO ONLINE</b>, tidak perlu memasukan area pengiriman. Silahkan lanjut ke langkah berikutnya. <br><br>
									Jika toko Anda tipe <b>RESTORAN</b>, lengkapi semua data area delivery toko Anda dengan akurat. Anda dapat menambah atau merubah area delivery sesuai keperluan Anda. <br>
									Klik tombol <b>LANJUT</b><br><br>

									<img src='images/panduan-merchant/DaftarToko4.png' style='width:100%;height:100%;' align='left'/> <br>
									Pilih maksimal 4 kategori menu untuk menu Anda. Kategori Menu ini akan menjadi buku menu atau etalase toko, yang nantinya akan ter-isi oleh semua menu Anda. <br>
									Klik <b>LANJUT</b><br><br>

									<img src='images/panduan-merchant/DaftarToko5.png' style='width:100%;height:100%;' align='left'/> <br>
									Upload semua menu Anda dan lengkapi setiap data nya. <br>
									Apabila menu memiliki pilihan, seperti topping, rasa, atau ukuran, pilih <b>"YA"</b> di dropdown "Apakah ingin menyediakan pilihan?".<br>
									Jika sudah lengkap semua, klik <b>KIRIM</b><br><br>

									<img src='images/panduan-merchant/DaftarToko6.png' style='width:100%;height:100%;' align='left'/> <br>
									Jika Anda berada diluar zona Waktu Indonesia Barat (WIB), pastikan Anda memasukan jam pesan-antar toko Anda sesuai zona waktu WIB.<br>
									Kemudian klik <b>LANJUT</b><br><br>

									<img src='images/panduan-merchant/DaftarToko7.png' style='width:100%;height:100%;' align='left'/> <br>
									Pilih tag toko yang menjelaskan menu dan jenis tempat usaha Anda agar dapat memudahkan pelanggan dalam memilih resto/toko. Klik tombol <b>LANJUT</b><br>
									Apabila Anda sudah yakin dengan semua kelengkapan informasi toko Anda, klik tombol <b>YA</b><br><br>

									<img src='images/panduan-merchant/DaftarToko8.png' style='width:100%;height:100%;' align='left'/> <br>
									Selamat! Toko Anda telah berhasil didaftarkan dan Anda dapat langsung berjualan. Tonton video tutorial untuk cara meng-akses dan menggunakan website admin Anda (atau klik tab Panduan Website Admin di halaman ini). Selamat berjualan! <br><br>

								</div>
							</div>
						</p>
					</div>
				</div>
			</div>

			<div id='terimaorder_content' style='display:none;'>
				<div class="col-sm-10 col-sm-offset-1 text-center" style='margin-bottom:20px; margin-top:50px;'>
					<div class="team-info" style='text-align:center;'>
						<p align="left">
							<div align="left">
								<div style='width:90%' >
									<img src='images/panduan-merchant/TerimaPesanan1.png' style='width:100%;height:100%;' align='left'/> <br>
									Jika ada push-notification di smartphone Anda mengenai pesanan Wakuliner, segera klik push-notification untuk membuka aplikasi Wakuliner Merchant Anda. Klik di <b>NEW</b> atau di area manapun di dalam kartu pesanan untuk melihat detil pesanan baru.<br><br>

									<img src='images/panduan-merchant/TerimaPesanan2.png' style='width:100%;height:100%;' align='left'/> <br>
									Klik <b>TERIMA PESANAN</b> dan segera siapkan pesanan pelanggan. Jika Anda harus menolak pesanan, mohon hubungi pelanggan sebelumnya, supaya pelanggan Anda tidak kecewa. <br><br>

									<img src='images/panduan-merchant/TerimaPesanan3.png' style='width:100%;height:100%;' align='left'/> <br>
									Kirim pesanan ke pelanggan Anda. Jika Anda mendaftarkan toko Anda sebagai <b>TOKO ONLINE</b>, segera kunjungi agen JNE terdekat dan kirimkan pesanan ke pelanggan menggunakan tipe layanan yang pelanggan pilih.<br>
									Jika Anda mendaftarkan toko Anda sebagai <b>RESTORAN</b>, langsung kirimkan pesanan dengan kurir Anda sendiri.<br><br>

									<img src='images/panduan-merchant/TerimaPesanan4.png' style='width:100%;height:100%;' align='left'/> <br>
									Setelah Anda mengirimkan pesanan, tekan tombol <b>KURIR</b> untuk memberitahukan kabar gembira kepada pelanggan, bahwa pesanan mereka sedang diantar oleh kurir Anda atau JNE.<br><br>

									<img src='images/panduan-merchant/TerimaPesanan5.png' style='width:100%;height:100%;' align='left'/> <br>
									Setelah pelanggan sudah konfirmasi telah menerima pesanan, tombol <b>CHECK</b> akan menyala. Anda harus klik tombol ini untuk memberikan rating / komentar terhadap pelanggan. Klik <b>KIRIM</b> untuk menyelesaikan pesanan. <br><br>

									<img src='images/panduan-merchant/TerimaPesanan6.png' style='width:100%;height:100%;' align='left'/> <br>
									Setelah memberikan rating terhadap pelanggan, dana akan otomatis masuk ke rekening bank Anda yang terdaftar di Wakuliner. Untuk pertanyaan mengenai ini, silahkan menghubungi Call Center Wakuliner.<br><br>

									<img src='images/panduan-merchant/FiturLain1.png' style='width:100%;height:100%;' align='left'/> <br>

									<img src='images/panduan-merchant/FiturLain2.png' style='width:100%;height:100%;' align='left'/> <br>

									<img src='images/panduan-merchant/FiturLain3.png' style='width:100%;height:100%;' align='left'/> <br>

									<img src='images/panduan-merchant/FiturLain4.png' style='width:100%;height:100%;' align='left'/> <br>

									<img src='images/panduan-merchant/FiturLain5.png' style='width:100%;height:100%;' align='left'/>
								</div>
							</div>
						</p>
					</div>
				</div>
			</div>

			<div id='cms_content' style='display:none;'>
				<div class="col-sm-10 col-sm-offset-1 text-center" style='margin-bottom:20px; margin-top:50px;'>
					<div class="team-info" style='text-align:center;'>
						<p align="left">
							<div align="left">
								<div style='width:90%' >
									<img src='images/panduan-merchant/PanduanCMS1.png' style='width:100%;height:100%;' align='left'/> <br>
									Login menggunakan alamat email dan password yang Anda gunakan waktu mendaftarkan Toko Anda di aplikasi. Gunakan Google Chrome / Firefox sebagai browser Anda. Jangan menggunakan Internet Explorer sebagai browser untuk membuka Wakuliner.<br>

									<img src='images/panduan-merchant/PanduanCMS2.png' style='width:100%;height:100%;' align='left'/> <br>

									<img src='images/panduan-merchant/PanduanCMS3.png' style='width:100%;height:100%;' align='left'/> <br>

									<img src='images/panduan-merchant/PanduanCMS4.png' style='width:100%;height:100%;' align='left'/> <br>
									Toko Anda dapat memiliki satu tambahan akun kasir, yang juga dapat menerima pesanan pelanggan. Tetapi akun kasir tidak dapat melihat laporan keuangan toko Anda.<br>

									<img src='images/panduan-merchant/PanduanCMS5.png' style='width:100%;height:100%;' align='left'/> <br>
									Kategori Menu adalah buku menu atau etalase toko Anda, yang ber-isi semua menu Anda. Pilih maksimal 4 kategori menu.<br>

									<img src='images/panduan-merchant/PanduanCMS6.png' style='width:100%;height:100%;' align='left'/> <br>
									Jika toko anda tipe TOKO ONLINE, wilayah pengiriman Anda sesuai dengan layanan JNE / POS, sehingga Anda sama sekali tidak perlu kuatir.<br>

									<img src='images/panduan-merchant/PanduanCMS7.png' style='width:100%;height:100%;' align='left'/> <br>
									Contoh: Jika Anda melayani seluruh kota Surabaya, setelah memilih Kota Surabaya, centang kotak dibawah nya yang  bertuliskan "Semua Kode Pos dan Kelurahan".<br>

									<img src='images/panduan-merchant/PanduanCMS8.png' style='width:100%;height:100%;' align='left'/> <br>
									Harus memasukan jam dalam format Waktu Indonesia Barat (WIB).<br>

									<img src='images/panduan-merchant/PanduanCMS9.png' style='width:100%;height:100%;' align='left'/> <br>

									<img src='images/panduan-merchant/PanduanCMS10.png' style='width:100%;height:100%;' align='left'/> <br>

									<img src='images/panduan-merchant/PanduanCMS11.png' style='width:100%;height:100%;' align='left'/> <br>

									<img src='images/panduan-merchant/PanduanCMS12.png' style='width:100%;height:100%;' align='left'/> <br>

									<img src='images/panduan-merchant/PanduanCMS13.png' style='width:100%;height:100%;' align='left'/> <br>
									Untuk saat ini, Anda hanya dapat memasang iklan promosi di salah satu area pelayanan Anda dengan gratis. Dan ukuran file promo Anda harus memiliki lebar/tinggi : 1360px/424px, dengan ukuran maksimal 2MB.<br>
								</div>
							</div>
						</p>
					</div>
				</div>
			</div>

			<p align="center">
				<h2 class="sub-title text-center" style='font-size:27px; font-weight:300;'>Persyaratan Penting</h2>
				<div align="center">
					<div style='width:75%' >
						<img src='images/logo_renilukaw_small.png' style='width:10%;height:10%;' align='center'/> <br>
						1. Aplikasi <b> Wakuliner Merchant</b> ter-install di smartphone. Jika belum, download dari Play Store / App Store<br>
						2. <b>Push-notification dan ringtone</b> harus menyala (jika ada pesanan, push-notification dan ringtone Wakuliner akan berbunyi di smartphone)<br><br>
						Untuk pertanyaan, silahkan menghubungi Call Center Wakuliner di:<br>
						(021) 5421-5151<br>
						0812-8113-2300<br>
						techsupport@wakuliner.com<br>
						Senin - Minggu, jam 09:00 - 21:00 WIB<br>
					</div>
				</div>
			</p>

        </div>
    </section>


    <section id="testimonial">
        <div class="container">
            <div class="section-header">
                <h2 class="title text-center wow fadeInDown" style='font-family:Arista light,sans-serif;'>Download Merchant App <br> Sekarang Juga!</h2>
            </div>
			<?
			if($mobile == 1) { ?>
				<a href='https://play.google.com/store/apps/details?id=com.bigit.wakuliner.merchant' target='_blank'>
					<img class='img-responsive' style='width:40%; float:left; margin-left:5%; margin-right:10%;' src='images/play1.png'>
				</a>
				<a href='https://itunes.apple.com/us/app/renilukaw/id1074598371?mt=8' target='_blank'>
					<img class='img-responsive' style='width:40%;' src='images/appstore2.png'>
				</a>

			<?
			} else {
			?>
			<div class="col-sm-3 col-sm-offset-3">
				<div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
				<a href='https://play.google.com/store/apps/details?id=com.bigit.wakuliner.merchant' target='_blank'>
					<div class="team-img">
						<img class='img-responsive' style='width:90%;' src='images/play1.png'>
					</div>
				</a>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
				<a href='https://itunes.apple.com/us/app/renilukaw/id1074598371?mt=8' target='_blank'>
					<div class="team-img">
						<img class='img-responsive' <? if($mobile == 1) { echo "style='width:80%;'"; } else { echo "style='width:95%;'"; } ?>  src='images/appstore2.png'>
					</div>
				</a>
				</div>
			</div>
			<? } ?>

        </div>
<?
		/*
		if($mobile == 0) {
			echo "<div style='margin-bottom:350px;'></div>";
		}
		else {
			echo "<div style='margin-bottom:150px;'></div>";
		}
		*/
		?>
<style>


.marginbottom {
		margin-bottom:350px;
}

@media screen and (orientation:landscape) {
	.marginbottom {
		margin-bottom:350px;
	}
}

@media screen and (orientation:portrait) {
	.marginbottom {
		margin-bottom:150px;
	}
}
</style>
		<div class='marginbottom'></div>
		<img id='foot' src="images/wakutunggu.png" alt=""/>


    </section><!--/#testimonial-->


    <footer id="footer">
        <div class="container">
            <div class="row" style='margin-bottom:20px;'>
			<? if($mobile == 1) { ?>
                <div class="text-center col-sm-3" style='font-size:10px;'>
                    &copy; 2017 Wakuliner. All rights reserved
                </div>
                <div class="text-center col-sm-6" style='font-size:5px;'>
                    <ul class="social-icons">
                        <li><a href="index.php#home">Apa Itu Wakuliner</a></li>
                        <li><a href="index.php#features">Cara Ber-WAKULINER</a></li>
                        <li><a href="index.php#cta2">Mengapa Ber-WAKULINER</a></li>
                        <li><a href="joinus.php">Join Us</a></li>
                    </ul>
                </div>
            </div>

			<div class="text-center col-sm-5 col-sm-offset-2">
				<ul class="social-icons">
					<li><a href="http://www.facebook.com/wakuliner/" target='_blank'><img src='images/fb.png'></a></li>
					<!--
					<li><a href="#"><img src='images/twit.png'></a></li>
					!-->
					<li><a href="http://www.instagram.com/wakuliner/" target='_blank'><img src='images/ig.png'></a></li>
				</ul>
			</div>
			<? } else { ?>
				<div class="col-sm-3">
                    &copy; 2017 Wakuliner. All rights reserved
                </div>
                <div class="col-sm-9">
                    <ul class="social-icons">
                        <li><a href="index.php#home">Apa Itu Wakuliner</a></li>
                        <li><a href="index.php#features">Cara Ber-WAKULINER</a></li>
                        <li><a href="index.php#cta2">Mengapa Ber-WAKULINER</a></li>
                        <li><a href="joinus.php">Join Us</a></li>
                    </ul>
                </div>
            </div>

			<div class="col-sm-5 col-sm-offset-2">
				<ul class="social-icons">
					<li><a href="http://www.facebook.com/wakuliner/" target='_blank'><img src='images/fb.png'></a></li>
					<!--
					<li><a href="#"><img src='images/twit.png'></a></li>
					!-->
					<li><a href="http://www.instagram.com/wakuliner/" target='_blank'><img src='images/ig.png'></a></li>
				</ul>
			</div>
			<? } ?>
        </div>
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mousescroll.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>