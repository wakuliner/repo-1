<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Wakuliner</title>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="images/ico/favicon.ico">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <style type="text/css">
    </style>
	<style>
	#main-slider .item3 {
	  height: 260px;
	  background-repeat: no-repeat;
	  background-position: 80% 0;
	  background-size: cover;
	}
	.media.service-box {
  margin: 25px 0;
}

.media.service-box .pull-left {

  margin-left: -25px;

}

.media.service-box .pull-left > i {
  font-size: 24px;
  height: 64px;
  line-height: 64px;
  text-align: center;
  width: 64px;
  border-radius: 100%;
  color: #45aed6;
  box-shadow: inset 0 0 0 1px #d7d7d7;
  -webkit-box-shadow: inset 0 0 0 1px #d7d7d7;
  transition: background-color 400ms, background-color 400ms;
  position: relative;
}
.media.service-box .pull-left > i:after {
  content: "";
  position: absolute;
  width: 20px;
  height: 20px;
  top: 50%;
  margin-top: -10px;
  right: -10px;
  border: 4px solid #fff;
  border-radius: 20px;
  background: #45aed6;
}
	</style>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/58e70501f7bbaa72709c4c15/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

</head><!--/head-->

<?
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
	$mobile = 1;
} else {
	$mobile = 0;
}
?>

<style>
<? if($mobile == 1) { ?>
#img_m {
	width:50%;
	margin-left:20px;
}
<? } ?>
</style>

<body id="home" class="homepage">

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
					<?
						if($mobile == 1) {
							echo " <a class='navbar-brand' href='index.php'><img height='57px' src='images/logo.png' alt='logo'></a>";
						} else {
							echo " <a class='navbar-brand' href='index.php'><img height='57px' src='images/logo.png' alt='logo'></a>";
						}
					?>

                </div>

                <div class="collapse navbar-collapse navbar-right">
				<? if($mobile == 0) {
					$br = '<br>';
				} else {
					$br = '';
				}
				?>
                    <ul class="nav navbar-nav">
                        <li class="scroll"><a class='menu-wrap' href="index.php">Apa itu <?= $br; ?>WAKULINER</a></li>
                        <li class="scroll"><a class='menu-wrap' href="index.php#features">Cara <?= $br; ?>Ber-WAKULINER</a></li>
                        <li class="scroll"><a class='menu-wrap' href="index.php#cta2">Mengapa <?= $br; ?>Ber-WAKULINER</a></li>
						<?
						if($mobile == 1) {
							echo "<li class='scroll'><a class='menu-wrap' href='joinus.php'>Join Us</a></li>";
						} else { ?>
							<li><a class='menu-wrap' href="joinus.php"><img class='img-responsive' width='50%' style='margin-top:-10px;' src='images/btnjoin.png'></a></li>
						<?
						}
                        ?>
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->

    <section id="cta" class="wow fadeIn">
	<?
	if($mobile == 0) { ?>
		<!-- <img src='' style='background-color:#87c03b;width:100%;height:25px;position:absolute;margin-left;margin-top:755px;'> -->
	<?
	}
	?>

	 <div class="container">
			<div class="section-header">
                <h2 class="title text-center wow fadeInDown" style='font-size:55px;font-family:Arista light,sans-serif;'>Pemenang Undian Berhadiah Wakuliner<br>Periode Januari - Maret 2017</h2>
            </div>

		<div class="row">
		<div class="col-md-6 col-md-offset-3">
		     <p style='text-align:left;font-size:15px;line-height:30px;font-weight:500;color:#000;font-family:Quicksand-Regular,sans-serif;'>
                SELAMAT! Berikut adalah daftar nama semua pemenang undian berhadiah Wakuliner (periode Januari - Maret 2017)<br><br>
				1. Royal Caribbean Cruise - Haji Riki (trsitarmultisarana2@gmail.com)<br><br>
				2. Bangkok Tour - Etik (pretty_himmatunnisa@yahoo.com)<br><br>
				3. Oppo F1 Plus - Francesca (francescapujianto@gmail.com)<br><br>
				4. Oppo F1 Selfie Expert - Nana (paschalia.tr@gmail.com)<br><br>
				5. Uang tunai Rp 500.000 - Adi (adst1711.as@gmail.com), Robby Imanuel (pingkymusic@yahoo.com), Edo (wahonojefri@gmail.com), Zakaria (zakaria.yahya58@gmail.com) & Rann (maryani.jurex@yahoo.com)<br><br>
                Silahkan cek inbox email anda untuk keterangan lebih lanjut dan segera lakukan konfirmasi untuk proses pengambilan hadiah anda!<br>
			</p>
		</div>
		</div>

		<div class="row">
		</div>

		<div class="row">
		</div>

	</div>
<div class="row">
  <div class="col-sm-12">
    <div class="row">
      <div >
        <img src='' style='background-color:#87c03b;width:100%;height:25px;position:absolute;margin-right;margin-top:51px;'>
      </div>
      <div>
        <img src='' style='background-color:#87c03b;width:100%;height:25px;position:absolute;margin-right;margin-top:51px;'>
      </div>
    </div>
  </div>
</div>

    </section><!--/#cta-->


	<style>
	img.ri
	{
		position: absolute;
		max-width: 80%;
		top: 10%;
		left: 10%;
		border-radius: 3px;
		box-shadow: 0 3px 6px rgba(0,0,0,0.9);
	}
	</style>


    <footer id="footer">
        <div class="container">
            <div class="row" style='margin-bottom:20px;'>
			<? if($mobile == 1) { ?>
                <div class="text-center col-sm-3" style='font-size:10px;'>
                    &copy; 2017 Wakuliner. All rights reserved
                </div>
                <div class="text-center col-sm-6" style='font-size:5px;'>
                    <ul class="social-icons">
                        <li><a href="index.php#home">Apa Itu Wakuliner</a></li>
                        <li><a href="index.php#features">Cara Ber-WAKULINER</a></li>
                        <li><a href="index.php#cta2">Mengapa Ber-WAKULINER</a></li>
                        <li><a href="joinus.php">Join Us</a></li>
                    </ul>
                </div>
            </div>

			<div class="text-center col-sm-5 col-sm-offset-2">
				<ul class="social-icons">
					<li><a href="http://www.facebook.com/wakuliner/" target='_blank'><img src='images/fb.png'></a></li>
					<!--
					<li><a href="#"><img src='images/twit.png'></a></li>
					!-->
					<li><a href="http://www.instagram.com/wakuliner/" target='_blank'><img src='images/ig.png'></a></li>
				</ul>
			</div>
			<? } else { ?>
				<div class="col-sm-3">
                    &copy; 2017 Wakuliner. All rights reserved
                </div>
                <div class="col-sm-9">
                    <ul class="social-icons">
                        <li><a href="index.php#home">Apa Itu Wakuliner</a></li>
                        <li><a href="index.php#features">Cara Ber-WAKULINER</a></li>
                        <li><a href="index.php#cta2">Mengapa Ber-WAKULINER</a></li>
                        <li><a href="joinus.php">Join Us</a></li>
                    </ul>
                </div>
            </div>

			<div class="col-sm-5 col-sm-offset-2">
				<ul class="social-icons">
					<li><a href="http://www.facebook.com/wakuliner/" target='_blank'><img src='images/fb.png'></a></li>
					<!--
					<li><a href="#"><img src='images/twit.png'></a></li>
					!-->
					<li><a href="http://www.instagram.com/wakuliner/" target='_blank'><img src='images/ig.png'></a></li>
				</ul>
			</div>
			<? } ?>
        </div>
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mousescroll.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>