<style>
.description {
	font-size:10px;
}
.underline {
    border-bottom: 1px solid #999
}
</style>

<form id="main-contact-form" name="contact-form" method="post" enctype='multipart/form-data' action="sendemail.php">
	
	<div class="form-item section">
		<div class="title">Jika Anda memenuhi semua persyaratan diatas, silahkan melanjutkan mengisi formulir pendaftaran dibawah ini</div>
	</div>
	<div class="form-item section underline">
        <div class="title"></div>
    </div>
	
	<div class="form-group">
		<label class="title">Nama tempat usaha <span class="required">*</span></label>
		<input type="text" name="1" class="form-control" placeholder="Nama restoran, depot, atau toko" required>
	</div>
	<div class="form-group">
		<label class="title">Profil atau keterangan tempat usaha dan makanan / minuman Anda <span class="required">*</span></label>
		<div class="description">Profil tempat usaha dan produk makanan / minuman Anda untuk dibaca pemakai aplikasi atau pelanggan</div>
		<textarea class="form-control" name='2'></textarea>
	</div>
	<div class="form-group">
		<label class="title">Upload Logo tempat usaha<span class="required">*</span></label>
		<input type='file' name='files'>
	</div>
	<div class="form-group">
		<label class="title">Alamat tempat usaha<span class="required">*</span></label>
		<input type="text" name="3" class="form-control" placeholder="(Nama & nomer jalan, kota, kode pos, propinsi)" required>
	</div>
	<div class="form-group">
		<label class="title">Nomer telepon tempat usaha <span class="required">*</span></label>
		<div class="description">Untuk menerima kabar / pertanyaan mengenai pesanan pelanggan</div>
		<input type="text" name="4" class="form-control" placeholder="Nomer telepon tempat usaha atau kasir" required>
	</div>
	<div class="form-group">
		<label class="title">Nama pemilik <span class="required">*</span></label>
		<input type="text" name="5" class="form-control" placeholder="Nama pemilik tempat usaha" required>
	</div>
	<div class="form-group">
		<label class="title">Nomer HP / telepon pemilik <span class="required">*</span></label>
		<div class="description">Untuk pertanyaan mengenai operasional dan pemberitahuan penting</div>
		<input type="text" name="6" class="form-control" placeholder="Nomer HP / cellphone pemilik" required>
	</div>
	<div class="form-group">
		<label class="title">Email pemilik<span class="required">*</span></label>
		<div class="description">Untuk log in aplikasi dan website Wakuliner, dan menerima pemberitahuan penting</div>
		<input type="text" name="7" class="form-control" placeholder="Alamat Email Pemilik" required>
	</div>
	<div class="form-group">
		<label class="title">Konfirmasi email pemilik <span class="required">*</span></label>
		<input type="text" name="8" class="form-control" placeholder="Konfirmasi email pemilik" required>
	</div>
	
	<div class="form-item section underline"></div>
	
	 <div class="form-item field checkbox required">
		<div class="title">Biaya komisi 10% <span class="required">*</span></div>
		<div class="description">Biaya komisi 10% (dari harga bersih makanan, sebelum pajak, service charge, dan ongkos kirim) adalah biaya promo pre-launching untuk 1000 merchant pertama. Tarif normal komisi setelah launching adalah 15%.</div>
		
		<div class="option"><label><input type="checkbox" name="checkbox-yui2" value="Saya setuju (digital signatur)"/> Saya setuju (digital signatur)</label></div>
		
	 </div>
	 
	 <div class="form-item field checkbox required">
		<div class="title">Metode pembayaran pelanggan <span class="required">*</span></div>
		<div class="description">Pilihan metode pembayaran pelanggan kepada Anda. Pembayaran tunai / COD dari pelanggan tidak dikenakan biaya. Pembayaran online dari pelanggan (kartu kredit) akan dikenakan biaya tambahan 3,2% dari jumlah total transaksi, termasuk pajak dan ongkos kirim. 3,2% ini adalah biaya bank - ditanggung oleh pengusaha tempat makan</div>
		
		<div class="option"><label><input type="checkbox" name="checkbox-yui3[]" value="Tunai / Cash on delivery"/> Tunai / Cash on delivery</label></div>
		
		<div class="option"><label><input type="checkbox" name="checkbox-yui3[]" value="Online / Kartu kredit"/> Online / Kartu kredit</label></div>
		
	 </div>
	 
	 <div class="form-item field checkbox required">
		<div class="title">Informasi rekening bank tempat usaha</div>
        <div class="description">Dibutuhkan untuk transfer pembayaran online dari pelanggan dan pembayaran voucher dari wakuliner</div>	
	 </div>
	 
	 <div class="form-item section underline"></div>
	 
	 <div class="form-group">
		<label class="title">Nama pemilik rekening <span class="required">*</span></label>
		<input type="text" name="9" class="form-control" required>
	</div>
	<div class="form-group">
		<label class="title">Nama bank <span class="required">*</span></label>
		<div class="description">Non BCA dikenakan biaya transfer antar rekening</div>
		<input type="text" name="10" class="form-control" required>
	</div>
	<div class="form-group">
		<label class="title">Cabang bank<span class="required">*</span></label>
		<input type="text" name="11" class="form-control" required>
	</div>
	<div class="form-group">
		<label class="title">Nomer rekening bank <span class="required">*</span></label>
		<input type="text" name="12" class="form-control" required>
	</div>
	
	<div class="form-item section underline"></div>
	
	<div class="form-item field checkbox required">
		<div class="title">Service / Pelayanan  <span class="required">*</span></div>
		
		<div class="option"><label><input type="checkbox" name="checkbox-yui4[]" value="Delivery / Pengiriman"/> Delivery / Pengiriman</label></div>
		
		<div class="option"><label><input type="checkbox" name="checkbox-yui4[]" value="Take away / Pengambilan"/> Take away / Pengambilan</label></div>
		
	 </div>
	 
	 <div class="form-item section underline"></div>
	
	<div class="form-group">
		<label class="title">Area Pelayanan dan Pengantaran  <span class="required">*</span></label>
		<div class="description">Cantumkan area pengiriman Anda, dengan format: Kota / area pengantaran - Ongkos kirim - Waktu lama kirim (menit, jam, hari) - Minimum order. Contoh: Seluruh Jakarta - 5.000 - 60 menit - 100.000 Darmo Wonokromo 60241 - Free ongkir - 45 menit - No minimum order</div>
		<textarea class="form-control" name='13' placeholder='Kota / area pengantaran - Ongkos kirim - Waktu lama kirim (menit, jam, hari) - Minimum order.'></textarea>
	</div>
	
	 <div class="form-group">
		<label class="title">Hari dan Jam Kerja  <span class="required">*</span></label>
		<div class="description">Cantumkan hari dan jam kerja Anda, dimana Anda sanggup menerima dan mengantar pesanan dari pelanggan Anda. Contoh: Senin - Sabtu, jam 10:00 - 21:00.</div>
		<textarea class="form-control" name='14' placeholder='Hari dan jam kerja'></textarea>
	</div>
	
	<div class="form-item section underline"></div>
	
	<div id="text-yui_3_17_2_1_1453024818555_121318" class="form-item field text required">
                <label class="title" for="text-yui_3_17_2_1_1453024818555_121318-field">Perjanjian Kerjasama Antara Wakuliner (PT BigIT Republik Aplikasi) dengan pemilik tempat usaha <span class="required">*</span></label>
                <div class="description">Pada hari ini, telah ditandatangani Perjanjian Kerjasama antara PT BigIT Republik Aplikasi, selaku pemilik dari dan oleh 

karenanya untuk dan atas nama serta sah mewakili “Wakuliner”
(Selanjutnya disebut Pihak Pertama) dan pemilik tempat usaha (yang akan memberikan nama di akhir perjanjian ini dan 

memberikan e-signature) (Selanjutnya disebut Pihak Kedua/ Pengusaha makanan/minuman);  Pihak Pertama dan Pihak Kedua 

secara bersama-sama disebut Para Pihak.<br>
Para Pihak menerangkan terlebih dahulu sebagai berikut : <br>
-	Bahwa Pihak Pertama merupakan badan usaha yang bergerak di bidang pembuatan, pengoperasian, dan pengaturan 

aplikasi untuk teknologi gadget dengan nama “Wakuliner” dan/atau nama aplikasi lainnya yang akan ada dan/atau digunakan di 

masa yang akan datang, yaitu aplikasi yang dapat dijadikan sarana bagi para pemilik tempat usaha makanan dan minuman yang 

setuju untuk memasarkan tempat usahanya dan mendapatkan pesanan dari masyarakat melalui aplikasi “Wakuliner” (selanjutnya 

disebut Aplikasi); <br>
-	Bahwa Pihak Kedua/ Pengusaha makanan/minuman merupakan  Pemilik Tempat usaha yang setuju menjualkan produk makanan 

atau minuman kepada konsumen melalui Aplikasi.   Para Pihak telah setuju dan sepakat untuk melakukan kerjasama tersebut 

diatas dengan syarat dan ketentuan sebagai berikut :  <br><br>

PASAL 1 LINGKUP KERJASAMA  <br>
Kerjasama ini berlaku dan mengikat Para Pihak di seluruh wilayah negara Republik Indonesia<br><br>

PASAL 2 KEWAJIBAN DAN HAK PIHAK PERTAMA  <br>
I.	KEWAJIBAN <br>
1.	Pihak Pertama akan memberikan pelayanan melalui Aplikasi bagi Konsumen untuk memesan makanan atau minuman dari 

Pihak Kedua. <br>
2.	Pihak Pertama akan memberikan pelayanan melalui Aplikasi bagi Pihak Kedua untuk menerima pesanan dari konsumen dan 

untuk menggunakan pelayanan dan semua fitur-fitur lain yg tersedia di Aplikasi. <br>
3.	Pihak Pertama tidak memungut biaya Registrasi maupun biaya penggunaan Aplikasi bulanan dari Pihak Kedua. <br>
4.	Pihak Pertama wajib memberikan layanan Customer Service terhadap Pihak Kedua dalam hal penggunaan Aplikasi dan 

manajemen informasi Pihak Kedua yang ada didalam Aplikasi. Layanan mana diberikan pada hari Senin sampai dengan Jumat, 

jam kerja pukul 08.00 sampai dengan 17.00 Waktu Indonesia Barat. <br>
5.	Pihak Pertama tidak menyediakan pelayanan pengiriman dan pengambilan makanan dan minuman.  Pihak Pertama tidak 

bertanggung jawab atas semua masalah yang disebabkan baik secara langsung atau tidak langsung oleh persiapan dan 

pengiriman makanan atau minuman oleh Pihak Kedua. <br>
6.	Jika Konsumen memilih untuk membayar secara Online, maka Pihak Pertama yang akan menerima pembayaran tersebut dan 

akan membayarkan bagian yang merupakan hak dari Pihak Kedua sebagaimana diatur dalam Pasal 4 di bawah ini.  <br>
II.	HAK  <br>
1.	Pihak Pertama berhak menerima Komisi dari semua penjualan kotor Pihak Kedua yang terjadi melalui Aplikasi (sebelum 

dikenakan pajak, service charge dan biaya pengiriman (bila ada biaya pengiriman)), sesuai dengan Pasal 4 butir 1. <br>
2.	Pihak Pertama berhak menawarkan dan memberikan Voucher atau Kupon kepada Konsumen dengan nominal yang besarnya 

ditentukan secara sepihak oleh Pihak Pertama.  Voucher/ Kupon yang sudah digunakan oleh Konsumen akan ditanggung dan 

dibayarkan oleh Pihak Pertama kepada Pihak Kedua sesuai dengan nominal voucher/ kupon yang digunakan tersebut.<br>
3.	Pihak Pertama mewajibkan Konsumen untuk memberikan nilai kepada pelayanan dan produk Pihak Kedua.  Pihak pertama 

juga mewajibkan Pihak Kedua untuk memberi nilai kepada Konsumen. <br>
4.	Pihak Pertama berhak  menampilkan dan/atau menghapus nilai dan ulasan yang diberikan oleh Konsumen dan Pihak Kedua 

di dalam Aplikasi tanpa diperlukan persetujuan terlebih dahulu dari semua pihak. <br>
5.	Jika Pihak Kedua sering mendapatkan nilai dan ulasan yang kurang baik, komplain atau sering lalai dalam tanggung 

jawab pengantaran pesanan, maka demi menjaga kualitas dan Reputasi Pihak Pertama dan Aplikasi, Pihak Pertama berhak 

memberhentikan Perjanjian ini secara sepihak sebagaimana diatur dalam Pasal 7 di bawah ini.  <br>
6.	Pihak Pertama berhak membuat perubahan terhadap pelayanan dan Aplikasi, tanpa pemberitahuan terlebih dahulu kepada 

pihak Kedua. 7.	Pihak Pertama berhak menggunakan nama tempat usaha, logo, dan menu Pihak Kedua di dalam Aplikasi, untuk 

menjalankan dan memasarkan Aplikasi dan juga untuk memasarkan Pihak Kedua di Aplikasi maupun di internet, tanpa 

persetujuan terlebih dahulu dari Pihak Kedua.  <br><br>

PASAL 3 HAK DAN KEWAJIBAN PIHAK KEDUA <br>
I.	 HAK <br>
1.	Pihak Kedua berhak untuk menggunakan Aplikasi yang disediakan Pihak Pertama secara Cuma-Cuma, yaitu tanpa biaya 

registrasi maupun biaya bulanan. <br>
2.	Pihak Kedua berhak menerima pembayaran dari pembayaran online yang diterima Pihak Pertama setiap bulan sesuai 

Pasal 4. <br>
3.	Pihak Kedua dibebaskan dari biaya marketing dan promosi yang dibutuhkan untuk mempromosikan Aplikasi kepada 

masyarakat luas. <br>
4.	Pihak Kedua berhak mendapatkan nilai dan / atau ulasan dari konsumen <br>
5.	Pihak Kedua berhak mendapatkan pelayanan Customer Service dari BigIT Mobile. Layanan mana diberikan pada hari 

Senin sampai dengan Jumat, jam kerja pukul 08.00 sampai dengan 17.00 Waktu Indonesia Barat. <br>
6.	Pihak Kedua berhak untuk memberhentikan perjanjian ini dengan alasan apapun, tetapi sebelumnya dengan 

pemberitahuan secara tertulis kepada Pihak Pertama dan Pihak Pertama harus menandatangani surat tersebut. <br>
II. 	KEWAJIBAN <br>
1.	Pihak Kedua  berkewajiban membayar Komisi dari semua transaksi penjualan yang terjadi melalui Aplikasi  kepada 

Pihak Pertama.  Pembayaran Komisi diatur pada Pasal 4. <br>
2.	Pihak Kedua mengetahui dan mengerti bahwa angka komisi yang lebih tinggi akan menempatkan Pihak Kedua di posisi 

lebih atas dan unggul di daftar tempat usaha di Aplikasi Pihak Pertama.  Angka komisi dapat diubah setiap saat atas 

permintaan Pihak kedua dengan perjanjian secara tertulis dengan Pihak Pertama. <br>
3.	Pihak Kedua mengumpulkan sendiri semua pembayaran dari konsumen dan wajib membayar komisi kepada Pihak Pertama 

setiap bulan sebagaimana diatur lebih lanjut dalam Pasal 4 di bawah ini, kecuali apabila Konsumen memilih untuk membayar 

secara Online sebagaimana diatur dalam Pasal 2 .  <br>
4.	Pihak Kedua wajib memenuhi kualifikasi dan persyaratan yang tercantum dibawah ini dan Pihak Kedua telah menyetujui 

untuk menjaga kualifikasi dan persyaratan tersebut selama hubungan kerja sama dan Perjanjian ini berlaku. <br>
I.	Menyediakan SMART PHONE yang akan digunakan KHUSUS untuk Aplikasi ini, guna mencegah segala bentuk 

interupsi/gangguan. <br>
II.	Mempunyai koneksi internet, baik melalui wifi atau paket langganan internet, berkapasitas cukup dan stabil untuk 

menerima pesanan selama jam kerja. <br>
III.	Mempunyai tempat usaha menetap dengan hari dan jam kerja yang pasti. <br>
IV.	Mengirim makanan dan minuman secara aman, bersih, sopan, profesional dan dalam janji waktu yang telah disepakati 

dan dijanjikan oleh Pihak Kedua (khusus untuk jasa pengiriman). <br>
V.	Memenuhi pesanan dan permintaan yang diminta oleh Konsumen melalui Aplikasi. <br>
VI.	Memberikan semua informasi yg diperlukan Pihak Pertama untuk menawarkan produk makanan atau minuman dari Pihak 

Kedua. Semua informasi dari Pihak Kedua tidak boleh melanggar hak cipta milik siapapun. Informasi yang diperlukan sebagai 

berikut: <br>
a.	Nama tempat usaha atau pengusaha makanan atau minuman <br>
b.	Nama perusahaan <br>
c.	Nama pemilik, alamat lengkap, nomor telepon, dan foto KTP <br>
d.	Logo tempat usaha atau pengusaha makanan atau minuman <br>
e.	Penjelasan mengenai tempat usaha atau pengusaha makanan atau minuman <br>
f.	Nomor telepon dan alamat tempat usaha atau pengusaha makanan atau minuman (untuk konsumen dan Pihak Pertama). 

Nomor telepon ini tidak boleh sama dengan nomor telepon smartphone yang akan digunakan untuk Aplikasi demi mencegah segala 

bentuk interupsi. <br>
g.	Hari dan jam kerja. Hari-hari libur dalam setahun <br>
h.	Label-label yang menjelaskan jenis tempat usaha dan makanan atau minuman Pihak Kedua akan mendapatkan daftar 

tersebut dari Pihak Pertama<br>
i.	5 digit kode pos untuk wilayah pengiriman (bisa memberi lebih dari satu kode pos) <br>
j.	Biaya pengiriman untuk masing-masing kode pos yang di daftarkan <br>
k.	Durasi / lama pengiriman <br>
l.	Harga minimum pembelian untuk pengiriman <br>
m.	Menu makanan dan/atau minuman (nama, harga, ukuran atau besar porsi, keterangan,  dan gambar) <br>
n.	Top 10 menu makanan dan/atau minuman <br>
o.	Keterangan pelayanan jasa. Contohnya: apakah menyediakan jasa pengiriman, pengambilan, atau keduanya. <br>
p.	Metode pembayaran yang diterima (bayar tunai di tempat atau pembayaran online) <br>
*Pihak Kedua bertanggung jawab atas informasi yg diberikan kepada Pihak Pertama. Pihak Kedua bertanggung jawab untuk 

memperbaharui semua keterangan seperlunya dan secukupnya melalui Aplikasi atau dengan menghubungi Pihak Pertama. Pihak 

Pertama tidak bertanggung jawab atas keterangan yang tidak akurat atau sudah tidak berlaku yang diberikan oleh Pihak 

Kedua, yang dapat merugikan pihak manapun. Oleh karenanya Pihak Kedua dengan ini membebaskan Pihak Pertama dari segala 

tuntutan dan/atau gugatan ganti rugi yang dapat timbul dikemudian hari, baik mengenai hak cipta maupun ketidak akuratan 

data yang diberikan oleh Pihak Kedua dalam Aplikasi. <br>
5.	Pihak Kedua bertanggung jawab sepenuhnya atas kualifikasi dan persyaratan dalam butir 4 di atas. Pihak Kedua 

membebaskan Pihak Pertama dari segala tuntutan dan/atau gugatan ganti rugi yang dapat timbul dikemudian hari apabila 

terjadi kerugian dan/atau musibah yang disebabkan oleh kelalaian Pihak Kedua dalam memenuhi kualifikasi dan persyaratan 

tersebut. <br>
6.	Pihak Kedua wajib menjalankan bisnis dalam hari dan jam kerja yang sudah ditentukan oleh Pihak Kedua dan telah 

diberitahukan pada Pihak Pertama. Pihak Kedua dapat merubah status Pihak Kedua di Aplikasi dari “Buka/Open” ke “Tutup” 

hanya dalam kondisi darurat. <br>
7.	Jika pesanan konsumen ditolak dan diakibatkan oleh kelalaian Pihak Kedua seperti “kesalahan menu, menu tidak 

tersedia atau habis” ataupun alasan lainya, dan konsumen telah membayar pesanan online, Pihak Kedua bertanggung jawab dan 

wajib membayar sepenuhnya biaya admin proses refund yang akan dikenakan oleh bank atau payment gateway.<br> 
8.	Ketika menggunakan Aplikasi dan melayani semua pesanan melalui Aplikasi Pihak Pertama, Pihak Kedua dilarang 

memasarkan kompetitor atau pesaing Pihak Pertama dalam bentuk apapun kepada konsumen. <br>
9.	Pihak Kedua telah melengkapi, mengetahui sepenuhnya, dan menyetujui semua informasi yang tercantum dalam Formulir 

Pendaftaran Pengusaha makanan / minuman.<br><br>

PASAL 4 TAGIHAN DAN PEMBAYARAN <br> 
1.	Komisi yang harus dibayar oleh Pihak Kedua ke Pihak Pertama adalah sebesar 10% dari nilai penjualan sebelum pajak, 

service charge, dan biaya pengiriman (bagi yang mempunyai jasa pengiriman). <br>
2.	Jika konsumen memilih untuk membayar secara tunai, Pihak Kedua akan menerima pembayaran dari konsumen dan membayar 

komisi Pihak Pertama setiap bulan sesuai dengan pasal 4 butir 1. <br>
3.	Jika konsumen memilih untuk membayar secara online, Pihak Kedua akan dikenakan biaya pembayaran online sebesar 3,2 

% dari jumlah transaksi. <br>
4.	Setiap bulan, Pihak Pertama akan memberikan surat tagihan dan laporan penyelesaian keuangan kepada Pihak Kedua 

yang akan berisi: <br>
a.	Jumlah total pembayaran yang diterima Pihak Pertama dari konsumen yang membayar online dalam bulan fiskal 

tersebut.  Beserta perhitungan komisi Pihak Pertama dan biaya pembayaran online sebagaimana diatur dalam butir 1 dan 3 di 

atas. <br>
b.	Jumlah total pembayaran tunai yang diterima Pihak Kedua dari konsumen yang membayar tunai / cash on delivery dalam 

bulan fiskal yang sama.  Beserta dengan perhitungan komisi untuk Pihak Pertama sebagaimana diatur dalam butir 1. <br>
5.	Pihak Kedua harus melunasi seluruh jumlah tagihan dan dilakukan secepatnya setelah mendapatkan tagihan dari Pihak 

Pertama. Pihak kedua mempunyai waktu 10 hari terhitung sejak tanggal diterimanya surat tagihan tersebut untuk membayar 

seluruh tagihan sebelum dinyatakan gagal dan lalai secara hukum untuk melakukan pembayaran. Pembayaran dilakukan melalui 

transfer ke rekening bank Pihak Pertama. <br>
6.	Pihak Pertama wajib membayarkan kepada Pihak Kedua seluruh pembayaran online dari konsumen setiap bulan, setelah 

dikurangi komisi dan biaya pembayaran online sebagaimana diatur dalam butir 1 dan 3 di atas, selambat-lambatnya 10 hari 

setelah laporan keuangan diterima Pihak Kedua. Pembayaran dilakukan melalui transfer ke rekening bank Pihak Kedua. <br> 
7.	Apabila salah satu dari Para Pihak telah lalai dalam menyelesaikan kewajiban keuangan sebagaimana diatur dalam 

butir 5 diatas, maka Pihak yang lalai akan dikenakan denda sebesar 5% / bulan dari jumlah tagihan yang wajib dibayarkan. 

Apabila dalam jangka waktu 30 hari setelah kelalaian tersebut masih tidak ada pelunasan, kerjasama ini akan berakhir 

sebagaimana diatur dalam pasal 7 di bawah ini.<br> 
8.	Bank pilihan Pihak Pertama untuk transfer dan pembayaran adalah BCA. Jika Pihak Kedua menggunakan bank lain, Pihak 

Kedua wajib menanggung biaya admin bank untuk melakukan transfer atau menerima transfer dari rekening BCA Pihak 

Pertama.<br><br>

PASAL 5 KEAMANAN, KENYAMANAN, DAN PRIVASI<br>  
1.	Pihak Pertama tidak akan bertanggung jawab atas kejadian atau situasi yang berada diluar kendali Pihak Pertama, 

seperti kualitas dan produk hasil Pihak kedua, pelayanan, pengantaran, dan persiapan makanan Pihak Kedua, dan pelanggaran 

hukum dan peraturan yang dilakukan oleh Pihak Kedua. Pihak Kedua dengan ini melepaskan Pihak Pertama dari segala tuntutan 

dan/atau gugatan ganti rugi yang dapat timbul di kemudian hari yang disebabkan oleh hal-hal tersebut. <br>
2.	Pihak  Pertama tidak menjamin kesempurnaan dalam penggunaan Aplikasi ini. Pihak Pertama akan berusaha semaksimal 

dan sebaik mungkin untuk menyelesaikan semua masalah yang disebabkan oleh Aplikasi dan akan menyediakan Customer Service 

untuk Pihak Kedua dan konsumen dalam mengatasi masalah Aplikasi. <br>
3.	Pihak Pertama dan Pihak Kedua wajib merahasiakan perjanjian ini beserta seluruh isinya dan semua informasi yang 

berhubungan dengan kerjasama antara Pihak Pertama dan Pihak kedua. <br><br> 
PASAL 6 PERUBAHAN DAN ADDENDUM<br>
Pihak Pertama berhak 

untuk secara sepihak merubah pasal-pasal beserta isinya, dengan memberikan pemberitahuan sebelumnya kepada Pihak Kedua. 

Jika Pihak Kedua tidak memberikan balasan dalam 2 (dua) minggu setelah menerima pemberitahuan, perubahan-perubahan akan 

dinyatakan sah dan telah disetujui oleh Pihak Kedua.<br><br>

PASAL 7 BERAKHIRNYA KERJASAMA  <br>
Dalam hal salah satu pihak meninggal dunia, maka kerjasama ini tidak berakhir dengan sendirinya melainkan akan beralih 

pada para ahli waris dan/atau penerusnya yang sah, dan ketentuan - ketentuan dalam kerjasama ini tetap berlaku dan 

mengikat para ahli waris dan/atau penerusnya yang sah.  Apabila terjadi hal-hal sebagaimana disebutkan dalam Pasal Pasal 2 

poin II butir 5, dan Pasal 4 butir 7, Para Pihak setuju dan sepakat untuk mengesampingkan Pasal 1266 Kitab Undang-undang 

Hukum Perdata, sehingga kerjasama ini dianggap batal dengan keputusan sepihak dari Pihak Pertama.  <br><br>

PASAL 8 PENYELESAIAN SENGKETA  <br>
Segala perselisihan yang mungkin timbul dikemudian hari atas kerjasama ini akan diselesaikan oleh Para Pihak secara 

musyawarah mufakat.<br><br>

PASAL 9 LAIN – LAIN<br>
Perjanjian Kerjasama ini berlaku sejak ditandatanganinya Perjanjian ini oleh Para Pihak. Hal-hal lain yang belum diatur 

dalam Perjanjian ini akan diatur kemudian hari oleh Para Pihak dan dituangkan dalam perjanjian tambahan yang tidak 

terpisahkan dari Perjanjian Kerjasama ini.<br>
</div>
    <input type="text" name="15" class="form-control" placeholder='Cantumkan Nama lengkap Anda sebagai penandatangan surat perjanjian kerjasama dan e-signature dibawah ini' required>          
	</div> <!-- End Pasal !-->
	
	<div class="form-item field checkbox required">
		<div class="title">Saya, yang mencantumkan nama lengkap diatas ini dan menandatangani digital signatur dibawah ini, telah membaca dan menyutujui perjanjian kerjasama antara Wakuliner dan saya. Saya menerima semua persyaratan dan kondisi dari Wakuliner. <span class="required">*</span></div>
		
		<div class="option"><label><input type="checkbox" name="checkbox-yui5" value="Saya setuju"/> Saya setuju (digital signatur)</label></div>
		
	 </div>
	
	<button type="submit" id='bukadaftar'>DAFTAR</button>
	
	<div style='margin-bottom:75px;'></div>
	
</form>