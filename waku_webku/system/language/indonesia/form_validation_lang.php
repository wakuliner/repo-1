<?php

$lang['required']			= "%s harus diisi.";
$lang['isset']				= "%s harus memiliki nilai.";
$lang['valid_email']		= "%s harus berisi alamat email yang valid.";
$lang['valid_emails']		= "%s harus berisi alamat email yang valid.";
$lang['valid_url']			= "%s harus berisi alamat URL yang valid.";
$lang['valid_ip']			= "%s harus berisi alamat IP yang valid.";
$lang['min_length']			= "%s harus memiliki minimal %s karakter.";
$lang['max_length']			= "%s harus memiliki maksimal %s karakter.";
$lang['exact_length']		= "%s field must be exactly %s characters in length.";
$lang['alpha']				= "%s field may only contain alphabetical characters.";
$lang['alpha_numeric']		= "%s field may only contain alpha-numeric characters.";
$lang['alpha_dash']			= "%s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "%s harus diisi hanya dengan angka.";
$lang['is_numeric']			= "%s harus diisi hanya dengan karakter numeric.";
$lang['integer']			= "%s harus bernilai integer.";
$lang['regex_match']		= "%s mempunyai format yang salah.";
$lang['matches']			= "%s tidak sama dengan %s.";
$lang['is_unique'] 			= "%s field must contain a unique value.";
$lang['is_natural']			= "%s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "%s field must contain a number greater than zero.";
$lang['decimal']			= "%s field must contain a decimal number.";
$lang['less_than']			= "%s field must contain a number less than %s.";
$lang['greater_than']		= "%s field must contain a number greater than %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */