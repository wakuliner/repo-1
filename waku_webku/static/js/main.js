$( document ).ready(function() {
	
	$( "#tombolRegister" ).click(function() {
  		$('#login-box').addClass("hide");
		$('#register-box').removeClass("hide");	
	});
	
	$( "#tombolLogin" ).click(function() {
  		$('#register-box').addClass("hide");
		$('#login-box').removeClass("hide");	
	});
	
	$('#example-css').barrating({
		theme: 'css-stars',
		showSelectedRating: false
	});
	
	$('.rating-css').barrating({
		theme: 'css-stars',
		showSelectedRating: false
	});
	
	$('#tabWakuAntar').click(function(){
		$('#imgWakuAntar').attr('src', base_url+'static/images/content/WAon.png');
		$('#imgWakuWiku').attr('src', base_url+'static/images/content/WWoff.png');
		$('#imgWakuKatering').attr('src', base_url+'static/images/content/WKoff.png');
	});
	
	$('#tabWakuWiku').click(function(){
		$('#imgWakuAntar').attr('src', base_url+'static/images/content/WAoff.png');
		$('#imgWakuWiku').attr('src', base_url+'static/images/content/WWon.png');
		$('#imgWakuKatering').attr('src', base_url+'static/images/content/WKoff.png');
	});
	
	$('#tabWakuKatering').click(function(){
		$('#imgWakuAntar').attr('src', base_url+'static/images/content/WAoff.png');
		$('#imgWakuWiku').attr('src', base_url+'static/images/content/WWoff.png');
		$('#imgWakuKatering').attr('src', base_url+'static/images/content/WKon.png');
	});
	
	$('#btnTakeAway').click(function(){
		$('#imgTakeAway').attr('src', base_url+'static/images/icon/takeawayon.png');
		$('#imgDeliv').attr('src', base_url+'static/images/icon/delivoff.png');
	});
	
	$('#btnDeliv').click(function(){
		$('#imgTakeAway').attr('src', base_url+'static/images/icon/takeawayoff.png');
		$('#imgDeliv').attr('src', base_url+'static/images/icon/delivon.png');
	});
});

function showitemdetail(id){
	$("#"+id).toggle();
	$("#"+id).focus();
}

function showarea(){
	 $('#cityModal').modal('toggle');
	 $('#areaModal').modal('show');

}

function select_area(lokasi){
	 $('#areaModal').modal('toggle');
	 $('#location').val(lokasi);
}

function select_city(lokasi,id){
	 $('#cityModal').modal('toggle');
	 $('#location').val(lokasi);
	 $('#idkota').val(id)
}

function showFrate(id){
	var act = $('#imgFrate').attr('data');
	if(act === "show")
	{
		$('#imgFrate').attr('src', base_url+'static/images/icon/toggle2.png');
		$('#imgFrate').attr('data','hide');
		$('#merchantRate_'+id).removeClass("hide");
	}else{
		$('#imgFrate').attr('src', base_url+'static/images/icon/toggle1.png');
		$('#imgFrate').attr('data','show');
		$('#merchantRate_'+id).addClass("hide");
	}	
}

function showForder(id){
	var act = $('#imgForder').attr('data');
	if(act === "show")
	{
		$('#imgForder').attr('src', base_url+'static/images/icon/toggle2.png');
		$('#imgForder').attr('data','hide');
		$('#contentOrder_'+id).removeClass("hide");
		$('#tombolToggle').removeClass("btn-toggle");
		$('#tombolToggle').addClass("btn-toggle2");
	}else{
		$('#imgForder').attr('src', base_url+'static/images/icon/togglewhite.png');
		$('#imgForder').attr('data','show');
		$('#contentOrder_'+id).addClass("hide");
		$('#tombolToggle').removeClass("btn-toggle2");
		$('#tombolToggle').addClass("btn-toggle");
	}	
}

function searchCity()
{
	var query = $('#city').val().toLowerCase();
	
	$('.list-group-item .cityTeks').each(function(){
		 var $this = $(this);
		 //alert($this.text().toLowerCase().indexOf(query));
		 if($this.text().toLowerCase().indexOf(query) === -1)
			 $this.closest('.list-group-item').fadeOut();
		 else 
		 $this.closest('.list-group-item').fadeIn();
	});
}