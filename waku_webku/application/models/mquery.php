<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class mquery extends CI_Model {

    function __construct() {
        parent::__construct();
    }

	function total_hutang($supplier)
	{
		$sql = "select sum(a.SisaPemb) as total from dt_hutang a LEFT JOIN dt_pembelian b ON a.kdPembelian = b.kdPembelian where b.kdSuplier = '".$supplier."' AND a.status = 'B' GROUP BY b.kdSuplier";
		return $this->db->query($sql)->result();
	}
	
	function data_hutang()
	{
		$sql = "select NamaSupplier,sum(a.SisaPemb) as total from dt_hutang a LEFT JOIN dt_pembelian b ON a.kdPembelian = b.kdPembelian LEFT JOIN dt_supplier c ON b.kdSuplier = c.kdSupplier where a.status = 'B' GROUP BY b.kdSuplier";
		return $this->db->query($sql)->result();
	}
	
	function data_piutang()
	{
		$sql = "select namaPelanggan, sum(a.SisaPemb) as total from dt_piutang a LEFT JOIN dt_penjualan b ON a.kdPenjualan = b.kdPenjualan LEFT JOIN dt_pelanggan c ON b.kdPelanggan = c.kdPelanggan where a.status = 'B' GROUP BY b.kdPelanggan";
		return $this->db->query($sql)->result();
	}
	
	function total_piutang($pelanggan)
	{
		$sql = "select sum(a.SisaPemb) as total from dt_piutang a LEFT JOIN dt_penjualan b ON a.kdPenjualan = b.kdPenjualan where b.kdPelanggan = '".$pelanggan."' AND a.status = 'B' GROUP BY b.kdPelanggan";
		return $this->db->query($sql)->result();
	}
	
	function laporan_kas($awal,$akhir)
	{
		$sql = "select * from dt_aruskas where tanggal >= '".$awal."' and tanggal <= '".$akhir."' order by id_aruskas asc";
		return $this->db->query($sql)->result();
	}
	
	function kas_awal($awal)
	{
		$sql = "select * from dt_aruskas where tanggal < '".$awal."' order by id_aruskas desc limit 1";
		return $this->db->query($sql)->result();
	}
	
	function rekap_pembelian($awal,$akhir)
	{
		$sql = "select * from dt_pembelian a LEFT JOIN dt_supplier b ON a.kdSuplier = b.kdSupplier where TglPembelian >= '".$awal." 00:00:00' and TglPembelian <= '".$akhir." 23:59:59' order by TglPembelian asc";
		return $this->db->query($sql)->result();
	}
	
	function rekap_penjualan($awal,$akhir)
	{
		$sql = "select * from dt_penjualan a LEFT JOIN dt_pelanggan b ON a.kdPelanggan = b.kdPelanggan where TglPenjualan >= '".$awal." 00:00:00' and TglPenjualan <= '".$akhir." 23:59:59' order by TglPenjualan asc";
		return $this->db->query($sql)->result();
	}
	
	function daftar_tagihan($bulan,$tahun)
	{
		$sql = "select * from dt_tagihan a LEFT JOIN dt_penjualan b ON a.kdPenjualan = b.kdPenjualan LEFT JOIN dt_pelanggan c ON b.kdPelanggan = c.kdPelanggan WHERE a.bulan = '".$bulan."' AND a.tahun = '".$tahun."' AND a.status = 'B' ORDER BY c.NamaPelanggan ASC";
		return $this->db->query($sql)->result();
	}
	
	function hitungOmset($tgl1,$tgl2)
	{
		$sql = "SELECT count(*) as trx, SUM(`TotalHarga`) as omset, SUM(`SisaPemb`) as sisa FROM `dt_penjualan` WHERE `TglPenjualan` >= '".$tgl1."' AND `TglPenjualan` <= '".$tgl2."'";
		return $this->db->query($sql)->result();
	}
	
	function omsetByPelanggan($tgl1,$tgl2)
	{
		$sql = "SELECT count(*) as trx, b.tipePelanggan FROM `dt_penjualan` a LEFT JOIN dt_pelanggan b ON a.kdPelanggan = b.kdPelanggan WHERE `TglPenjualan` >= '".$tgl1."' AND `TglPenjualan` <= '".$tgl2."' GROUP BY b.tipePelanggan";
		$data = $this->db->query($sql)->result();
		foreach($data as $d):
			$result[$d->tipePelanggan] = $d->trx;
		endforeach;
		return $result;
	}
	
	function omsetByTipe($tgl1,$tgl2)
	{
		$sql = "SELECT jPembayaran, count(*) as total FROM `dt_penjualan` WHERE `TglPenjualan` >= '".$tgl1."' AND `TglPenjualan` <= '".$tgl2."' GROUP BY jPembayaran";
		$data = $this->db->query($sql)->result();
		foreach($data as $d):
			$result[$d->jPembayaran] = $d->total;
		endforeach;
		return $result;
	}
	
	function omsetByProduk($tgl1,$tgl2)
	{
		$sql = "SELECT namaBarang,SUM(Qty) as total FROM `dt_penjualan` a RIGHT JOIN dt_penjualandetail b ON a.kdPenjualan = b.kdPenjualan LEFT JOIN mstr_barang c ON b.kdBarang = c.kdBarang WHERE `TglPenjualan` >= '".$tgl1."' AND `TglPenjualan` <= '".$tgl2."' GROUP BY b.kdBarang";
		return $this->db->query($sql)->result();
	}
	
}
