<?php 
$faq = '';
$terms = '';
$visi = '';
if($list_about->status == 1){
	foreach($list_about->info as $li){
		if($li->id_info == 9){
			$faq = htmlspecialchars($li->value_setting);
		}
		if($li->id_info == 1){
			$terms = htmlspecialchars($li->value_setting);
		}
		if($li->id_info == 18){
			$visi = htmlspecialchars($li->value_setting);
		}
	}
}

$b1= "default"; $b2 = "default"; $b3="default";
switch($tipe):
	case "faq":	 $b1 = "success"; $k1 = "active"; break;
	case "visi": $b2 = "success"; $k2 = "active"; break;
	case "term": $b3 = "success"; $k3 = "active"; break;
	default : $b1 = "success"; $k1 = "active"; break;
	break;
endswitch;
?>
	<section class="wk-container-1 nobg" style="background-color:white">
      <div class="container-fluid nopadding">
        
        <div class="col-lg-4 col-xs-12 side_about">
        	<section>    
                <div class="col-lg-offset-1 col-lg-10">
                    
                    <div class="text-center">
                        <h4><span class="bhs_ingg">ABOUT</span><span class="bhs_indo">TENTANG</span></h4>
                        <br />
                        <p>How to have a Fun Shopping <Br />in Wakuliner</p>
                    </div>
                    
                    <center><img class="konten" src="<?=static_file()?>images/content/aboutimage.png"/></center>
                    
                    <div class="btn_about">
                        <a class="btn btn-<?=$b1?> btn-block" id="tbl_faq" href="#faq" aria-controls="home" role="tab" data-toggle="tab">
                            FAQ <img src="<?=static_file()?>images/icon/aboutgo.png" id="imgtbl_faq" class="icon pull-right"/>
                        </a>
                        <a class="btn btn-<?=$b2?> btn-block" id="tbl_visi" href="#visimisi" aria-controls="profile" role="tab" data-toggle="tab">
                            Visi & Misi <img src="<?=static_file()?>images/icon/aboutgo.png" id="imgtbl_visi" class="icon pull-right"/>
                        </a>
                        <a class="btn btn-<?=$b3?> btn-block" id="tbl_term" href="#terms" aria-controls="messages" role="tab" data-toggle="tab">
                            <span class="bhs_ingg">Terms & Condition</span><span class="bhs_indo">Syarat dan Ketentuan</span> <img src="<?=static_file()?>images/icon/aboutgo.png" id="imgtbl_term" class="icon pull-right"/>
                        </a>
                    </div>
                    
                </div>
            </section>
        </div>
        
        <div class="col-lg-8 col-xs-12" style="padding-left:30px; padding-right:10px;">
        	
            <div class="tab-content content_about" style="padding-left: 10px;">
                <div role="tabpanel" class="tab-pane <?=$k1?>" id="faq">
                    <?php echo nl2br($faq);?>
                    
                </div>
                <div role="tabpanel" class="tab-pane <?=$k2?>" id="visimisi"><?php echo nl2br($visi);?></div>
                <div role="tabpanel" class="tab-pane <?=$k3?>" id="terms"><?php echo nl2br($terms);?></div>
            </div>
            
            <br />
            <br />
            <p align="center"><a href="#"><i class="glyphicon glyphicon-arrow-up"></i> Kembali ke atas</a></p>
            <br />
        </div>
            
     </div>
   </section>
   
   
        <script>
			$('.btn_about a').click(function(){
				$('.icon').attr("src", "<?=static_file()?>images/icon/aboutgo.png");;
				$('.btn_about a').removeClass("btn-success");
				$('.btn_about a').addClass("btn-default");
				$('#'+this.id).addClass("btn-success");
				$('#img'+this.id).attr("src", "<?=static_file()?>images/icon/arrownextwhite.png");
			});
		</script>