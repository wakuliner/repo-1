<?php 
$undian = '';
$point_register = 0;
$point_order_complete = 0;
$total_no_undian_customer = 0;
$nama_undian = '';
if($data_rewards->status == 1){
	$undian = $data_rewards->undian;
	$point_register = $undian->point_register;
	$point_order_complete = $undian->point_order_complete;
	$nama_undian = $undian->nama_undian;
	$total_no_undian_customer = $undian->total_no_undian_customer > 0 ? $undian->total_no_undian_customer : '0';
}

$total_points = 0;
$my_rewards = 0;
$redeemed = 0;
if($data_reward2->status == 1){
	foreach($data_reward2->info_rewards as $ir){
		$total_points = $ir->total_points;
		$my_rewards = $ir->rewards > 0 ? number_format($ir->rewards,0,'.','.') : '0';
		$redeemed = $ir->redeemed;
	}
}
?>
	<section class="wk-container-1 nobg" style="background-color: #ffffff;">
      <div class="container wk_style">
            
            <h3 class="page-header text-center" style="border-bottom: 0px;">WAKU-POINTS</h3>
             <ul class="nav nav-tabs nav-pills nav-justified nav_order" style="border-bottom:1px solid #259b00;">
              <li role="tab" class="active" data-toggle="tab" data-target="#rewards"><a href="#"><span class="bhs_ingg">REWARDS</span><span class="bhs_indo">HADIAH</span></a></li>
              <li role="tab" data-toggle="tab" data-target="#referal"><a href="#">REFERAL</a></li>
            </ul>
            
            <div class="tab-content content_about">
            	
                 <div role="tabpanel" class="tab-pane active" id="rewards">
				 <div class="col-lg-6 col-lg-offset-3">
				 
                 	
                    <div class="text-center clearfix">
                        <h4><span class="bhs_ingg">Waku point coming soon!</span><span class="bhs_indo">Nantikan segera Waku poin!</span></h4>
                        <p class="text-muted"><b><span class="bhs_ingg">10 point for every Rp 10.000 spent</span><span class="bhs_indo">10 Poin untuk setiap Rp 10.000 yang dibelanjakan</span></b></p>
                    	<p class="text-muted"><small>Wakuliner rewards you each time you make an order.Earn points to spend on your future orders. </small></p>
                    </div>
                    
                    <div class="col-lg-12 col-xs-12">
                    	
                        <div class="alert alert-warning text-center wk_reward1">
                        	<h4><span class="bhs_ingg">wakuliner sweepstakes</span><span class="bhs_indo">undian berhadiah Wakuliner</span></h4>
                            <h5>Until 31 Maret 2017</h5>
                            <p>You've received <?php echo $point_register;?> sweeptakes number when you registered with us. You can earn <?php echo $point_order_complete;?> number for every completed order in WAKU-ANTAR,with no limit. For the complete prizes and rules, please visit www.wakuliner.com. 
                        </div>
                    	  
                    
                    
                    	
                        <div class="panel panel-warning wk_reward2">
                        	<div class="panel-body">
                            	<span class="bhs_ingg">You have</span><span class="bhs_indo">Anda Memiliki</span> <span class="pull-right wk_black"><?php echo $total_no_undian_customer;?> <b class="wk_style">sweepstakes numbers</b></span>
                        	</div>
                        </div>
                        
                        <div class="panel panel-success wk_reward3">
                        	<div class="panel-body">
                            	<span class="bhs_ingg">You have</span><span class="bhs_indo">Anda Memiliki</span> <span class="pull-right"><?php echo $total_points;?> <b class="wk_black"><span class="bhs_ingg">Points</span><span class="bhs_indo">Poin</span></b></span>
                        	</div>
                        </div>
                        
                        <div class="panel panel-success wk_reward3">
                        	<div class="panel-body text-success">
                            	<span class="bhs_ingg">Your rewards</span><span class="bhs_indo">Hadiah anda</span> <span class="pull-right"><b>Rp. <?php echo $my_rewards;?> </b></span><br />
                        		<small class="text-muted">You can use this during checkout of your next order</small>
                            </div>
                        </div>
                        
                        <div class="panel panel-success wk_reward3">
                        	<div class="panel-body text-success">
                            	<span class="bhs_ingg">You have redeemed</span><span class="bhs_indo">Anda telah menukarkan</span> <span class="pull-right"><?php echo $redeemed;?> <b class="wk_Style"><span class="bhs_ingg">Points</span><span class="bhs_indo">Poin</span></b></span>
                        	</div>
                        </div>
                        </div>
                    </div>
                 </div>
                 
                 <div role="tabpanel" class="tab-pane" id="referal">
                 	
                    	<div class="col-lg-999 col-xs-999">
                        	<!--
                             <p align="center">
                            	Share your referal code <?php echo $data_referal->referral_code;?> to your friends and they will receive Rp. <?php echo $data_referal->nilai_reward;?> reward. When they order, you will receive Rp. <?php echo $data_referal->nilai_reward;?> reward too!.
                            </p>
							-->
							<p align="center">
								<span class="bhs_ingg">Soon you can share your referral code <?php echo $data_referal->referral_code;?> to your friends. They will earn Waku-Points after entering your code, and you will also earn Waku-Points after they make an order!</span>
								<span class="bhs_indo">Sebentar lagi Anda dapat membagikan kode referral anda <?php echo $data_referal->referral_code;?> ke teman anda dan mereka akan menerima Waku-Points. Saat mereka memesan, Anda akan menerima Waku-Points juga!</span>
							</p>
                            <br /><center><img src="<?=static_file()?>images/content/REferrslimage.png"/></center><br />
                            <p align="center">
                           		<span class="bhs_ingg">You can view your reward point in the Waku-Points Rewards section</span>
                                <span class="bhs_indo">Anda dapat melihat poin reward Anda di bagian Waku-Points Rewards</span>
                            </p>
                            
                        </div>
                        <!--
                        <div class="col-lg-6 col-xs-12">
                        	
                            <div class="panel panel-success">
                                <div class="panel-body bg-success text-center">
                                    <h4>Welcome to WAKULINER</h4>
                                    <p class="text-muted">Enter your friends referal code below and receive Rp 10.000 reward</p>
                                    <div class="input-group">
                                      <input type="text" class="form-control" id="reward_code" name="reward_code" placeholder="Referal Code">
                                      <span class="input-group-btn">
                                        <button class="btn btn-success" id="btn_rewards" type="button"><i class="glyphicon glyphicon-ok"></i></button>
                                      </span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel panel-success">
                                <div class="panel-body bg-success text-center">
                                    <h4>Yeah, you have received Rp. <?php echo $data_referal->nilai_reward;?> reward.</h4>
                                    <p class="text-muted">You can view your reward in the Rewards section</p>
                                    <button class="btn btn-success" type="button">OK</button>
                                </div>
                            </div>
                            
                        </div>
                    	-->
                 </div>
                
            </div>
            
     </div>
   </section>
<script>
$('#btn_rewardsku').click(function(){
	var referral_code = $('#reward_code').val();
	var url = '<?php echo site_url('reward/submit_referal');?>';
	$.ajax({
		data : {referral_code : referral_code},
		url : url,
		type : "POST",
		success:function(response){
			alert(response);
			location.reload();
		}
	});	
});
</script>