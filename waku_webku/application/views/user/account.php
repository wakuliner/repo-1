<?php
$first_name = '';
$email = '';
$telp = ''; 
$handphone = '';
if($profile->status == 1){
	foreach ($profile->info as $i){
		$first_name = $i->first_name;
		$email = $i->email;
		$telp = $i->telp; 
		$handphone = $i->handphone;
	}
}


$lang_user = $this->session->userdata('lang_user');
$place_username = 'Nama';
$place_telp = 'Nomer Telepon';
$place_hp = 'Nomor Handphone';
if($lang_user == 'bhs_ingg'){
	$place_username = "Username";
	$place_telp = 'Phone Number';
	$place_hp = 'Mobile number';
}
?>
	<section class="wk-container-1 nobg" style="background-color:white;">
      <div class="container wk_style">
            
            <h3 class="page-header text-center"><span class="bhs_ingg">ACCOUNT</span><span class="bhs_indo">AKUN</span></h3>
            <div class="col-lg-4 col-xs-12">
                
                <div class="">
                    <div class="panel-body">
                        <h4><span class="bhs_ingg">Your Profile</span><span class="bhs_indo">Profile Anda</span></h4>
                       <form id="frm_account">
                       	<div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon-profileaccount"><img src="<?=static_file()?>images/icon/profile.png" /></span>
                              <input type="text" class="form-control" name="fullname" placeholder="<?php echo $place_username;?>" value="<?php echo $first_name;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon-profileaccount"><img src="<?=static_file()?>images/icon/phone.png" /></span>
                              <input type="text" class="form-control" name="telp" placeholder="<?php echo $place_telp;?>" value="<?php echo $telp;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon-profileaccount"><img src="<?=static_file()?>images/icon/mobile.png" /></span>
                              <input type="text" class="form-control" name="hp" placeholder="<?php echo $place_hp;?>" value="<?php echo $handphone;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon-profileaccount"><img src="<?=static_file()?>images/icon/mail.png" /></span>
                              <input type="text" class="form-control" name="email_edit" id="email_edit" placeholder="Email" value="<?php echo $email;?>">
                            </div>
                        </div>
                        <div class="form-group">
                        	<button type="button" id="btn_edit" class="btn btn-success btn-block"><span class="bhs_ingg">SAVE</span><span class="bhs_indo">SIMPAN</span></button>
                        </div>
                        </form>
                    </div>
                </div>
                
            </div>
            
            <div class="col-lg-4 col-xs-12">
            	
                <h4><span class="bhs_ingg">Your Saved Address</span><span class="bhs_indo">Alamat anda disimpan</span> <button type="button"  class="btn btn-success pull-right" data-toggle="modal" data-target="#addressModal"><span class="bhs_ingg">ADD NEW</span><span class="bhs_indo">TAMBAH BARU</span></button></h4>
                <BR />
                <?php 
					foreach($profile->address as $a){ ?>
						 <div class="panel" id="del_<?php echo $a->id_address;?>">
                            <div class="panel-body">
                                <h5>
                                    <a role="button" data-toggle="collapse" href="#contentAlamat_<?php echo $a->id_address;?>" aria-expanded="true" aria-controls="contentAlamat1"><b><?php echo $a->area_name;?></b> <span class="caret"></span> </a> 
                                    <a href="#" class="text-danger btn_edit_address" id="editÞ<?php echo $a->id_address.'Þ'.$a->city.'Þ'.$a->id_area.'Þ'.$a->address;?>" >
                                        <img src="<?=static_file()?>images/icon/edit1.png" width="16" />
                                    </a>
                                    <a href="#" class="pull-right text-danger btn_del" id="id_<?php echo $a->id_address;?>" >
                                        <img src="<?=static_file()?>images/icon/delette.png" width="18" />
                                    </a>
                                </h5>
                                <div class="collapse in" id="contentAlamat_<?php echo $a->id_address;?>">
                                    <hr />
                                    <p><i class="glyphicon glyphicon-user"></i> <?php echo $a->first_name;?></p>
                                    <p><i class="glyphicon glyphicon-map-marker"></i> <?php echo $a->address.', '.$a->city_name;?></p>
                                    <p><i class="glyphicon glyphicon-phone"></i> <?php echo $a->handphone;?></p>
                                </div>
                            </div>
                        </div>
				<?php	}
				?>

            </div>
            
            <div class="col-lg-4 col-xs-12" style="overflow-y: scroll;height: 500px;">
            	
                <h4><span class="bhs_ingg">Your Rating</span><span class="bhs_indo">Nilai Anda</span> <small class="pull-right text-muted"><?php echo count($rating->detail_rating);?> <span class="bhs_ingg">Sellers</span><span class="bhs_indo">Penjual</span></small></h4>
                
                <?php 
				foreach($rating->detail_rating as $dr){ ?>
					<div class="panel panel-rateaccount">
                        <div class="panel-body nopadding">
                            <div class="col-lg-6 col-xs-6"><h5><b><?php echo $dr->merchant_name;?></b></h5></div>
                            <div class="col-lg-6 col-xs-6">
                            <?php 
								$_rating = $dr->vote;
								$_rating = $_rating / 10;
								$_rating = (int) $_rating;
								
							?>
                                <select class="rating-css" name="rating" autocomplete="off">
                                  <option value="1" <?php echo $_rating == 1 ? ' selected' : '';?>>1</option>
                                  <option value="2" <?php echo $_rating == 2 ? ' selected' : '';?>>2</option>
                                  <option value="3" <?php echo $_rating == 3 ? ' selected' : '';?>>3</option>
                                  <option value="4" <?php echo $_rating == 4 ? ' selected' : '';?>>4</option>
                                  <option value="5" <?php echo $_rating == 5 ? ' selected' : '';?>>5</option>
                                </select>
                            </div>
                            <div class="col-lg-12 col-xs-12">
                                <p class="text-muted"><?php echo $dr->description;?></p>
                            </div>
                        </div>
                    </div>
				<?php	}
				?>
                
            </div>
     
      </div>
  	</section>
    
    <div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <form id="frm_address">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Add Address</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
            	<select class="form-control" name="id_kota" id="id_kotaa" onchange="changeArea(this.value);">
                  <option>Pilih Kota</option>
                  <?php foreach($city->city as $ct):?>
                  	<option value="<?=$ct->id_kota?>"><?=$ct->nama_kota?></option>
				  <?php endforeach; ?>
                </select>
            </div>
            <input type="hidden" name="id_address" id="id_address" value="0"  />
            <div class="form-group hide" id="selArea">
            </div>
            <div class="form-group">
            	<textarea class="form-control" rows="3" name="address" id="addressku" placeholder="Address"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-inverse pull-left" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> Cancel</button>
            <button type="button" id="save_address" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>
<script>
var lang_user = '<?php echo $lang_user;?>';
var msg_confirm = 'Apakah anda ingin menghapus alamat ini ?';
$('.btn_del').click(function(){
	var val = $(this).get(0).id;
	var dt = val.split('_');
	var id = dt[1];
	var url = '<?php echo site_url('setting/save_address');?>';
	if (confirm(msg_confirm)) {
		$.ajax({
			data : {type_action:3, id_address : id},
			url : url,
			type : "POST",
			success:function(response){
				if(response > 0){
					var msg = 'Berhasil menhapus alamat';
					if(lang_user == 'bhs_ingg'){
						msg = 'Delete user address successful';
					}
					$('#teksNotifModal').html(msg);
					$('#errorNotifModal').modal('show');
					$("#del_"+id).hide();
				}	
			}
		});
	}

});

$('#save_address').click(function(){
    var id_kotaa = $('#id_kotaa').val();
	var id_areaa = $('#id_areaa').val();
	var addressku = $('#addressku').val();
	if(id_kotaa == ''){
		alert('Silahkan pilih kota');
		return false;
	}
	if(id_areaa == ''){
		alert('Silahkan pilih area');
		return false;
	}
	if(addressku == ''){
		alert('Silahkan isi alamat lengkap');
		return false;
	}
	var dt = $('#frm_address').serialize();
	var url = '<?php echo site_url('setting/save_edit_address');?>';
	
	$.ajax({
		data : dt,
		url : url,
		type : "POST",
		success:function(response){
			if(response > 0){
				var msg = 'Berhasil menambahkan alamat';
				if(lang_user == 'bhs_ingg'){
					msg = 'Add account address success';
				}
				alert(msg);
				location.reload();
			}
		}
	});
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

$('#btn_edit').click(function(){
	var email = $('#email_edit').val();
	if(email == ''){
		$('#teksNotifModal').html('Email harus di isi');
		$('#errorNotifModal').modal('show');
		return false;
	}
	if(!(validateEmail(email))){
		$('#teksNotifModal').html('Email tidak valid');
		$('#errorNotifModal').modal('show');
		return false;
	}
	var dt = $('#frm_account').serialize();
	var url = '<?php echo site_url('setting/edit_account');?>';
	$.ajax({
		data:dt,
		type:'POST',
		url : url,
		success:function(response){	
			var obj = JSON.parse(response);		
			msg = obj.message;
			$('#teksNotifModal').html(msg);
			$('#errorNotifModal').modal('show');
			//if(obj.status == 1){
			//	msg += '\n Kami mungkin perlu menghbungi Anda mengenai pesanan dan pembayaran Anda, dan untuk memberikan pelayanan yang terbaik untuk Anda';
		//	}
		//	alert(msg);
			//if(obj.status == 1){
		//		window.location.href = '<?php echo site_url('login');?>';
		//	}
		}
	});
});

$('.btn_edit_address').click(function(){
	var id = $(this).get(0).id;
	var dt = id.split('Þ');
	$('#id_address').val(dt[1]);
	$('#id_kotaa').val(dt[2]);
	changeArea(dt[2], dt[3]);
	$('#addressku').text(dt[4]);
	$('#addressModal').modal('show');
});

function changeArea(val, selected_val)
{
	$.ajax({
		type:'POST',
		data : {id_kota : val, selected_val:selected_val},
		url : '<?php echo site_url('setting/get_area');?>',
		success:function(response){
			
			$('#selArea').removeClass("hide");
			$('#selArea').html(response);
		}
	});
}
</script>