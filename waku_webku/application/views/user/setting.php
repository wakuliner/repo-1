<?php
$lang = 0;
$notif = 0;
foreach($setting->SETTING as $s){
	$lang = $s->language;
	$notif = $s->notification;
}

$lang_user = $this->session->userdata('lang_user');
$place_old = 'Password Lama';
$place_new = 'Password Baru';
$place_confirm = 'Konfirmasi password baru';
if($lang_user == 'bhs_ingg'){
	$place_old = "Old Password";
	$place_new = "New Passsword";
	$place_confirm = "Re enter new Password";
}
?>
	<section class="wk-container-1 nobg">
      <div class="container wk_style" style="margin-top:20px;">
            
            <div class="col-lg-6 col-xs-12">
            	<h4><span class="bhs_ingg">Setting</span><span class="bhs_indo">Pengaturan</span></h4>
                
                <p><b><span class="bhs_ingg">Langguage</span><span class="bhs_indo">Bahasa</span></b></p>
                <table class="table">
                    <tr>
                    	<td><span class="bhs_ingg">English</span><span class="bhs_indo">Inggris</span></td>
                         <td><input type="radio" onclick="send_dt(2,0)" value="2" name="lang" class="pull-right" <?php echo $lang == 2 ? 'checked' : '';?> /></td>
                    </tr>
                    <tr>
                    	<td><span class="bhs_ingg">Indonesian Langguage</span><span class="bhs_indo">Bahasa Indonesia</span></td>
                           <td><input type="radio" onclick="send_dt(1,0)" value="1" name="lang" class="pull-right" <?php echo $lang == 1 ? 'checked' : '';?> /></td>
                    </tr>
                </table>
                
                <p><b><span class="bhs_ingg">Notification</span><span class="bhs_indo">Pemberitahuan</span></b></p>
                <table class="table">
                    <tr>
                    	<td><span class="bhs_ingg">News from wakuliner team</span><span class="bhs_indo">Berita dari tim wakuliner</span></td>
                         <td><input type="checkbox" <?php echo $notif == 1 ? 'checked' : '';?> name="notif" id="notif" class="pull-right" /></td>
                    </tr>
                </table>
            </div>
            
            <div class="col-lg-6 col-xs-12">
            	
                <div class="panel panel-success">
                    <div class="panel-body bg-success text-center">
                        <h4><span class="bhs_ingg">Change Password</span><span class="bhs_indo">Ubah Password</span></h4>
                       <form>
                        <div class="form-group">
                          <input type="password" class="form-control" name="old_pass" id="old_pass" placeholder="<?php echo $place_old;?>">
                        </div>
                        <div class="form-group">
                          <input type="password" class="form-control" name="new_pass" id="new_pass" placeholder="<?php echo $place_new;?>">
                        </div>
                        <div class="form-group">
                          <input type="password" class="form-control" name="conf_pass" id="conf_pass" placeholder="<?php echo $place_confirm;?>">
                        </div>
                        <div class="form-group">
                        	<button type="button" id="btn_submit" class="btn btn-success btn-block">Submit</button>
                        </div>
                        </form>
                    </div>
                </div>
                
            </div>
     
      </div>
  	</section>
<script>
var lang_user = '<?php echo $lang_user;?>';
function send_dt(lang, notif){
	var url = '<?php echo site_url('setting/send_setting');?>';	
	if(notif <= 0){
		if ($('#notif').is(":checked")){
			var notif = 1;
		}else{
			var notif = 2;
		}
	}
	$.ajax({
		data : {lang : lang, notif:notif},
		url : url,
		type : "POST",
		success:function(response){
			location.reload();				
		}
	});	
}

$('#notif').change(function(){
	var val = 2;
	var lang = $('input[name="lang"]:checked').val();
	if($(this).prop('checked')){
		var val = 1;
	}
	send_dt(lang, val);
});

$('#btn_submit').click(function(){
	var old_pass = $('#old_pass').val();
	var new_pass = $('#new_pass').val();
	var conf_pass = $('#conf_pass').val();
	var msg_confirm2 = 'Confirm password tidak sesuai dengan new password';
	var msg_oldFailed = 'Password lama tidak sesuai';
	if(lang_user == 'bhs_ingg'){
		msg_confirm2 = "Confirm password doesn't match with new password";
		msg_oldFailed = 'Old Password Failed';
	}
	if(old_pass == ''){
		$('#teksNotifModal').html('Old password harus diisi');
		$('#errorNotifModal').modal('show');
		return false;
	}
	if(new_pass == ''){
		$('#teksNotifModal').html('New password harus diisi');
		$('#errorNotifModal').modal('show');
		return false;
	}
	if(conf_pass == ''){
		$('#teksNotifModal').html('Confirm password harus diisi');
		$('#errorNotifModal').modal('show');
		return false;
	}
	if(new_pass != conf_pass){
		$('#teksNotifModal').html(msg_confirm2);
		$('#errorNotifModal').modal('show');
		return false;
	}
	var url = '<?php echo site_url('setting/change_pass');?>';
	$.ajax({
		data : {old : old_pass, new_pass:new_pass},
		url : url,
		type : "POST",
		success:function(response){
			if(response == 1){
				$('#teksNotifModal').html('Change Password Successful');
				$('#errorNotifModal').modal('show');
			}else{
				$('#teksNotifModal').html(msg_oldFailed);
				$('#errorNotifModal').modal('show');
			}
		}
	});	
});
</script>