    <style type="text/css">
	#wakukatering{
		height:auto;
		min-height:0;
	}
	#wakukatering h4{
		margin-top:0;
	}
	ul.nav-center li{
		white-space:nowrap;
	}
	@media screen and (min-width:504px)
	{
		ul.nav-center{
			max-width: 464px;
			margin: auto;
		}
	}
	@media screen and (max-width:503px)
	{
		ul.nav-center{
			width: 302px;
			margin: auto;
		}
	}
	.wk_content_desc p{
		text-align:center;
		max-width:800px;
		margin-left:auto;
		margin-right:auto;
	}
	@media screen and (min-width:640px){
		.image-600x164{
			display:block;
		}
		.image-480x164{
			display:none;
		}
		.image-180x500{
			display:none;
		}
	}
	@media screen and (min-width:500px) and (max-width:639px){
		.image-600x164{
			display:none;
		}
		.image-480x164{
			display:block;
		}
		.image-180x500{
			display:none;
		}
	}
	@media screen and (max-width:499px){
		.image-600x164{
			display:none;
		}
		.image-480x164{
			display:none;
		}
		.image-180x500{
			display:block;
		}
	}
	
	</style>
    
    <section class="wk-container-1 bg-full2">
        <div class="container wk-content-1" style="background-color:white">
            <div class="col-lg-7 col-centered">
            	
            </div>
        </div>
    </section>
    <section style="background-color:white">
    
    <h3 align="center" class="wk_style wk_login_tagline bhs_ingg" style="padding-top:35px; margin-top:-28px;">The First Culinary Marketplace for all kinds of culinary in Indonesia</h3>
    
    <h3 align="center" class="wk_style wk_login_tagline bhs_indo" style="padding-top:35px; margin-top:-28px;">Wadah kuliner untuk semua jenis kuliner, pertama di Indonesia</h3>
        <div class="container-fluid nopadding wk_style">
			
            <div class="clearfix">
                <div class="">
                <ul class="nav nav-center">
                  <li role="tab" class="active" data-toggle="tab" data-target="#wakuantar"><a href="#" id="tabWakuAntar"><img id="imgWakuAntar" src="<?=static_file()?>images/content/WAon.png" /></a></li>
                  <li role="tab" data-toggle="tab" data-target="#wakuwiku"><a href="#" id="tabWakuWiku"><img id="imgWakuWiku" src="<?=static_file()?>images/content/WWoff.png" /></a></li>
                  <li role="tab" data-toggle="tab" data-target="#wakukatering"><a href="#" id="tabWakuKatering"><img id="imgWakuKatering" src="<?=static_file()?>images/content/WKoff.png" /></a></li>
                </ul>
                </div>
			</div>
			
			<div class="tab-content content_tab">
                <div role="tabpanel" class="tab-pane active wk_content_desc" id="wakuantar">
                	<p align="center" class="text-muted">
                    <span class="bhs_ingg">Waku-Antar provides the fastest order-delivery service in Indonesia, with 3.500 merchants/sellers in 60 cities. Orders will be delivered directly by merchants to your location or your loved ones'</span><span class="bhs_indo">Waku-Antar memberikan jasa pesan-antar tercepat di indonesia, dengan 3.500 merhants di 60 kota. Pesan makanan langsung diantar oleh penjual ke lokasi anda atau orang kesayangan anda</span>
                    </p>
                    <br /><h5 align="center"><b><span class="bhs_ingg">Very easy to use!</span><span class="bhs_indo">Caranya mudah sekali!</span></b></h5><br />
                    <center>
                    	<img src="<?=static_file()?>images/content/pesan-600x164.png" class="image-600x164" />
                    	<img src="<?=static_file()?>images/content/pesan-480x164.png" class="image-480x164" />
                    	<img src="<?=static_file()?>images/content/pesan-180x500.png" class="image-180x500" />
                    </center>
                	<div class="text-center" style="margin-top:30px;">
                        <div class="btn-group" style="white-space:nowrap">
                          <a href="<?=base_url()?>wakuantar" class="btn btn-warning btn-orange btn-lg"><span class="bhs_ingg">TRY WAKU-ANTAR NOW</span><span class="bhs_indo">COBA WAKU-ANTAR SEKARANG</span></a>
                          <a href="<?=base_url()?>wakuantar">
                            <img src="<?=static_file()?>images/icon/arrow-gobutton.png" height="47px;" style="margin:-1px 0 0 -2px;" />
                          </a>
                        </div>
                   </div>
                </div>
                <div role="tabpanel" class="tab-pane wk_content_desc" id="wakuwiku">
                	<p align="center" class="text-muted">
                    <span class="bhs_ingg">Find popular restaurants & legendary culinary destinations in your favorite cities. Get the restaurants' info, menu, food & location pictures, and direction to get there.</span>
                   <span class="bhs_indo">Temukan restoran & tempat makan populer yang legendaris di kota-kota kesayangan Anda. Dapatkan info restoran, menu, foto-foto menu dan lokasi, & petunjuk arah kesana.</span>
                    
                    </p>
                   <br /><h5 align="center"><b><span class="bhs-ingg">How easy it is!!</span><span class="bhs_indo">Caranya mudah sekali!!</span></b></h5><br />
                    <center>
                    	<img src="<?=static_file()?>images/content/antar-600x164.png" class="image-600x164" />
                    	<img src="<?=static_file()?>images/content/antar-480x164.png" class="image-480x164" />
                    	<img src="<?=static_file()?>images/content/antar-180x500.png" class="image-180x500" />
                    </center>
                    <div class="text-center" style="margin-top:30px;">
                    	<div class="btn-group" style="white-space:nowrap;">
                          <a href="<?=base_url()?>wakuwiku" class="btn btn-warning btn-orange btn-lg"><span class="bhs_ingg">TRY WAKU-WIKU NOW</span><span class="bhs_indo">COBA WAKU-WIKU SEKARANG</span></a>
                          <a href="<?=base_url()?>wakuwiku">
                            <img src="<?=static_file()?>images/icon/arrow-gobutton.png" height="47px;" style="margin:-1px 0 0 -2px;" />
                          </a>
                        </div>
                   </div>
                </div>
                <div role="tabpanel" class="tab-pane wk_content_desc" id="wakukatering">
                	<h4 style="color:#F00;" align="center">Coming Soon !</h4>
                    <p align="center" class="text-muted">
<span class="bhs_ingg">Soon you can enjoy our catering service (event or daily). Please wait for it.</span><span class="bhs_indo">Anda akan segera dapat menikmati pelayanan katering (event atau harian). Tunggu ya.</span></p>
                </div>
            </div>
        </div>
        
    </section>
    
    <section style="background-color:white;">
    	
        <div class="container-fluid nopadding">
        	
            <div class="box-comment">
            	<div class="box-comment-inner">
                    <img src="<?=static_file()?>images/icon/wakutestimoni.png"/>
                    <p align="center" class="bhs_indo">APA KATA MEREKA TENTANG WAKULINER</p>
                	<p align="center" class="bhs_ingg">WHAT THEY KNOW ABOUT US</p>
                </div>
            </div>
            
            <div class="box_coment_content">
            	
            	<div class="col-lg-6">
                	<div class="well clearfix">
                    	
                        <div class="col-lg-3 col-xs-3">
                        	<img src="<?=static_file()?>images/testimonial/yuanita.jpg" class="img-responsive img-circle"/>
                        </div>
                        <div class="col-lg-9 col-xs-9">
                        	<p><b>Yuanita Christiani</b></p>
                            <p>Sahabatku rekomendasiin aku download aja app kuliner baru. Aseelikk!!! ini app nya keren buanget!!! Solusi kuliner... pesen makanan, cari wisata kuliner legendaris, juga bakalan bisa pesen catering :D</p>
                        </div>
                        
                    </div>
                </div>
                
                <div class="col-lg-6">
                	<div class="well clearfix">
                    	
                        <div class="col-lg-3  col-xs-3">
                        	<img src="<?=static_file()?>images/testimonial/ferry_salim.jpg" class="img-responsive img-circle"/>
                        </div>
                        <div class="col-lg-9 col-xs-9">
                        	<p><b>Ferry Salim</b></p>
                            <p>Ada app kuliner baru nih! WAKULINER! Solusi kuliner! bisa pesen makanan, cari wisata kuliner legendaris, juga bakalan bisa pesen catering. Cobain download guys, FREE!</p>
                        </div>
                        
                    </div>
                </div>
                
                <div class="col-lg-6">
                	<div class="well clearfix">
                    	
                        <div class="col-lg-3  col-xs-3">
                        	<img src="<?=static_file()?>images/testimonial/alena.jpg" class="img-responsive img-circle"/>
                        </div>
                        <div class="col-lg-9 col-xs-9">
						    <p><b>Alena</b></p>
                            <p>Sebagai pencinta kuliner sejak kecil, Wakuliner apps ini aku banget! Segala tentang kuliner ada: guide wisata kuliner LEGENDARIS nya lengkap. Lalu pesen makanan panas, frozen, dari luar pulau juga bisa. Menanti fitur catering yang coming soon.</p>
                        </div>
                        
                    </div>
                </div>
                
                <div class="col-lg-6">
                	<div class="well clearfix">
                    	
                        <div class="col-lg-3 col-xs-3">
                        	<img src="<?=static_file()?>images/testimonial/evelin.jpg" class="img-responsive img-circle"/>
                        </div>
                        <div class="col-lg-9 col-xs-9">
                        	<p><b>Evelyn</b></p>
                            <p>Aplikasi Wakuliner memudahkan saya untuk memesan makanan dengan biaya yang sangat ekonomis. Apalagi saya suka melakukan wisata kuliner, dengan fitur Waku Wiku saya dpat mengetahui tmpat makan terupdate. Wakuliner merupakan aplikasi kuliner all in 1</p>						
                        </div>
                        
                    </div>
                </div>
                
                 <div class="col-lg-6 col-lg-offset-3">
                	<div class="well clearfix">
                    	
                        <div class="col-lg-3 col-xs-3">
                        	<img src="<?=static_file()?>images/testimonial/hangesti.jpg" class="img-responsive img-circle"/>
                        </div>
                        <div class="col-lg-9 col-xs-9">
						    <p><b>Hangesti</b></p>
                            <p>Saya pesan trus di Wakuliner, dari pesen sampe pengiriman praktis banget ya. Wahhh kalau begini ga punya pulsa pun bisa pesen makanan nihh.</p>
                        </div>
                        
                    </div>
                </div>
                
            </div>
                        
        </div>
        
    </section>
    
    
