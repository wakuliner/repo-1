<?php
$lang_user = $this->session->userdata('lang_user');
$place_holder = 'Nama resto atau nama makanan';

if($lang_user == 'bhs_ingg'){
	$place_holder = "Resto's name or food name";

}
?>
<style type="text/css">
@media screen and (min-width:781px)
{
	#filter-container{
		display:none;
	}
}
</style>

	<section class="wk-container-1 nobg">
      <div class="container-fluid nopadding">
        
        <div class="col-lg-3 col-xs-12 side_about mobile-hide filter-tags">
			
            <div class="page-header text-center">
              <h4>FILTER</h4>
            </div>
            
            <div class="text-left">
                 <b><span class="bhs_indo">Populer</span><span class="bhs_ingg">Popular</span></b> <a href="#" class="pull-right text-success" onclick="clear_popular()"><i class="glyphicon glyphicon-remove"></i> <span class="bhs_ingg"> Clear</span><span class="bhs_indo"> Hapus</span></a>
            </div>
            <ul class="list-group" id="chk_popular">
            <?php 
				foreach($filter_merchant->tag as $fm){
					if($fm->tipe == 1){ ?>
						<li class="list-group-item wk_filter">
                        	<?php echo $fm->tags_name;?> <span class="pull-right"><input type="checkbox" name="tags[]" value="<?php echo $fm->tags_name;?>" onclick='handleClick();' /></span>
                        </li>
			<?php	}
				}
			?>
             
            </ul>
            
            <div class="text-left">
                <b>Reguler</b> <a href="#" class="pull-right text-success" onclick="clear_regular()"><i class="glyphicon glyphicon-remove"></i> <span class="bhs_ingg"> Clear</span><span class="bhs_indo"> Hapus</span></a>
            </div>
            <ul class="list-group" id="chk_regular">
            <?php 
				foreach($filter_merchant->tag as $fm){
					if($fm->tipe == 2){ ?>
						<li class="list-group-item wk_filter">
                        	<?php echo $fm->tags_name;?> <span class="pull-right"><input type="checkbox" name="tags[]" value="<?php echo $fm->tags_name;?>" onclick='handleClick();' /></span>
                        </li>
			<?php	}
				}
			?>
            </ul>
  
        </div>
        
        <div class="col-lg-9 col-xs-12 nopadding">
        	
            <div class="row" style="padding-top:10px">
                
                <div class="col-lg-7 col-xs-12">
                	
                    <div class="col-lg-3 col-xs-4" align="center">
                		<a href="<?=base_url()?>wakuantar">
                        	<img src="<?=static_file()?>images/icon/homeWA.png" /><br />
                        	<small class="wk_black bhs_indo">Ganti Kota/Area</small>
                            <small class="wk_black bhs_ingg">Change City/Area</small>
                    	</a>
                    </div>
                    <div class="col-lg-9 col-xs-8 wk_style wk_header_text">
                    	<?php echo $my_city;?> - <?php echo $location;?><!-- - <?php echo $kode_pos;?> -->
                    </div>
                
                </div>
                <div class="col-lg-5 col-xs-12">
                	
                    <div class="input-group">
                        <span class="input-group-btn" id="filter-container">
                            <button class="btn btn-default" id="filter-by-tag" onclick="popupFilterMobile()"><span class="fa fa-filter"></span></button>
                        </span>
                        <input type="text" id="search_food" class="form-control" placeholder="<?php echo $place_holder;?>" onkeyup="searchMerchant();">
                        <span class="input-group-btn">
                            <button class="btn btn-default" id="btn_search" type="button"><img src="<?=static_file()?>images/icon/search.png" width="16px;" /></button>
                        </span>
                    </div>
                    
                </div>
                
            </div>
            <div class="loading" align="center">
            	<img src="<?=static_file()?>images/icon/loading.gif" />
            </div>
            <div id="list_merchants"></div>

        <div>
            
     </div>
   </section>
<script>
var kode_pos = '<?php echo $kode_pos;?>';
var id_kelurahan = '<?php echo $id_kelurahan;?>';
var statuscari = '<?php echo $statuscari;?>';
localStorage.kode_pos_wa = kode_pos;
localStorage.id_kelurahan = id_kelurahan; //id_kelurahan
localStorage.my_city = '<?php echo $my_city;?>'; //my_city
localStorage.location = '<?php echo $location;?>'; //Input
localStorage.statuscari = '<?php echo $statuscari;?>'; //Input
load_data(kode_pos,'',id_kelurahan,'');
function load_data(kode_pos, food_name, id_kelurahan, tag){	
	var html = '';	
	$("#list_merchants").html(html);
	var url = '<?php echo site_url('wakuantarku/load_merchants');?>';	
	$.ajax({
		data : {kode_pos : kode_pos, food_name : food_name, id_kelurahan : id_kelurahan, tag : tag, statuscari: statuscari},
		url : url,
		type : "POST",
		beforeSend  : function(){ $('#container-loader-list').show(); },
		success:function(response){
			html += response;
			$("#list_merchants").html(html);
			$('.loading').addClass("hide");
		}
	});	
}

$('#btn_search').click(function(){
	handleClick();
});

function handleClick() {
    $('.loading').removeClass("hide");
  var tags = [];
  var food_name = $('#search_food').val();
  tags = $("input[name^='tags']:checked").map(function(idx, elem) {
		var elems = $(elem).val();
		return elems;
  }).get();
  load_data(kode_pos,food_name,id_kelurahan,tags);
}
function handleClick2() {
	$('.loading').removeClass("hide");
  var tags = [];
  var food_name = $('#search_food').val();
  tags = $("input[name='filertags']:checked").map(function(idx, elem) {
		var elems = $(elem).val();
		return elems;
  }).get();
  load_data(kode_pos,food_name,id_kelurahan,tags);
}
function searchMerchant()
{
	var query = $('#search_food').val().toLowerCase();
	
	$('.list-group-item .merchant_names').each(function(){
		 var $this = $(this);
		 //alert($this.text().toLowerCase().indexOf(query));
		 if($this.text().toLowerCase().indexOf(query) === -1)
			 $this.closest('.list-group-item').fadeOut();
		 else 
		 $this.closest('.list-group-item').fadeIn();
	});
}
function clear_regular(){
	$("#chk_regular").find('input:checkbox').prop('checked', false);
	$('.loading').removeClass("hide");
  	var tags = [];
  	var food_name = $('#search_food').val();
  	tags = $("input[name^='tags']:checked").map(function(idx, elem) {
		var elems = $(elem).val();
		return elems;
  	}).get();
  	load_data(kode_pos,food_name,id_kelurahan,tags);
}
function clear_popular(){
	$("#chk_popular").find('input:checkbox').prop('checked', false);
	$('.loading').removeClass("hide");
  	var tags = [];
  	var food_name = $('#search_food').val();
  	tags = $("input[name^='tags']:checked").map(function(idx, elem) {
		var elems = $(elem).val();
		return elems;
  	}).get();
  	load_data(kode_pos,food_name,id_kelurahan,tags);
}
function popupFilterMobile()
{
	var currentData = $('#filter-mobile-container').html();
	if(currentData.length < 50)
	{
		var html = $('.filter-tags').html();
		
		html = html.split('name="tags[]"').join('name="filertags[]"');
		html = html.split('handleClick();').join('handleClick2();');
		
		$('#filter-mobile-container').html(html);
	}
	$('#filter-mobile').modal();
}
</script>
<style type="text/css">
#filter-mobile .page-header{
	margin-top:0;
}
#filter-mobile .modal-footer{
	text-align:center;
}
</style>
<div class="modal fade" id="filter-mobile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content modal-city">
      <div class="modal-body" id="filter-mobile-container"></div>
      <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>