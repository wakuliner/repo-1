	<script type="text/javascript">
	$(document).ready(function(e) {
        $(document).on('click', '#btn-jam-buka', function(e){
			$('.toggle-layanan, .toggle-rating').css({'display':'none'});
			var visible = $('.toggle-jam-buka').is(':visible');
			if(visible)
			{
				$('.toggle-jam-buka').css({'display':'none'});
			}
			else
			{
				$('.toggle-jam-buka').css({'display':'block'});
			}
		});
        $(document).on('click', '#btn-layanan', function(e){
			$('.toggle-jam-buka, .toggle-rating').css({'display':'none'});
			var visible = $('.toggle-layanan').is(':visible');
			if(visible)
			{
				$('.toggle-layanan').css({'display':'none'});
			}
			else
			{
				$('.toggle-layanan').css({'display':'block'});
			}
		});
        $(document).on('click', '#btn-rating', function(e){
			$('.toggle-jam-buka, .toggle-layanan').css({'display':'none'});
			var visible = $('.toggle-rating').is(':visible');
			if(visible)
			{
				$('.toggle-rating').css({'display':'none'});
			}
			else
			{
				$('.toggle-rating').css({'display':'block'});
			}
		});
    });
	</script>
    <style type="text/css">
	#btn-jam-buka, #btn-layanan, #btn-rating{
		display:block;
		margin:0px auto;
		padding:4px 12px;
	}
	.button-area{
		padding: 10px 0;
		display: block;
	}
	.toggle-layanan{
		padding-top: 10px;
	}
	</style>
	<section class="wk-container-1 nobg" style="background-color:white">
      <div class="container-fluid nopadding">
        
        <div class="col-lg-4 col-xs-12 side_menu2" style="border-right:1px #259b00 solid;">
        	<section>    
                <div class="col-lg-offset-1 col-lg-10">
                    
                    <h4 class="text-center"><b>Merchant Info</b></h4>
                    <div class="clearfix">
                        <h4><?php echo $merchant_info->merchant_name;?></h4>
                        <p><b><?php echo $merchant_info->address;?></b></p>
                        <p align="justify" class="text-muted">
                           <?php echo $merchant_info->description;?>
                        </p>
                    </div>
                    <div class="mobile-view">
                    	<div class="button-area">
                    	<div class="col-xs-4">
                        	<button type="button" id="btn-jam-buka" class="btn btn-ww btn-xs">JAM BUKA</button>
                        </div>
                        <div class="col-xs-4">
                        	<button type="button" id="btn-layanan" class="btn btn-ww btn-xs">LAYANAN</button>
                        </div>
                        <div class="col-xs-4">
                        	<button type="button" id="btn-rating" class="btn btn-ww btn-xs">RATINGS</button>
                        </div>
                        </div>
                    </div>
                    <br />
                    <div class="alert alert-success mobile-hide toggle-jam-buka" align="center">
                        <p><b><span class="bhs_ingg">Operational Hours:</span><span class="bhs_indo">Jam Pengantaran</span></b></p>
                        <?php 
							foreach($merchant_info->operation_hours as $oh){
								echo '<p><b>'.$oh->name_days.' : '.date('H:i', strtotime($oh->start_operation)).' - '.date('H:i', strtotime($oh->end_operation)).' WIB</b></p>';
								
							}
						?>
                    </div>
                    <div class="mobile-hide toggle-layanan">
                        <?php if($merchant_info->promo > 0) { ?>
                        <div class="clearfix">
                            <img src="<?=static_file()?>images/icon/infopromo.png" align="left"/> <b>Promo 10%</b>
                        </div><br>
                        <?php } 
                            if($merchant_info->delivery > 0) {
                        ?>
                        <div class="clearfix">
                            <img src="<?=static_file()?>images/icon/infodeliv.png" align="left"/> <b><span class="bhs_ingg">Delivery Service</span><span class="bhs_indo">Pelayanan Pengiriman</span> <bR />Rp. <?php echo number_format($merchant_info->delivery_fee,0,'.','.');?> -/+ <?php echo $merchant_info->delivery_time;?></b>
                        </div><br>
                        <?php } 
                            if($merchant_info->take_away > 0) {
                        ?>
                        <div class="clearfix">
                            <img src="<?=static_file()?>images/icon/infotakeaway.png" align="left"/> <b>Take Away</b>
                        </div><Br>
                        <?php } ?>
                        <div class="clearfix">
                            <img src="<?=static_file()?>images/icon/infocost.png" align="left"/> <b>Min Rp. <?php echo number_format($merchant_info->minimum_order,0,'.','.');?></b>
                        </div><br>
                        <div class="clearfix">
                            <img src="<?=static_file()?>images/icon/infopayment.png" align="left"/> <b><span class="bhs_ingg">Payment Method</span><span class="bhs_indo">Metode Pembayaran</span></b><br />
                            <?php 
                                $payments = array();
                                foreach($merchant_info->payment_method as $pm){
                                    $payments[] = ' <span>'.$pm->name_type_payment.'</span> ';
                                }
                                echo implode('/',$payments);
                            ?>
                           
                        </div>
                    </div>
                    <br />
                    <div class="mobile-hide toggle-rating" style="overflow-y: scroll;height: 500px;">
                        <h4 align="center" class="text-muted">Rating 
                            <?php 
							$rate = 0;
							$rates = 0;
							$int_rate = 0;
							foreach($merchant_info->rate as $r){
								$rate = 0;
								$rate = $r->rate;
							}
							$rates = $rate/10;
							$int_rate = (int)$rates;
							$add = 0;
							$btg_setengah = '';
							if($rates > $int_rate){
								$btg_setengah = '<img src="'.static_file().'images/icon/starhalf.png" />';
								$add = 1;
							}
							$star_empty = 5 - ($int_rate + $add);
						?>
                       
                        
						<?php for($b=0;$b<$int_rate;$b++): 
							
						?>
                        <img src="<?=static_file()?>images/icon/starfull.png" />
                        <?php endfor; 
							echo $btg_setengah;
						?>
                        <?php for($b=0;$b<$star_empty;$b++): ?>
                        <img src="<?=static_file()?>images/icon/starempty.png" />
                        <?php endfor; ?> (<?php echo $rate;?>)
                        </h4>
                        <bR />
                        <?php
				   		$_i = 0;
						if(!empty($merchant_info->user_rating)){
							foreach($merchant_info->user_rating as $ur){ 
								
								?>
								<div class="panel panel-success">
                                <div class="panel-body nopadding">
                                    <div class="col-lg-6 col-xs-6"><h5><b><?php echo $ur->first_name;?></b></h5></div>
                                    <div class="col-lg-6 col-xs-6">
                                    	<?php
											$_rate = 0;
											$_star_empty = 0;
											$_rate_int = 0;
											$_rate = (int)$ur->rating;
											$_rate = $_rate/10;
											$_rate_int = (int)$_rate;
											$_star_empty = 5 - ($_rate_int);
											for($b=0;$b<$int_rate;$b++): ?>
                        					<img src="<?=static_file()?>images/icon/starfull.png" />
                        				<?php endfor; 
											for($b=0;$b<$_star_empty;$b++): ?>
                        					<img src="<?=static_file()?>images/icon/starempty.png" />
                        				<?php endfor; ?> 
                                        
                                    </div>
                                    <div class="col-lg-12 col-xs-12">
                                        <p class="text-muted"><?php echo $ur->description;?></p>
                                    </div>
                                </div>
                            </div>
					<?php }
						$_i++;	
						}
						
					?>
                    </div>
                </div>
            </section>
        </div>
        
        <div class="col-lg-8 col-xs-12">
        	
            <div class="row">
                
                <div class="col-lg-7 col-xs-12">
                	<!--
                    <div class="col-lg-3 col-xs-4" align="center">
                		<a href="#">
                        	<img src="<?=static_file()?>images/icon/homeWA.png" /><br />
                        	<small class="wk_black">Ganti Kota/Area</small>
                    	</a>
                    </div>
                    <div class="col-lg-9 col-xs-8 wk_style wk_header_text wk_turun_dikit">
                    	<?php echo $my_city;?> - <?php echo $location;?> - <?php echo $kode_pos;?>
                    </div>
                    <br><Br><br>
					-->
                    <a href="#" id="backMerchantList" class="text-success"><i class="glyphicon glyphicon-menu-left"></i> <span class="bhs_ingg"> Back to Merchant list</span><span class="bhs_indo"> Kembali ke daftar merchant</span></a>
                </div>
                <div class="col-lg-5 col-xs-12 wk_turun_dikit">
                	
					<!--
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Resto's name or food name">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><img src="<?=static_file()?>images/icon/search.png" width="16px;" /></button>
                      </span>
                    </div>
					-->
                    
                </div>
                
            </div>
            
            <div class="tab-content content_about">
                
                <ul class="nav nav-justified tab-merchant-menu">
                <?php 
					$i = 1;
					foreach($list_menu->food_category as $lm){ ?>
                    	<li role="tab" class="<?php echo $i==1 ? 'active' : '';?>" data-toggle="tab" data-target="#foodmenu<?php echo $lm->food_category_id;?>"><a href="#"><?php echo $lm->name_category;?></a></li>
				<?php $i++; } ?>
                 	<!-- <li role="tab" data-toggle="tab" data-target="#top"><a href="#">TOP SELLER</a></li>
                	<li role="tab" data-toggle="tab" data-target="#app"><a href="#">APPETIZER</a></li>
                 	<li role="tab" data-toggle="tab" data-target="#main"><a href="#">MAIN MENU</a></li>
                  	<li role="tab" data-toggle="tab" data-target="#desert"><a href="#">DESSERT / DRINK</a></li> -->
                </ul>
                <center><img src="<?=static_file()?>images/icon/triangle.png"/></center>
            	
                <div class="tab-content content_about">
                	 <?php 
					 $n = 1;
					foreach($list_menu->food_category as $fc){ ?>
                    	<div role="tabpanel" class="tab-pane <?php echo $n == 1 ? ' active' : '';?>" id="foodmenu<?php echo $fc->food_category_id;?>">
                            <?php foreach($list_menu->food as $f){
								if($f->food_category_id == $fc->food_category_id){
								?>
                                <div class="col-lg-3 col-xs-6">
                                    <div class="produk">
                                        <a href="<?php echo base_url('wakuantar/menu_detail/'.$f->merchant_id.'/'.$kode_pos.'/'.$id_kelurahan.'/'.$statusCari.'/'.$f->merchant_food_id)?>">
                                        <img src="<?=$f->loc_image.''.$f->image?>" class="img-responsive" />
                                        <p class="text-center text-muted">
                                            <span><b><?php echo $f->food_name;?></span></b><br />
                                            <span style='color:black'>
											<?php
												if($f->discount > 0)
												{
													echo "<span style='color:#FD7902'>Rp. ";
													echo number_format($f->price,0,'.','.');
													echo "</span>";
												}
												else
												{
													echo "<span>Rp. ";
													echo number_format($f->price,0,'.','.');
													echo "</span>";
												}
											?>
											</span>
                                            <!--
											<span>Rp. <?php echo number_format($f->price,0,'.','.');?></span>
											-->
                                            <?php if($f->discount > 0){ ?>
                                            <br /> <span style='color:#FD7902;text-decoration:line-through'><span style='color:black'>Rp. <?php echo number_format($f->price_pre_promo,0,'.','.').'</span></span> <span style="color:#FD7902;">'.$f->discount.'%';?></span>
                                            <?php } ?>
                                        </p>
                                        </a>
                                    </div>
                                </div>
                            <?php } } ?>
                        </div>
					<?php $n++; } ?>
                	
                                              
                </div>
                
            </div>
            
        </div>
            
            <br />
            <br />
            <p align="center" class="bhs_indo"><a href="#"><i class="glyphicon glyphicon-arrow-up"></i> Kembali ke atas</a></p>
            <p align="center" class="bhs_ingg"><a href="#"><i class="glyphicon glyphicon-arrow-up"></i>Back to top</a></p>
            <br />
     </div>
   <form id="my_search" method="POST" name="my_search">
     		<input type="hidden" name="kode_pos" id="kode_posku" value="" />
            <input type="hidden" name="id_kelurahan" id="id_kelurahanku" value="" />
            <input type="hidden" name="Input" id="location" value="" />
            <input type="hidden" name="my_city" id="my_city" value="" />
            <input type="hidden" name="status" id="statusCari" value="" />
     </form>
   </section>
<script type="text/javascript">
	
	var kode_pos = localStorage.getItem("kode_pos_wa");
	var id_kelurahan = localStorage.getItem("id_kelurahan");
	var Input = localStorage.getItem("location");
	var my_city = localStorage.getItem("my_city");
	var statusCari = localStorage.getItem("statusCari");
    
	$('#kode_posku').val(kode_pos);
	$('#id_kelurahanku').val(id_kelurahan);
	$('#location').val(Input);
	$('#my_city').val(my_city);
	$('#statusCari').val(statusCari);
	
	$('#backMerchantList').click(function(){
		var url = '<?php echo site_url('wakuantars/search');?>';
		$('#my_search').attr('action', url);
		$('#my_search').submit();
	});
 </script>