
<div style="margin-top:20px;">
                
                <div class="list-group"> 
                
                	<?php 
					$lang_user = $this->session->userdata('lang_user');
						$logo = '';
						$rate = 0;
						$btg_setengah = '';
						$rates = 0;
						$int_rate = 0;
						$add = 0;
						$star_empty = 0;
						$payment_online = '';
						$payment_cod = '';
						$tags_name = array();
						if(!empty($statusCari)){
                            if($statusCari == 1) $data = $list_merchant->merchant;
                            else $data = $list_merchant->merchant_online;

                            if(!empty($data)){
                            foreach($data as $lm){
                                $logo = '';
                                $logo = $lm->image_banner_path.'/'.$lm->banner;
                            ?>
                    <!--
					onclick="redirect_detail(<?php echo $lm->merchant_id.','.$kode_pos.','.$id_kelurahan;?>)"
					-->
					<div class="list-group-item"> 
                    	<?php if($lm->status_store=="0"): ?>
                            <button type="button" class="btn btn-disabled btn-block wk_white bhs_ingg">The merchant are closed</button><button type="button" class="btn btn-disabled btn-block wk_white bhs_indo">Toko tutup</button><bR />
                            <div style="opacity: 0.5;">
                        <?php endif; 
							$url_name = str_replace(' ','-',strtolower($lm->merchant_name));
						?>
                        <div class="row">
                        	
                    		<div class="product-image">
                            	<a href="<?php echo base_url('wakuantar/'.$lm->merchant_id)?>"><img src="<?php echo $logo;?>" class="img-responsive" /></a>
                            </div>
                            <div class="info-semua">
                            	<div class="info-nama-merchant nopadding">
                                    <p>
                                        <a href="<?php echo base_url('wakuantar/'.$url_name)?>" class="wk_style"><b><span class="merchant_names"><?php echo $lm->merchant_name;?></span></b></a> 
                                        <a href="#"><img src="<?=static_file()?>images/icon/info.png" width="16px" /></a>
										<a href="https://maps.google.com/?ll=<?php echo $lm->latitude?>,<?php echo $lm->longitude?>" target="_blank"><img src="<?=static_file()?>images/icon/direction_small.png" width="25px" /></a><br /> 
                                        <small class="hastag" style="color:#259b00;"><b>
                                        <?php 
											$cnt = 0;
											$cnt = count($lm->tag);
											$i = 1;
											$tags_name = array();
											foreach($lm->tag as $t){
												echo $t->tags_name;
												if($i < $cnt){
													echo ', ';
												}
												array_push($tags_name, $t->tags_name);
												$i++;
											}
											
										?>
                                        </b></small>
                                    </p>
                                </div>
                                <div class="info-group-3">
                                	<div class="info-rating">
                                    	<?php 
											$rate = 0;
											$rate = $lm->rate;
											$rates = $rate/10;
											$int_rate = (int)$rates;
											$add = 0;
											$btg_setengah = '';
											if($rates > $int_rate){
												$btg_setengah = '<img src="'.static_file().'images/icon/greenhalf.png" />';
												$add = 1;
											}
											$star_empty = 5 - ($int_rate + $add);
										?>
                                    	<?php for($b=0;$b<$int_rate;$b++): ?>
                                    	<img src="<?=static_file()?>images/icon/greenfull.png" />
                                        <?php endfor; 
											echo $btg_setengah;
										?>
                                        <?php for($b=0;$b<$star_empty;$b++): ?>
                                    	<img src="<?=static_file()?>images/icon/greenempty.png" />
                                        <?php endfor; ?>
                                        <b class="wk_style">(<?php echo $lm->total_order;?> Orders)</b>
                                    </div>
                                    <div class="info-harga">
                                    	
                                        <img src="<?=static_file()?>images/icon/infocost.png" />
                                        <b class="wk_style">Min Rp <?php echo number_format($lm->minimum_order,0,'.','.');?></b>
                                    </div>
									<!--
                                    <div class="info-arah text-center">
                                        <a href="https://maps.google.com/?ll=<?php echo $lm->latitude?>,<?php echo $lm->longitude?>" target="_blank"><img src="<?=static_file()?>images/icon/direction.png" /></a>
                                        
                                        <?php 
                                            if(in_array('Halal', $tags_name)){ ?>
                                                <img src="<?=static_file()?>images/icon/halal.png" style="width: 25px;" class="mobile-view" style="display:none;"/>
                                        <?php } else { ?>
                                                <img src="<?=static_file()?>images/icon/halaloff.png" style="width: 25px;" class="mobile-view" style="display:none;"/>
                                        <?php } ?>
                                    </div>
									-->
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="product-info-group" onclick="redirect_detail(<?php echo $lm->merchant_id.','.$kode_pos.','.$id_kelurahan;?>)">
                                <div class="product-info-item wk_info_space">
                                    <small class="wk_style bhs_ingg"><b>Payment Method</b></small>
                                    <small class="wk_style bhs_indo"><b>Metode pembayaran</b></small><br />
                                    <?php
                                        $payment_online = '<img src="'.static_file().'images/icon/ccoff.png" />';
                                        $payment_cod = '<img src="'.static_file().'images/icon/cashoff.png" />';
                                        foreach($lm->payment_method as $sp){
                                            if($sp->id_type_payment == 1){
                                                $payment_online = '<img src="'.static_file().'images/icon/ccon.png" />';
                                            }
                                            if($sp->id_type_payment == 2){
                                                $payment_cod = '<img src="'.static_file().'images/icon/cashon.png" />';
                                            }
                                        }
                                        echo $payment_cod;
                                        echo $payment_online;
                                    ?>
                                </div>
                                <div class="product-info-item wk_info_space">
                                    <small class="wk_style bhs_ingg"><b>Take Away</b></small>
                                    <small class="wk_style bhs_indo"><b>Diambil</b></small><br />
                                    <?php
                                    if($lm->take_away == 1){ ?>
                                        <img src="<?=static_file()?>images/icon/listtakewayaon.png" />
                                    <?php }else{ ?>
                                        <img src="<?=static_file()?>images/icon/listtakeawayoff.png" />
                                    <?php }?>
                                </div>
                                <div class="product-info-item wk_info_space">
                                    <small class="wk_style bhs_ingg"><b>Delivery</b></small>
                                    <small class="wk_style bhs_indo"><b>Dikirim</b></small><Br />
                                    <!--<img src="<?=static_file()?>images/icon/listdelivon.png" align="absmiddle" />--> <?php echo "Rp ";echo number_format($lm->delivery_fee,0,'.','.');?> <br /> <?php echo $lm->delivery_time;?>
                                </div>
                                <div class="product-info-item wk_info_space">
                                    <small class="wk_style"><b>Promo</b></small><br />
                                    <?php 
                                        if($lm->promo > 0){ ?>
                                            <img src="<?=static_file()?>images/icon/listpromo.png"/>
                                    <?php }else{ ?>
                                            <img src="<?=static_file()?>images/icon/listpromooff.png"/>
                                    <?php } ?>
                                </div>
                                <div class="product-info-item wk_info_space mobile-hide">
                                	<br />
                                    <?php 
                                        if(in_array('Halal', $tags_name)){ ?>
                                            <img src="<?=static_file()?>images/icon/halal.png" style="width: 25px;"/>
                                    <?php } else { ?>
                                            <?php echo "";?><!--<img src="<?=static_file()?>images/icon/halaloff.png"/>-->
                                    <?php } ?>
                                </div>        
                                
                            </div>
                        </div>
                    </div> 
                    <?php if($lm->status_store=="0"): ?>
                   </div>
                <?php endif; ?>
							<?php }
						    }
                        }
						?>
                    
                </div>
                
            </div>
            
            <br />
             <p align="center" class="bhs_ingg"><a href="#"><i class="glyphicon glyphicon-arrow-up"></i> Back to top</a></p>
            <p align="center" class="bhs_indo"><a href="#"><i class="glyphicon glyphicon-arrow-up"></i> Kembali ke atas</a></p>
            <br />

<script>
function redirect_detail(merchant_id, kode_pos, id_kelurahan){
	var url = '<?php echo base_url('wakuantar/menu')?>/'+merchant_id+'/'+kode_pos+'/'+id_kelurahan+'/'+statusCari;
	window.location.href = url;
}
var lang_user = '<?php echo $lang_user;?>';
$('.bhs_indo').hide();
$('.bhs_ingg').hide();
if(lang_user != ''){
	$('.'+lang_user).show();
}else{
	$('.bhs_indo').show();
}

</script>