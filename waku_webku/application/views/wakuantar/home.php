<?php
$lang_user = $this->session->userdata('lang_user');
$place_holder = 'Atau masukan lokasi anda disini';
$place_city = 'Kota';
if($lang_user == 'bhs_ingg'){
	$place_holder = 'Or enter your location here';
	$place_city = 'City';
}
?>
<script>
var lang_user = '<?php echo $lang_user;?>';
function chk_kota(){
	var statuscari = $('#statuscari').val();
	if (statuscari == 0){
		var msg = 'Maaf, Silahkan pilih dulu jenis makanan';
		if(lang_user == 'bhs_ingg'){
			msg = 'Sorry, Please select a food type first';
		}
		$('#error_reg_info').html(msg);
		$('#errorRegModal').modal('show');
		return false;
	}
	else{
		var kode_posku = $('#kode_posku').val();
		var id_kelurahan = $('#id_kelurahan').val();
		if(kode_posku == '' || id_kelurahan== ''){
			var msg = 'Masukkan lokasi Anda terlebih dahulu atau masukkan kota dan daerah Anda';
			if(lang_user == 'bhs_ingg'){
				msg = 'Lets get your location first or please enter your city and area';
			}
			$('#error_reg_info').html(msg);
			$('#errorRegModal').modal('show');
			return false;
		}
	}
}
</script>

    <section class="wk-container-1 bg-full">
        <div class="container wk-content-1">
            <div class="wk-content-search">
                <div class="col-lg-12">
                     <p class=" set-search " style="margin-bottom: 40px;color: black;"><i><span class="bhs_ingg">Use the inter stay service and enjoy your favorite food at home</span><span class="bhs_indo" style="color: black;">Gunakan Layanan WAKU ANTAR dan Nikmati Makanan Favorit Anda di Rumah</span></i> </p>
                    <div class="col-md-12" style="margin-top: 20px;margin-bottom: 40px;">
	                    <div class="col-md-4 col-xs-12 text-center" style="height: 60px;">
	                    	<h3 style="color: #0AA41B; font-weight: bolder;">Mau Makan Apa ?</h3>
	                    </div>
	                    <div class="col-md-4 col-xs-6 col-sm-6" style="height: auto; cursor: pointer;">
	                    		<div class="col-md-12 hidden-xs" style="height: 60px; background-color: #e5fbe5; text-align: center; border-radius: 5px; color: #0aa41b; border: solid 1px #a5d800;" id="siapsaji">
	                    			<h3 style="margin-top: 19px;"><img src="<?=static_file()?>images/icon/siapsaji.png" width="15%;" style="margin-right:10px;margin-top:-15px;"/>SIAP SAJI</h3>
	                    		</div>

	                    		<div class="col-md-12 visible-xs" style="padding-left:10px !important;padding-right:9px !important; background-color: #e5fbe5; text-align: center; border-radius: 5px; color: #0aa41b; border: solid 1px #a5d800;" id="siapsaji2">
	                    			<h5 style="margin-top: 15px;"><img src="<?=static_file()?>images/icon/siapsaji.png" width="15%;" style="margin-right:4px; margin-top:-8px;"/>SIAP SAJI</h5>
	                    		</div>
	                    </div>

							  <div class="col-md-4 col-xs-6 col-sm-6" style="height: auto; cursor: pointer;">
	                    		<div class="col-md-12 hidden-xs" style="height: 60px; background-color: #e5fbe5; text-align: center; border-radius: 5px; color: #0aa41b; border: solid 1px #a5d800;" id="snack">
	                    			<h3 style="margin-top: 19px;"><img src="<?=static_file()?>images/icon/snack.png" width="15%;" style="margin-right:10px;margin-top:-10px;"/>SNACK</h3>
	                    		</div>

	                    		<div class="col-md-12 visible-xs" style="background-color: #e5fbe5; text-align: center; border-radius: 5px; color: #0aa41b; border: solid 1px #a5d800;" id="snack2">
	                    			<h5 style="margin-top: 15px;"><img src="<?=static_file()?>images/icon/snack.png" width="15%;" style="margin-right:5px;margin-top:-4px;"/>SNACK</h5>
	                    		</div>
	                    </div>
                    </div>

						  <div class="col-xs-12 visible-xs" style="height: 30px;"></div>
                    <div class="location-container">
                        <div class="location-trigger">
                            <div class="location-trigger-bar-grey" id='bar'>
                                <a href="javascript:;" id="btn_getLoc"><span class="bhs_ingg">Get your location</span><span class="bhs_indo">Dapatkan lokasi anda</span></a>
                            </div>
                            <div class="location-trigger-button-grey" id='loc_icon'>
                                <a href="javascript:;" id="btn_getLoc2"></a>
                            </div>
                        </div>
                    </div>


                    <input type="hidden" id="myLat" class="myLat" value="" />
                    <input type="hidden" id="myLong" class="myLong" value="" />
                    <form id="frm_search_merchants" action="<?=base_url()?>wakuantars/search" method="post" onsubmit="return chk_kota()">
                    	<!-- JIKA SNACK MAKA VALUE DI ISI 1, JIKA OLEH OLEH VALUE DI ISI 2 -->
                    		<input type="hidden" name="status" class="status" value="" id='statuscari'>
                        <input type="hidden" name="kode_pos" id="kode_posku" value="" />
                        <input type="hidden" name="id_kelurahan" id="id_kelurahanku" value="" />
                        <input type="hidden" name="my_city" id="my_city" value="" />
                        <div class="location-container">
                            <div class="location-trigger">
                                <div class="location-trigger-bar">
                                    <input class="input_go" type="text" id="location" name="Input" placeholder="<?php echo $place_holder;?>" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?php echo $place_holder;?>'" data-toggle="modal">
                                </div>
                                <div class="location-trigger-button-grey" id="tombol_go">
                                    <button type="submit" class="btn btn-success btn-block btn-lg searchgo" href="#"><span class="bhs_ingg">GO</span><span class="bhs_indo">PERGI</span></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="cityModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body">
            <div class="form-group">
            	<div class="input-group">
                  <input type="text" class="form-control" id="nama_kota" placeholder="<?php echo $place_city;?>" onkeyup="searchCities();">
                  <span class="input-group-btn">
                    <button class="btn btn-success" id="btn_search_city" type="button"><i class="glyphicon glyphicon-search"></i></button>
                  </span>
                </div>
            </div>
            <div class="form-group">
            	<div class="list-group" style="height: 300px; overflow-y: scroll;">
                  <a href="#" class="list-group-item wk_city"><b class="wk_style"><span class="bhs_ingg">TOP CITIES</span><span class="bhs_indo">KOTA TERATAS</span></b></a>
                  <div id="top_cities"></div>


                  <a href="#" class="list-group-item wk_city"><b class="wk_style"><span class="bhs_indo">Kota lainnya</span><span class="bhs_ingg"OTHER CITIES</span></b></a>
                  <div id="others_cities"></div>
                </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  <input type="hidden" id="id_kotaku" value="0" />
 <div class="modal fade" id="areaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body">
            <div class="form-group">
            	<div class="input-group">
                	<span class="input-group-btn">
                    <button class="btn btn-default" id="btn_back_to_city" type="button" onclick="backToCity()"><span class="fa fa-arrow-left" style="color: green;"></span></button>
                    </span>
                  <input type="text" id="nama_area" class="form-control" placeholder="Enter area here" onkeyup="searchAreas();">
                  <span class="input-group-btn">
                    <button class="btn btn-default" id="btn_search_area" type="button"><img src="<?=static_file()?>images/icon/search.png" width="16px;" /></button>
                  </span>
                </div>
            </div>
            <div class="form-group">
            	<div class="list-group" style="height: 300px; overflow-y: scroll;">
				<!--
                  <a href="#" class="list-group-item wk_city"><b class="wk_style">TOP CITIES</b></a>
				-->
                 <div id="top_area"></div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<script type="text/javascript">
get_city();
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (p) {
		$('#myLong').val('');
		$('#myLat').val('');
		var latitude = p.coords.latitude;
		var longitude =  p.coords.longitude;
		$('#myLong').val(longitude);
		$('#myLat').val(latitude);
		console.log('lat :'+latitude);
		console.log('long :'+longitude);

    });
} else {
    alert('Geo Location feature is not supported in this browser.');
}

$('#btn_getLoc').click(function(){
	var statuscari = $('#statuscari').val();
	if (statuscari == 0){
		var msg = 'Maaf, Silahkan pilih dulu jenis makanan';
		if(lang_user == 'bhs_ingg'){
			msg = 'Sorry, Please select a food type first';
		}
		$('#error_reg_info').html(msg);
		$('#errorRegModal').modal('show');
	}
	else{
		var long = $('#myLong').val();
		var lat = $('#myLat').val();
		var url = '<?php echo site_url('wakuantar/get_area_ll');?>';
		//var action = '<?php echo site_url('wakuantar/search');?>';
		$('#kode_posku').val('');
		$('#id_kelurahanku').val('');
		$('#id_kotaku').val('');
		$('#location').val('');
		$('#my_city').val('');
		$.ajax({
			url : url,
			type:"POST",
			data : {lat : lat, longitude:long},
			success: function(data){
				console.log(data);
				var dt_json = JSON.parse(data);
				$('#id_kotaku').val(dt_json['id_kota']);
				$('#kode_posku').val(dt_json['kode_pos']);
		 		$('#id_kelurahanku').val(dt_json['id_kelurahan']);
				$('#location').val(dt_json['area']);
				$('#my_city').val(dt_json['kota']);
				localStorage.id_kotaku = dt_json['id_kota'];
				localStorage.id_kelurahan = dt_json['id_kelurahan']; //id_kelurahan
				localStorage.my_city = dt_json['kota']; //my_city
				localStorage.location = dt_json['area']; //Input
				//$('#frm_search_merchants').attr('action', action);
				//$('#frm_search_merchants').submit();
			}
		});
	}
});

$('#btn_getLoc2').click(function(){
	var statuscari = $('#statuscari').val();
	if (statuscari == 0){
		var msg = 'Maaf, Silahkan pilih dulu jenis makanan';
		if(lang_user == 'bhs_ingg'){
			msg = 'Sorry, Please select a food type first';
		}
		$('#error_reg_info').html(msg);
		$('#errorRegModal').modal('show');
	}
	else{
		var long = $('#myLong').val();
		var lat = $('#myLat').val();
		var url = '<?php echo site_url('wakuantar/get_area_ll');?>';
		//var action = '<?php echo site_url('wakuantar/search');?>';
		$('#kode_posku').val('');
		$('#id_kelurahanku').val('');
		$('#id_kotaku').val('');
		$('#location').val('');
		$('#my_city').val('');
		$.ajax({
			url : url,
			type:"POST",
			data : {lat : lat, longitude:long},
			success: function(data){
				var dt_json = JSON.parse(data);
				$('#id_kotaku').val(dt_json['id_kota']);
				$('#kode_posku').val(dt_json['kode_pos']);
		 		$('#id_kelurahanku').val(dt_json['id_kelurahan']);
				$('#location').val(dt_json['area']);
				$('#my_city').val(dt_json['kota']);
				localStorage.id_kotaku = dt_json['id_kota'];
				localStorage.id_kelurahan = dt_json['id_kelurahan']; //id_kelurahan
				localStorage.my_city = dt_json['kota']; //my_city
				localStorage.location = dt_json['area']; //Input

				//$('#frm_search_merchants').attr('action', action);
				//$('#frm_search_merchants').submit();
			}
		});
	}
});

$('#btn_search_city').click(function(){
	var nama_kota = $('#nama_kota').val();
	get_city(nama_kota);
});
$('#btn_search_area').click(function(){
	var nama_area = $('#nama_area').val();
	var id_kota = $('#id_kotaku').val();
	var kota_selected = $('#my_city').val();
	showAreas(id_kota, kota_selected, nama_area);
});

function get_city(nama_kota){
	var html = '';
	var url = '<?php echo site_url('wakuantars');?>';
	$.ajax({
		url : url,
		type:"POST",
		data : {nama_kota : nama_kota},
		success: function(data){
			var dt_json = JSON.parse(data);
			//html +=data;
			$('#top_cities').html(dt_json['top']);
			$('#others_cities').html(dt_json['others']);
		}
	});
}

function showAreas(id_kota, kota_selected, nama_area){
	$('#id_kotaku').val(id_kota);
	var id_kotaku = $('#id_kotaku').val();
	if(id_kotaku != ''){
		id_kota = id_kotaku;
	}
	$('#my_city').val('');
	$('#my_city').val(kota_selected);
	var html = '';
	var url = '<?php echo site_url('wakuantars/get_area');?>';
	$.ajax({
		url : url,
		type:"POST",
		data : {id_kota : id_kota, nama_area:nama_area},
		success: function(data){
			html +=data;
			$('#top_area').html(html);
			$('#cityModal').modal('toggle');
			$('#areaModal').modal('show');
		}
	});
}
function backToCity()
{
	$('#areaModal').modal('hide');
	$('#cityModal').modal();
}
function selectAreas(kode_pos, id_kelurahan,lokasi){
	 $('#areaModal').modal('hide');
	 $('#kode_posku').val(kode_pos);
	 $('#id_kelurahanku').val(id_kelurahan);
	 $('#location').val(lokasi);
	 
	 $('#frm_search_merchants').submit();
}
function searchCities()
{
	var query = $('#nama_kota').val().toLowerCase();

	$('.list-group-item .cityTeks').each(function(){
		 var $this = $(this);
		 //alert($this.text().toLowerCase().indexOf(query));
		 if($this.text().toLowerCase().indexOf(query) === -1)
			 $this.closest('.list-group-item').fadeOut();
		 else
		 $this.closest('.list-group-item').fadeIn();
	});
}

function searchAreas()
{
	var query = $('#nama_area').val().toLowerCase();

	$('.list-group-item .areaTeks').each(function(){
		 var $this = $(this);
		 //alert($this.text().toLowerCase().indexOf(query));
		 if($this.text().toLowerCase().indexOf(query) === -1)
			 $this.closest('.list-group-item').fadeOut();
		 else
		 $this.closest('.list-group-item').fadeIn();
	});
}
$('body').scrollspy({ target: '#top_area' })
</script>
<script type="text/javascript">
	$('#location').click(function(){
		var statuscari = $('#statuscari').val();
		if (statuscari == 0){

			var msg = 'Maaf, Silahkan pilih dulu jenis makanan';
			if(lang_user == 'bhs_ingg'){
				msg = 'Sorry, Please select a food type first';
			}
			$('#error_reg_info').html(msg);
			$('#errorRegModal').modal('show');
		}
		else{
			$('#location').attr('data-target','#cityModal');
		}
	})
	$(document).ready(function() {
		//$('#bar a').addClass('bar-ok');
		$("#siapsaji").click(function(event) {
			$("#statuscari").val(1);
			$('.ok').removeClass('ok');
			$(this).addClass('ok');
			$("#bar").attr('class', 'location-trigger-bar');
			$("#loc_icon").attr('class', 'location-trigger-button');
			$("#tombol_go").attr('class', 'location-trigger-button');
		});
		$("#snack").click(function(event) {
			$("#statuscari").val(2);
			$('.ok').removeClass('ok');
			$(this).addClass('ok');
			$("#bar").attr('class', 'location-trigger-bar');
			$("#loc_icon").attr('class', 'location-trigger-button');
			$("#tombol_go").attr('class', 'location-trigger-button');
		});
		$("#siapsaji2").click(function(event) {
			$("#statuscari").val(1);
			$('.ok').removeClass('ok');
			$(this).addClass('ok');
			$("#bar").attr('class', 'location-trigger-bar');
			$("#loc_icon").attr('class', 'location-trigger-button');
			$("#tombol_go").attr('class', 'location-trigger-button');
		});
		$("#snack2").click(function(event) {
			$("#statuscari").val(2);
			$('.ok').removeClass('ok');
			$(this).addClass('ok');
			$("#bar").attr('class', 'location-trigger-bar');
			$("#loc_icon").attr('class', 'location-trigger-button');
			$("#tombol_go").attr('class', 'location-trigger-button');
		});
	});
</script>

<style type="text/css">
	.ok{
		background-color: #9bea9b !important;
		color:white !important;
	}
	.ok h3{
	    color:white !important;
	}
</style>
