<?php 
$carts = $this->cart->contents();
$cart_merchant = '';
if(!empty($carts)){
	$cart_merchant = '';
	foreach($carts as $c){
		$cart_merchant = '';
		$cart_merchant = $c['merchant_id'];
	}
}
$lang_user = $this->session->userdata('lang_user');
?>
	<section class="wk-container-1 nobg" style="background-color:white;">
      <div class="container-fluid nopadding">
        
        <div class="col-lg-4 col-xs-12 side_menu mobile-hide">
        	<section>    
                <div class="col-lg-offset-1 col-lg-10">
                    
                    <h4 class="text-center"><b>Merchant Info</b></h4>
                    <hr />
                    <h4><a href="<?php echo base_url('wakuwiku/merchant/'.$merchant_info->merchant_id.'/'.$kode_pos.'/'.$id_kelurahan)?>" class="wk_style">
                    <b><?php 
						$merchant_id = $merchant_info->merchant_id;
						$merchant_name = $merchant_info->merchant_name;
					echo $merchant_name;?></b></a></h4>
                    <p><b><?php echo $merchant_name;?></b></p>
                    <p align="justify" class="text-muted">
                      <?php echo $merchant_info->description;?>
                    </p>
                    <br />
                    <div class="alert alert-success" align="center" style="background-color:white;">
                        <p><b><span class="bhs_ingg">Operational Hours:</span><span class="bhs_indo">Jam Pengantaran</span></b></p>
                        <?php 
							foreach($merchant_info->operation_hours as $oh){
								echo '<p><b>'.$oh->name_days.' : '.date('H:i', strtotime($oh->start_operation)).' - '.date('H:i', strtotime($oh->end_operation)).' WIB</b></p>';
								
							}
						?>
                        
                        
                    </div>
                    <?php if($merchant_info->promo > 0) { ?>
                    <div class="clearfix">
                        <img src="<?=static_file()?>images/icon/infopromo.png" align="left"/> <b>Promo 10%</b>
                    </div>
                    <?php } 
						if($merchant_info->delivery > 0) {
					?>
                    <div class="clearfix">
                        <img src="<?=static_file()?>images/icon/infodeliv.png" align="left"/> <b><span class="bhs_ingg">Delivery Service</span><span class="bhs_indo">Pelayanan Pengiriman</span> <bR />Rp. <?php echo number_format($merchant_info->delivery_fee,0,'.','.');?> -/+ <?php echo $merchant_info->delivery_time;?></b>
                    </div>
                    <?php } 
						if($merchant_info->take_away > 0) {
					?>
                    <div class="clearfix">
                        <img src="<?=static_file()?>images/icon/infotakeaway.png" align="left"/> <b>Take Away</b>
                    </div>
                    <?php } ?>
                    <div class="clearfix">
                        <img src="<?=static_file()?>images/icon/infocost.png" align="left"/> <b>Min Rp. <?php echo number_format($merchant_info->minimum_order,0,'.','.');?></b>
                    </div>
                    <div class="clearfix">
                        <img src="<?=static_file()?>images/icon/infopayment.png" align="left"/> <b><span class="bhs_ingg">Payment Method</span><span class="bhs_indo">Metode Pembayaran</span></b><br />
                        <?php 
							$payments = array();
							foreach($merchant_info->payment_method as $pm){
								$payments[] = ' <span>'.$pm->name_type_payment.'</span> ';
							}
							echo implode('/',$payments);
						?>
                       
                    </div>
                    <br />
					<div class="mobile-hide" style="overflow-y: scroll;height: 500px;">
                    <h4 align="center" class="text-muted">Rating 
                    	<?php 
							$rate = 0;
							$rates = 0;
							$int_rate = 0;
							foreach($merchant_info->rate as $r){
								$rate = 0;
								$rate = $r->rate;
							}
							$rates = $rate/10;
							$int_rate = (int)$rates;
							$add = 0;
							$btg_setengah = '';
							if($rates > $int_rate){
								$btg_setengah = '<img src="'.static_file().'images/icon/starSetengah.png" />';
								$add = 1;
							}
							$star_empty = 5 - ($int_rate + $add);
						?>
                       
                        
						<?php for($b=0;$b<$int_rate;$b++): ?>
                        <img src="<?=static_file()?>images/icon/starfull.png" />
                        <?php endfor; 
							echo $btg_setengah;
						?>
                        <?php for($b=0;$b<$star_empty;$b++): ?>
                        <img src="<?=static_file()?>images/icon/starempty.png" />
                        <?php endfor; ?> (<?php echo $rate;?>)
                    </h4>
                    <bR />
                    <?php
                        $_i = 0;
						if(!empty($merchant_info->user_rating)){
							foreach($merchant_info->user_rating as $ur){ 
							    
								?>
								<div class="panel panel-success">
                                <div class="panel-body nopadding">
                                    <div class="col-lg-6 col-xs-6"><h5><b><?php echo $ur->first_name;?></b></h5></div>
                                    <div class="col-lg-6 col-xs-6">
                                    	<?php
											$_rate = 0;
											$_star_empty = 0;
											$_rate_int = 0;
											$_rate = (int)$ur->rating;
											$_rate = $_rate/10;
											$_rate_int = (int)$_rate;
											$_star_empty = 5 - ($_rate_int);
											for($b=0;$b<$int_rate;$b++): ?>
                        					<img src="<?=static_file()?>images/icon/starfull.png" />
                        				<?php endfor; 
											for($b=0;$b<$_star_empty;$b++): ?>
                        					<img src="<?=static_file()?>images/icon/starempty.png" />
                        				<?php endfor; ?> 
                                        
                                    </div>
                                    <div class="col-lg-12 col-xs-12">
                                        <p class="text-muted"></p>
                                    </div>
                                </div>
                            </div>
				    <?php }
						$_i++;	
						}
						
					?>
					</div>
                </div>
            </section>
        </div>
        
        <div class="col-lg-8 col-xs-12">
        	
            <div class="row" style="padding-top:20px">
               <a href="#" onclick="window.history.back();" class="text-success"><i class="glyphicon glyphicon-menu-left"></i><span class="bhs_ingg"> Back to menu list</span><span class="bhs_indo"> Kembali ke daftar menu</a>
            </div>
            
            <div class="tab-content content_about">
                
                <div class="col-lg-5 col-xs-4">
                	<img src="<?=$list_menu->loc_image.''.$list_menu->image?>" class="img-responsive" />
                </div>
                
                <div class="col-lg-7 col-xs-8">
                	<p align="justify" class="text-muted">
                    	<?php echo $list_menu->description; ?>    
                    </p>
                    
                    <h4 class="paketheader"><b><?php echo $list_menu->food_name; ?>  <span class="pull-right">Rp. <?php echo number_format($list_menu->price,0,'.','.'); ?> </span></b></h4>
                    <?php 
						$cnt_package = count($list_menu->menu_package);
						if($cnt_package > 0){
					?>
                    <div class="alert-info header-info-menu">
                    	<h4 align="center" class="wk_style bhs_ingg">Please make your selection</h4>
                        <h4 align="center" class="wk_style bhs_indo">Tolong buat pilihan</h4>
                    </div>
                    <?php } ?>
                    <form id="frm_mycart" action="<?=base_url()?>cart" method="post" onsubmit="return chk_merchant()">
                    <input type="hidden" name="kode_pos" value="<?php echo $kode_pos;?>" />
                    <input type="hidden" name="cart_merchant" value="<?php echo $cart_merchant;?>" />
                    <input type="hidden" name="id_kelurahan" value="<?php echo $id_kelurahan;?>" />
                    <input type="hidden" name="merchant_id" value="<?php echo $merchant_id;?>" />
                    <input type="hidden" name="merchant_name" value="<?php echo $merchant_name;?>" />
                    <input type="hidden" name="id_food" value="<?php echo $list_menu->merchant_food_id; ?>" />
                    <input type="hidden" name="food_name" value="<?php echo $list_menu->food_name; ?>" />
                    <input type="hidden" name="price" id="price" value="<?php echo $list_menu->price; ?>" />
                    <input type="hidden" name="berat_porsi" value="<?php echo $list_menu->berat_per_porsi; ?>" />
                    <input type="hidden" name="max_value" id="max_value" value="<?php echo $list_menu->maximum_order; ?>" />
                     <input type="hidden" name="min_value" id="min_value" value="<?php echo $list_menu->minimum_order; ?>" />
                      <input type="hidden" name="my_img" value="<?=$list_menu->loc_image.''.$list_menu->image?>" />
                      <input type="hidden" name="statusCari" value="<?php echo $statusCari;?>" />
                    <?php 
						$i = 1;
						$menu_id_package = '';
						
						
						foreach($list_menu->menu_package as $mp){ ?>
                    <div class="form-group clearfix">
                    	<div class="col-lg-5 col-xs-6">
                        	<h4><b><?php echo $mp->menu_name;?> *</b></h4>
                        </div>
                        <div class="col-lg-7 col-xs-6">
                       		<input type="hidden" id="ttl_<?php echo $i;?>" name="ttl[]"/>
                        	<select class="form-control" name="choices[]" onchange="getPrice(this, <?php echo $i;?>);">
                            	<option value="">Select</option>
                            	<?php 
									$menu_id_package = $mp->menu_id_package;
									foreach($mp->choices as $c){
										echo '<option value="'.$c->choices_id.'Þ'.$c->price.'Þ'.$menu_id_package.'Þ'.$c->choices_name.'">'.$c->choices_name.'</option>';
									}
								?>
                            	
                            </select>
                        </div>
                    </div>
                    <?php $i++; } ?>
                    
                    
                    <div class="form-group clearfix">
                    	<div class="col-lg-5 col-xs-6">
                        	<h4><b>Qty</b></h4>
                        </div>
                        <div class="col-lg-7 col-xs-6">
                            <div class="input-group plus-minus">
                              <span class="input-group-btn">
                                <button class="btn btn-success btn-sm" type="button" onclick="del_qty()"><i class="glyphicon glyphicon-minus"></i></button>
                              </span>
                              <div class="form-group">
                              <input type="text" class="form-control input-sm" name="qty" id="qty" value="1">
                              </div>
                              <span class="input-group-btn">	
                                <button class="btn btn-success btn-sm" type="button" onclick="add_qty()"><i class="glyphicon glyphicon-plus"></i></button>
                              </span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                    	<div class="col-lg-5 col-xs-6">
                        	<h4><b>Total</b></h4>
                        </div>
                        <div class="col-lg-7 col-xs-6">
                        	
                        	<h4 class="pull-right" id="total">Rp. <?php echo number_format($list_menu->price,0,'.','.'); ?> </h4>
                        </div>
                    </div>
                    
                    
                    
                    <div class="form-group">
                    	<div class="col-lg-12 col-xs-12">
                        	<h4 id="add_notes"><a href="#" class="wk_style"><img src="<?=static_file()?>images/icon/edit1.png" /> <b><span class="bhs_ingg">Add Note</span><span class="bhs_indo">Tambahkan Catatan</span></b></a></h4>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <input type="text" class="form-control input-sm" name="notes" id="notes" value="" style="display:none;">
                        <input type="hidden" name="submit_cart" class="submit_cart" value="0" />
                    </div>
                    
                    <div class="form-group">
                    	<div class="col-lg-12 col-xs-12">
                        	<button type="button" id="submit_cart" class="btn btn-success btn-block"><span class="bhs_ingg">ADD TO CART</span><span class="bhs_indo">TAMBAH KE KERANJANG</span></button>
                        </div>
                    </div>
                    
                    </form>
                </div>
            </div>
            
        </div>
            
     </div>
     
     <div class="modal fade" id="cartaddModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body wk_white text-center">
          		<div>
                	<button type="button" class="btn btn-defaultclose pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <h4 class="msg_cart2">Berhasil menambahkan kekeranjang</h4>
          </div>
        </div>
      </div>
    </div>
     
   </section>
<script>
var merchant_id = '<?php echo $merchant_id;?>';
var kode_pos = '<?php echo $kode_pos;?>';
var id_kelurahan = '<?php echo $id_kelurahan;?>';
localStorage.back_url = '<?php echo site_url('wakuantar/menu');?>/'+merchant_id+'/'+kode_pos+'/'+id_kelurahan;
var lang_user = '<?php echo $lang_user;?>';
var msg = 'Untuk saat ini, anda hanya dapat belanja dari satu merchant dalam satu pemesanan. Apakah anda yakin ingin melanjutkan dan mengosongkan keranjang belanja anda ?';
var html_cart = 'Berhasil menambahkan kekeranjang';
if(lang_user == 'bhs_ingg'){
	msg = 'For now, you can only shop from one merchant from one reservation,are you sure want to continue and empty your cart ?';
	html_cart = 'Add to cart success';
}
 <?php if($id!="" && $submit_cart > 0):?>
     $('.msg_cart2').html('');
     $('.msg_cart2').html(html_cart);
	 $('#cartaddModal2').modal({
		backdrop: 'static',
		keyboard: false
	});
	 $('#cartaddModal2').modal('show');
	 //window.location = "<?php echo site_url('wakuantar/menu');?>/"+merchant_id+"/"+kode_pos+"/"+id_kelurahan;
 <?php endif; ?>
function chk_merchant(){
	var merchant_cart = '<?php echo $cart_merchant;?>';
	if(merchant_cart != ''){
		if(merchant_cart != merchant_id){
			if (confirm(msg)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
}

function getPrice(elem, i){
	
	var val = elem.value;
	var dt = val.split('Þ');
	var id = dt[0];
	var price = dt[1];
	$('#ttl_'+i).val(price);
	var qty = $("#qty").val();
	hitung_total(qty);
	
}

function hitung_total(qty){
	var prices_ttls = 0;
	var lengths = '<?php echo $i;?>';
	var price = $('#price').val();
	if(lengths > 0){
		lengths = Number(lengths) - 2;
	}
	var prices_ttl = $("input[name^='ttl']").map(function(idx, elem) {
		var elems = elem.value;
		elems = Number(elems);
		prices_ttls += elems;
		return prices_ttls;
	}).get();
	var ttl_prices = prices_ttl[lengths] > 0 ? prices_ttl[lengths] : 0;
	var totals = Number(qty) * (Number(ttl_prices) + Number(price));
	//console.log(totals);
	totals = totals > 0 ? 'Rp. '+numberWithCommas(totals) : 'Rp. 0.00';
	$('#total').html(totals);
}

function add_qty(){
	var qty = $("#qty").val();
	var add = Number(qty) + 1;
	$("#qty").val(add);
	hitung_total(add);
}

function del_qty(){
	var qty = $("#qty").val();
	if(qty > 1){
		var add = Number(qty) - 1;
		$("#qty").val(add);
		hitung_total(add);
	}
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
$("#add_notes").click(function(){
	$("#notes").show();
});

$('#submit_cart').click(function(){
	chk_merchant();
	$('.submit_cart').val(1);
	var dataku = $('#frm_mycart').serialize();
	var url = '<?php echo site_url('cart');?>';
	$.ajax({
		data : dataku,
		url : url,
		type : "POST",
		success:function(response){
			if(response > 0){
				$('.msg_cart2').html('');
				$('.msg_cart2').html(html_cart);
				$('#cartaddModal2').modal({
					backdrop: 'static',
					keyboard: false
				});
				$('#cartaddModal2').modal('show');	
				window.location = '<?php echo site_url('wakuantar/menu');?>/'+merchant_id+'/'+kode_pos+'/'+id_kelurahan;
			}
		}
	});
	//$('.submit_cart').val(1);
	//$('#frm_mycart').submit();
});
</script>