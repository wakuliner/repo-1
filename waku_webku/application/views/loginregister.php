<?php
$lang_user = $this->session->userdata('lang_user');
$place_pass = 'Kata sandi';
$place_name = 'Nama';
$place_email = 'Email';
$place_phone = 'Nomor Telepon';
$place_gender = 'Jenis Kelamin';
$place_tgllahir = 'Tanggal Lahir';
$place_confirm = 'Konfirmasi kata sandi';
if($lang_user == 'bhs_ingg'){
	$place_pass = "Password";
	$place_name = 'Your Name';
	$place_name = 'Your Email';
	$place_phone = 'Phone Number';
	$place_gender = 'Gender';
	$place_tgllahir = 'Birthdate';
	$place_confirm = 'Confirm Password';
}
?>

<link type="text/css" rel="stylesheet" href="static/css/bootstrap-datetimepicker.css">
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-top:90px;">
      <div class="modal-dialog" role="document" style="width:calc(100% - 20px);max-width: 400px;margin-left: auto;margin-right: auto;">
        <div class="modal-content modal_logreg">
          <div class="modal-body text-center">
          	  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?=static_file()?>images/icon/delette.png" /></button>
              <h4 align="center" class="wk_style bhs_ingg">Welcome to WAKULINER</h4>
              <h4 align="center" class="wk_style bhs_indo">Selamat datang di WAKULINER</h4>
                <center><span class="wk_black bhs_ingg">You are entering your cool store zone</span><span class="wk_black bhs_indo">Anda memasuki zona toko keren Anda</span></center>
                <br />
                <p id="error_msg" class="hide" style="color:red;"></p>
                <form id="frm_login" method="post">
                  <div class="form-group">
                    <input type="email" class="form-control" id="email_login" name="email_login" required="required" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control" id="pass_login" name="pass_login" required="required" placeholder="<?php echo $place_pass;?>">
                  </div>
                  <div class="form-group clearfix">
                    <button type="button" id="tombolLogin" value="1" class="btn btn-warning btn-orange col-lg-5 col-xs-5"><span class="bhs_ingg">Login</span><span class="bhs_indo">MASUK</span></button>
                    <span class="col-lg-2 col-xs-2"></span>
                    <button type="button" class="btn btn-success col-lg-5 col-xs-5" data-dismiss="modal" data-toggle="modal" data-target="#registrasiModal"><span class="bhs_ingg">Register</span><span class="bbhs_indo">Daftar</span></button>
                  </div>
                  <div class="form-group clearfix">
                  	<a href="#" class="pull-right wk_black" data-dismiss="modal" data-toggle="modal" data-target="#forgetModal"><span class="bhs_ingg">Forget password ?</span><span class="bhs_indo">Lupa kata sandi</span></a>
                  </div>
                </form>
                  <div class="form-group clearfix">
                    <button type="button" class="btn btn-disabled col-lg-12 col-xs-12" disabled="disabled"><img src="<?= static_file() ?>images/icon/fb2.png" /> <span class="bhs_ingg">Login/Sign up with Facebook</span><span class="bhs_indo">Masuk/Daftar dengan Facebook</span></button>
                  </div>	
                  <div class="form-group clearfix">
                  	<a href="#" class="pull-right wk_style" data-dismiss="modal"><span class="bhs_ingg">Continue as Guest </span><span class="bhs_indo">Lanjutkan sebagai Tamu</span> ></a>
                  </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="forgetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-top:90px;">
      <div class="modal-dialog" role="document" style="width:calc(100% - 20px);max-width: 400px;margin-left: auto;margin-right: auto;">
        <div class="modal-content modal_logreg">
          <div class="modal-body text-center">
          	  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?=static_file()?>images/icon/delette.png" /></button>
              <h4 align="center" class="wk_style">Forget Your Password ?</h4>
                <center><span class="wk_black">Please enter your registered email address and will be send you a new temporary email password.</span></center>
                <br />
                <p id="error_msg" class="hide" style="color:red;"></p>
                <form id="frm_forget" method="post">
                  <div class="form-group">
                    <input type="email" class="form-control" id="email_forget" name="email_forget" required="required" placeholder="Email">
                  </div>
                  <div class="form-group clearfix">
                    <button type="button" id="tombolForget" value="1" class="btn btn-success col-lg-12 col-xs-12">Login</button>
                  </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="registrasiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-top:30px;">
      <div class="modal-dialog" role="document" style="width:calc(100% - 20px);max-width: 400px;margin-left: auto;margin-right: auto;">
        <div class="modal-content modal_logreg">
          <form id="form-register">
          <div class="modal-body text-center">
            	
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?=static_file()?>images/icon/delette.png" /></button>
            <h4 align="center" class="wk_style"><span class="bhs_indo">Selamat datang di WAKULINER</span><span class="bhs_ingg">Welcome to WAKULINER</span></h4>
            <center><span class="wk_black bhs_ingg">please complete the registration form below</span><span class="wk_black bhs_indo">tolong lengkapi formulir pendaftaran dibawah ini</span></center>
            <br />
              <div class="form-group clearfix">
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="<?php echo $place_name;?>">
              </div>
              <div class="form-group clearfix">
                    <input type="email" class="form-control" name="email_register" id="email_register" onkeyup="$('#email_reg_info').html(this.value);" placeholder="<?php echo $place_email;?>">
             </div>
              <div class="form-group clearfix">
                    <input type="text" class="form-control" name="hp" id="hp" onkeyup="$('#hp_reg_info').html(this.value);" placeholder="<?php echo $place_phone;?>">
              </div>
              <div class="form-group clearfix">
                 <select class="form-control" name="jns_kelamin" id="jns_kelamin" placeholder="<?php echo $place_gender;?>">
                    <option value="">Jenis Kelamin</option>
                    <option value="1">Pria</option>
                    <option value="2">Wanita</option>
                 </select>
              </div>
              <div class="form-group clearfix">
                <div class="date" id="datetimepicker1" style="position:relative;">
                    <input type="text" class="form-control datetimepicker" name="ulang_tahun" id="ulang_tahun" placeholder="<?php echo $place_tgllahir;?>">
                </div>
              </div>
              <div class="form-group clearfix">
                    <input type="password" class="form-control" name="pass" id="pass" placeholder="<?php echo $place_pass;?>">
              </div>
              <div class="form-group clearfix">
                    <input type="password" class="form-control" name="conf_pass" id="conf_pass" placeholder="<?php echo $place_confirm;?>">
              </div>
              <div class="form-group">
                    <p><small class="text-muted pull-left"><input type="checkbox" type="checkbox" id="chk_tc" name="chk_tc" /> <span class="bhs_ingg">I have read and accept</span><span class="bhs_indo">Saya sudah baca dan terima</span> <a target="_blank" href="<?php echo site_url('about/index/term');?>"  class="wk_style"><span class="bhs_ingg">Term & Conditions</span><span class="bhs_indo">Syarat dan kententuan</span></a></small></p><Br />
              </div>
          </div>
          <div class="modal-footer footer_reg">
            <button type="button" id="btn_register1" class="btn btn-block btn-lg btn-orange wk_white"><span class="bhs_ingg">REGISTER NOW</span><span class="bhs_indo">DAFTAR SEKARANG</span></button>
          </div>
          </form>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="confRegModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body wk_white text-center">
            	<div>
                	<button type="button" class="btn btn-defaultclose pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <h4><span class="bhs_indo">Apakah nomer HP</span><span class="bhs_ingg">Is</span> <span id="hp_reg_info"></span> <span class="bhs_ingg">and</span><span class="bhs_indo">dan</span> email <span id="email_reg_info"></span> <span class="bhs_indo">right ?</span><span class="bhs_indo">benar ?</span></h4>
                <p><span class="bhs_ingg">We may need to contact you about your order and payment, and provide the best service for you.</span><span class="bhs_indo">Kami mungkin perlu menghubungi anda mengenai pemesanan dan pembayaran anda, dan untuk memberikan pelayanan yang terbaik untuk anda.</span></p>
                
          </div>
          <div class="modal-footer">
            <button type="button" id="btn_register2" class="btn btn-warning pull-left">Ya, Benar</button>
            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Kembali</button>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="errorRegModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body wk_white text-center">
            	<div>
                	<button type="button" class="btn btn-defaultclose pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <h4 id="error_reg_info"></h4>  
          </div>
        </div>
      </div>
    </div>
    
     <div class="modal fade" id="promoWakWiku" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body wk_white text-center">
            	<div>
                	<button type="button" class="btn btn-defaultclose pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
               
                <h4>This section <br/>is coming soon</h4>  
          </div>
        </div>
      </div>
    </div>
    
<script type="text/javascript">
var lang_user = '<?php echo $lang_user;?>';
    function validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	
	$('#tombolLogin').click(function(){
		login();
	});
	function login(){
		var email_login = $('#email_login').val();
		var pass_login = $('#pass_login').val();
		var _msg = '';
		if(email_login == '' || pass_login == ''){
			_msg = 'Masukan email dan password Anda';
			$('#error_msg').html(_msg);
			$('#error_msg').removeClass("hide");
			return false;
		}
		var dt = $('#frm_login').serialize();
		var url = '<?php echo site_url('main');?>';
		
		var msg = '';
		$.ajax({
			data:dt,
			type:'POST',
			url : url,
			success:function(response){	
				var obj = JSON.parse(response);
				msg = obj.message;
				if(obj.status == 0){
					$('#error_msg').html(msg);
					$('#error_msg').removeClass("hide");
				}else{
					if(obj.cart > 0){
						window.location.href = '<?php echo site_url('/payment');?>';
					}else{
						window.location.href = '<?php echo site_url('home');?>';
					}
				}
			}
		})
	}
	$(document).keypress(function(e) {
		if(e.which == 13) {
			login();
		}
	});
	
	$('#btn_register1').click(function(){
		var pass = $('#pass').val();
		var conf_pass = $('#conf_pass').val();
		var email = $('#email_register').val();
		var jns_kelamin = $('#jns_kelamin').val();
		var nama = $('#nama').val();
		var hp = $('#hp').val();
		var msg_nama = 'Nama harus di isi';
		var msg_email = 'Email harus di isi';
		var msg_kelamin = 'Jenis Kelamin harus di isi';
		var msg_phone = 'Handphone harus di isi';
		var msg_pass = 'Password harus di isi';
		var msg_confirm = 'Konfirmasi Password harus di isi';
		if(lang_user == 'bhs_ingg'){
			msg_nama = 'Name is mandatory';
			msg_email = 'Email is mandatory';
			msg_kelamin = 'Gender is mandatory';
			msg_phone = 'Phone number is mandatory';
			msg_pass = 'Password is mandatory';
			msg_confirm = 'Confirm password is mandatory';
		}
		if(nama == ''){
			$('#error_reg_info').html(msg_nama);
			$('#errorRegModal').modal('show');
			return false;
		}
		if(email == ''){
			$('#error_reg_info').html(msg_email);
			$('#errorRegModal').modal('show');
			return false;
		}
		if(!(validateEmail(email))){
			$('#error_reg_info').html('Email tidak valid');
			$('#errorRegModal').modal('show');
			return false;
		}
		if(jns_kelamin == ''){
			$('#error_reg_info').html(msg_kelamin);
			$('#errorRegModal').modal('show');
			return false;
		}
		if(hp == ''){
			$('#error_reg_info').html(msg_phone);
			$('#errorRegModal').modal('show');
			return false;
		}
		if(pass == ''){
			$('#error_reg_info').html(msg_pass);
			$('#errorRegModal').modal('show');
			return false;
		}
		if(conf_pass == ''){
			$('#error_reg_info').html(msg_confirm);
			$('#errorRegModal').modal('show');
			return false;
		}
		if(pass != conf_pass){
			$('#error_reg_info').html('Password dan Konfirmasi Password tidak sesuai');
			$('#errorRegModal').modal('show');
			return false;
		}
		if ($('#chk_tc').prop('checked')==false){ 
			$('#error_reg_info').html('Anda harus menyetujui term & condition');
			$('#errorRegModal').modal('show');
			return false;
		}
		$('#confRegModal').modal('show'); 
	});
	$('#btn_register2').click(function(){
		$('#confRegModal').modal('hide');
		
		var pass = $('#pass').val();
		var nama = $('#nama').val();
		var hp = $('#hp').val();
		var email = $('#email_register').val();
		var jns_kelamin = $('#jns_kelamin').val();
		var ulang_tahun = $('#ulang_tahun').val();
		
		var dt = $('#form-register').serialize();
		var url = '<?php echo site_url('main/reg');?>';
		var msg = '';
		var lang_user = '<?php echo $lang_user;?>';
		$.ajax({
			//data:dt,
			data : {nama : nama, email_register:email, hp:hp, ulang_tahun:ulang_tahun, pass:pass, jns_kelamin:jns_kelamin},
			type:'POST',
			url : url,
			success:function(response){	
				var obj = JSON.parse(response);
				msg = obj.message;
				if(obj.status == 1){
					msg ='\n Halo pencinta kuliner untuk menjaga kualiatas dan keamanan transaksi anda akan menilai merchant dan merchant juga akan menilai anda selamat berbelanja.';
					if(lang_user == 'bhs_ingg'){
						 msg ='\n Hello cullinary lover.To ensure quality and safety of our transactions, tou will rate merchants and merchants will rate you too. Happy shopping.';
					}
					//window.location.href = '<?php echo site_url('home');?>';
					$('#registrasiModal').modal('hide');
				}
				$('#error_reg_info').html(msg);
				$('#errorRegModal').modal({
					backdrop: 'static',
					keyboard: false
				});
				$('#errorRegModal').modal('show');
				if(obj.status == 1){
					//$('#loginModal').modal('show');
					location.reload();
				}
			}
		})
	
	});
	$('#tombolForget').click(function(){
		var email_forget = $('#email_forget').val();
		if(email_forget == ''){
			alert('Masukkan email yang terdaftar');
			return false;
		}
		if(!(validateEmail(email_forget))){
			alert('Email tidak valid');
			return false;
		}
		var url = '<?php echo site_url('main/forget_pass');?>';
		$.ajax({
			data : {email_forget : email_forget},
			url : url,
			type : "POST",
			success:function(response){
				var dt_json = JSON.parse(response);
				var status = dt_json['status'];
				alert(dt_json['message']);
				if(status == 1){
					location.reload();
				}
			}
		});	
	});

	$(function () {
        $('.datetimepicker').datetimepicker({ format: 'DD/MM/YYYY'});
    });
</script>