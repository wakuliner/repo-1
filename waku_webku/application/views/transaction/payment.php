<?php
//print_r($merchant_info);
$merchant_name = '';
$grand_ttl += 0;
$weight = 0;
$weights = 0;
$id_area = 0;
$area_cust = '';
if(!empty($carts)){
	foreach($carts as $c){
		$weights = 0;
		$id_area = $c['kode_pos'];
		$merchant_name = $c['merchant_name'];
		$grand_ttl += $c['subtotal'];
		$kode_pos = $c['kode_pos'];
		$weights = ($c['weight'] * $c['qty']) / 1000;
		$weight += $weights;
	}
}
$style_delivery = '';
$delivery_fee = '0.0';
if($merchant_info->tipe_merchant != 3){
	$style_delivery = ' style="display:none;" ';
	$delivery_fee = $merchant_info->delivery_fee;
}

$lang_user = $this->session->userdata('lang_user');
$place_metode = 'Pilih metode';
$place_address = 'Pilih';
$place_city = 'Pilih Kota';
$place_area = 'Pilih Area';
$place_address = 'Alamat';
if($lang_user == 'bhs_ingg'){
	$place_metode = "Choose metode";
	$place_address = "Choose";
	$place_city = 'Choose City';
	$place_area = 'Choose Area';
	$place_address = 'Address';
}
$lang_user = $this->session->userdata('lang_user');
?>
<style type="text/css">
@media screen and (min-width:1000px)
{
	.col-lg-7.col-xs-12
	{
		width: 58.33333333% !important;
	}
	.col-lg-5.col-xs-12
	{
		width: 41.66666667% !important;
	}
}
.col-lg-4.col-xs-4 {
	width: 33.33333333% !important;
}
@media screen and (min-width:1000px)
{
}
</style>
<form id="frm_payment">
	<input type="hidden" name="ttl_weight" id="ttl_weight" value="<?php echo $weight;?>" />
    <input type="hidden" name="pay_tipe" id="pay_tipe" value="" />
	<section class="wk-container-1 nobg" style="background-color:white;">

	    <div class="loading" align="center" style="display:none;">
            <img src="<?=static_file()?>images/icon/loading.gif" />
        </div>

      <div class="container wk_style" id="body_main" style="padding-left:0px !important; padding-right:0px !important;">

    		<div class="col-lg-7 col-xs-12">
				<div col-lg-12 style="height: auto; border-bottom: 2px solid green;">
             	<h3 class="text-center"><span class="bhs_ingg">Your Order</span><span class="bhs_indo">Pesanan Anda</span></h3>
            </div>

				<div class="row" style="border-bottom: 2px solid green;">
					<div class="col-lg-5 col-xs-8">
                 	<h4 style="color: black"><b><?php echo $merchant_name;?></b></h4>
               </div>
             	<div class="col-lg-3 hidden-xs">
              		<h4 class="pull-right">Sub Total</h4>
             	</div>
             	<div class="col-lg-4 col-xs-4">
               	<h4 class="pull-right" style="color: black; margin-right: 3px;">Rp. <?php echo number_format($grand_ttl,0,'.','.');?></h4>
               </div>
				</div>

				<div class="form-group">
				  <div class="col-lg-12 col-sm-12 col-xs-12" style="margin-top: 10px">
					  <h5><b><span class="bhs_indo">PILIH METODE PENGIRIMAN</span><span class="bhs_ingg">CHOOSE DELIVERY METHOD</span></b></h5>
						  <h6><span class="bhs_indo">Pilih salah satu</span><span class="bhs_ingg">Choose one</span></h6>
						  <div class="row">

							  <div class="col-lg-6 col-sm-6 col-xs-12">
								  <ul class="nav nav-pills">
										<?php if($merchant_info->take_away > 0 || $merchant_info->take_away == 0) {
								  			if($merchant_info->take_away > 0){ ?>

										  		<li class="takes" id="2" role="tab" data-toggle="tab" data-target="#take">
											  		<a href="#" id="btnTakeAway" style="background-color: #ffffff;"><img id="imgTakeAway" style="margin-left: 5px;" src="<?=static_file()?>images/icon/takeawayon.png" /><br /><span class="bhs_indo" style="color: #00AA0B;">Diambil</span><span class="bhs_ingg" style="color: #00AA0B;">Take Away</span></a></li>
								<?php } else{ ?>
								  				<li class="">
											  		<img style="margin-top:10px; margin-left: 8px;" id="imgTakeAway" src="<?=static_file()?>images/icon/takeawayoff.png" /> <br /><span class="bhs_indo">Diambil</span><span class="bhs_ingg">Take Away</span></li>
							 <?php }	}
							  if($merchant_info->delivery > 0 || $merchant_info->delivery == 0) {
								  if($merchant_info->delivery > 0) {?>
											  <li class="takes" id="<?php echo $delivery_fee;?>" role="tab" data-toggle="tab" data-target="#delivery">
												  <a href="#" id="btnDeliv" style="background-color: #ffffff;"><img style="margin-left: -1px;" id="imgDeliv" src="<?=static_file()?>images/icon/delivon.png" /> <br /><span class="bhs_ingg" style="color: #00AA0B;">Delivery</span><span class="bhs_indo" style="color: #00AA0B;">Dikirim</span></a></li>
									 <?php }else{ ?>
									  <li class="">
												  <img style="margin-left: -1px; margin-top:10px;" id="imgDeliv" src="<?=static_file()?>images/icon/delivoff.png" /> <br /><span class="bhs_ingg">Delivery</span><span class="bhs_indo">Dikirim</span></li>
						  <?php } } ?>
								  </ul>
							  </div>
							  <div class="col-lg-6 col-sm-6" id="tulisan_delivery" style="position:absolute; bottom: 0; right: 0; display:none;">
								  <div class="col-lg-6 col-sm-6">
									  <h4 style="margin-left: -8px">Delivery Fee</h4>
								  </div>
								  <div class="col-lg-6 col-sm-6 pull-right">
									  <h4 class="pull-right"><b id="tulisan_delivery_fee"></b></h4>
								  </div>
							  </div>
						  </div>

						  <div class="tab-content" style="padding:0px !important;">
								<div role="tabpanel" class="tab-pane" id="take" style="background-color: #E5FBE5; border-bottom: 2px solid green;">
										<div class="row">
											<div class="col-lg-12 col-xs-8">
											  	<h5>Pemesan</h5>
											  	<h5><b><?php echo strtoupper ($this->session->userdata('name'));?></b></h5>
											  	<h5><?php echo $this->session->userdata('handphone');?></h5>
												<br />
											</div>

											<div class="col-lg-12 col-xs-12">
												<h4><b><span class="bhs_indo">Harap ambil pesanan Anda di:</span><span class="bhs_ingg">Please pick up your order at:</span></b></h4><br />
											</div>

											<div class="col-lg-12 col-xs-12">
												<div class="row">
													<div class="col-lg-1 col-xs-1 text-center">
  													  	<img src="<?=static_file()?>images/icon/address.png" />
  												 	</div>
  												 	<div class="col-lg-11 col-xs-1">
  													  	<h4><b><?php echo $merchant_name;?></b></h4>
  													  	<h5><?php echo $merchant_info->address;?></h5>
  													  	<!--<h5>Kelurahan Gambir, Kecamatan Gambir</h5>
  													  	<h5>Jakarta Pusat 123444</h5> -->
  												 	</div>
												</div>
											</div>
										  </div>
								</div>


								<div role="tabpanel" class="tab-pane col-lg-12 col-xs-12" id="delivery" style="background-color: #E5FBE5;" <?php echo $style_delivery;?> >
								  	<div class="col-lg-12 col-xs-6">
									  <h5><b><span class="bhs_ingg">Select Courier Agency</span><span class="bhs_indo">Pilih Kurir</span></b></h5>
									</div>
									<div class="col-lg-4 col-xs-6">
										<select class="form-control" name="nama_kurir" id="nama_kurir">
											<option value="">-Pilih-</option>
											  <?php $ii=0;
								  	  			foreach($merchant_info->kurir_service as $k){
									  				if($ii == $k->id_kurir){
										  				echo '<option value="'.$k->id_kurir.'">'.$k->nama_kurir.'</option>';
									  				}
									  				$ii++;
								  				}?>
										</select>
									</div>
									<div class="col-lg-12 col-xs-6">
										<h5><b><span class="bhs_ingg">Choose Service</span><span class="bhs_indo">Pilih Servis</span></b></h5>
									</div>
									<div class="col-lg-4 col-xs-6">
									  <select class="form-control" name="kurir_services" id="kurir_services">
											<option value="">- <?php echo $place_address;?> -</option>
										</select>
									</div>
								</div>
						  </div>
					 </div>
				</div>

				<div class="col-lg-12 col-xs-12" id="input_promo" style="margin-bottom: 10px; display: none;">
					<div class="row" style="border-top: 2px solid green;">
						<div class="col-lg-3 col-lg-offset-6 col-xs-4 col-xs-offset-2" style="margin-top: 15px;">
							<h4 style="margin-left: 7px;">Promo Code</h4>
						</div>
						<div class="col-lg-3 col-xs-6" style="margin-top: 15px; padding-left: 0px !important; padding-right: 0px !important;">
							<div class="input-group">
                          <input type="text" class="form-control" name="promo_codes" id="promo_codes" >
                          <span class="input-group-btn">
                            <button id="btn_promo" type="button" class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button>
                          </span>
                     </div>
							<input type="hidden" class="form-control" name="promo_code" id="promo_code" />
							<input type="hidden" name="type_voucher" id="type_voucher" />
							<input type="hidden" name="voucher_amount" id="voucher_amount" />
							<input type="hidden" name="type_nilai_promo" id="type_nilai_promo" />
							<input type="hidden" name="id_kota_merchant" id="id_kota_merchant" value="<?php echo $id_kota;?>" />
						</div>
						<div class="col-lg-3 col-xs-6 pull-right">
							<h4 class="pull-right"><b id="jml_promo"></b></h4>
						</div>
					</div>
				</div>

            <div class="form-group">
					<div class="col-lg-12 col-xs-12" >
						<div class="row" style="margin-bottom: 10px; border-top: 2px solid green; padding-left: 15px;">
							<div class="col-lg-12 col-xs-12" >
								<h5><b><span class="bhs_indo">PILIH TIPE PEMBAYARAN</span><span class="bhs_ingg">CHOOSE PAYMENT TYPE</span></b></h5>
							</div>
							<div class="col-lg-12 col-xs-12" >
								<h6><span class="bhs_indo">Pilih satu</span><span class="bhs_ingg">Choose one</span></h6>
							</div>
                     <?php if($statusCari == 1){ ?>
                     <div class="col-lg-12 col-xs-11">
                     	<input type="radio" name="pay" value="cash" onclick="payment_type(1);"/> Cash
                     </div>
                     <?php } ?>
                     <div class="col-lg-12 col-xs-11">
                     	<input type="radio" name="pay" value="transfer" onclick="payment_type(2);" /> Bank Transfer
                 		</div>
                     <div class="col-lg-12 col-xs-11">
                         <input type="radio" name="pay" value="online" onclick="payment_type(3);" /> Online
                     </div>
	               </div>
				 	</div>
            </div>

		      <div class="form-group" id="cash_ol" style="display: none;";>
		       	<div class="col-lg-12 col-xs-12">
	               <div class="col-lg-12 col-xs-12">
	                   <h6><span class="bhs_indo">Pilih metode</span><span class="bhs_ingg">Choose methode</h6>
	               </div>
	               <div class="col-lg-4 col-xs-8" style="margin-bottom: 10px">
	                   <select disabled="disabled" class="form-control" name="payment_metode" id="payment_metode">
	                   <option value="0">-<?php echo $place_metode;?>-</option>
	                   <?php
						if(!empty($payment_channel->payment_channel)){
							foreach($payment_channel->payment_channel as $pc){
								echo '<option value="'.$pc->payment_id.'Þ'.$pc->type_fee.'Þ'.$pc->fee.'">'.$pc->channel_name.'</option>';
							}
						}
					?>
	                   </select>
	               </div>
						<div class="col-lg-6 col-sm-6 col-lg-offset-2 col-sm-offset-2" id="tulisan_adminfee" style="position:absolute; bottom: 0; right: 0; display: none;">
						   <div class="col-lg-6 col-sm-6">
							   <h4 style="margin-left: -8px">Admin Fee</h4>
						   </div>
						   <div class="col-lg-6 col-sm-6 pull-right">
							   <h4 class="pull-right"><b id="adminfee"></b></h4>
						   </div>
						</div>
	               <input type="hidden" name="input_admFee" id="input_admFee" value="" />
						<input type="hidden" name="input_deliveryFee" id="input_deliveryFee" value="0"/>
	               <input type="hidden" name="input_deliveryType" id="input_deliveryType" value="0"/>
	               <input type="hidden" name="input_deliveryType2" id="input_deliveryType2" value="0"/>
		         </div>
		      </div>

				<div class="col-md-12 col-xs-12" style="margin-bottom: 5px;">
					<div class="row" style="background-color: green; border-radius: 3px;">
						<div class="col-md-8 col-xs-7">
                    	<b><h4 style="color: white;" class="pull-right">GRAND TOTAL</h4></b>
                  </div>
						<div class="col-md-4 col-xs-5">
                  	<b><h4 style="color: white;" class="pull-right grd_ttl">Rp. <?php echo number_format($grand_ttl,0,'.','.');?></h4></b>
       					<input type="hidden" name="grandTtl" value="<?php echo $grand_ttl;?>" id="grandTtl" />
                  </div>
					</div>
				</div>
         </div>

         <div class="col-lg-5 col-xs-12" style="padding-top:10px;">
				<div class="col-lg-12 hidden-xs" style="padding-top:43px;"></div>
				<div class="col-lg-12 col-xs-12">
					<div class="panel panel-success" id="cus_loc" style="display: none;">
	             	<div class="panel-body">
	                 	<h4><span class="bhs_ingg">Customer</span><span class="bhs_indo">Pelanggan</span></h4>
	                     <div class="clearfix">
	                     	<div class="col-lg-1 col-xs-2"><i class="glyphicon glyphicon-user"></i></div>
	                         <div class="col-lg-11 col-xs-10 lbl_first_name">-</div>
	                     </div>
	                     <div class="clearfix">
	                     	<div class="col-lg-1 col-xs-2"><i class="glyphicon glyphicon-phone"></i></div>
	                         <div class="col-lg-11 col-xs-10 lbl_hp"></div>
	                     </div>
	                     <div class="clearfix">
	                         <div class="col-lg-1 col-xs-2">
	                             <i class="glyphicon glyphicon-map-marker"></i>
	                         </div>
	                         <div class="col-lg-6 col-xs-6">
	                             <select class="form-control" name="address" id="address">
	                             	<option value="">- <?php echo $place_address;?> -</option>
	                                 <?php
										$area_cust = '';
										if(!empty($address->address)){
											foreach($address->address as $a){
												$_val = $a->id_address.'Þ'.$a->first_name.'Þ'.$a->handphone.'Þ'.$a->address.'Þ'.$a->detail.'Þ'.$a->city.'Þ'.$a->id_area;
												if($id_area == $a->id_area){
													$area_cust = $a->id_area;
													echo '<option selected="selected" value="'.$_val.'">'.$a->address.'</option>';
												}else{
													echo '<option value="'.$_val.'">'.$a->address.'</option>';
												}
											}
										}
									?>
	                             </select>
	                         </div>
	                         <div class="col-lg-5 col-xs-4">
	                             <a href="#" class="wk_style" data-toggle="modal" data-target="#addressModal"><img src="<?=static_file()?>images/icon/addnewaddress.png" /> <span class="bhs_ingg">New Address</span><span class="bhs_indo">Alamat Baru</span></a>
	                         </div>
	                     </div>
	                     <br />
	                     <div class="col-lg-12 col-lg-offset-1 lbl_address">
	                     	-
	                     </div>
	                 </div>
	             </div>
				</div>

				<div class="col-lg-12 col-xs-12">
	            <div class="panel panel-success">
	             	<div class="panel-body">
	                 	<div class="col-lg-2"><b><span class="bhs_ingg">Note</span><span class="bhs_indo">Catatan</span></b></div>
	                     <div class="col-lg-10">
	                     	<!--<p class="lbl_note">-</p>-->
	                         <textarea class="form-control" name="notes_alamat"></textarea>
	                     </div>
	                 </div>
	             </div>
				 </div>
         </div>

			<div class="col-md-6 col-md-offset-3 col-xs-12" style="margin-bottom: 20px; margin-top: 50px">
					<a id="btn_place_order" class="btn btn-success btn-lg btn-block searchgo">PLACE ORDER</a>
			</div>
     </div>
	</section>
</form>

	 <div class="modal fade" id="confirModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	   <div class="modal-dialog modal-sm" role="document">
	     <div class="modal-content modal-city">
	       <div class="modal-body wk_white text-center">
	             <h4 id="teksConfir"><span class="bhs_ingg">Do you really want to order this order ?</span><span class="bhs_indo">Apakah anda sungguh ingin memesan pesanan ini ?</span></h4>
	       </div>
	       <div class="modal-footer">
	         <button type="button" class="btn btn-warning pull-left btn_confir"><i class="glyphicon glyphicon-ok"></i> Ya</button>
	         <button type="button" class="btn btn-warning pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Tidak</button>
	       </div>
	     </div>
	   </div>
	 </div>

    <div class="modal fade" id="confirModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body wk_white text-center">
                <h4 id="teksConfir"><span class="bhs_ingg">Do you really want to order this order? You can not cancel this order after we submit it to the merchant.</span><span class="bhs_indo">Apakah anda sungguh ingin memesan pesanan ini ? Anda tidak dapat membatalkan pesanan ini setelah pesanan kami serahkan ke merchant.</span></h4>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning pull-left btn_confir2"><i class="glyphicon glyphicon-ok"></i> Ya</button>
            <button type="button" class="btn btn-warning pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Tidak</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <form id="frm_address">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><span class="bhs_ingg">Add Address</span><span class="bhs_indo">Tambah Alamat</span></h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
            	<select class="form-control" name="id_kota" onchange="changeArea(this.value);">
                  <option><?php echo $place_city;?></option>
                  <?php foreach($city_list->city as $ct):?>
                  	<option value="<?=$ct->id_kota?>"><?=$ct->nama_kota?></option>
				  <?php endforeach; ?>
                </select>
            </div>
            <input type="hidden" name="id_address" id="id_address" value="0"  />
            <div class="form-group hide" id="selArea">
            </div>
            <div class="form-group">
            	<textarea class="form-control" rows="3" name="address" id="addressku" placeholder="<?php echo $place_address;?>"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-inverse pull-left" data-dismiss="modal"> <i class="glyphicon glyphicon-remove"></i> <span class="bhs_ingg">Cancel</span><span class="bhs_indo">Batal</span></button>
           <button type="button" id="save_address" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> <span class="bhs_ingg">Save</span><span class="bhs_indo">Simpan</span></button>
          </div>
        </div>
      </div>
      </form>
    </div>

	 <div class="modal fade" id="errorMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	    <div class="modal-dialog modal-sm" role="document">
	       <div class="modal-content modal-city">
	          <div class="modal-body wk_white text-center">
	             <div>
	                <button type="button" class="btn btn-defaultclose pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	             </div>
	             <h4 class="msg_cart"></h4>
	          </div>
	       </div>
	    </div>
	 </div>

<script>
var lang_user = '<?php echo $lang_user;?>';
address_change();

function showMsg(indo, eng){
   var msg = indo;
   if(lang_user == 'bhs_ingg'){
      msg = eng;
   }
   $('.msg_cart').html(msg);
   $('#errorMsg').modal('show');
}
$('#btn_promo').click(function(){
	var promo_code = $('#promo_codes').val();
	var kode_pos = '<?php echo $kode_pos;?>';
	$('#type_voucher').val('');
	$('#voucher_amount').val('');
	$('#type_nilai_promo').val('');
	if(promo_code == '' || promo_code.length < 1){
		alert('Promo Code harus di isi');
		return false;
	}
	var url = '<?php echo site_url('payment/chk_promo');?>';
	$.ajax({
		url : url,
		type:"POST",
		data : {kode_pos : kode_pos, promo_code:promo_code},
		success: function(data){
			var res = JSON.parse(data);
			if(res.length != ''){
				$('#promo_code').val(promo_code);
				$('#type_voucher').val(res.type_voucher);
				$('#voucher_amount').val(res.value_promo);
				$('#type_nilai_promo').val(res.type_nilai_promo);
				alert('Promo code valid');
				var input_admFee = $('#input_admFee').val();
				var input_deiveryFee = $('#input_deliveryFee').val();
				var grand_ttl = '<?php echo $grand_ttl;?>';
				var voucher_amount = $('#voucher_amount').val();
				$('#jml_promo').text('-Rp. '+numberWithCommas(voucher_amount));
				hitung_cart(grand_ttl, input_admFee, input_deiveryFee, voucher_amount);
			}else{
				var msg = 'Promo code tidak valid';
				if(lang_user == 'bhs_ingg'){
					msg = 'promo code not valid';
				}
				alert(msg);
			}

		}
	});
});

$('.takes').click(function(){
	var id = $(this).get(0).id;
	$('#input_deliveryType2').val(1);
	$('#input_deliveryType').val(id);
	$('.deliveryFee').text('Rp. '+numberWithCommas(id));
	$('#input_deliveryFee').val(id);
	var input_admFee = $('#input_admFee').val();
	var input_deiveryFee = $('#input_deliveryFee').val();
	var grand_ttl = '<?php echo $grand_ttl;?>';
	var voucher_amount = $('#voucher_amount').val();
	if(id == 2){ //untuk button take away
		$('#cus_loc').hide();
		$('#input_promo').show();
		id = '0.0';
		$('#input_deliveryType2').val(2);
	}
	else { //untuk button delivery
		$('#cus_loc').show();
		$('#input_promo').show();
		<?php if($statusCari == 1){ ?>
			$('#delivery').hide();
		<?php } ?>
		$('#tulisan_delivery_fee').text('Rp. '+numberWithCommas(input_deiveryFee));
		$('#tulisan_delivery').show();
	}
	hitung_cart(grand_ttl, input_admFee, input_deiveryFee, voucher_amount);
});
$("#payment_metode").change(function(){
	$('.adm_fee').text('Rp. 0.00');

	var val = $("#payment_metode option:selected").val();
	var dt = val.split('Þ');
	var payment_id = dt[0];
	var type = dt[1];
	var fee = dt[2];
	var grand_ttl = '<?php echo $grand_ttl;?>';
	$('.ttl_cart').text('Rp. '+numberWithCommas(grand_ttl));
	$('.grd_ttl').text('Rp. '+numberWithCommas(grand_ttl));
	var input_deiveryFee = $('#input_deliveryFee').val();
	if(type == 2){
		var total = Number(grand_ttl) + Number(fee);
		$('.adm_fee').text('Rp. '+numberWithCommas(fee));
		$('#input_admFee').val(fee);
		$('.ttl_cart').text('Rp. 0.00');
	}
	if(type == 1){
		var _fee = Number(grand_ttl) * (Number(fee)/100);
		$('#input_admFee').val(_fee);
		var total = Number(grand_ttl) + Number(_fee);
		$('.adm_fee').text('Rp. '+numberWithCommas(_fee));
	}
	var input_admFee = $('#input_admFee').val();
	var voucher_amount = $('#voucher_amount').val();
	$('#adminfee').text('Rp. '+numberWithCommas(input_admFee));
	$('#tulisan_adminfee').show();
	hitung_cart(grand_ttl, input_admFee, input_deiveryFee, voucher_amount);
});

function payment_type(val){
	$('.adm_fee').text('Rp. 0.00');
	$('#pay_tipe').val(val);
	var grand_ttl = '<?php echo $grand_ttl;?>';
	var input_admFee = $('#input_admFee').val();
	var voucher_amount = $('#voucher_amount').val();
	var input_deiveryFee = $('#input_deliveryFee').val();
	if(val == 1){
		$('#cash_ol').hide();
		$('#input_admFee').val(0);
		$('select[name="payment_metode"]').attr('disabled', true);
		$('select[name="payment_metode"]').prop('selectedIndex', '0');
	}
	if(val == 2){
        $('#cash_ol').hide();
        $('#input_admFee').val(0);
        $('select[name="payment_metode"]').attr('disabled', true);
        $('select[name="payment_metode"]').prop('selectedIndex', '0');
	}
    if(val == 3){
        $('select[name="payment_metode"]').attr('disabled', false);
        $('select[name="payment_metode"]').prop('selectedIndex', '0');
        $('#cash_ol').show();
		  $('#tulisan_adminfee').hide();
    }
	var input_admFee = $('#input_admFee').val();
	hitung_cart(grand_ttl, input_admFee, input_deiveryFee, voucher_amount);
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

$('#address').change(function(){
	address_change();
});

function address_change(){
	$('#nama_kurir').val('');
	$('#kurir_services').val('');
	$('#input_deliveryFee').val(0);
	$('.deliveryFee').text('Rp. 0.0');
	$('.lbl_first_name').text('-');
	$('.lbl_hp').text('-');
	$('.lbl_address').text('-');
	$('.lbl_note').text('-');
	var val = $('#address option:selected').val();
	var input_admFee = $('#input_admFee').val();
	var voucher_amount = $('#voucher_amount').val();
	var grand_ttl = '<?php echo $grand_ttl;?>';
	var id_area = '<?php echo $id_area;?>';
	var input_deiveryFee = $('#input_deliveryFee').val();
	hitung_cart(grand_ttl, input_admFee, input_deiveryFee, voucher_amount);
	if(val !=''){
		var dt = val.split('Þ');
		var id_address = dt[0];
		var first_name = dt[1];
		var hp = dt[2];
		var address = dt[3];
		var detail = dt[4];
		var id_area_cust = dt[6];
		if(id_area != id_area_cust){
			$('#address').prop('selectedIndex', 0);
			$('#addressModal').modal('show');
		}else{
			$('.lbl_first_name').text(first_name);
			$('.lbl_hp').text(hp);
			$('.lbl_address').text(address);
			$('.lbl_note').text(detail);
		}
	}else{
		$('#address').prop('selectedIndex', 0);
		$('#addressModal').modal('show');
	}
}
$('#nama_kurir').change(function(){
	var html = '';
	var dt_address = $('#address').val();
	var weight = $('#ttl_weight').val();
	if(weight < 1){
		weight = 1;
	}
	if(dt_address == ''){
		$('#nama_kurir').val('');
		alert('Silahkan pilih alamat customer');
		return false;
	}
	var dt = dt_address.split('Þ');
	var id_kota_cust = dt[5];

	html = '<option value="">-Pilih-</option>';
	var val = $('#nama_kurir').val();
	var url = '<?php echo site_url('payment/kurir_services');?>';
	var id_kota_merchant = $('#id_kota_merchant').val();
	$.ajax({
		url : url,
		type:"POST",
		data : {id_kurir : val, id_kota_cust:id_kota_cust, id_kota_merchant : id_kota_merchant, weight:weight},
		beforeSend: function(){
			var wait = '<option value="">Waiting ...</option>';
			$('#kurir_services').html(wait);
			},
		success: function(data){

			html +=data;
			$('#kurir_services').html(html);
		}
	});

});

$('#kurir_services').change(function(){
	var val = $(this).val();
	var dt = val.split('Þ');
	var deliveryFee = dt[1];
	$('#input_deliveryFee').val(deliveryFee);
	$('.deliveryFee').text('Rp. '+numberWithCommas(deliveryFee));
	var input_admFee = $('#input_admFee').val();
	var voucher_amount = $('#voucher_amount').val();
	var grand_ttl = '<?php echo $grand_ttl;?>';
	var input_deiveryFee = $('#input_deliveryFee').val();
	hitung_cart(grand_ttl, input_admFee, input_deiveryFee, voucher_amount);
});

$('#btn_place_order').click(function(){
	var isChecked = $('input[name=pay]').prop('checked');
	var pay = $('#pay_tipe').val();
	var deliveryType = $('#input_deliveryType2').val();
	var address = $('#address option:selected').val();
	var valku = $("#payment_metode option:selected").val();
	if(pay == '' || pay < 1){
		var indo = 'Silahkan pilih pembayaran';
		var eng = 'Please choose payment type';
		showMsg(indo, eng);
		return false;
	}
	if(pay == 3){
		if(valku == '' || valku < 1){
			var indo = 'Silahkan pilih metode pembayaran';
	      var eng = 'Please choose payment method';
	      showMsg(indo, eng);
			return false;
		}
	}
	if(address == ''){
		var indo = 'Silahkan pilih alamat customer';
		var eng = 'Please choose address';
		showMsg(indo, eng);
		return false;
	}
	if(deliveryType == '' || deliveryType < 1){
		var indo = 'Silahkan pilih jenis delivery';
		var eng = 'Please choose delivery type';
		showMsg(indo, eng);
		return false;
	}
	$('#confirModal').modal('show');
});

$('.btn_confir').click(function(){
	$('#confirModal').modal('hide');
	$('#confirModal2').modal('show');
});

$('.btn_confir2').click(function(){
	$('#confirModal2').modal('hide');
	submit_payment();
});

function submit_payment()
{
	var data_payment = $('#frm_payment').serialize();
   //var grand_ttl = $('#grandTtl').val();
	var url = '<?php echo site_url('payment/submit_payment');?>';

	$.ajax({
		url : url,
		type:"POST",
		data : data_payment,
		beforeSend: function(){
			$('#body_main').hide();
			$('.loading').show();
		},
		success: function(data){
			if(data != ''){
				$('.loading').hide();
				$('#body_main').show();
				var indo = 'Sukses';
		      var eng = 'Success';
		      showMsg(indo, eng);
				window.location = data;
			}
		}
	});
}
function hitung_cart(hrg_makanan, adm_fee, delivery_fee, voucher_ammount){
	var total = (Number(hrg_makanan) + Number(adm_fee) + Number(delivery_fee)) - Number(voucher_ammount);
	$('.ttl_cart').text('Rp. '+numberWithCommas(total));
	$('.grd_ttl').text('Rp. '+numberWithCommas(total));
	$('#grandTtl').val(total);
	return total;
}

$('#save_address').click(function(){
	var dt = $('#frm_address').serialize();
	var url = '<?php echo site_url('setting/save_edit_address');?>';

	$.ajax({
		data : dt,
		url : url,
		type : "POST",
		success:function(response){
			if(response > 0){
				var indo = 'Berhasil mengubah profil';
				var eng = 'Edit profile successful';
				showMsg(msg);
				location.reload();
			}
		}
	});
});

function changeArea(val, selected_val)
{
	$.ajax({
		type:'POST',
		data : {id_kota : val, selected_val:selected_val},
		url : '<?php echo site_url('setting/get_area');?>',
		success:function(response){
			$('#selArea').removeClass("hide");
			$('#selArea').html(response);
		}
	});
}
</script>
