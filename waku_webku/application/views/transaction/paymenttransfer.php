<section class="wk-container-1 nobg" style="background-color: white">
   <div class="container wk_style">
      <h3 class="text-center"><b>Bank Transfer</b></h3>
      <h6 class="text-center" style="color: black; margin-top: 30px">Harap melakukan transfer pembayaran dalam waktu <b>24 jam</b> sebesar</h6>
      <h3 class="text-center"><b>Rp. <?php echo number_format($grand_ttl,0,'.','.');?>,-</b></h3>
      <h5 class="text-center" style="color: black; margin-top: 30px">ke salah satu rekening dibawah ini</h5>
      <?php foreach ($bank as $b){ ?>
      <div class="col-md-12 col-xs-12" style="margin-top:20px; margin-bottom:30px">
         <div class="col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 hidden-xs text-right">
            <img src="<?php echo $b['logo']; ?>" style="width: 180px">
            <div class="col-md-6 col-md-offset-6 col-sm-12 col-xs-12 text-center">
               <h5 style="color: black;">Bank <b><?php echo $b['nama_bank']; ?></h5>
            </div>
            <br />
         </div>
         <div class="col-md-5 col-sm-5 hidden-xs text-left" style="padding-right: 55px;">
            <h5 style="color: black;">No Rek <b><?php echo $b['no_rekening']; ?></b></h5>
            <h5 style="color: black;">Cab. <b><?php echo $b['cabang']; ?></b></h5>
            <h5 style="color: black;">a/n <b><?php echo $b['nama_pemilik']; ?></b></h5>
         </div>

         <div class="col-xs-12 visible-xs text-center">
            <img src="<?php echo $b['logo']; ?>" style="width: 160px">
         </div>
         <div class="col-xs-12 visible-xs text-center" style="margin-top: 10px;">
            <h5 style="color: black;">Bank <b><?php echo $b['nama_bank']; ?></h5>
            <h5 style="color: black;">No Rek <b><?php echo $b['no_rekening']; ?></b></h5>
            <h5 style="color: black;">Cab. <b><?php echo $b['cabang']; ?></b></h5>
            <h5 style="color: black;">a/n <b><?php echo $b['nama_pemilik']; ?></b></h5>
         </div>


      </div>
      <?php }; ?>
      <div class="col-md-12 text-center" style="margin-top:20px; margin-bottom:30px">
         <h6 style="color: black;">Setelah Anda melakukan pembayaran,</h6>
         <h6 style="color: black;">klik tombol dibawah ini untuk konfirmasi pembayaran Anda,</h6>
         <h6 style="color: black;">agar pembelian Anda dapat segera di proses</h6>
         <button id="konfirmasi" name="konfirmasi" class="btn btn-success btn-lg searchgo" style="margin-top: 10px"><b>Konfirmasi Pembayaran</b></button>
      </div>
   </div>
</section>

<script>
   $('#konfirmasi').click(function(){
      var url = '<?php echo site_url('payment/konfirmasi');?>';
      window.location = url + "/" + <?php echo $purchase_id;?> + "/" + <?php echo $grand_ttl;?>;
   });
</script>
