<style>
select.input-lg {
  -webkit-appearance: none;
  -moz-appearance: none;
  -o-appearance: none;
  /* no standardized syntax available, no ie-friendly solution available */
}
select + i.fa {
  float: right;
  margin-top: -40px;
  margin-right: 8px;
  /* this is so when you click on the chevron, your click actually goes on the dropdown menu */
  pointer-events: none;
  /* everything after this is just to cover up the original arrow */
  /* (for browsers that don't support the syntax used above) */
  background-color: #fff;
  padding-right: 5px;
}
select option.service-small {
    font-size: 14px;
}
.form-control {
    line-height: 1 !important;
}
body .modal-c1 {
    background-color: #73F07B;
}
</style>
<section class="wk-container-1 nobg" style="background-color: white">
   <div class="container wk_style">
      <div class="col-md-12 hidden-xs" style="margin-top: 50px; margin-bottom: 80px;">
         <div class="row">
            <div class="col-md-6 text-center">
               <h4><b>Konfirmasi Pembayaran</b></h4>
               <br/>
               <h5 style="padding-left: 50px; padding-right: 50px">Pembelian Anda akan segera diproses setelah melakukan konfirmasi pembayaran</h5>
               <div class="col-md-12 form-group" style="padding: 30px;">
                  <h4 class="text-left" style="margin-left: 15px"><b>Wakuliner Order ID<span style="color: #FF0000">*</span></b></h4>
                  <div class="form-group has-success has-feedback" style="margin-bottom: 30px;">
                     <input type="text" class="form-control input-lg" id="orderid" value='<?php echo $this->uri->segment(3); ?>'>
                  </div>
                  <h4 class="text-left" style="margin-left: 15px"><b>Bank Tujuan<span style="color: #FF0000">*</span></b></h4>
                  <div class="form-group has-success has-feedback" style="margin-bottom: 30px;">
                     <select class="form-control input-lg" id="banktujuan">
                        <option style="display:none" value="" disabled selected></option>
                        <?php foreach ($bank as $b){ ?>
                        <option value="<?php echo $b['nama_bank']; ?>">Bank <?php echo $b['nama_bank']; ?></option>
                        <?php };?>
                     </select>
                     <i class="fa fa-sort-desc fa-2x" aria-hidden="true"></i>
                  </div>
                  <h4 class="text-left" style="margin-left: 15px"><b>Bank Anda<span style="color: #FF0000">*</span></b></h4>
                  <div class="form-group has-success has-feedback" style="margin-bottom: 30px;">
                     <input type="text" class="form-control input-lg" id="bankanda">
                  </div>
                  <h4 class="text-left" style="margin-left: 15px"><b>Rekening atas nama<span style="color: #FF0000">*</span></b></h4>
                  <div class="form-group has-success has-feedback" style="margin-bottom: 30px;">
                     <input type="text" class="form-control input-lg" id="an_rekening">
                  </div>
                  <h4 class="text-left" style="margin-left: 15px"><b>Tanggal Transfer<span style="color: #FF0000">*</span></b></h4>
                  <div class="form-group has-success has-feedback" style="margin-bottom: 30px;">
                     <input type="text" class="form-control input-lg" id="tgl_transfer">
                     <span class="form-control-feedback"><img src="<?=static_file()?>images/icon/pastordercalender.png"></span>
                  </div>
                  <h4 class="text-left" style="margin-left: 15px"><b>Jumlah Nominal Pembayaran<span style="color: #FF0000">*</span></b></h4>
                  <div class="form-group has-success has-feedback" style="margin-bottom: 15px;">
                     <input type="text" class="form-control input-lg" id="total" value='<?php echo $this->uri->segment(4); ?>'/>
                  </div>
                  <h5 style="padding-left: 50px; padding-right: 50px; margin-bottom: 20px;">Anda akan mendapat notifikasi dari kami setelah pembayaran Anda berhasil kami verifikasi</h5>
                  <button type="submit" class="btn btn-success btn-block btn-lg searchgo" id="btn_konfirmasi"><b>KIRIM KONFIRMASI</b></button>
               </div>

            </div>
            <div class="col-md-6 text-center" style="background-color: #E5FBE5; padding-top:20px;">
               <h5>INFORMASI REKENING WAKULINER</h5>
               <br/>
               <?php foreach ($bank as $b){ ?>
               <h4><b>Bank <?php echo $b['nama_bank']; ?></b></h4>
               <h5 style="color: black;">No Rek <b><?php echo $b['no_rekening']; ?></b></h5>
               <h5 style="color: black;">Cab. <b><?php echo $b['cabang']; ?></b></h5>
               <h5 style="color: black;">a/n <b><?php echo $b['nama_pemilik']; ?></b></h5>
               <br/>
               <?php };?>
            </div>
         </div>
      </div>

      <div class="col-xs-12 visible-xs" style="margin-top: 50px; margin-bottom: 80px;">
         <div class="row" style="padding-right:0px !important; padding-left:0px !importtant">
            <div class="col-xs-12 text-center">
               <h4><b>Konfirmasi Pembayaran</b></h4>
               <br/>
               <h5>Pembelian Anda akan segera diproses setelah melakukan konfirmasi pembayaran</h5>
               <div class="col-xs-12 form-group">
                  <h4 class="text-left" style="margin-left: 15px"><b>Wakuliner Order ID<span style="color: #FF0000">*</span></b></h4>
                  <div class="form-group has-success has-feedback" style="margin-bottom: 30px;">
                     <input type="text" class="form-control input-lg" id="orderid_xs" value='<?php echo $this->uri->segment(3); ?>'>
                  </div>
                  <h4 class="text-left" style="margin-left: 15px"><b>Bank Tujuan<span style="color: #FF0000">*</span></b></h4>
                  <div class="form-group has-success has-feedback" style="margin-bottom: 30px;">
                     <select class="form-control input-lg service-small" id="banktujuan_xs">
                        <option style="display:none" value="" disabled selected></option>
                        <?php foreach ($bank as $b){ ?>
                        <option class="service-small" value="<?php echo $b['nama_bank']; ?>">Bank <?php echo $b['nama_bank']; ?></option>
                        <?php };?>
                     </select>
                     <i class="fa fa-sort-desc fa-2x" aria-hidden="true"></i>
                  </div>
                  <h4 class="text-left" style="margin-left: 15px"><b>Bank Anda<span style="color: #FF0000">*</span></b></h4>
                  <div class="form-group has-success has-feedback" style="margin-bottom: 30px;">
                     <select class="form-control input-lg" id="bankanda_xs">
                        <option style="display:none" value="" disabled selected></option>
                        <?php foreach ($bank as $b){ ?>
                        <option value="<?php echo $b['nama_bank']; ?>">Bank <?php echo $b['nama_bank']; ?></option>
                        <?php };?>
                     </select>
                     <i class="fa fa-sort-desc fa-2x" aria-hidden="true"></i>
                  </div>
                  <h4 class="text-left" style="margin-left: 15px"><b>Rekening atas nama<span style="color: #FF0000">*</span></b></h4>
                  <div class="form-group has-success has-feedback" style="margin-bottom: 30px;">
                     <input type="text" class="form-control input-lg" id="an_rekening_xs">
                  </div>
                  <h4 class="text-left" style="margin-left: 15px"><b>Tanggal Transfer<span style="color: #FF0000">*</span></b></h4>
                  <div class="form-group has-success has-feedback" style="margin-bottom: 30px;">
                     <input type="text" class="form-control input-lg" id="tgl_transfer_xs">
                     <span class="form-control-feedback"><img src="<?=static_file()?>images/icon/pastordercalender.png"></span>
                  </div>
                  <h4 class="text-left" style="margin-left: 15px"><b>Jumlah Nominal Pembayaran<span style="color: #FF0000">*</span></b></h4>
                  <div class="form-group has-success has-feedback" style="margin-bottom: 15px;">
                     <input type="text" class="form-control input-lg" id="total_xs" value='<?php echo $this->uri->segment(4); ?>'/>
                  </div>
                  <h5 style="margin-bottom: 10px;">Anda akan mendapat notifikasi dari kami setelah pembayaran Anda berhasil kami verifikasi</h5>
               </div>
            </div>
            <div class="col-xs-12 text-center" style="margin-bottom: 20px;">
               <div style="background-color: #E5FBE5; padding-top:20px;">
                  <h5>INFORMASI REKENING WAKULINER</h5>
                  <br/>
                  <?php foreach ($bank as $b){ ?>
                  <h4><b>Bank <?php echo $b['nama_bank']; ?></b></h4>
                  <h5 style="color: black;">No Rek <b><?php echo $b['no_rekening']; ?></b></h5>
                  <h5 style="color: black;">Cab. <b><?php echo $b['cabang']; ?></b></h5>
                  <h5 style="color: black;">a/n <b><?php echo $b['nama_pemilik']; ?></b></h5>
                  <br/>
                  <?php };?>
               </div>

            </div>
            <div class="col-xs-12 text-center">
               <button type="submit" class="btn btn-success btn-block btn-lg searchgo" id="btn_konfirmasi_xs"><b>KIRIM KONFIRMASI</b></button>
            </div>
         </div>
      </div>

   </div>
</section>
<div class="modal fade" id="errorMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content modal-city">
         <div class="modal-body wk_white text-center">
            <div>
               <button type="button" class="btn btn-defaultclose pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <h4 class="msg_cart"></h4>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="sukses" role="dialog">
   <div class="modal-dialog modal-sm">
      <div class="modal-content modal-c1">
         <div class="modal-body wk_white text-center">
            <h4><span class="bhs_ingg">Thank you for ordering with us.</span><span class="bhs_indo">Terima kasih atas permintaan Anda di Wakuliner</span></h4>
            <h4><span class="bhs_ingg">You may see your order status in the</span><span class="bhs_indo">Anda dapat melihat status pemesanan Anda di bagian</span></h4>
            <a type="button" class="btn btn-warning btn-lg" style="margin-top: 5px" href="<?php echo site_url('order');?>"><b><span class="bhs_ingg">Order - in Progress</span><span class="bhs_indo">Pesanan - Berlangsung</span></b></a>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
$(function(){
   $("#tgl_transfer").datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent: true
   });
});
$(function(){
   $("#tgl_transfer_xs").datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent: true
   });
});
function showMsg(indo, eng){
   var lang_user = '<?php echo $this->session->userdata('lang_user');?>';
   var msg = indo;
   if(lang_user == 'bhs_ingg'){
      msg = eng;
   }
   $('.msg_cart').html(msg);
   $('#errorMsg').modal('show');
}
function validasi(order_id, banktujuan, bankanda, an_rekening, tgl_transfer, total) {
   if($.trim(order_id) == ''){
      var indo = 'Order ID Harus Diisi';
      var eng = 'Wakuliner Order ID must be filled';
      showMsg(indo, eng);
   }
   else if ($.trim(banktujuan) == '') {
      var indo = 'Bank Tujuan harus Diisi';
      var eng = 'Bank Tujuan must be filled';
      showMsg(indo, eng);
   }
   else if ($.trim(bankanda) == '') {
      var indo = 'Bank Anda harus Diisi';
      var eng = 'Bank Anda must be filled';
      showMsg(indo, eng);
   }
   else if ($.trim(an_rekening) == '') {
      var indo = 'Rekening Atas Nama harus Diisi';
      var eng = 'Rekening Atas Nama must be filled';
      showMsg(indo, eng);
   }
   else if ($.trim(tgl_transfer) == '') {
      var indo = 'Jumlah Nominal Pembayaran harus Diisi';
      var eng = 'Jumlah Nominal Pembayaran must be filled';
      showMsg(indo, eng);
   }
   else if ($.trim(total) == '') {
      var indo = 'Tanggal Tansfer harus Diisi';
      var eng = 'Tanggal Tansfer must be filled';
      showMsg(indo, eng);
   }
   else return true;
}
function kirim_konfirmasi(orderid, banktujuan, bankanda, rekening, tanggal, total){
   $.ajax({
      url : '<?php echo URL_API.'purchaseServiceVersion2/confirm_payment';?>',
      type:"POST",
      data : {id_purchase: orderid, bank_tujuan: banktujuan, bank_asal: bankanda, nama_pemilik: rekening, tgl_transfer: tanggal, amount_transfer: total},
      success: function(data){
         if(data['status'] == "1"){
            $('#sukses').modal('show');
         }
         else if(data['status'] == "-2") {
            var indo = "Jumlah Nominal Salah";
            var eng = "Invalid Amount";
            showMsg(indo, eng);
         }
         else if(data['status'] == "0") {
            var indo = "Order ID Tidak Ditemukan!!!";
            var eng = "No Such Order ID!!!";
            showMsg(indo, eng);
         }
      }
   });
}
$(document).ready(function() {
   $('#btn_konfirmasi').click(function(event) {
      var orderid = $('#orderid').val();
      var banktujuan = $('#banktujuan').val();
      var bankanda = $('#bankanda').val();
      var rekening = $('#an_rekening').val();
      var tanggal = $('#tgl_transfer').val();
      var total = $('#total').val();
      if(validasi(orderid, banktujuan, bankanda, rekening, tanggal, total)){
         kirim_konfirmasi(orderid, banktujuan, bankanda, rekening, tanggal, total);
      }
   });
   $('#btn_konfirmasi_xs').click(function(event) {
      var orderid = $('#orderid_xs').val();
      var banktujuan = $('#banktujuan_xs').val();
      var bankanda = $('#bankanda_xs').val();
      var rekening = $('#an_rekening_xs').val();
      var tanggal = $('#tgl_transfer_xs').val();
      var total = $('#total_xs').val();
      if(validasi(orderid, banktujuan, bankanda, rekening, tanggal, total)){
         kirim_konfirmasi(orderid, banktujuan, bankanda, rekening, tanggal, total);
      }
   });

});
</script>
