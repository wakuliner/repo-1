<?php
$lang_user = $this->session->userdata('lang_user');
$place_prev = 'Cari pesanan lalu';
$place_comment = 'Tambah Komentar (opsional)';
if($lang_user == 'bhs_ingg'){
	$place_prev = "Search previous order";
	$place_comment = 'Add comment (optional)';
}
?>
<section class="wk-container-1 nobg" style="background-color:white;">
	<div class="container wk_style">

    	<h3 class="text-center">ORDER</h3>

        <ul class="nav nav-tabs nav-pills nav-justified nav_order" style="border-bottom:1px solid #259b00;">
          <li role="tab" class="active" data-toggle="tab" data-target="#progres"><a href="#"><span class="bhs_ingg">IN PROGRESS</span><span class="bhs_indo">SEDANG PROSES</span></a></li>
          <li role="tab" data-toggle="tab" data-target="#rate"><a href="#"><span class="bhs_ingg">RATE MERCHANTS</span><span class="bhs_indo">NILAI TOKO</span></a></li>
          <li role="tab" data-toggle="tab" data-target="#pastorder"><a href="#"><span class="bhs_ingg">PAST ORDERS</span><span class="bhs_indo">PESANAN YANG LALU</span></a></li>
        </ul>

        <div class="tab-content content_about">

            <div role="tabpanel" class="tab-pane active" id="progres">

                <?php
					$payment = '';
					$stage = '';
					$stage_id = '';
					$subtotal = 0;
					$id_purchasesku = '';
					if(!empty($list_order)){
						$subtotal = 0;
						foreach($list_order->customer_order as $lo){ ?>

             	<div class="well clearfix nopadding" style="padding-bottom:10px; background-color:white;">
                    <div class="bg-success wk_header_order wk_order">
                    	<span class="teks-orange">WAKU-ANTAR</span><br /><small class="text-success"><?php echo str_replace(',','',$lo->tgl_purchase);?>, <?php echo date('H:i', strtotime($lo->jam_pesan));?></small>
                    </div>
                    <div class="col-lg-1 col-xs-12">
                    	<b>Order No <?php echo $lo->kode_purchase;?></b>
                    </div>
                    <div class="col-lg-4 col-xs-12 text-muted">
                    	<b><?php foreach($lo->detail as $d){
								$payment = '';
								$stage = '';
								$stage_id = '';
								echo $d->merchant_name;
								$payment = $d->name_type_payment;
								$stage = $d->stage;
								$stage_id = $d->id_stage;
							}?>
                        <table class="table" id="table-item1">
                        <tbody>
                            <?php
							$subtotal = 0;
							$_merchant_id = '';
							foreach($lo->detail as $d){
								$_merchant_id = '';
								$_merchant_id = $d->merchant_id;
								foreach($d->menu as $mn){

									$subtotal +=$mn->amount;
								?>
									<tr>
                                        <td><?php echo $mn->food_name;?></td>
                                        <td><?php echo $mn->qty;?></td>
                                        <td align="right">Rp. <?php echo number_format($mn->amount,'0',',','.');?></td>
                                    </tr>
							<?php }
							}?>
                        </tbody>
                        <tfoot>
                            <tr align="right">
                                <td colspan="2" class="wk_style">Subtotal</td>
                                <td>Rp. <?php echo number_format($subtotal,'0',',','.');?></td>
                            </tr>
                            <tr align="right">
                                <td colspan="2" class="wk_style"><span class="bhs_indo">Biaya Pengiriman</span><span class="bhs_ingg">Delivery Fee</span></td>
                                <td>Rp. <?php echo number_format($lo->delivery_fee,'0',',','.');?></td>
                            </tr>
                            <tr align="right">
                                <td colspan="2" class="wk_style"><span class="bhs_ingg">Promo Code</span><span class="bhs_indo">Kode Promo</span></td>
                                <td>- Rp. <?php echo number_format($lo->coupon,'0',',','.');?></td>
                            </tr>
                            <tr align="right">
                                <td colspan="2" class="wk_style">Grand Total</td>
                                <td>Rp <?php echo number_format($lo->grand_total,'0',',','.');?></td>
                            </tr>
                        </tfoot>
                        </table>
                        </b>
                    </div>
                    <div class="col-lg-2 col-xs-6 text-center">
                    	<small class="text-muted"><span class="bhs_ingg">Payment Type</span><span class="bhs_indo">Tipe Pembayaran</span></small><br /><b><?php echo $payment;?></b>
                    </div>
                    <div class="col-lg-2 col-xs-6 text-center">
                    		<small class="text-muted"><span class="bhs_ingg">Order Status</span><span class="bhs_indo">Status Pemesanan</span></small><br /><!--<img src="<?=static_file()?>images/icon/cooking.png" />--> <b class="teks-orange"><?php if($stage == "Autoreject"){echo "Masih Menunggu Merchant";}else{echo $stage;}?></b>
                    </div>
                    <div class="col-lg-3 col-xs-12 text-center">
                    		<small class="text-muted"><span class="bhs_ingg">If you have received your order. Click this button</span><span class="bhs_indo">Jika Anda sudah menerima pesanan ini. Silahkan klik tombol ini</span></small><br /><br />
                        <?php if($stage_id == 3){ ?>
									<button type="button" onclick="received(<?php echo $_merchant_id;?>,<?php echo $lo->id_purchase;?>)" class="btn btn-success btn-block text-muted"><span class="bhs_indo">Saya sudah menerima</span><span class="bhs_ingg">I Have Received</span></button>
								<?php }else{
						 			if($payment == "Bank Transfer"){
										if($stage_id != 14 ) {?>
											<button type="button" onclick="received(<?php echo $_merchant_id;?>,<?php echo $lo->id_purchase;?>)" class="btn btn-success btn-block"><span class="bhs_indo">Saya sudah Menerima</span><span class="bhs_ingg">I Have Received</span></button>
										<?php } else {?>
											<a href="<?php echo base_url('payment/konfirmasi/'.$lo->id_purchase.'/'.$lo->grand_total)?>" class="btn btn-success btn-block"><span class="bhs_indo">Konfirmasi Pembayaran</span><span class="bhs_ingg">Confirm Payment</span></a>

								<?php }} else {?>

                    		<button type="button" onclick="received(<?php echo $_merchant_id;?>,<?php echo $lo->id_purchase;?>)" class="btn btn-success btn-block"><span class="bhs_indo">Saya sudah menerima</span><span class="bhs_ingg">I Have Received</span></button>
                       		 <!-- <button type="button" class="btn btn-disabled btn-block text-muted">I HAVE RECEIVED</button> -->

						  <?php }} ?>
                    </div>
                </div>

				<?php	}
					}
				?>
           </div>

            <div role="tabpanel" class="tab-pane" id="rate">
           		<?php
					$merchant_id = '';
					$id_purchase = '';
					if(count($get_rating_order_merchant->order_rating) > 0){
						echo '<p><span class="bhs_ingg">You have underdone ratings</span><span class="bhs_indo">Anda perlu memberikan rating di bawah ini</span></p>';
						foreach($get_rating_order_merchant->order_rating as $gr){
							$id_purchase = '';
							$id_purchase = $gr->id_purchase;
						?>
                <div class="well clearfix nopadding" style="padding-bottom:10px; background-color:white;">
                    <div class="wk_order wk_header_order">
                    	<div class="container">
                            <div class="col-lg-4 col-xs-4">
                                <Br /><?php echo str_replace(',','',$gr->tgl_purchase);?>, <?php echo date('H:i', strtotime($gr->jam_pesan));?>
                            </div>
                            <div class="col-lg-6 col-xs-6 wk_black" align="left">
                                <Br /><img src="<?=static_file()?>images/icon/costgrey-ratingbar.png" /> Rp. <?php echo number_format($gr->total_amount,'0',',','.');?>
                            </div>
                            <div class="col-lg-2 col-xs-2" align="center">
                                <Br /><a href="#" onclick="showFrate('<?=$id_purchase?>');"><img id="imgFrate" data="show" src="<?=static_file()?>images/icon/toggle1.png" /></a>
                            </div>
                        </div>
                    </div>

                    <div id="merchantRate_<?=$id_purchase?>" class="text-muted hide">
                    <br />
                        <div class="col-lg-2 col-xs-6">
                            <b><?php foreach($gr->merchant as $gd){
								$merchant_id = '';
								echo $gd->merchant_name;
								$merchant_id = $gd->merchant_id;
							}?></b>
                        </div>
                        <div class="col-lg-2 col-xs-6 clearfix">
                        	<div class="well nopadding wk_rate_rating">
                                <select id="example-css" class="rating_<?php echo $id_purchase;?>" name="rating_<?php echo $id_purchase;?>" autocomplete="off">
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-5 col-xs-12 clearfix">
                        	<textarea class="form-control" name="comment_<?php echo $id_purchase;?>"  id="comment_<?php echo $id_purchase;?>" placeholder="<?php echo $place_comment;?>"></textarea>
                        </div>
                        <div class="col-lg-3 col-xs-12 clearfix btn-rating">
                        	<button type="button" onclick="rate_merchant(<?php echo $id_purchase;?>, <?php echo $merchant_id;?>)" class="btn btn-success btn-block">Submit</button>
                        </div>
                    </div>

                </div>

                <?php }
					}else{
						echo '<p><span class="bhs_ingg">Orders is empty</span><span class="bhs_indo">Pesanan kosong</span></p>';
					}
				?>

           </div>

        	<div role="tabpanel" class="tab-pane" id="pastorder">

                <div class="col-lg-12 clearfix wk_search_order">
                	<div class="input-group">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><img src="<?=static_file()?>images/icon/pastordercalender.png" width="16" /></button>
                      </span>
                      <input type="text" class="form-control" placeholder="<?php echo $place_prev;?>">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><img src="<?=static_file()?>images/icon/search.png" width="16" /></button>
                      </span>
                    </div>
                </div>

                <div class="col-lg-12">
                	<?php
					$id_purchases = '';
					if(!empty($order_past)){
						foreach($order_past->customer_order as $op){
							$id_purchases = '';
							$id_purchases = $op->id_purchase;
					?>
                    <div class="well clearfix nopadding" style="background-color:white;" id="hide_<?php echo $id_purchases;?>">
                    	 <div class="wk_order wk_header_order">
                            <a href="#" onclick="showForder('<?php echo $id_purchases;?>');" id="tombolToggle" class="pull-right btn-toggle">
                                <img id="imgForder" data="show" src="<?=static_file()?>images/icon/togglewhite.png" />
                            </a>
                            <span class="teks-orange">WAKU-ANTAR</span><br /><small><?php echo str_replace(',','',$op->tgl_purchase);?>, <?php echo date('H:i', strtotime($op->jam_pesan));?></small>
                         </div>
                         <div id="contentOrder_<?php echo $id_purchases;?>" class="hide">
                         	<div class="col-lg-1 col-xs-12">
                            <b>Order No <?=$op->id_purchase?></b>
                            </div>
                            <div class="col-lg-4 col-xs-12 text-muted">
                                <b><?php foreach($op->detail as $d){
                                    $payment = '';
                                    $stage = '';
                                    $stage_id = '';
                                    echo $d->merchant_name;
                                    $payment = $d->name_type_payment;
                                    $stage = $d->stage;
                                    $stage_id = $d->id_stage;
                                }?>
                                <table class="table" id="table-item3" id="<?php echo 'op_'.$op->id_purchase;?>">
                                <tbody>
                                    <?php
                                        $subtotal = 0;
                                        foreach($op->detail as $d){
                                            foreach($d->menu as $mn){
                                                $subtotal +=$mn->amount;
                                            ?>
                                                <tr>
                                                    <td><?php echo $mn->food_name;?></td>
                                                    <td><?php echo $mn->qty;?></td>
                                                    <td align="right">Rp. <?php echo number_format($mn->amount,'0',',','.');?></td>
                                                </tr>
                                        <?php }
                                        }?>
                                </tbody>
                                <tfoot>
                                    <tr align="right">
                                        <td colspan="2" class="wk_style">Subtotal</td>
                                        <td>Rp. <?php echo number_format($subtotal,'0',',','.');?></td>
                                    </tr>
                                    <tr align="right">
                                        <td colspan="2" class="wk_style">Delivery Fee</td>
                                        <td>Rp. <?php echo number_format($op->delivery_fee,'0',',','.');?></td>
                                    </tr>
                                    <tr align="right">
                                        <td colspan="2" class="wk_style">Kode Promo</td>
                                        <td>- Rp. <?php echo number_format($op->coupon,'0',',','.');?></td>
                                    </tr>
                                    <tr align="right">
                                        <td colspan="2" class="wk_style">Grand Total</td>
                                        <td>Rp <?php echo number_format($op->grand_total,'0',',','.');?></td>
                                    </tr>
                                </tfoot>
                                </table>
                                </b>
                            </div>
                            <div class="col-lg-2 col-xs-12 text-center">
                                <small class="text-muted">Tipe Pembayaran</small><br /><b><?php echo $payment;?></b>
                            </div>
                            <div class="col-lg-5 col-xs-12 text-center">
                                <span class="col-lg-1 col-xs-1"></span>
                                <button type="button" id="<?php echo $id_purchases;?>" class="btn btn-disabled col-lg-4 col-xs-4 wk_white del_order">Delete</button>
                                <span class="col-lg-1 col-xs-2"></span>
                                <button type="button" id="re_<?php echo $id_purchases;?>" class="btn btn-success col-lg-4 col-xs-4 btn_reorder">Reorder</button>
                                <span class="col-lg-1 col-xs-1"></span>
                            </div>
                         </div>
                    </div>
                    <?php }
						}
					?>
                </div>

            </div>

        </div>

    </div>
</section>

<div class="modal fade" id="confirModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content modal-city">
      <div class="modal-body wk_white text-center">
            <h4 id="text_confirm"></h4>
      </div>
      <div class="modal-footer">
      	<input type="hidden" id="post1"/>
      	<input type="hidden" id="post2" />
        <input type="hidden" id="post3" />
        <a onclick="post_confirm();" class="btn btn-warning pull-left"><i class="glyphicon glyphicon-ok"></i> Ya</a>
        <button type="button" class="btn btn-warning pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Tidak</button>
      </div>
    </div>
  </div>
</div>

<script>
var lang_user = '<?php echo $lang_user;?>';
function received(merchant_id, id_purchase){
	$('#post1').val(merchant_id);
	$('#post2').val(id_purchase);
	$('#post3').val("received");
	var msg = 'Apakah anda yakin telah menerima pesanan ini ?';
	if(lang_user == 'bhs_ingg'){
		var msg = 'Are you sure you have received your order ?';
	}
	$('#text_confirm').html(msg);
	$('#confirModal').modal('show');
}
function post_confirm()
{
	var merchant_id = $('#post1').val();
	var id_purchase = $('#post2').val();
	var post3 = $('#post3').val();
	if(post3 === "received"){
		var url = '<?php echo site_url('order/received');?>';
		$.ajax({
			data : {merchant_id : merchant_id, id_purchase : id_purchase},
			url : url,
			type : "POST",
			success:function(response){
				if(response > 0){
					location.reload();
				}
			}
		});
	}else if(post3 === "del_order"){
		var url = '<?php echo site_url('order/del_order');?>';
		$.ajax({
			data : {order_id:merchant_id},
			url : url,
			type : "POST",
			success:function(response){

				if(response > 0){
					//$('#teksNotifModal').html("Data order dengan id purchase : "+merchant_id+" telah dihapus");
					//$('#errorNotifModal').modal('show');
					$('#hide_'+merchant_id).hide();
					$('#contentOrder_'+merchant_id).hide();
					$('#confirModal').modal('hide');

				}
			}
		});
	}else if(post3 === "reorder"){
		window.location.href = '<?php echo site_url('cart/reorder');?>/'+id_purchase;
	}else{
		var rating = $('.rating_'+id_purchase).val();
		var description = $('#comment_'+id_purchase).val();
		var url = '<?php echo site_url('order/rate_merchant');?>';
		$.ajax({
			data : {merchant_id:merchant_id,id_purchase:id_purchase,rating:rating,description:description},
			url : url,
			type : "POST",
			success:function(response){
				if(response > 0){
					location.reload();
				}
			}
		});
	}
}
function rate_merchant(id_purchase, merchant_id){
	$('#post1').val(merchant_id);
	$('#post2').val(id_purchase);
	$('#post3').val("rate");
	var rating = $('.rating_'+id_purchase).val();
	var msg = 'Apakah anda yakin memberikan rating '+rating+' bintang ?';
	if(lang_user == 'bhs_ingg'){
		msg = 'Are you sure you want to give ?';
	}
	$('#text_confirm').html(msg);
	$('#confirModal').modal('show');
}
$('.btn_reorder').click(function(){
	var id = $(this).get(0).id;
	var dt = id.split('_');
	var id_purchase = dt[1];
	$('#post2').val(id_purchase);
	$('#post3').val("reorder");
	var msg = 'Apakah anda ingin memesan ulang pesanan lalu ini ?';
	if(lang_user == 'bhs_ingg'){
		msg = 'Do you really want to re-order this purchase ?';
	}
	$('#text_confirm').html(msg);
	$('#confirModal').modal('show');
});
$('.del_order').click(function(){
	var id = $(this).get(0).id;
	$('#post1').val(id);
	$('#post3').val("del_order");
	var msg = 'Apakah anda ingin menghapus pesanan lalu ?';
	if(lang_user == 'bhs_ingg'){
		msg = 'Do you really want to delete this past order ?';
	}
	$('#text_confirm').html(msg);
	$('#confirModal').modal('show');
});
</script>
