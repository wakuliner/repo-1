<style>
.btn-waku1 {
    width:45%;
}
.btn-waku2 {
    width:55%;
}
</style>

<form id="frm_cart">
    <input type="hidden" name="min_order" id="min_order" value="<?php echo $merchant_info->minimum_order;?>" />
	<section class="wk-container-1 nobg" style="background-color:white;">
      <div class="container wk_style">
            
            <h3 class="page-header text-center">CART</h3>
            <div class="table-responsive2">
              <table class="table cart-table">
              	<thead>
                	<tr style="background-color:#EFEFEF;">
                    	<th class="text-center" width="45%"><span class="bhs_ingg">Dish</span><span class="bhs_indo">Makanan</span></th>
                        <th class="text-center" width="15%"><span class="bhs_ingg">Qty</span><span class="bhs_indo">Jumlah</span></th>
                        <th class="text-center" width="20%"><span class="bhs_ingg">Price</span><span class="bhs_indo">Harga</span></th>
                        <th class="text-center"><button type="button" class="btn btn-xs btn-danger btn_clear_all"><span class="bhs_ingg">Clear All</span><span class="bhs_indo">Hapus Semua</span></button></th>
                    </tr>
                </thead>
                <tbody>
                <?php 
					$lang_user = $this->session->userdata('lang_user');
					$grand_ttl = 0;
					$i = 0;
					foreach($carts as $c){ 
						$merchant_id = $c['merchant_id'];
						$kode_pos = $c['kode_pos'];
						$id_kelurahan = $c['id_kelurahan'];
						if($c['qty'] > 0) {
					echo '<input type="hidden" name ="merchant_id[]" value="'.$c['merchant_id'].'" />';
					echo '<input type="hidden" name ="merchant_food_id[]" value="'.$c['food_id'].'" />';
					echo '<input type="hidden" name="kode_pos[]" value="'.$c['kode_pos'].'" />';
					echo '<input type="hidden" name="id_kelurahan[]" value="'.$c['id_kelurahan'].'" />';
							
					?>
                	<tr>
                    	<td>
                        	<div class="">
                            	<img src="<?=$c['img'];?>" width="80px;"/>
                            </div>
                            <div class="wk_black">
                            	<p><b><?php echo $c['name'];?><br />Rp. <?php echo number_format($c['hrg_asli'],0,'.','.');?></b></p>
                        	</div>
                            <div class="wk_black">
                            	<p><img src="<?=static_file()?>images/icon/edit2.png" onclick="show_descr('<?php echo $c['rowid'];?>');" id="edit_note_<?php echo $c['rowid'];?>" /> <span id="span_<?php echo $c['rowid'];?>"><?php echo $c['notes'];?></span></p>
                                <input type="text" style="display:none;" class="form-control input-sm" id="input_note_<?php echo $c['rowid'];?>" value="<?php echo $c['notes'];?>" /><button type="button" class="btn btn-info btn-sm" id="btn_save_<?php echo $c['rowid'];?>" style="display:none;" onclick="edit_descr('<?php echo $c['rowid'];?>')">Save</button>
                            </div>
                        </td>
                    	<td>
                        	<div class="input-group plus-minus">
                              <span class="input-group-btn">
                                <button class="btn btn-success btn-sm" type="button" onclick="minus_qty('<?php echo $c['rowid'];?>',<?php echo $c['hrg_asli'];?>);" ><i class="glyphicon glyphicon-minus"></i></button>
                              </span>
                              <div class="form-group">
                              <input type="text" class="form-control input-sm" name="qty[]" id="qty_<?php echo $c['rowid'];?>" value="<?php echo $c['qty'];?>" >
                              </div>
                              <span class="input-group-btn">	
                                <button class="btn btn-success btn-sm" type="button" onclick="add_qty('<?php echo $c['rowid'];?>',<?php echo $c['hrg_asli'];?>);" ><i class="glyphicon glyphicon-plus"></i></button>
                                <input type="hidden" id="sttl_<?php echo $c['rowid'];?>" name="total_amount[]" value="<?php echo $c['subtotal'];?>"/>
                              </span>
                            </div>
                        </td>
                    	<td align="center">
                        	<b><span class="wk_black subttl_<?php echo $c['rowid'];?>" >Rp. <?php echo number_format($c['subtotal'],0,'.','.');?></span></b>
                        </td>
                    	<td align="center">
                        	<a href="#" class="btn_delCart" id="<?php echo $c['rowid'];?>"><img src="<?=static_file()?>images/icon/delette.png" /></a>
                        </td>
                    </tr>
                <?php 
					$grand_ttl += $c['subtotal'];
					$i++;
				} } ?>
                </tbody>
                <tfoot>
                	<tr>
                    	<td align="center" colspan="2"><h4>SUB TOTAL</h4></td>
                        <td align="center"><b><h4 id="grand_ttl">Rp. <?php echo number_format($grand_ttl,0,'.','.');?></h4></b></td>
                        <input type="hidden" name="my_grand" id="my_grand" value="<?php echo $grand_ttl;?>" />
                        <td>&nbsp;</td>
                    </tr>
                </tfoot>
              </table>
            </div>
            
            <div class="col-lg-12 col-xs-12" align="center" style="padding:20px 0 30px 0;">
            	  
                  <div class="btn-group" role="group" aria-label="...">
                    <button type="button" id="btn_back_shop" class="btn btn-waku1 btn-orange btn-warning btn-lg col-xs-6"> <i class="glyphicon glyphicon-chevron-left"></i> <span class="bhs_ingg">Back Shopping</span><span class="bhs_indo">Kembali Berbelanja</span></button>
                    <button type="button" class="btn btn-waku2 btn-yellow btn-lg col-xs-6" id="btn_checkout"> <span class="bhs_ingg">CHECKOUT SEKARANG</span><span class="bhs_indo">KELUAR SEKARANG</span> <i class="glyphicon glyphicon-chevron-right"></i></button>
                  </div>
                  
            </div>
            <bR />
            <br />
            <br />
            <p align="center" class="bhs_ingg"><a href="#"><i class="glyphicon glyphicon-arrow-up"></i> Back to top</a></p>
            <p align="center" class="bhs_indo"><a href="#"><i class="glyphicon glyphicon-arrow-up"></i> Kembali ke atas</a></p>
            <br />
     </div>
   </section>
   </form>
   
   <div class="modal fade" id="confirModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body wk_white text-center">
          		<div>
                	<button type="button" class="btn btn-defaultclose pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <input type="hidden" id="post1" />
                <h4><span class="bhs_ingg">Are you sure you want to delete this menu ?</span><span class="bhs_indo">Apakah anda yakin ingin  menghapus menu ini ?</span></h4>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning pull-left btn_delCart2"><i class="glyphicon glyphicon-ok"></i> Ya</button>
            <button type="button" class="btn btn-warning pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Tidak</button>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="cartaddModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body wk_white text-center">
          		<div>
                	<button type="button" class="btn btn-defaultclose pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <h4 class="msg_cart">Berhasil menambahkan kekeranjang</h4>
          </div>
        </div>
      </div>
    </div>
    
    
    
     <div class="modal fade" id="confirmCartLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body wk_white text-center">
          		<div>
                	<button type="button" class="btn btn-defaultclose pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <h4 class="msg_cart">Untuk dapat menikmati seluruh fitur aplikasi, Anda harus mempunyai akun Wakuliner. Gratis dan mudah sekali membuatnya. Silahkan login atau buat akun</h4>
                <button type="button" class="btn btn-block btn-warning" data-dismiss="modal" data-toggle="modal" data-target="#registrasiModal"><span class="bhs_ingg">Register</span><span class="bbhs_indo">Daftar</span></button>
                <button type="button" class="btn btn-info btn-block" data-dismiss="modal" data-toggle="modal" data-target="#loginModal"><span class="bhs_ingg">Login</span><span class="bbhs_indo">Masuk</span></button>
          </div>
        </div>
      </div>
    </div>
    
<script>
var lang_user = '<?php echo $lang_user;?>';
var html_cart = 'Berhasil menambahkan kekeranjang';
if(lang_user == 'bhs_ingg'){
	html_cart = 'Add to cart success';
}
var merchant_id = '<?php echo $merchant_id;?>';
var kode_pos = '<?php echo $kode_pos;?>';
var id_kelurahan = '<?php echo $id_kelurahan;?>';
var statusCari = '<?php echo $statusCari;?>';
 <?php if($id!="" && $submit_cart > 0):?>
     $('.msg_cart2').html('');
     $('.msg_cart2').html(html_cart);
	 $('#cartaddModal2').modal({
		backdrop: 'static',
		keyboard: false
	});
	 $('#cartaddModal2').modal('show');
	 //window.location = "<?php echo site_url('wakuantar/menu');?>/"+merchant_id+"/"+kode_pos+"/"+id_kelurahan;
 <?php endif; ?>

$('.close_cart').click(function(){
	$('#cartaddModal2').modal('hide');
	window.location = "<?php echo site_url('wakuantar/menu');?>/"+merchant_id+"/"+kode_pos+"/"+id_kelurahan+"/"+statusCari;
});

function minus_qty(rowids, subtotal){
	var adds = 1;
	var qty = $('#qty_'+rowids).val();
	if(qty > 1){
		adds = Number(qty) - 1;
	}
	var subttl = Number(adds) * Number(subtotal);
	$('#qty_'+rowids).val(adds);
	$('#sttl_'+rowids).val(subttl);
	$('.subttl_'+rowids).html('Rp. '+numberWithCommas(subttl));
	htg_total();
	upd_cart(rowids, adds);
}

function add_qty(rowids, subtotal){
	var qty = $('#qty_'+rowids).val();
	var adds = Number(qty) + 1;
	
	var subttl = Number(adds) * Number(subtotal);
	$('#sttl_'+rowids).val(subttl);
	$('#qty_'+rowids).val(adds);
	$('.subttl_'+rowids).html('Rp. '+numberWithCommas(subttl));
	htg_total();
	upd_cart(rowids, adds);
}

function htg_total(){
	var lengths = '<?php echo $i;?>';
	var prices_ttls = 0;
	lengths = Number(lengths) - 1;
	var prices_ttl = $("input[name^='total_amount']").map(function(idx, elem) {
		var elems = elem.value;
		elems = Number(elems);
		prices_ttls += elems;
		return prices_ttls;
	}).get();
	$('#grand_ttl').html('Rp. '+numberWithCommas(prices_ttl[lengths]));
}

$('.btn_delCart').click(function(){
	var id = $(this).get(0).id;
	$('#post1').val(id);
	$('#confirModal').modal('show');
});
$('.btn_delCart2').click(function(){
	$('#confirModal').modal('hide');
	var id = $('#post1').val();
	var url = '<?php echo site_url('cart/del_cart');?>';
	$.ajax({
		data : {rowid : id},
		url : url,
		type : "POST",
		
		success:function(response){
			
			window.location.href = '<?php echo site_url('cart/view');?>';
		}
	});	
});

function upd_cart(rowids, qty){
	var url = '<?php echo site_url('cart/upd_cart');?>';
	var notes = $('#input_note_'+rowids).val();
	$.ajax({
		data : {rowid : rowids, qty:qty, notes:notes},
		url : url,
		type : "POST",
		success:function(response){

		}
	});	
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

$('#btn_cart').click(function(){
	var url = '<?php echo site_url('payment');?>';
	var data_cart = $('#frm_cart').serialize();
	$.ajax({
		data : data_cart,
		url : url,
		type : "POST",
		success:function(response){
			window.location.href = '<?php echo site_url('payment');?>';
		}
	});	
});
$('#btn_back_shop').click(function(){
	window.location.href = localStorage.getItem("back_url");
});
$('#btn_checkout').click(function(){
	var min_order = Number($('#min_order').val());
	var my_grand = Number($('#my_grand').val());
	var min_html = numberWithCommas(min_order);
	var merchant_name = '<?php echo $merchant_name;?>';
	var session = '<?php echo $this->session->userdata('login_user');?>';
	
	if(my_grand < min_order){
		$('.msg_cart').html('');
		html_cart = 'Pemesanan minimum di merchant '+merchant_name+' adalah Rp. '+min_html+ '.(sebelum pajak dan ongkos lainnya). Silahkan pesan lagi untuk dapat lanjut ke checkout';
		if(lang_user == 'bhs_ingg'){
			html_cart = 'Minimum order for merchant '+merchant_name+' is Rp. '+min_html+ '. (before taxx and other charge). Please add more item to proceed';
		}
     	$('.msg_cart').html(html_cart);
	 	$('#cartaddModal').modal('show');
		return false;
	}
	if(session == false){
		//$('#loginModal').modal('show');
		$('#confirmCartLogin').modal('show');
		return false;
	}
	
	window.location.href = '<?php echo site_url('payment');?>';
});

$('.btn_clear_all').click(function(){
	$("#myModal").modal();
});

function clearAll(){
	var url = '<?php echo site_url('cart/del_all');?>';
	$.ajax({
		data : 'del',
		url : url,
		type : "POST",
		success:function(response){
			window.location.href = '<?php echo site_url('cart');?>';
		}
	});	
}

function show_descr(id){
	$('#span_'+id).hide();
	$('#input_note_'+id).show();
	$('#btn_save_'+id).show();
}

function edit_descr(id){
	var url = '<?php echo site_url('cart/upd_cart');?>';
	var notes = $('#input_note_'+id).val();
	var qty = $('#qty_'+id).val();
	$.ajax({
		data : {rowid : id, qty:qty, notes:notes},
		url : url,
		type : "POST",
		success:function(response){
			$('#span_'+id).html(notes);
			$('#input_note_'+id).hide();
			$('#btn_save_'+id).hide();
			$('#span_'+id).show();
			//location.reload();
		}
	});	
}
</script>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content modal-city">
      <div class="modal-body wk_white text-center">
        <h4><span class="bhs_ingg">Are you sure you want to ?</span><span class="bhs_indo">Apakah Anda yakin ?</span></h4>
      </div>
      
      <div class="modal-footer">
        <a href="javascript:clearAll()" class="btn btn-warning pull-left"><i class="glyphicon glyphicon-ok"></i> Ya</a>
        <button type="button" class="btn btn-warning pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Tidak</button>
      </div>
      
    </div>

  </div>
</div>