<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <?= $head ?>
    </head>
    <body>
        
		<?= $navbar ?>
		<?= $content ?>
        <?= $footer ?>
	
    <div class="modal fade" id="confLogoutModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body wk_white text-center">
                <h4><span class="bhs_ingg">Are you sure you want to log out ?</span><span class="bhs_indo">Apakah anda yakin ingin keluar dari akun anda ?</span></h4>
          </div>
          <div class="modal-footer">
            <a href="<?=base_url()?>logout" class="btn btn-warning pull-left"><i class="glyphicon glyphicon-ok"></i> Ya</a>
            <button type="button" class="btn btn-warning pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Tidak</button>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="errorNotifModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body wk_white text-center">
                <h4 id="teksNotifModal"></h4>
          </div>
        </div>
      </div>
    </div>
    
    </body>
</html>
<?php
$lang_user = $this->session->userdata('lang_user');
?>
<script>
var lang_user = '<?php echo $lang_user;?>';
console.log(lang_user);
if(lang_user != ''){
	$('.'+lang_user).show();
}else{
	$('.bhs_indo').show();
}
</script>