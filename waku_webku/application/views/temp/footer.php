<?php $this->load->view('loginregister'); ?>
<section class="footer">
    <div class="container">
		<!--
        <p class="text-center"><i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum Lorem Ipsum is simply dummy text of the </i>
		</p>
		-->
        <div class="container nav-foot ">
            <div class="col-md-3 col-xs-6 col-sm-3 tag-menu">
                 <a href="<?=base_url()?>"><span class="bhs_ingg">Home</span><span class="bhs_indo">Beranda</span></a>

                <a href="<?=base_url()?>wakuantar">WAKU-ANTAR</a>
                <a href="<?=base_url()?>wakuwiku">WAKU-WIKU</a>
                <a href="#">WAKU-KATERING</a>
            </div>
            <div class="col-md-3 col-xs-6 tag-menu col-sm-3">
            	<a href="<?=base_url()?>about/index/term">Term & Conditions</a>
                <a href="<?=base_url()?>about/index/faq">FAQ</a>
                <a href="<?=base_url()?>contact">Contact Us</a>
                <a href="<?=base_url()?>join">Join Us</a>
            </div>
            <div class="col-md-2 col-xs-6 tag-menu col-sm-3">
                <p>FOLLOW OUR</p>
                <p>SOCIAL MEDIA</p>
                <a class="footer-social" href="http://www.instagram.com/wakuliner/" target="_blank"><img src="<?= static_file() ?>images/icon/ig.png"></a>
                <a class="footer-social" href="http://www.facebook.com/wakuliner/" target="_blank"><img src="<?= static_file() ?>images/icon/fb.png"></a>
            </div>
            <div class="col-md-4 col-xs-12 wk-reserved col-sm-3" >

                <img src="<?= static_file() ?>images/logo/logo-bottom.png" width="150" class='visible-sm'>
                <img src="<?= static_file() ?>images/logo/logo-bottom.png" width="245" class='hidden-sm'>
                <div class="clearfix">
                <a class="footer-social-dwn" target="_blank" href="https://play.google.com/store/apps/details?id=com.bigit.wakuliner.customer"><img src="<?= static_file() ?>images/icon/gplay.png" width="120px"></a>
                <a class="footer-social-dwn" target="_blank" href="https://itunes.apple.com/us/app/wakuliner-food-delivery-order/id1070066694?ls=1&mt=8"><img src="<?= static_file() ?>images/icon/appstore.png" width="120px"></a>
                </div>
                <p>@2016 copyrights. All rights reserved</p>
            </div>
        </div>
    </div>
</section>
<?php
$lang_user = $this->session->userdata('lang_user');
?>
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/58e70501f7bbaa72709c4c15/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();

var lang_user = '<?php echo $lang_user;?>';
console.log(lang_user);
if(lang_user != ''){
	$('.'+lang_user).show();
}else{
	$('.bhs_indo').show();
}
</script>

<style type="text/css">
.addition-footer{
	background-color:#FFFFFF;
	width:100%;
	bottom:0;
	padding:10px 10px;
	box-sizing:border-box;
}

.flex-container > div{
	color:#000000;
	text-transform:uppercase;
}
.flex-container > div > img{
	padding:5px;
	margin-bottom:5px;
}
.flex-container > div > div
{
	white-space:nowrap;
}
@media screen and (min-width:900px) {
	.flex-container{
		display:table-row;
	}
	.flex-container > div{
		display:table-cell;
		padding:0px 5px;
	}
}
</style>
<div class="addition-footer">
	<div class="flex-container">
    	<div class="left">
        <div>Metode Pembayaran</div>
        <img src="<?=base_url()?>static/images/footer-logo/bank-transfer/alto.png" /> <img src="<?=base_url()?>static/images/footer-logo/bank-transfer/atm-bersama.png" /> <img src="<?=base_url()?>static/images/footer-logo/bank-transfer/bca.png" /> <img src="<?=base_url()?>static/images/footer-logo/bank-transfer/bri.png" /> <img src="<?=base_url()?>static/images/footer-logo/bank-transfer/mandiri.png" /> <img src="<?=base_url()?>static/images/footer-logo/bank-transfer/prima.png" /> <img src="<?=base_url()?>static/images/footer-logo/credit-card-debit-online/cimb-click.png" /> <img src="<?=base_url()?>static/images/footer-logo/credit-card-debit-online/jcb.png" /> <img src="<?=base_url()?>static/images/footer-logo/credit-card-debit-online/mandiri-e-cash.png" /> <img src="<?=base_url()?>static/images/footer-logo/credit-card-debit-online/master-card.png" /> <img src="<?=base_url()?>static/images/footer-logo/credit-card-debit-online/visa.png" />
        </div>
    	<div class="middle">
        <div>Secure Payment</div>
        <img src="<?=base_url()?>static/images/footer-logo/compliance/master-card-source-code.png" /> <img src="<?=base_url()?>static/images/footer-logo/compliance/mcafee.png" /> <img src="<?=base_url()?>static/images/footer-logo/compliance/msc-multimedia.png" /> <img src="<?=base_url()?>static/images/footer-logo/compliance/pci.png" /> <img src="<?=base_url()?>static/images/footer-logo/compliance/trustwave.png" /> <img src="<?=base_url()?>static/images/footer-logo/compliance/visa-verified.png" />
        </div>
    	<div class="right">
        <div>Metode Pengiriman</div>
        <img src="<?=base_url()?>static/images/footer-logo/logistic/jne-express.png" />
        </div>
    </div>
</div>
