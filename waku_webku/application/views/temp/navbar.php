<?php
$carts = $this->cart->contents();
$cnt_carts = count($carts);
$cnt_carts = $cnt_carts > 0 ? $cnt_carts : '0';
$url_download = '';
$url2 = $this->uri->segment(1);
switch($browser) {
	case 'Chrome';
		$url_download = 'https://play.google.com/store/apps/details?id=com.bigit.wakuliner.customer';
	break;
	case 'Safari';
	$url_download = 'https://itunes.apple.com/us/app/wakuliner-food-delivery-order/id1070066694?ls=1&mt=8';
	break;
	default:
		$url_download = 'https://play.google.com/store/apps/details?id=com.bigit.wakuliner.customer';
}
$is_home = false;
if(stripos($_SERVER['REQUEST_URI'], '/wakuantar') !== false)
{
	$menu_wakuantar = true;
	$menu_wakuwiku = false;
	$menu_wakukatering = false;
}
if(stripos($_SERVER['REQUEST_URI'], '/wakuwiku') !== false)
{
	$menu_wakuantar = false;
	$menu_wakuwiku = true;
	$menu_wakukatering = false;
}
if(stripos($_SERVER['REQUEST_URI'], '/wakukatering') !== false)
{
	$menu_wakuantar = false;
	$menu_wakuwiku = false;
	$menu_wakukatering = true;
}
if(
	stripos($_SERVER['REQUEST_URI'], '/wakuantar') === false &&
	stripos($_SERVER['REQUEST_URI'], '/wakuwiku') === false &&
	stripos($_SERVER['REQUEST_URI'], '/wakukatering') === false
	)
{
	$menu_wakuantar = true;
	$menu_wakuwiku = true;
	$menu_wakukatering = true;
	$is_home = true;
}

?>
<style type="text/css">
    @media only screen and (max-device-width: 767px) {
    /* For desktop: */
    .logowakucustom img{
        width: 20% !important;
    }
}


</style>
    <div class="wsmenucontainer respmenu clearfix">
        <div class="overlapblackbg"></div>
        <div class="wsmobileheader clearfix  logowakucustom" style="background-color: #ffffff;">
                        <a href="<?=base_url()?>"><img src="<?= static_file() ?>images/logo/logo-left.png" alt="" /></a><a href="<?=$url_download;?>"><img src="<?= static_file() ?>images/logo/logo-right.png" alt="" /></a>
            <a id="wsnavtoggle" class="animated-arrow"><span></span></a>
        </div>
        
        <div class="wk-container-nav">
            <div class="headtoppart">
            	<div class="mobile-view">
					<?php if($this->session->userdata('login_user') == false): ?>
                        <ul class="wsmenu-list2">
                        <?php

						if($menu_wakuantar)
						{
						?>
                        <li><a href="<?=base_url()?>wakuantar"><img src="<?= static_file() ?>images/icon/navWA.png"><br /><span class="wk_black">WAKU-ANTAR</span> </a></li>
                        <?php
						}
						if($menu_wakuwiku)
						{
						?>
                        <li><a href="<?=base_url()?>wakuwiku"><img src="<?= static_file() ?>images/icon/navWW.png"><br /><span class="wk_black">WAKU-WIKU</span> </a></li>
                        <?php
						}

						if($menu_wakukatering)
						{
						?>
                        <li><a href="#"><img src="<?= static_file() ?>images/icon/navWK.png"><br /><span class="wk_black">WAKU-KATERING</span> </a></li>
                        <?php
						}
						if(!$is_home)
						{
						?>
                        <li style="max-width:70px;"><a href="<?=base_url()?>promo"><img src="<?= static_file() ?>images/icon/listpromo.png"><br /><span class="wk_black">PROMO</span> </a></li>
                        <?php
						}
						?>
                        </ul>
                    <?php else : ?>
                        <ul class="wsmenu-list3">
                        <?php

						if($menu_wakuantar)
						{
						?>
                        <li><a href="<?=base_url()?>wakuantar"><img src="<?= static_file() ?>images/icon/navWA.png"><br /><span class="wk_black">WAKU-ANTAR</span> </a></li>
                        <?php
						}
						if($menu_wakuwiku)
						{
						?>
                        <li><a href="<?=base_url()?>wakuwiku"><img src="<?= static_file() ?>images/icon/navWW.png"><br /><span class="wk_black">WAKU-WIKU</span> </a></li>
                        <?php
						}

						if($menu_wakukatering)
						{
						?>
                        <li><a href="#"><img src="<?= static_file() ?>images/icon/navWK.png"><br /><span class="wk_black">WAKU-KATERING</span> </a></li>
                        <?php
						}
						if(!$is_home)
						{
						?>
                        <li style="max-width:70px;"><a href="<?=base_url()?>promo"><img src="<?= static_file() ?>images/icon/listpromo.png"><br /><span class="wk_black">PROMO</span> </a></li>
                        <?php
						}
						?>
                        </ul>
                    <?php endif; ?>
                </div>
                <div class="headerwp mobile-hide" style="margin-top: 8px;">
                    <div class="headertopright">
                        <a href="<?=base_url()?>"><b><span class="bhs_ingg">HOME</span><span class="bhs_indo">BERANDA</span></b></a>
                    <?php if($this->session->userdata('login_user') == false): ?>
                        <a href="#" data-toggle="modal" data-target="#registrasiModal" class="bordercenter"><b><span class="bhs_ingg">REGISTER</span><span class="bhs_indo">DAFTAR</span></b></a>
                        <a href="#" data-toggle="modal" data-target="#loginModal"><b><span class="bhs_ingg">LOGIN</span><span class="bhs_indo">MASUK</span></b></a>
                        <a href="<?=base_url()?>cart"><img src="<?=static_file()?>images/icon/cart.png" /> <span class="text-cart bhs_indo">Keranjang</span> <span class="badge badge-warning"><?php echo $cnt_carts;?></span></a>
                    <?php else: ?>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-link dropdown-toggle wk_style" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b>Welcome, <?=$this->session->userdata('name')?></b> <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                              <li><a href="<?=base_url()?>order"><img src="<?=static_file()?>images/icon/order.png" width="23" /> <span class="bhs_ingg">Order</span><span class="bhs_indo">Pesanan</span></a></li>
                              <li><a href="<?=base_url()?>setting/account"><img src="<?=static_file()?>images/icon/profile.png" width="23" /> <span class="bhs_ingg">Account</span><span class="bhs_indo">Akun</span></a></li>
                              <li><a href="<?=base_url()?>reward"><img src="<?=static_file()?>images/icon/rewards.png" width="23" /> WAKU POINTS</a></li>
										<li><a href="<?=base_url()?>payment/konfirmasi"><img src="<?=static_file()?>images/icon/konfirmasi.png" width="23" /> <span class="bhs_ingg">Confirm Payment</span><span class="bhs_indo">Konfirmasi Pembayaran</span></a></li>
                              <li><a href="<?=base_url()?>setting"><img src="<?=static_file()?>images/icon/setting.png" width="23" /> <span class="bhs_ingg">Setting</span><span class="bhs_indo">Pengaturan</span></a></li>
                              <li><a href="<?=base_url()?>about"><img src="<?=static_file()?>images/icon/about-us.png" width="23" /> <span class="bhs_ingg">About</span><span class="bhs_indo">Tentang</span></a></li>
                              <li><a href="<?=base_url()?>contact"><img src="<?=static_file()?>images/icon/contact.png" width="23" /> <span class="bhs_ingg">Contact Us</span><span class="bhs_indo">Kontak Kami</span></a></li>
                              <li><a href="#" data-toggle="modal" data-target="#confLogoutModal"><span class="label label-warning"><i class="glyphicon glyphicon-log-out"></i></span> <span class="text-cart bhs_ingg">Logout</span> <span class="text-cart bhs_indo">Keluar</span></a></li>
                            </ul>
                        </div>
                         <a href="<?=base_url()?>cart"><img src="<?=static_file()?>images/icon/cart.png" /> <span class="text-cart bhs_ingg">Cart</span> <span class="text-cart bhs_indo">Keranjang</span> <span class="badge badge-warning"><?php echo $cnt_carts;?></span></a>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="headerfull mobile-hide">
                <!--Main Menu HTML Code-->
                <div class="wsmain">
                    <div class="smllogo">
                        <a href="<?=base_url()?>"><img src="<?= static_file() ?>images/logo/logo-left.png" alt="" /></a><a href="<?=$url_download;?>"><img src="<?= static_file() ?>images/logo/logo-right.png" alt="" /></a>
                    </div>
                    <nav class="wsmenu clearfix">
                        <ul class="mobile-sub wsmenu-list">
                                <li>
                                    <a class=" styletriangle clearfix <?=$mn_wakuantar?>" href="<?=base_url()?>wakuantar"><img src="<?= static_file() ?>images/icon/navWA.png"><span class="wk-nav-list">WAKU-ANTAR</span> </a>
                                </li>
                                <li>
                                    <a class=" styletriangle clearfix <?=$mn_wakuwiku?>" href="<?=base_url()?>wakuwiku"><img src="<?= static_file() ?>images/icon/navWW.png"><span class="wk-nav-list">WAKU-WIKU</span> </a>
                                </li>
                                <li>
                                    <span class="disable clearfix" href="#"><img src="<?= static_file() ?>images/icon/navWK.png"><span class="coming-soon-menu">Coming Soon</span><span class="wk-nav-list">WAKU-KATERING</span> </span>
                                </li>
                                <?php if($page!="home"): ?>
                                <li>
                                    <?php if($this->session->userdata('login_user') == false): ?>
                                        <a class="wk-promo-nav-list <?=$mn_promo?>" href="#" data-toggle="modal" data-target="#loginModal"><b>PROMO</b></a>
                                    <?php else:
										if($url2 == 'wakuantar'){
									?>

                                        <a id="promo_url" href="#" class="wk-promo-nav-list <?=$mn_promo?>" href=""> <span class="wk-nav-list">PROMO</span> </a>
                                    <?php }
										if($url2 == 'wakuwiku'){ ?>
											<a class="wk-promo-nav-list <?=$mn_promo?>" href="#" data-toggle="modal" data-target="#promoWakWiku"><b>PROMO</b></a>
									<?php } endif; ?>
                                </li>
                                <?php endif; ?>
                        </ul>
                    </nav>
                </div>
                <!--Menu HTML Code-->
            </div>
            <div class="headerfull mobile-view">
                <!--Main Menu HTML Code-->
                <div class="wsmain">
                    <div class="smllogo">
                        <a href="<?=base_url()?>"><img src="<?= static_file() ?>images/logo/logo-left.png" alt="" /></a>
                    </div>
                    <nav class="wsmenu clearfix">
                        <ul class="mobile-sub wsmenu-list">
                          <?php if($this->session->userdata('login_user') == false): ?>
                          	  <li><button type="button" class="btn btn-primary col-lg-12 col-xs-12"><img src="<?= static_file() ?>images/icon/fb2.png" /> Login/Sign up with Facebook</button></li>
                              <li><a href="#" data-toggle="modal" data-target="#registrasiModal" class="bordercenter"><b>REGISTER</b></a></li>
                              <li> <a href="#" data-toggle="modal" data-target="#loginModal"><b>LOGIN</b></a></li>
                          <?php else: ?>
                              <li> <a href="<?=base_url()?>cart"><img src="<?=static_file()?>images/icon/cart.png" /> <b><span class="text-cart">Cart</span> <span class="badge badge-warning"><?php echo $cnt_carts;?></span></b></a></li>
                              <li><a href="<?=base_url()?>order"><img src="<?=static_file()?>images/icon/order.png" width="23" /> Order</a></li>
                              <li><a href="<?=base_url()?>setting/account"><img src="<?=static_file()?>images/icon/profile.png" width="23" /> Account</a></li>
                              <li><a href="<?=base_url()?>reward"><img src="<?=static_file()?>images/icon/rewards.png" width="23" /> WAKU POINTS</a></li>
										<li><a href="<?=base_url()?>payment/konfirmasi"><img src="<?=static_file()?>images/icon/konfirmasi.png" width="23" /> Confirm Payment</a></li>
                              <li><a href="<?=base_url()?>setting"><img src="<?=static_file()?>images/icon/setting.png" width="23" /> Setting</a></li>
                              <li><a href="<?=base_url()?>about"><img src="<?=static_file()?>images/icon/about-us.png" width="23" /> About</a></li>
                              <li><a href="<?=base_url()?>contact"><img src="<?=static_file()?>images/icon/contact.png" width="23" /> Contact Us</a></li>                              
                              <li><a href="#" data-toggle="modal" data-target="#confLogoutModal"><span class="label label-warning"><i class="glyphicon glyphicon-log-out"></i></span> <span class="text-cart">Logout</span></a></li>
                          <?php endif; ?>
                        </ul>
                    </nav>
                </div>
                <!--Menu HTML Code-->
            </div>
        </div>
    </div>
<script>

$('#promo_url').click(function(){
	var new_href = '<?php echo site_url('wakuantar');?>';
	var local_kodepos_wa = localStorage.getItem("kode_pos_wa");

	if(local_kodepos_wa > 0){
		new_href = '<?php echo site_url('promo/wakuantar');?>/'+local_kodepos_wa;
	}else{
	    alert('Harus Pilih Area terlebih dahulu');
	}
	window.location.href = new_href;
})

</script>
