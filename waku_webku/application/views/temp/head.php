<title>Wakuliner</title>
<meta name="keywords" content="Wakuliner" />
<meta name="description" content="Wakuliner-Culinary Marketplace" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="apple-touch-icon" href="#">
<link rel="icon" type="image/png" href="<?= base_url() ?>favicon.ico" sizes="32x32" />
<link rel="icon" type="image/png" href="<?= base_url() ?>favicon.ico" sizes="16x16" />
<!-- jquery -->
<script type="text/javascript" src="<?= static_file() ?>js/jquery.min.js"></script>
<!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="<?= static_file() ?>css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?= static_file() ?>css/css-stars.css">
<script type="text/javascript" src="<?= static_file() ?>js/bootstrap.js"></script>
<!-- menu slide -->
<link rel="stylesheet" type="text/css" media="all" href="<?= static_file() ?>css/webslidemenu.css" />
<script type="text/javascript" src="<?= static_file() ?>js/webslidemenu.js"></script>
<!-- fontawesome -->
<link rel="stylesheet" href="<?= static_file() ?>fonts/font-awesome/css/font-awesome.min.css" />
<!-- style me -->
<link rel="stylesheet" type="text/css" href="<?= static_file() ?>css/style.css">
<link rel="stylesheet" type="text/css" href="<?= static_file() ?>css/responsive.css">
<script type="text/javascript" src="<?= static_file() ?>js/jquery.barrating.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<script src="<?=static_file()?>js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?= static_file() ?>js/main.js"></script>
<script>
	var base_url = '<?=base_url()?>';
</script>
<!-- end -->
<style>
.bhs_ingg{
	display:none;
}
.bhs_indo{
	display:none;
}
</style>