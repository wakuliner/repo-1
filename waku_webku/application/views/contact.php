<?php
$lang_user = $this->session->userdata('lang_user');
$place_name = 'Nama Anda';
$place_email = 'Alamat Email';
$place_comment = 'Komentar atau Pertanyaan Anda';
$place_telp = 'Nomor Telepon';
if($lang_user == 'bhs_ingg'){
	$place_name = "Your Name";
	$place_email = 'Email';
	$place_comment = 'Coment or Question';
	$place_telp = 'Phone Number';
}
?>
	<section class="wk-container-1 nobg" style="background-color:white;">
      <div class="container-fluid nopadding">
        
        <div class="col-lg-4 col-xs-12 side_contact" style="background-color:white;border-right: 1px #259b00 solid;">
        	<section>    
                <div class="col-lg-offset-1 col-lg-10">
                    
                    <div class="text-center">
                        <h4><span class="bhs_ingg">CONTACT US</span><span class="bhs_indo">KONTAK KAMI</span></h4>
                    </div>
                    
                    <center><img src="<?=static_file()?>images/content/contactimg.png" class="img-responsive imgicon"/></center>
                    
                    <div class="text-center">
                        <p><span class="bhs_ingg"If you have question, please contact us at :</span><span class="bhs_indo">Jika anda punya pertanyaan, tolong hubungi kami</span></p>
                    </div>
                    
                    <div class="contact_info hidden-xs">
                        <p><img src="<?=static_file()?>images/icon/phone.png" align="absmiddle" /> <?=$info->info[6]->value_setting?></p>
                        <p><img src="<?=static_file()?>images/icon/phonesmall.png" align="absmiddle" /> <?=$info->info[8]->value_setting?></p>
                        <p><img src="<?=static_file()?>images/icon/mail.png" align="absmiddle" /> <?=$info->info[10]->value_setting?></p>
                    </div>
                    <div class="contact_info visible-xs" style="text-align: center;">
                        <p><img src="<?=static_file()?>images/icon/phone.png" align="absmiddle" /> <?=$info->info[6]->value_setting?></p>
                        <p><img src="<?=static_file()?>images/icon/phonesmall.png" align="absmiddle" /> <?=$info->info[8]->value_setting?></p>
                        <p><img src="<?=static_file()?>images/icon/mail.png" align="absmiddle" /> <?=$info->info[10]->value_setting?></p>
                    </div>
                    
                </div>
            </section>
        </div>
        
        <div class="col-lg-8 col-xs-10 hidden-xs">
        	<div class="col-xs-1 visible-xs" ></div>
        	<div style="padding-top:30px;">
            	<p><span class="bhs_ingg">Any quetion or feedback for us ?</span><span class="bhs_indo">Ada pertanyaan atau masukan untuk kami ?</span></p>
                <p><span class="bhs_ingg">Please fill out the form below, we will contact you.</span><span class="bhs_indo">Silahkan isi form dibawah ini, kami akan menghubungi anda.</span></p>
            </div>      
            <hr />             
            <form style="width:70%">
              <div class="radio">
                  <label><input type="radio" name="jenis"> <span class="bhs_ingg">Are you a merchant?</span><span class="bhs_indo">Apakah anda merchant?</span></label>
                  <label><input type="radio" name="jenis"> <span class="bhs_ingg">Are you a customer?</span><span class="bhs_indo">Apakah anda customer?</span></label>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="nama" placeholder="<?php echo $place_name;?>">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="email" placeholder="<?php echo $place_email;?>">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="telp" placeholder="<?php echo $place_telp;?>">
              </div>
              <div class="form-group">
                <textarea class="form-control" placeholder="<?php echo $place_comment;?>"></textarea>
              </div>
              <button type="submit" class="btn btn-success btn-block">Submit</button>
            </form> 
        </div>
        <div class="col-lg-8 col-xs-12 visible-xs" >
          <div class="col-xs-1 visible-xs" ></div>
          <div style="padding-top:30px;text-align: center;">
              <p><span class="bhs_ingg">Any quetion or feedback for us ?</span><span class="bhs_indo">Ada pertanyaan atau masukan untuk kami ?</span></p>
                <p><span class="bhs_ingg">Please fill out the form below, we will contact you.</span><span class="bhs_indo">Silahkan isi form dibawah ini, kami akan menghubungi anda.</span></p>
            </div>      
            <hr />             
            <form style="width:70%;margin-left: 15%;">
              <div class="radio">
                  <label><input type="radio" name="jenis"> <span class="bhs_ingg">Are you a merchant?</span><span class="bhs_indo">Apakah anda merchant?</span></label>
                  <label><input type="radio" name="jenis"> <span class="bhs_ingg">Are you a customer?</span><span class="bhs_indo">Apakah anda customer?</span></label>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="nama" placeholder="<?php echo $place_name;?>">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="email" placeholder="<?php echo $place_email;?>">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="telp" placeholder="<?php echo $place_telp;?>">
              </div>
              <div class="form-group">
                <textarea class="form-control" placeholder="<?php echo $place_comment;?>"></textarea>
              </div>
              <button type="submit" class="btn btn-success btn-block">Submit</button>
            </form> 
        </div>
            
     </div>
   </section>