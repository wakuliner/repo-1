
	<section class="wk-container-1 nobg">
    
      <div class="container-fluid nopadding">
      	<section id="cta3" class="wow fadeIn">
            <div class="container">
                <div class="section-header">
                    <h2 class="title text-center wow fadeInDown animated" style="color: rgb(255, 255, 255); font-weight: bold; font-family: &quot;Arista light&quot;, sans-serif; visibility: visible; animation-name: fadeInDown;">                Daftarkan Toko Anda dan Langsung Mulai Jualan di Wakuliner!</h2>
                </div>
    
            </div>
        </section>
        
      </div>
      
      <div class="container-fluid">
        
        <div class="col-lg-12 col-xs-12">
        	  
        	<section id="features" style="padding:50px 0;">
                <div class="container">
                    <div class="sub-header">
                        <p class="sub-title col-sm-offset-1" style="font-size:15px; line-height:30px;">
                        Ayo daftarkan toko Anda dan mulai berjualan! Ga pakek lama, ga pake ribet!<br><br>
                        Anda punya toko online atau usaha rumahan di bidang kuliner, restoran, depot, atau cafe? Mau punya <b>toko kuliner online</b> dan kendali penuh terhadap produk dan pemesanan Anda? Yuk jadi Waku-Merchant di Wakuliner, Wadah kuliner pertama di Indonesia.<br><br>
        
                        Syarat untuk menjadi Waku-Merchant:<br><br>
                        1. Punya smartphone, untuk download aplikasi Merchant (Android atau iOS).<br>
                        *Kenapa harus ada smartphone?*<br>
                        Pesanan dari pelanggan akan langsung masuk ke dalam aplikasi di smartphone, yang telah kami <b>design secara khusus untuk kuliner</b>, sehingga Anda dapat melayani pesanan dengan cepat dan akurat. <br><br>
                        2. Punya logo tempat usaha<br><br>
                        3. Punya foto menu<br><br>
                        4. Bisa antar pesanan dengan JNE atau kurir sendiri <i>(Wakuliner tidak menyediakan kurir pengantaran)</i><br><br>
                        5. Punya rekening bank (untuk menerima pembayaran online)<br><br>
        
                        Ayo segera download <b>Wakuliner Merchant app</b> dari Google Play Store / App Store dan langsung daftarkan toko Anda dan langsung mulai berjualan.<br><br>
                        Kunjungi <a href="http://wakuliner.com/panduan-merchant.php" target="_blank">halaman panduan-merchant</a> untuk detil cara daftar, cara terima pesanan, dan kelola toko.<br><br>
                        Selamat berjualan!<br><br>
                        <i>(Klik dibawah ini untuk download Wakuliner Merchant)</i> <br>
                        </p><div>
                                                <div class="col-sm-3 col-sm-offset-3">
                                <div>
                                <a href="https://play.google.com/store/apps/details?id=com.bigit.wakuliner.merchant" target="_blank">
                                    <div class="team-img">
                                        <img class="img-responsive" style="width:90%;" src="<?= static_file() ?>images/icon/gplay.png">
                                    </div>
                                </a>
                                </div>
                            </div>
        
                            <div class="col-sm-3">
                                <div>
                                <a href="https://itunes.apple.com/us/app/renilukaw/id1074598371?mt=8" target="_blank">
                                    <div class="team-img">
                                        <img class="img-responsive" style="width:90%;" src="<?= static_file() ?>images/icon/appstore.png">
                                    </div>
                                </a>
                                </div>
                            </div>
                                            </div> <br>
        <p></p>
                    </div>
                    
                </div>
            </section>
            
        	</div>
        </div>
        
        <div class="container-fluid nopadding">    
     		<section id="joinus">
                <div class="col-sm-offset-4 text-center" style="margin-top:20px;">
                    <div class="form_join">
                    <img src="<?=static_file()?>images/content/joinus2.png" style="position:absolute;margin-left:-500px; margin-top:-70px;">
                        <h1 style="font-size:28px; color:#006306; font-weight:500px;">Ada pertanyaan atau masukan untuk kami?</h1>
                        <p style="color:#000;line-height:15px;">Silahkan isi form dibawah ini.<br> Kami akan menghubungi Anda</p>
                        <div style="margin-bottom:40px;"></div>
                        <form id="main-contact-form" name="contact-form" method="post" action="#">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Name" required="">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Email" required="">
                            </div>
                            <div class="form-group">
                                <input type="text" name="telp" class="form-control" placeholder="No Telp" required="">
                            </div>
                            <div class="form-group">
                                <input type="text" name="tanya" class="form-control" placeholder="Pertanyaan / Masukan" required="">
                            </div>
                            <button id="contactLink"><p style="font-size:20px;margin-top:10px;">SUBMIT</p></button>
                            <div style="margin-bottom:75px;"></div>
                
                        </form>
                
                    </div>
                </div>
                <div style="margin-bottom:30px;"></div>
           </section>
       </div>
   </section>