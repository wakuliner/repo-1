
    <section class="wk-container-1 bg-full2">
        <div class="container wk-content-1">
            <div class="col-lg-7 col-centered">
                <div id="login-box" class="col-lg-offset-2 col-lg-8 <?=$content_login?>">
                   	
                    <div class="well wk_style">
                    	<h4 align="center">Welcome to WAKULINER</h4>
                        <center><span>You are entering your cool store zone</span></center>
                        <br />
                        <?php 
							if(!empty($msg)){
								echo '<center><span style="color:red;">'.$msg.'</center>';
							}
						?>
                        <form id="frm_login" method="post">
                          <div class="form-group">
                            <input type="email" class="form-control" name="email_login" required="required" placeholder="Email">
                          </div>
                          <div class="form-group">
                            <input type="password" class="form-control" name="pass_login" required="required" placeholder="Password">
                          </div>
                          <div class="form-group clearfix">
                          	<button type="button" id="tombolRegister" class="btn btn-success col-lg-5 col-xs-5">Register</button>
                            <span class="col-lg-2 col-xs-2"></span>
                  			<button type="submit" name="btn_login" value="1" class="btn btn-warning col-lg-5 col-xs-5">Login</button>
                          </div>
                        </form>
                          <div class="form-group clearfix">
                            <button type="button" class="btn btn-primary col-lg-12 col-xs-12"><img src="<?= static_file() ?>images/icon/fb2.png" /> Login/Sign up with Facebook</button>
                          </div>
                    </div>
                    
                </div>
                <div id="register-box" class="col-lg-12 <?=$content_register?>">
                   	
                    <div class="well wk_style">
                    	<h4 align="center">Welcome to WAKULINER</h4>
                        <center><span>You are entering your cool store zone</span></center>
                        <br />
                        
                        <form id="form-register">
                          <div class="form-group clearfix">
                            <div class="col-lg-12 col-xs-12">
                            	<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama">
                          	</div>
                          </div>
                          <div class="form-group clearfix">
                          	<div class="col-lg-6 col-xs-6">
                            	<input type="email" class="form-control" name="email_register" id="email_register" placeholder="Alamat Email">
                            </div>
                            <div class="col-lg-6 col-xs-6">
                                <input type="text" class="form-control" name="hp" id="hp" placeholder="No Handphone">
                            </div>
                          </div>
                          <div class="form-group clearfix">
                          	<div class="col-lg-6 col-xs-6">
                            	<input type="text" class="form-control" name="ulang_tahun" id="ulang_tahun" placeholder="Ulang Tahun">
                          	</div>                            
                            <div class="col-lg-6 col-xs-6">
                                <select class="form-control" name="jns_kelamin" id="jns_kelamin" placeholder="Jenis Kelamin">
                                <option value="">Jenis Kelamin</option>
                                <option value="1">Pria</option>
                                <option value="2">Wanita</option>
                                </select>
                            </div>
                          </div>
                          <div class="form-group clearfix">
                          	<div class="col-lg-6 col-xs-6">
                            	<input type="password" class="form-control"  name="pass" id="pass" placeholder="Password">
                          	</div>
                            <div class="col-lg-6 col-xs-6">
                                <input type="password" class="form-control" name="conf_pass" id="conf_pass" placeholder="Konfirmasi Password">
                            </div>
                          </div>
                          <div class="form-group clearfix">
                          	<div class="col-lg-12 col-xs-12">
                                <button type="button" id="tombolLogin" class="btn btn-success col-lg-5 col-xs-5">Login</button>
                                <span class="col-lg-2 col-xs-2"></span>
                                <button type="button" class="btn btn-warning col-lg-5 col-xs-5" id="btn_register">Register</button>
                          	</div>
                          </div>
                          <small class="text-muted">Dengan menekan tombol "Register", Anda sudah membaca dan menerima <a href="#">Syarat & Ketentuan</a> Wakuliner.</small>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    
    <h3 align="center" class="wk_style wk_login_tagline">Wadah Kuliner Untuk Semua Jenis Kuliner, Pertama di Indonesia.</h3>
    
    <section>
    	
        <div class="container wk_style">

            <ul class="nav nav-tabs nav-justified">
              <li role="tab" class="active" data-toggle="tab" data-target="#wakuantar"><a href="#">Waku Antar</a></li>
              <li role="tab" data-toggle="tab" data-target="#wakuwiku"><a href="#">Waku Wiku</a></li>
              <li role="tab" data-toggle="tab" data-target="#wakukatering"><a href="#">Waku Katering</a></li>
            </ul>
			
            <div class="tab-content content_about">
                <div role="tabpanel" class="tab-pane active wk_content_desc" id="wakuantar">
                	<p align="center" class="text-muted">
                    	Waku-antar memberikan jasa pesan-antar tercepat di indonesia. dengan 2.850 merhants di 30 kota. Pesan makanan langsung diantar oleh penjual ke lokasi anda atau orang kesayangan anda.
                    </p>
                    <br /><h5 align="center"><b>Caranya mudah sekali!!</b></h5><br />
                    <center><img src="<?=static_file()?>images/content/WAScheme.png" /></center>
                	<div class="text-center" style="margin-top:30px;">
                    	<a href="<?=base_url()?>" class="btn btn-warning btn-lg">TRY WAKU ANTAR NOW <i class="glyphicon glyphicon-chevron-right"></i></a>
                   </div>
                </div>
                <div role="tabpanel" class="tab-pane wk_content_desc" id="wakuwiku">
                	<p align="center" class="text-muted">
                    	Waku-Wiku memberikan jasa pesan-antar tercepat di indonesia. dengan 2.850 merhants di 30 kota. Pesan makanan langsung diantar oleh penjual ke lokasi anda atau orang kesayangan anda.
                    </p>
                    <bR /><h5 align="center"><b>Caranya mudah sekali!!</b></h5><br />
                    <center><img src="<?=static_file()?>images/content/WWScheme.png" /></center>
                    <div class="text-center" style="margin-top:30px;">
                    	<a href="<?=base_url()?>" class="btn btn-warning btn-lg">TRY WAKU WIKU NOW <i class="glyphicon glyphicon-chevron-right"></i></a>
                   </div>
                </div>
                <div role="tabpanel" class="tab-pane wk_content_desc" id="wakukatering">
                	waku katering
                </div>
            </div>
        </div>
        
    </section>
    
  <section>
    	
        <div class="container-fluid">
        	
            <div class="box-coment">
            	<p align="center">APA KATA MEREKA TENTANG WAKULINER</p>
                <img src="<?=static_file()?>images/icon/wakutestimoni.png" class="mobile-hide"/>
            </div>
            
            <div class="box_coment_content">
            
            	<div class="col-lg-6">
                	<div class="well clearfix">
                    	
                        <div class="col-lg-3 col-xs-3">
                        	<img src="<?=static_file()?>images/testimonial/Yuanita Christiani.PNG" class="img-responsive img-circle"/>
                        </div>
                        <div class="col-lg-9 col-xs-9">
                        	<p><b>Yuanita Christiani</b></p>
                            <p>Sahabatku rekomendasiin aku download aja app kuliner baru. Aseelikk!!! ini app nya keren buanget!!! Solusi kuliner... pesen makanan, cari wisata kuliner legendaris, juga bakalan bisa pesen catering :D</p>
                        </div>
                        
                    </div>
                </div>
                
                <div class="col-lg-6">
                	<div class="well clearfix">
                    	
                        <div class="col-lg-3 col-xs-3">
                        	<img src="<?=static_file()?>images/testimonial/Ferry Salim.JPG" class="img-responsive img-circle"/>
                        </div>
                        <div class="col-lg-9 col-xs-9">
                        	<p><b>Ferry Salim</b></p>
                            <p>Ada app kuliner baru nih! WAKULINER! Solusi kuliner! bisa pesen makanan, cari wisata kuliner legendaris, juga bakalan bisa pesen catering. Cobain download guys, FREE!</p>
                        </div>
                        
                    </div>
                </div>
            	
            	<div class="col-lg-6">
                	<div class="well clearfix">
                    	
                        <div class="col-lg-3 col-xs-3">
                        	<img src="<?=static_file()?>images/testimonial/Alena.jpeg" class="img-responsive img-circle"/>
                        </div>
                        <div class="col-lg-9 col-xs-9">
                        	<p><b>Alena</b></p>
                            <p>Sebagai pencinta kuliner sejak kecil, Wakuliner apps ini aku banget! Segala tentang kuliner ada: guide wisata kuliner LEGENDARIS nya lengkap. Lalu pesen makanan panas, frozen, dari luar pulau juga bisa. Menanti fitur catering yang coming soon.</p>
                        </div>
                        
                    </div>
                </div>
                
                <div class="col-lg-6">
                	<div class="well clearfix">
                    	
                        <div class="col-lg-3  col-xs-3">
                        	<img src="<?=static_file()?>images/testimonial/Evelyn.jpeg" class="img-responsive img-circle"/>
                        </div>
                        <div class="col-lg-9 col-xs-9">
                        	<p><b>Evelyn</b></p>
                            <p>Aplikasi Wakuliner memudahkan saya untuk memesan makanan dengan biaya yang sangat ekonomis. Apalagi saya suka melakukan wisata kuliner, dengan fitur Waku Wiku saya dpat mengetahui tmpat makan terupdate. Wakuliner merupakan aplikasi kuliner all in 1</p>
                        </div>
                        
                    </div>
                </div>
                
                <div class="col-lg-6">
                	<div class="well clearfix">
                    	
                        <div class="col-lg-3  col-xs-3">
                        	<img src="<?=static_file()?>images/testimonial/Hangesti.jpg" class="img-responsive img-circle"/>
                        </div>
                        <div class="col-lg-9 col-xs-9">
                        	<p><b>Hangesti</b></p>
                            <p>Saya pesan trus di Wakuliner, dari pesen sampe pengiriman praktis banget ya. Wahhh kalau begini ga punya pulsa pun bisa pesen makanan nihh.</p>
                        </div>
                        
                    </div>
                </div>
                
                
                
                 
                
            </div>
            
        </div>
        
    </section>
<script>
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
$('#btn_register').click(function(){
	var pass = $('#pass').val();
	var conf_pass = $('#conf_pass').val();
	var email = $('#email_register').val();
	var jns_kelamin = $('#jns_kelamin').val();
	if(email == ''){
		alert('Email harus di isi');
		return false;
	}
	if(!(validateEmail(email))){
		alert('Email tidak valid');
		return false;
	}
	if(jns_kelamin == ''){
		alert('Jenis Kelamin harus di isi');
		return false;
	}
	if(pass == ''){
		alert('Password harus di isi');
		return false;
	}
	if(conf_pass == ''){
		alert('Konfirmasi Password harus di isi');
		return false;
	}
	if(pass != conf_pass){
		alert('Password dan Konfirmasi Password tidak sesuai');
		return false;
	}
	var dt = $('#form-register').serialize();
	var url = '<?php echo site_url('main/reg');?>';
	var msg = '';
	$.ajax({
		data:dt,
		type:'POST',
		url : url,
		success:function(response){	
			var obj = JSON.parse(response);		
			msg = obj.message;
			if(obj.status == 1){
				msg += '\n Kami mungkin perlu menghbungi Anda mengenai pesanan dan pembayaran Anda, dan untuk memberikan pelayanan yang terbaik untuk Anda';
				// msg +='\n Hello cullinary lover.To ensure quality and safety of our transactions, tou will rate merchants and merchants will rate you too. Happy shopping.';
			}
		
			alert(msg);
			if(obj.status == 1){
			    //$('#registrasiModal').modal('hide');
			    //$('#loginModal').modal('show');
				window.location.href = '<?php echo site_url('login');?>';
			}
		}
	})

});
</script>