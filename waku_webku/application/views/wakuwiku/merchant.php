<script type="text/javascript">
$(document).on('click', 'img.preview-on-click', function(e){
	e.preventDefault();
	var url = $(this).attr('src');
	$('#modal-preview .modal-body').html('<img src="'+url+'">');
	$('#modal-preview').modal();
});
</script>
<style type="text/css">
#modal-preview .modal-body{
	position:relative;
	text-align:center;
}
#modal-preview .modal-body img{
	width:100%;
	vertical-align:bottom;
	max-width:600px;
}
</style>

<div class="modal fade" id="modal-preview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body wk_white text-center">
      	
      </div>
    </div>
  </div>
</div>
	<section class="wk-container-1 nobg" style="background-color:white;">
      <div class="container-fluid nopadding">
        
        <div class="col-lg-3 col-xs-12 side_menu_wiku">
			<?=$merchant_info2= $merchant_info->merchant_info[0];?>
            <h4 class="text-center"><b>Merchant Info</b></h4>
            <hr />
            <h4><!--<a href="<?php echo base_url('wakuwiku/merchant/'.$merchant_id)?>" class="wk_style">--><b><?php echo $merchant_info2->merchant_name;?></b></h4>
            <p><b><?=$merchant_info2->nama_jalan;?> <?=$merchant_info2->no_jalan;?> <?=$merchant_info2->nama_kota;?></b></p>
            <img src="<?=$merchant_info2->loc_banner.'/'.$merchant_info2->banner?>" class="img-responsive" />
			<br>
            <p align="justify" class="text-muted">
            	<?php echo $merchant_info2->about;?>
            </p>
            <p><a href="#"><?php echo $merchant_info2->link_website;?></a></p>
            <div><b><span class="bhs_ingg">Cost for two</span><span class="bhs_indo">Harga Berdua</span> : +/- Rp. <?php echo $merchant_info2->kisaran_harga;?></b></div>
            <br />
            <div class="alert alert-success" align="center" style="background-color:white;">
            	<p><b><span class="bhs_ingg">Operational Hours:</span><span class="bhs_indo">Jam Buka</span></b></p>
            	<?php 
					foreach($merchant_info2->operation_hours as $oh){
						echo '<p><b>'.$oh->name_days.' : '.date('H:i', strtotime($oh->start_operation)).' - '.date('H:i', strtotime($oh->end_operation)).' WIB</b></p>';
					}
				?>
            </div>
            <div class="row">
                <div class="col-lg-6 col-xs-6">
                    <a onclick="alert('<?=$merchant_info2->telp?>');" class="btn btn-ww btn-block"> Call Us <img src="<?=static_file()?>images/icon/call.png" width="20" /></a>
                </div>
                <div class="col-lg-6 col-xs-6">
                    
                    <a href="http://www.google.com/maps/place/<?=$merchant_info2->lat?>,<?=$merchant_info2->long?>" target="_blank" class="btn btn-ww btn-block"> Visit Us <img src="<?=static_file()?>images/icon/location.png" width="20" /> </a>
                </div>
            </div>
            <br />
            <div>
            	<img src="<?=static_file()?>images/icon/infopromo.png"/>
                <b style="margin-left: 10px;">Promo 10%
				<!--
				<?php foreach($merchant_info2->promo as $promo_wiku): ?>
              	<?php echo "$promo_wiku->detail"?>
                <?php endforeach; ?>
				-->
				</b>
            </div>
            <div>
            	<img src="<?=static_file()?>images/icon/infotakeaway.png"/>
                <b style="margin-left: 10px;">
				<?php foreach($merchant_info2->service as $sv): ?>
              	<?=$sv->name_service?>, 	
                <?php endforeach; ?>
                </b>
            </div>
        </div>
        
        <div class="col-lg-9 col-xs-12 wk_style">
        	
            <div class="row" style="padding-top:20px">
               <a href="#" onclick="window.history.back();" class="text-success"><i class="glyphicon glyphicon-menu-left"></i> <span class="bhs_ingg">Back to merchant list</span><span class="bhs_indo">Kembali ke list merchant</span></a>
            </div>
            
            <div style="margin-top:20px;">
                
                <div class="col-lg-7 col-xs-12">
                	<?php $merchant_menu = $merchant_info2->menu; ?>
                    <div class="menu clearfix">
                		<div class="wk_header_text"><img src="<?=static_file()?>images/icon/menu.png" height="20" /> Menu (<?=count($merchant_menu)?>)</div>
                        <hr />
                        <div class="clearfix">
						<?php $a=1; foreach($merchant_menu as $mm):?>
						<?php if($a<=3): ?>
                            <div class="col-lg-4 col-xs-6" style="margin-bottom:5px;">
                                <img src="<?=$mm->loc_image?>/<?=$mm->image?>" class="img-responsive preview-on-click" />
                            </div>
                       	<?php endif;?>
                        <?php $a++; endforeach; ?>
                        </div>
                		<div class="pull-right">
                            <div class="col-lg-12">
                                <a href="<?=base_url()?>wakuwiku/merchant_menu/<?=$merchant_id?>" class="btn btn-ww2"><span class="bhs_ingg">See more</span><span class="bhs_indo">Lihat Semua</span></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="galeri">
                    	<?php $merchant_galery = $merchant_info2->gallery; ?>
                		<div class="wk_header_text"><img src="<?=static_file()?>images/icon/photo.png" height="20" /> <span class="bhs_ingg">Photo Galery</span><span class="bhs_indo">Galeri Photo</span> (<?=$merchant_info2->jml_photo_gallery?>)</div>
                        <hr />
                		<div class="clearfix">
						<?php $b=1; foreach($merchant_galery as $gl):?>
                        <?php if($b<=3): ?>    
                            <div class="col-lg-4 col-xs-6" style="margin-bottom:5px;">
                                <img src="<?=$gl->loc_image?>/<?=$gl->image?>" class="img-responsive preview-on-click" />
                            </div>
                        <?php endif; ?>
                        <?php $b++; endforeach; ?>
                        </div>
                		<div class="pull-right">
                            <div class="col-lg-12">
                                <a href="<?=base_url()?>wakuwiku/merchant_galeri/<?=$merchant_id?>" class="btn btn-ww2"><span class="bhs_ingg">See more</span><span class="bhs_indo">Lihat Semua</span></a>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="col-lg-5 col-xs-12">
                    <div class="wk_header_text"><img src="<?=static_file()?>images/icon/rating.png" height="20" /> Rating (<?=$merchant_info2->rating?>)</div>
					<div align="center">
                    	<h4 align="center">
                        	<?php 
								$rate = 0;
								$rate = $merchant_info2->rating;
								$rates = $rate/10;
								$int_rate = (int)$rates;
								$add = 0;
								$btg_setengah = '';
								if($rates > $int_rate){
									$btg_setengah = '<img src="'.static_file().'images/icon/starhalf.png" />';
									$add = 1;
								}
								$star_empty = 5 - ($int_rate + $add);
							?>
							<?php for($b=0;$b<$int_rate;$b++): ?>
							<img src="<?=static_file()?>images/icon/starfull.png" />
							<?php endfor; 
								echo $btg_setengah;
							?>
							<?php for($b=0;$b<$star_empty;$b++): ?>
							<img src="<?=static_file()?>images/icon/starempty.png" />
							<?php endfor; ?> 
                        </h4>
                        <b><?=$merchant_info2->jml_rating?> <span class="bhs_ingg">People</span><span class="bhs_indo">Orang</span></b>
                    </div>
                    <bR />
                    <?php $c=1; foreach($merchant_rating->list_user_rating_merchant as $mr): ?>
                    <?php if($c<=4): ?>
                    <div class="panel panel-success">
                        <div class="panel-body nopadding">
                            <div class="col-lg-6 col-xs-6"><h5><b><?=$mr->first_name?></b></h5></div>
                            <div class="col-lg-6 col-xs-6">
                                <h4 align="center">
                                	<?php 
										$rate = 0;
										$rate = $mr->rating;
										$rates = $rate/10;
										$int_rate = (int)$rates;
										$add = 0;
										$btg_setengah = '';
										if($rates > $int_rate){
											$btg_setengah = '<img src="'.static_file().'images/icon/starhalf.png" />';
											$add = 1;
										}
										$star_empty = 5 - ($int_rate + $add);
									?>
									<?php for($b=0;$b<$int_rate;$b++): ?>
									<img src="<?=static_file()?>images/icon/starfull.png" />
									<?php endfor; 
										echo $btg_setengah;
									?>
									<?php for($b=0;$b<$star_empty;$b++): ?>
									<img src="<?=static_file()?>images/icon/starempty.png" />
									<?php endfor; ?>
                                </h4>
                            </div>
                            <div class="col-lg-12 col-xs-12">
                                <p class="text-muted"><?=$mr->review?></p>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php $c++; endforeach; ?>
                </div>
                <div class="pull-right">
                	<div class="col-lg-12">
                		<a href="<?=base_url()?>wakuwiku/merchant_rating/<?=$merchant_id?>" class="btn btn-ww2"><span class="bhs_ingg">Add Rating</span><span class="bhs_indo">Tambah Rating</span></a>
                	</div>
                </div>
                
            </div>
        <div>
            
     </div>
   </section>