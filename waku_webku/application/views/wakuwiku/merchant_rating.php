
	<section class="wk-container-1 nobg" style="background-color:white;">
      <div class="container-fluid nopadding">
        
        <div class="col-lg-3 col-xs-12 side_menu_wiku">
			
            <?=$merchant_info2= $merchant_info->merchant_info[0];?>
            <h4 class="text-center"><b>Merchant Info</b></h4>
            <hr />
            <h4><!--<a href="<?php echo base_url('wakuwiku/merchant/'.$merchant_id)?>" class="wk_style">--><b><?php echo $merchant_info2->merchant_name;?></b></h4>
            <p><b><?=$merchant_info2->nama_jalan;?> <?=$merchant_info2->no_jalan;?> <?=$merchant_info2->nama_kota;?></b></p>
            <img src="<?=$merchant_info2->loc_banner.'/'.$merchant_info2->banner?>" class="img-responsive" />
            <br>
			<p align="justify" class="text-muted">
            	<?php echo $merchant_info2->about;?>
            </p>
            <p><a href="#"><?php echo $merchant_info2->link_website;?></a></p>
            <div><b>Cost for two : +/- Rp <?php echo $merchant_info2->kisaran_harga;?></b></div>
            <br />
            <div class="alert alert-success" align="center" style="background-color:white;">
            	<p><b>Operational Hours:</b></p>
            	<?php 
					foreach($merchant_info2->operation_hours as $oh){
						echo '<p><b>'.$oh->name_days.' : '.date('H:i', strtotime($oh->start_operation)).' - '.date('H:i', strtotime($oh->end_operation)).' WIB</b></p>';
					}
				?>
            </div>
            <div class="row">
                <div class="col-lg-6 col-xs-6">
                    <a onclick="alert('<?=$merchant_info2->telp?>');" class="btn btn-ww btn-block"> Call Us <img src="<?=static_file()?>images/icon/call.png" width="20" /></a>
                </div>
                <div class="col-lg-6 col-xs-6">
                    
                    <a href="http://www.google.com/maps/place/<?=$merchant_info2->lat?>,<?=$merchant_info2->long?>" target="_blank" class="btn btn-ww btn-block"> Visit Us <img src="<?=static_file()?>images/icon/location.png" width="20" /> </a>
                </div>
            </div>
            <br />
            <div>
            	<img src="<?=static_file()?>images/icon/infopromo.png"/>
                <b style="margin-left: 10px;">Promo 10%</b>
            </div>
            <div>
            	<img src="<?=static_file()?>images/icon/infotakeaway.png"/>
                <b style="margin-left: 10px;">
				<?php foreach($merchant_info2->service as $sv): ?>
              	<?=$sv->name_service?>, 	
                <?php endforeach; ?>
                </b>
            </div>
            
        </div>
        
        <div class="col-lg-9 col-xs-12 wk_style">
        	
            <div class="row" style="padding-top:20px">
               <a href="<?=base_url()?>wakuwiku/merchant/<?=$merchant_id?>" class="text-success"><i class="glyphicon glyphicon-menu-left"></i> Back to merchant list</a>
            </div>
            
            <div style="margin-top:20px;">
                
                <div class="col-lg-12 col-xs-12">
                	
                    <div class="galeri">
                		<div class="wk_header_text"><img src="<?=static_file()?>images/icon/rating.png" height="20" /> Rating (<?=$merchant_rating->rating_akumulasi?>)</div>
                        <hr />
                		<div align="center">
                    	<h4 align="center">
                        	<?php 
								$rate = 0;
								$rate = $merchant_rating->rating_akumulasi;
								$rates = $rate/10;
								$int_rate = (int)$rates;
								$add = 0;
								$btg_setengah = '';
								if($rates > $int_rate){
									$btg_setengah = '<img src="'.static_file().'images/icon/starhalf.png" />';
									$add = 1;
								}
								$star_empty = 5 - ($int_rate + $add);
							?>
							<?php for($b=0;$b<$int_rate;$b++): ?>
							<img src="<?=static_file()?>images/icon/starfull.png" />
							<?php endfor; 
								echo $btg_setengah;
							?>
							<?php for($b=0;$b<$star_empty;$b++): ?>
							<img src="<?=static_file()?>images/icon/starempty.png" />
							<?php endfor; ?> 
                        </h4>
                        <b><?=$merchant_rating->total_review?> People</b>
                    </div>
                    <bR />
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <div class="col-lg-3 col-xs-12 clearfix">
                                <div class="well" align="center">
                                    <select id="example-css" name="rating" autocomplete="off">
                                      <option value="">0</option>
                                      <option value="10">1</option>
                                      <option value="20">2</option>
                                      <option value="30">3</option>
                                      <option value="40">4</option>
                                      <option value="50">5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-7 col-xs-6 clearfix">
                                <textarea class="form-control" id="komentar" placeholder="write your comment here" rows="3"></textarea>
                            </div>
                            <div class="col-lg-2 col-xs-6 clearfix btn-rating">
                                <button type="button" onclick="sendrating();" class="btn btn-success btn-block" style="height:70px;">Send</button>
                            </div>
                        </div>
                    </div>
                    <br />
                    <?php foreach($merchant_rating->list_user_rating_merchant as $mr): ?>
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <div class="col-lg-6 col-xs-12">
                            	<h5><b><?=$mr->first_name?></b></h5>
                                <p class="text-muted"><?=$mr->review?></p>
                           </div>
                            <div class="col-lg-6 col-xs-12">
                                <h4 align="center">
                                	<?php 
										$rate = 0;
										$rate = $mr->rating;
										$rates = $rate/10;
										$int_rate = (int)$rates;
										$add = 0;
										$btg_setengah = '';
										if($rates > $int_rate){
											$btg_setengah = '<img src="'.static_file().'images/icon/starhalf.png" />';
											$add = 1;
										}
										$star_empty = 5 - ($int_rate + $add);
									?>
									<?php for($b=0;$b<$int_rate;$b++): ?>
									<img src="<?=static_file()?>images/icon/starfull.png" />
									<?php endfor; 
										echo $btg_setengah;
									?>
									<?php for($b=0;$b<$star_empty;$b++): ?>
									<img src="<?=static_file()?>images/icon/starempty.png" />
									<?php endfor; ?>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    </div>
                    
                </div>

        <div>
            
     </div>
   </section>
   
<script>
	function sendrating()
	{
		var merchant_id = <?=$merchant_id?>;
		var rate = $('#example-css').val();
		var komentar = $('#komentar').val();
		var url = '<?php echo site_url('wakuwiku/send_rating');?>';	
		$.ajax({
			data : { merchant_id : merchant_id, rating:rate, review:komentar},
			url : url,
			type : "POST",
			success:function(response){
				alert(response);
				location.reload();					
			}
		});	
	}
</script>