<script type="text/javascript">
$(document).on('click', 'img.preview-on-click', function(e){
	e.preventDefault();
	var url = $(this).attr('src');
	$('#modal-preview .modal-body').html('<img src="'+url+'">');
	$('#modal-preview').modal();
});
</script>
<style type="text/css">
#modal-preview .modal-body{
	position:relative;
	text-align:center;
}
#modal-preview .modal-body img{
	width:100%;
	vertical-align:bottom;
	max-width:600px;
}
</style>

<div class="modal fade" id="modal-preview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body wk_white text-center">
      	
      </div>
    </div>
  </div>
</div>

	<section class="wk-container-1 nobg" style="background-color:white;">
      <div class="container-fluid nopadding">
        
        <div class="col-lg-3 col-xs-12 side_menu_wiku">
			
            <?=$merchant_info2= $merchant_info->merchant_info[0];?>
            <h4 class="text-center"><b>Merchant Info</b></h4>
            <hr />
            <h4><!--<a href="<?php echo base_url('wakuwiku/merchant/'.$merchant_id)?>" class="wk_style">--><b><?php echo $merchant_info2->merchant_name;?></b></h4>
            <p><b><?=$merchant_info2->nama_jalan;?> <?=$merchant_info2->no_jalan;?> <?=$merchant_info2->nama_kota;?></b></p>
            <img src="<?=$merchant_info2->loc_banner.'/'.$merchant_info2->banner?>" class="img-responsive" />
            <br>
			<p align="justify" class="text-muted">
            	<?php echo $merchant_info2->about;?>
            </p>
            <p><a href="#"><?php echo $merchant_info2->link_website;?></a></p>
            <div><b>Cost for two : +/- Rp <?php echo $merchant_info2->kisaran_harga;?></b></div>
            <br />
            <div class="alert alert-success" align="center" style="background-color:white;">
            	<p><b>Operational Hours:</b></p>
            	<?php 
					foreach($merchant_info2->operation_hours as $oh){
						echo '<p><b>'.$oh->name_days.' : '.date('H:i', strtotime($oh->start_operation)).' - '.date('H:i', strtotime($oh->end_operation)).' WIB</b></p>';
					}
				?>
            </div>
            <div class="row">
                <div class="col-lg-6 col-xs-6">
                    <a onclick="alert('<?=$merchant_info2->telp?>');" class="btn btn-ww btn-block"> Call Us <img src="<?=static_file()?>images/icon/call.png" width="20" /></a>
                </div>
                <div class="col-lg-6 col-xs-6">
                    
                    <a href="http://www.google.com/maps/place/<?=$merchant_info2->lat?>,<?=$merchant_info2->long?>" target="_blank" class="btn btn-ww btn-block"> Visit Us <img src="<?=static_file()?>images/icon/location.png" width="20" /> </a>
                </div>
            </div>
            <br />
            <div>
            	<img src="<?=static_file()?>images/icon/infopromo.png"/>
                <b style="margin-left: 10px;">Promo 10%</b>
            </div>
            <div>
            	<img src="<?=static_file()?>images/icon/infotakeaway.png"/>
                <b style="margin-left: 10px;">
				<?php foreach($merchant_info2->service as $sv): ?>
              	<?=$sv->name_service?>, 	
                <?php endforeach; ?>
                </b>
            </div>
            
        </div>
        
        <div class="col-lg-9 col-xs-12 wk_style">
        	
            <div class="row" style="padding-top:20px">
               <a href="<?=base_url()?>wakuwiku/merchant/<?=$merchant_id?>" class="text-success"><i class="glyphicon glyphicon-menu-left"></i><span class="bhs_ingg"> Back to merchant list</span><span class="bhs_indo"> Kembali ke list merchant</span></a>
            </div>
            
            <div style="margin-top:20px;">
                
                <div class="col-lg-12 col-xs-12">
                	
                    <div class="galeri">
                		<div class="wk_header_text"><i class="glyphicon glyphicon-camera"></i> Photo Galery (<?=count($merchant_galery->gallery)?>)</div>
                        <hr />
                		<div class="clearfix">
						<?php foreach($merchant_galery->gallery as $gl):?>
                            <div class="col-lg-3 col-xs-4" style="margin-bottom:5px;">
                                <img src="<?=$gl->loc_image?>/<?=$gl->image?>" class="img-responsive preview-on-click" />
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                    
                </div>

        <div>
            
     </div>
   </section>