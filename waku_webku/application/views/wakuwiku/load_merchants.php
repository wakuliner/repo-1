<style type="text/css">
/*
.list-column-1, list-column-2, .list-column-3{
	vertical-align:top;
}
.list-column-1{
	width:30% !important;
	position:relative !important;
	display:table-cell;
	max-width:133px;
}
.list-column-2{
	display:table-cell;
	width:70% !important;
	min-width:calc(90% - 133px);
}
.list-column-3{
	width:10% !important;
	text-align:center !important;
	position:absolute;
	top:0;
	right:0;
}
.list-group-item .row.clearfix{
	display:block !important;
	position:relative !important;
	padding-bottom:10px;
}
.list-column-1 a img{
	max-width:calc(100% - 10px);
	width:calc(100% - 10px);
}
*/
@media screen and (min-width:781px)
{
.list-group-item .rating, .list-group-item .pricing{
	float:left;
	padding:5px 20px 5px 0;	
	text-align:left;	
	width:50%;
}
}
@media screen and (max-width:780px)
{
.list-group-item .rating, .list-group-item .pricing{
	float:none;
	padding:5px 0px 5px 0;
	text-align:left;	
}
.table-content td p{
	margin-bottom:0;
}
}
td.list-column-1{
	width:18%;
	max-width:133px;
	padding-right:10px;
}
td.list-column-1 a img{
	width:100%;
	min-width: 60px;
}
td.list-column-3{
	width:43px;
	text-align:center;
	padding-left:5px;
}
.table-content{
	margin-bottom:10px;
}
.table-content td{
	vertical-align:top;
}
</style>
<div style="margin-top:20px;">
                
                <div class="list-group"> 
                
                	<?php 
						$lang_user = $this->session->userdata('lang_user');
						$logo = '';
						$rate = 0;
						$btg_setengah = '';
						$rates = 0;
						$int_rate = 0;
						$add = 0;
						$star_empty = 0;
						$payment_online = '';
						$payment_cod = '';
						$tags_name = array();
						if(!empty($list_merchant->list_merchant)){
							foreach($list_merchant->list_merchant as $lm){ 
								#print_r($lm);
								$logo = '';
								$logo = $lm->loc_banner.'/'.$lm->banner;
							?>
                       <div class="list-group-item"> 
                    	<div class="row clearfix">
                        	<table cellpadding="0" cellpadding="0" border="0" width="100%" class="table-content">
                            <tr>
                            <td class="list-column-1">
                                <a href="<?=base_url()?>wakuwiku/merchant/<?=$lm->merchant_id?>"><img src="<?php echo $logo;?>" class="img-responsive" /></a>
                            </td>
                            <td class="list-column-2">
							<a href="<?=base_url()?>wakuwiku/merchant/<?=$lm->merchant_id?>">
                            	<div class="col-lg-12 col-xs-12 nopadding">
                                    <p>
                                        <a href="<?=base_url()?>wakuwiku/merchant/<?=$lm->merchant_id?>" class="wk_style"><b><?php echo $lm->merchant_name;?></b></a><br /> 
                                        <small class="hastag">
                                        <?php 
											$cnt = 0;
											$cnt = count($lm->tag);
											$i = 1;
											$tags_name = array();
											foreach($lm->tag as $t){
												echo '<a href="#">'.$t->tags_name.'</a>';
												if($i < $cnt){
													echo ', ';
												}
												array_push($tags_name, $t->tags_name);
												$i++;
											}
											
										?>
                                        </small>
                                    </p>
                                    <p><b><?php echo $lm->nama_jalan;?> <?php echo $lm->no_jalan;?></b></p>
                                </div>
                                <div class="row" style="margin-bottom:10px;">
                                    <div class="rating">
                                       <?php 
											$rate = 0;
											$rate = $lm->rating;
											$rates = $rate/10;
											$int_rate = (int)$rates;
											$add = 0;
											$btg_setengah = '';
											if($rates > $int_rate){
												$btg_setengah = '<img src="'.static_file().'images/icon/greenhalf.png" />';
												$add = 1;
											}
											$star_empty = 5 - ($int_rate + $add);
										?>
                                    	<?php for($b=0;$b<$int_rate;$b++): ?>
                                    	<img src="<?=static_file()?>images/icon/greenfull.png" />
                                        <?php endfor; 
											echo $btg_setengah;
										?>
                                        <?php for($b=0;$b<$star_empty;$b++): ?>
                                    	<img src="<?=static_file()?>images/icon/greenempty.png" />
                                        <?php endfor; ?>
                                        (<?php $jml_ratingweb = $lm->jml_rating;if (is_null($jml_ratingweb)) {echo $jml_ratingweb = "0";}else{echo $jml_ratingweb;}?> reviews)
                                    </div>
                                    <div class="pricing">
                                        <img src="<?=static_file()?>images/icon/infocost.png" />
                                        +/- Rp. <?=$lm->kisaran_harga?>/2 <span class="bhs_ingg">person</span><span class="bhs_indo">orang</span>
                                    </div>
                                </div>
							</a>
                            </td>
                            <td class="list-column-3">
                            	<?php 
									if($lm->is_halal == 1){ ?>
										<img src="<?=static_file()?>images/icon/halal.png"/>
								<?php } else { ?>
										<?php echo ""; ?>
								<?php } ?>
                                <br /><br />
                                <a href="<?=base_url()?>wakuwiku/merchant/<?=$lm->merchant_id?>"><img src="<?=static_file()?>images/icon/next.png" width="20px" /></a>
                            </td>
                            </tr>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-xs-6">
                                <a onclick="alert('<?=$lm->telp?>');" class="btn btn-ww btn-block-callus"> <img src="<?=static_file()?>images/icon/call.png" width="20" /><span class="bhs_ingg"> Call Us</span><span class="bhs_indo"> Hubungi Kami</span></a>
                            </div>
                            <div class="col-lg-6 col-xs-6">
                                
                                <a href="http://www.google.com/maps/place/<?=$lm->lat?>,<?=$lm->long?>" target="_blank" class="btn btn-ww btn-block-visitus"> <img src="<?=static_file()?>images/icon/location.png" width="20" /><span class="bhs_ingg"> Visit Us</span><span class="bhs_indo"> Kunjungi Kami</span></a>
                            </div>
                        </div>
                    </div> 
							<?php }
						}
						?>
                	
                    
                    
                </div>
                
            </div>
<script>

var lang_user = '<?php echo $lang_user;?>';
$('.bhs_indo').hide();
$('.bhs_ingg').hide();
if(lang_user != ''){
	$('.'+lang_user).show();
}else{
	$('.bhs_indo').show();
}

</script>