
	<section class="wk-container-1 nobg">
      <div class="container-fluid nopadding">
        
        <div class="col-lg-3 col-xs-12 side_wiku mobile-hide">
			
            <div class="page-header text-center">
              <h4>FILTER</h4>
            </div>
            
            <div class="text-left">
                <b><span class="bhs_indo">Populer</span><span class="bhs_ingg">Popular</span></b> <a href="#" class="pull-right text-success"><i class="glyphicon glyphicon-remove"></i> <span class="bhs_ingg"> Clear</span><span class="bhs_indo"> Hapus</span></a>
            </div>
            <ul class="list-group wk_black">
              <?php 
				foreach($filter_merchant->tag as $fm){
					if($fm->tipe == 1){ ?>
						<li class="list-group-item">
                        	<?php echo $fm->tags_name;?> <span class="pull-right"><input type="checkbox" name="tags[]" value="<?php echo $fm->tags_name;?>" onclick='handleClick();' /></span>
                        </li>
			<?php	}
				}
			?>
            </ul>
            
            <div class="text-left">
                <b>Reguler</b> <a href="#" class="pull-right text-success"><i class="glyphicon glyphicon-remove"></i> <span class="bhs_ingg"> Clear</span><span class="bhs_indo"> Hapus</span></a>
            </div>
            <ul class="list-group wk_black">
              <?php 
				foreach($filter_merchant->tag as $fm){
					if($fm->tipe == 2){ ?>
						<li class="list-group-item">
                        	<?php echo $fm->tags_name;?> <span class="pull-right"><input type="checkbox" name="tags[]" value="<?php echo $fm->tags_name;?>" onclick='handleClick();' /></span>
                        </li>
			<?php	}
				}
			?>
            </ul>
  
        </div>
        
        <div class="col-lg-9 col-xs-12 wk_style nopadding">
        	
            <div class="row" style="padding-top:30px">
                
                <div class="col-lg-7 col-xs-12">
                	
                    <div class="col-lg-3 col-xs-4" align="center">
                		<a href="<?=base_url()?>wakuwiku">
                        	<img src="<?=static_file()?>images/icon/homeWA.png" /><br />
                        	<small class="wk_black bhs_indo">Ganti Kota/Area</small>
                            <small class="wk_black bhs_ingg">Change City/Area</small>
                    	</a>
                    </div>
                    <div class="col-lg-9 col-xs-8 wk_style wk_header_text">
                    	<?php echo ucfirst($nama_kota);?>
                    </div>
                
                </div>
                <div class="col-lg-5 col-xs-12">
                	
                    <div class="input-group">
                      <input type="text" name="merchant_name" class="form-control" id="merchant_name" placeholder="Resto's name or food name" onkeyup="handleClick();">
                      <span class="input-group-btn">
                        <button class="btn btn-default" id="btn_search" type="button"><img src="<?=static_file()?>images/icon/search.png" width="16px;" /></button>
                      </span>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="row">
            	
                <div class="col-lg-3" align="right" style="padding-top:5px;">Urutkan</div>
                <div class="col-lg-7">
                	<select class="form-control" id="sort" onchange="handleClick();" />
                    	<option value="2">Yang paling dekat dengan saya</option>
                        <option value="1">Rating</option>
                    </select>
                </div>
            </div>
            <div class="loading" align="center">
            	<img src="<?=static_file()?>images/icon/loading.gif" />
            </div>
            <div id="list_merchants"></div>
           
            </div>

        <div>
            
     </div>
   </section>
   
<script>
var id_kota = '<?php echo $id_kota;?>';
load_data(id_kota,'','','');
function load_data(id_kota, merhcant_name, tag, sortlist){	
	var html = '';	
	$("#list_merchants").html(html);
	var url = '<?php echo site_url('wakuwiku/load_merchants');?>';	
	$.ajax({
		data : {id_kota : id_kota, merchant_name : merhcant_name, tag : tag, sortlist : sortlist },
		url : url,
		type : "POST",
		beforeSend  : function(){ $('#container-loader-list').show(); },
		success:function(response){
			html += response;
			$("#list_merchants").html(html);
			$('.loading').addClass("hide");
		}
	});	
}
$('#btn_search').click(function(){
	//var food_name = $('#search_food').val();
	//load_data(kode_pos,food_name,id_kelurahan,'');
	handleClick();
});
function handleClick() {
    $('.loading').removeClass("hide");
  var tags = [];
  var merchant_name = $('#merchant_name').val();
  var sortlist = $('#sort').val();
  tags = $("input[name^='tags']:checked").map(function(idx, elem) {
		var elems = $(elem).val();
		return elems;
  }).get();
  load_data(id_kota,merchant_name, tags, sortlist);
}

</script>