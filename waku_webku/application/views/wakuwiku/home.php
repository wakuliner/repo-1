<?php
$lang_user = $this->session->userdata('lang_user');
$place_holder = 'Atau masukan lokasi anda disini';
$place_city = 'Kota';
if($lang_user == 'bhs_ingg'){
	$place_holder = 'Or enter your location here';
	$place_city = 'City';
}

?>
	
    <section class="wk-container-1 bg-full3">
        <input type="hidden" id="myLat" class="myLat" value="" />
        <input type="hidden" id="myLong" class="myLong" value="" />
        <form action="<?=base_url()?>wakuwiku/search" method="post" onsubmit="return chk_kota()">
        <div class="container wk-content-1">
            <div class="wk-content-search">
                <div class="col-lg-12 clearfix">
                    <p class=" set-search " style="color:black;"><i><span class="bhs_ingg">Use Waku-Wiku's service to find your favorite restaurants</span><span class="bhs_indo">Gunakan layanan Waku-Wiku dan temukan restoran-restoran favorit Anda</span></i> </p>
                    <!--  <img src="images/content/map1.png" class="img-responsive"> -->
                    <div class="location-container">
                        <div class="location-trigger">
                            <div class="location-trigger-bar">
                                <a href="javascript:;" id="btn_getLoc"><span class="bhs_ingg">Get your location</span><span class="bhs_indo">Dapatkan lokasi anda</span></a>
                            </div>
                            <div class="location-trigger-button">
                                <a href="javascript:;" id="btn_getLoc2"></a>
                            </div>
                        </div>
                    </div>
                        <div class="location-container">
                            <div class="location-trigger">
                                <div class="location-trigger-bar">
                                    <input class="input-go" id="location" type="text" name="kota" placeholder="<?php echo $place_holder;?>" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?php echo $place_holder;?>'" data-toggle="modal" data-target="#cityModal">
                        		</div>
                                <div class="location-trigger-button">
                                    <input type="hidden" name="id_kota" id="idkota" />
                                    <button type="submit" class="btn btn-success btn-block btn-lg searchgo"> <span class="bhs_ingg">GO</span><span class="bhs_indo">PERGI</span> </button>
                                </div>
                            </div>
                        </div>

               </div>
            </div>
        </div>
    	</form>
    </section>
    
    <div class="modal fade" id="cityModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-city">
          <div class="modal-body">
            <div class="form-group">
            	<div class="input-group">
                  <input type="text" class="form-control" id="city" onkeyup="searchCity();" placeholder="<?php echo $place_city;?>">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><img src="<?=static_file()?>images/icon/search.png" width="16px;" /></button>
                  </span>
                </div>
            </div>
            <div class="form-group">
            	<div class="list-group">
                  <a href="#" class="list-group-item wk_city"><b class="wk_style"><span class="bhs_ingg">top cities</span><span class="bhs_indo">kota populer</span></b></a>
                  <?php foreach($kota->city as $k): ?>
                  <a href="#" class="list-group-item wk_city" onclick="select_city('<?=$k->nama_kota?>','<?=$k->id_kota?>');"><span class="cityTeks"><?=$k->nama_kota?></span></a>
                  <?php endforeach; ?>
                  <!--<a href="#" class="list-group-item wk_city"><b class="wk_style">OTHER CITIES</b></a>-->
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<script>

var lang_user = '<?php echo $lang_user;?>';
function chk_kota(){
	var id_kota = $('#idkota').val();
	if(id_kota == ''){
		var msg = 'Masukkan dulu kota dan area anda';
		if(lang_user == 'bhs_ingg'){
			msg = 'First enter your city and area';
		}
		$('#error_reg_info').html(msg);
		$('#errorRegModal').modal('show');
		return false;
	}
}

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (p) {
		$('#myLong').val('');
		$('#myLat').val('');
		var latitude = p.coords.latitude;
		var longitude =  p.coords.longitude;
		$('#myLong').val(longitude);
		$('#myLat').val(latitude);
		console.log('lat :'+latitude);
		console.log('long :'+longitude);
       
    });
} else {
    alert('Geo Location feature is not supported in this browser.');
}
$('#btn_getLoc').click(function(){
	var long = $('#myLong').val();
	var lat = $('#myLat').val();
	var url = '<?php echo site_url('wakuwiku/get_area_ll');?>';
	//var action = '<?php echo site_url('wakuwiku/search');?>';
	$('#kode_posku').val('');
	$('#id_kelurahanku').val('');
	$('#id_kotaku').val('');
	$('#location').val('');
	$('#my_city').val('');
	$.ajax({
		url : url,
		type:"POST",
		data : {lat : lat, longitude:long},
		success: function(data){
			//console.log(data);
			var dt_json = JSON.parse(data);
			$('#idkota').val(dt_json['id_kota']);
			//$('#kode_posku').val(dt_json['kode_pos']);
	 		//$('#id_kelurahanku').val(dt_json['id_kelurahan']);
			$('#location').val(dt_json['keywords']);
			//$('#my_city').val(dt_json['kota']);
			//$('#frm_search_merchants').attr('action', action);
			//$('#frm_search_merchants').submit();
		}
	});
});
$('#btn_getLoc2').click(function(){
	var long = $('#myLong').val();
	var lat = $('#myLat').val();
	var url = '<?php echo site_url('wakuwiku/get_area_ll');?>';
	//var action = '<?php echo site_url('wakuwiku/search');?>';
	$('#kode_posku').val('');
	$('#id_kelurahanku').val('');
	$('#id_kotaku').val('');
	$('#location').val('');
	$('#my_city').val('');
	$.ajax({
		url : url,
		type:"POST",
		data : {lat : lat, longitude:long},
		success: function(data){
			//console.log(data);
			var dt_json = JSON.parse(data);
			$('#idkota').val(dt_json['id_kota']);
			//$('#kode_posku').val(dt_json['kode_pos']);
	 		//$('#id_kelurahanku').val(dt_json['id_kelurahan']);
			$('#location').val(dt_json['keywords']);
			//$('#my_city').val(dt_json['kota']);
			//$('#frm_search_merchants').attr('action', action);
			//$('#frm_search_merchants').submit();
		}
	});
});
</script>