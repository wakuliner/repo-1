<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class converter {

    var $secret_key = 'optik';
    var $secret_iv = 'ramaoptikbandung';
	
    function __construct() {
        $ci = & get_instance();
    }

    function encode($string) {
		if($string != ""):
			$encrypt_method = "AES-256-CBC";
			$key = hash('sha256', $this->secret_key);
			$iv = substr(hash('sha256', $this->secret_iv), 0, 16);
			
			$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
			$output = base64_encode($output);
		else:
			$output = "";
		endif;
		
		return $output;
    }

    function decode($string) {
		
		if($string != ""):
			$encrypt_method = "AES-256-CBC";
			$key = hash('sha256', $this->secret_key);
			$iv = substr(hash('sha256', $this->secret_iv), 0, 16);	
			$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    	else:
			$output = "";
		endif;
		
		return $output;
	}

    function safe_b64encode($string) {

        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }

    function safe_b64decode($string) {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    function calculate_string($mathString) {
        $mathString = trim($mathString);     // trim white spaces
        $mathString = ereg_replace('[^0-9\+-\*\/\(\) ]', '', $mathString);    // remove any non-numbers chars; exception for math operators
        $compute = create_function("", "return (" . $mathString . ");");
        return 0 + $compute();
    }

    function encrypt($input) {
        $size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
        $input = $this->pkcs5_pad($input, $size);
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, '');
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $this->apikey, $iv);
        $data = mcrypt_generic($td, $input);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $data = base64_encode($data);
        return $data;
    }

    function pkcs5_pad($text, $blocksize) {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

    function decrypt($sStr) {
        $decrypted = mcrypt_decrypt(
                MCRYPT_RIJNDAEL_128, $this->apikey, base64_decode($sStr), MCRYPT_MODE_ECB
        );
        $dec_s = strlen($decrypted);
        $padding = ord($decrypted[$dec_s - 1]);
        $decrypted = substr($decrypted, 0, -$padding);
        return $decrypted;
    }

    function random($length = 10) {
        $str = '0123456789abcdefghjklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $random_word = str_shuffle($str);
        $word = substr($random_word, 0, $length);

        return $word;
    }
	
	function set_dateIndo($data) {
        date_default_timezone_set('Asia/Jakarta');
        $arr = explode(' ', $data);

        list($t, $b, $h) = split('[-]', $arr[0]);
        switch ($b) {
            case"01";
                $bln = "Januari";
                break;
            case"02";
                $bln = "Februari";
                break;
            case"03";
                $bln = "Maret";
                break;
            case"04";
                $bln = "April";
                break;
            case"05";
                $bln = "Mei";
                break;
            case"06";
                $bln = "Juni";
                break;
            case"07";
                $bln = "Juli";
                break;
            case"08";
                $bln = "Agustus";
                break;
            case"09";
                $bln = "September";
                break;
            case"10";
                $bln = "Oktober";
                break;
            case"11";
                $bln = "November";
                break;
            case"12";
                $bln = "Desember";
                break;
        }

        $tglIndo = "$h $bln $t";
        return $tglIndo;
    }

    function namaBulan($data) {
        
        switch ($data) {
            case"01";
                $bln = "Januari";
                break;
            case"02";
                $bln = "Februari";
                break;
            case"03";
                $bln = "Maret";
                break;
            case"04";
                $bln = "April";
                break;
            case"05";
                $bln = "Mei";
                break;
            case"06";
                $bln = "Juni";
                break;
            case"07";
                $bln = "Juli";
                break;
            case"08";
                $bln = "Agustus";
                break;
            case"09";
                $bln = "September";
                break;
            case"10";
                $bln = "Oktober";
                break;
            case"11";
                $bln = "November";
                break;
            case"12";
                $bln = "Desember";
                break;
        }
        return $bln;
    }
}

?>