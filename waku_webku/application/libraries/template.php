<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class template {

    function load($setting = '', $konten = '') {
        
        $ci = &get_instance();
		$setting['navbar']	=  $setting['set_menu'];
		
        $temp['head']           = $ci->load->view('temp/head', $setting['head'], TRUE);
		$temp['footer']         = $ci->load->view('temp/footer', $setting['footer'], TRUE);
        $temp['navbar']         = $ci->load->view('temp/navbar', $setting['navbar'], TRUE);
        $temp['content']        = $konten;
		
        /* MAIN CONTAINER */
        $ci->load->view('temp/container', $temp);
    }
}
