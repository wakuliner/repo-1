<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Join extends MY_Controller {

	function __construct() {
        parent::__construct();
    }
	
	public function index()
	{	
		$content = $this->load->view('join', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
}
