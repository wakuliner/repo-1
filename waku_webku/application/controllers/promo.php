<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends MY_Controller {

	function __construct() {
        parent::__construct();
    }
	
	public function index()
	{
		$post_data = array(
			'kode_pos'		=> '5678345'
		);
		$url = URL_API.'customerService/promo';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data['promo'] = json_decode($data_api);
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$setting['set_menu'] = array('mn_promo' => 'active');
		$content = $this->load->view('promo', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	public function wakuantar($kode_pos)
	{
		if($kode_pos != '' || !empty($kode_pos)){
			$post_data = array(
				'kode_pos'		=> $kode_pos
			);
			$url = URL_API.'customerService/promo';
			$data_api = $this->send_api->post_data($url, $post_data);
			$data['promo'] = json_decode($data_api);
			$this->load->library('user_agent');
			$data['browser'] = $this->agent->browser();
			$setting['set_menu'] = array('mn_promo' => 'active');
			$content = $this->load->view('promo', $data, TRUE);
			
			$this->template->load($setting, $content);
		}else{
			redirect('/wakuantar', 'refresh');
		}
		
	}
	
}
