<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wakuantar extends CI_Controller {

	function __construct() {
        parent::__construct();
    }
	
	public function index($nama_merchant = '')
	{
		$post_data = array(
			'last_update'		=> '2015-09-09'
		);
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		if(!empty($nama_merchant) && $nama_merchant != 'static' && $nama_merchant != 'search'){
			$res = '';
			$mn = '';
			$merchant_id = 0;
			$mn = str_replace('-',' ', $nama_merchant);
			$this->db->select('merchant_id');
			$this->db->where('merchant_name', $mn);
			$res = $this->db->get(merchant)->row();
			$merchant_id = $res->merchant_id;
			$kode_pos = $this->session->userdata('kode_pos');
			$id_kelurahan = $this->session->userdata('id_kelurahan');
			$statusCari = '';
			error_log($this->db->last_query());
			error_log($merchant_id);
			error_log($kode_pos);
			error_log($id_kelurahan);
			$this->menu($merchant_id, $kode_pos, $id_kelurahan, $statusCari);
		}else{
			$content = $this->load->view('wakuantar/home', $data, TRUE);
			$setting['set_menu'] = array('mn_wakuantar' => 'active');
	
			$this->template->load($setting, $content);

		}
		//$url_city = URL_API.'merchantServiceVersion2/get_location';
		//$list_city = $this->send_api->send_data($url_city, $post_data);
		//$data['list_city'] = json_decode($list_city);
	}
	
	public function menu($merchant_id = 0, $kode_pos = 0, $id_kelurahan = 0, $statusCari = 0){
		$post_data = array(
			"merchant_id"	=> $merchant_id,
			"kode_pos"		=> $kode_pos,
			"id_kelurahan"	=> $id_kelurahan,
			"statusCari"   => $statusCari
		);
		$url = URL_API.'merchantService/merchant_info';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data['merchant_info'] = json_decode($data_api);
		$url2 = URL_API.'merchantService/menu';
		$data_api2 = $this->send_api->post_data($url2, $post_data);
		$data['kode_pos'] = $kode_pos;
		$data['id_kelurahan'] = $id_kelurahan;
		$data['list_menu'] = json_decode($data_api2);
        $setting['set_menu'] = array('mn_wakuantar' => 'active');
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$data['statusCari'] = $statusCari;
		$content = $this->load->view('wakuantar/menu', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	public function load_merchants(){
		$kode_pos = isset($_POST['kode_pos']) ? $_POST['kode_pos'] : 0;
		$id_kelurahan = isset($_POST['id_kelurahan']) ? $_POST['id_kelurahan'] : 0;
		$food_name = isset($_POST['food_name']) ? $_POST['food_name'] : '';
		$tag = isset($_POST['tag']) ? $_POST['tag'] : '';
		$statuscari = isset($_POST['statuscari']) ? $_POST['statuscari'] : '';
		if(!empty($tag)){
			$tag = implode(',', $tag);
		}
		$post_data = array(
			"kode_pos"		=> $kode_pos,
			"tag"			=> $tag,
			"food_name"		=> $food_name,
			"id_kelurahan"	=> $id_kelurahan
		);
		
		//filtering URL_API
		if($statuscari == "1"){//siapsaji
			$url = URL_API.'merchantServiceVersion2/search_merchant';
			$data['statusCari'] = 1;
		}
		else{//snack
			$url = URL_API.'merchantServiceVersion2/search_merchant_new_list';
			$data['statusCari'] = 2;
		}
		$data_api = $this->send_api->post_data($url, $post_data);
		$data['list_merchant'] = json_decode($data_api);
		$data['kode_pos'] = $kode_pos;
		$data['id_kelurahan'] = $id_kelurahan;
		$this->load->view('wakuantar/load_merchants', $data);
	}
	
}
