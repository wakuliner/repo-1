<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller {

	function __construct() {
        parent::__construct();
  }
  public function konfirmasi(){
		$url = URL_API.'customerService/bank_wakuliner';
		$data_api = $this->send_api->post_data($url, true);
		$data = json_decode($data_api,true);
		$content = $this->load->view('transaction/konfirmasi', $data, TRUE);
		$this->template->load($setting, $content);
	}
	public function paymenttransfer(){
		$url = URL_API.'customerService/bank_wakuliner';
		$data_api = $this->send_api->post_data($url, true);
		$data = json_decode($data_api,true);
		//var_dump($data["bank"][0]["logo"]);

		$grand_ttl = $this->uri->segment(4);
		$purchaseid = $this->uri->segment(3);
		$data['grand_ttl'] = $grand_ttl;
		$data['purchase_id'] = $purchaseid;
		// var_dump($grand_ttl);
		// var_dump($data["grand_ttl"]);
		$content = $this->load->view('transaction/paymenttransfer', $data, TRUE);
		$this->template->load($setting, $content);
	}
	
	public function index(){
		$carts = $this->cart->contents();
		//error_log(serialize($carts));
		$LOGIN = $this->session->userdata('login_user');
		if($LOGIN == TRUE){
			$merchant_id = '';
			$kode_pos = '';
			$id_kelurahan = '';
			if(!empty($carts)){
				foreach($carts as $c){
					$merchant_id = '';
					$kode_pos = '';
					$id_kelurahan = '';
					$merchant_id = $c['merchant_id'];
					$kode_pos = $c['kode_pos'];
					$id_kelurahan = $c['id_kelurahan'];
					$statusCari = $c['statusCari'];
				}
			}
			$user_id = $this->session->userdata('user_id');
			//$user_id = 906;
			$post_data = array(
					"user_id"		=> $user_id
			);
			$url_address = URL_API.'customerService/profile';
			$data_api = $this->send_api->post_data($url_address, $post_data);
			$data['address'] = json_decode($data_api);
			//$merchant_id = 930;
			//$kode_pos = 1348;
			//$id_kelurahan = 3982;
			$post_merchant = array(
				"merchant_id"	=> $merchant_id,
				"kode_pos"		=> $kode_pos,
				"id_kelurahan"	=> $id_kelurahan
			);
			$url_merchant = URL_API.'merchantServiceVersion2/merchant_info';
			$data_merchant = $this->send_api->post_data($url_merchant, $post_merchant);
			$data['merchant_info'] = json_decode($data_merchant);
			
			
			$url_merchant2 = URL_API.'merchantServiceVersion2/search_merchant';
			$data_merchant2 = $this->send_api->post_data($url_merchant2, $post_merchant);
			$merchant_info2 = json_decode($data_merchant2);
			$id_kota = 0;
			foreach($merchant_info2->merchant as $merchant_info){
				if($merchant_info->merchant_id == $merchant_id){
					$id_kota = 0;
					$id_kota = $merchant_info->id_kota;
				}
			}
			
		    $post_data3 = array('last_update'=>'2015-09-09');
		   	$url_city = URL_API.'merchantServiceVersion2/get_location';
		    $list_city = $this->send_api->send_data($url_city, $post_data3);
		    $data['city_list'] = json_decode($list_city);
		    
			//$merchant_id = 930;
			//$url_kurir = URL_API.'merchantService/get_kurir_service/'.$merchant_id;
			//$data_kurir = $this->send_api->post_data($url_kurir, '');
			//$data['data_kurir'] = json_decode($data_kurir);
		  $data['statusCari'] = $statusCari;
			$data['id_kota'] = $id_kota;
			$data['carts'] = $carts;
			$data['merchant_id'] = $merchant_id;
			$url = URL_API.'merchantService/get_payment_channel';
			$payment_channel = $this->send_api->send_data($url, '');
			$data['payment_channel'] = json_decode($payment_channel);
			$this->load->library('user_agent');
		    $data['browser'] = $this->agent->browser();
			$content = $this->load->view('transaction/payment', $data, TRUE);
			
			$this->template->load($setting, $content);
		}else{
			redirect('/login');
		}
	}
	
	
	
	public function kurir_services(){
		$id_kurir = $_POST['id_kurir'];
		$id_kota_merchant = $_POST['id_kota_merchant'];
		$id_kota_customer = $_POST['id_kota_cust'];
		$weight = $_POST['weight'];
		
		$html = '';
		$post_kurir = array(
			"id_kurir"			=> $id_kurir,
			"id_kota_merchant"	=> $id_kota_merchant,
			"weight"			=> $weight,
			"id_kota_customer"	=> $id_kota_customer
		);
		//if((int)$id_kurir > 0 && (int)$merchant_id > 0){
			$url_kurir = URL_API.'purchaseServiceVersion2/get_price';
			$data_kurirs = $this->send_api->post_data($url_kurir, $post_kurir);
			$data_kurir = json_decode($data_kurirs);
			
			foreach($data_kurir->service_price as $sp){
				$html .= '<option value="'.$sp->id_service_kurir.'Þ'.$sp->price.'">'.$sp->service.' ('.$sp->service_code.')</option>';
			}
		//}
		echo $html;
	}
	
	function submit_payment(){
		$carts = $this->cart->contents();
		$merchant_id = '';
		$kode_pos = '';
		$id_kelurahan = '';
		$type_payment = isset($_POST['pay']) ? $_POST['pay'] : '';
		$payment_metode = isset($_POST['payment_metode']) ? $_POST['payment_metode'] : '';
		$input_admFee = isset($_POST['input_admFee']) ? $_POST['input_admFee'] : 0;
		$nama_kurir = isset($_POST['nama_kurir']) ? $_POST['nama_kurir'] : '';
		$kurir_services = isset($_POST['kurir_services']) ? $_POST['kurir_services'] : '';
		$input_deliveryFee = isset($_POST['input_deliveryFee']) > 0 ? $_POST['input_deliveryFee'] : 0;
		$grandTtl = isset($_POST['grandTtl']) ? $_POST['grandTtl'] : '';
		$address = isset($_POST['address']) ? $_POST['address'] : '';
		$voucher_code = isset($_POST['promo_code']) ? $_POST['promo_code'] : '';
		$type_voucher = isset($_POST['type_voucher']) ? $_POST['type_voucher'] : '';
		$voucher_amount = isset($_POST['voucher_amount']) ? $_POST['voucher_amount'] : '';
		$type_nilai_promo = isset($_POST['type_nilai_promo']) ? $_POST['type_nilai_promo'] : '';
		$ks = explode('Þ',$kurir_services);
		
		//$delivery = 2;
		//if($input_deliveryFee > 0){
			//$delivery = 1;
		//}
		$delivery = $_POST['input_deliveryType2'];
		
		$_menu_tambahans = array();
		$menu_tambahan = array();
		$details = array();
		$detail = array();
		$detailku = array();
		$pm = explode('Þ',$payment_metode);
		$adr = explode('Þ',$address);
		$id_payment_metode = $pm[0] > 0 ? $pm[0] : 0;
		$id_address = $adr[0];
		$user_id = $this->session->userdata('user_id');
		//$user_id = 906;
		
		$url_payment = URL_API.'purchaseServiceVersion2/checkout_order_ipay88_web';
		
		$data_post = array();
		
		if($type_payment == 'online'){
			$type_payment = 1;
		}
		if($type_payment == 'cash'){
			$type_payment = 2;
		}
		if($type_payment == 'transfer'){
			$type_payment = 3;
		}
		//error_log($type_payment);
		if(!empty($carts)){
			foreach($carts as $_carts){
				$_merchant_id = '';
				$_kode_pos = '';
				$_id_kelurahan = '';
				$_merchant_id = $_carts['merchant_id'];
				$_kode_pos = $_carts['kode_pos'];
				$_id_kelurahan = $_carts['id_kelurahan'];
			}
			//$_merchant_id = 930;
			//$_kode_pos = 1348;
			//$_id_kelurahan = 3982;
			$post_data = array(
				"kode_pos"		=> $_kode_pos,
				"id_kelurahan"	=> $_id_kelurahan
			);
			
			$url = URL_API.'merchantServiceVersion2/search_merchant';
			$data_api = $this->send_api->post_data($url, $post_data);
			$merchant_infos = json_decode($data_api);
			$minimum_order = '';
			$maxValue = '';
			$merchant_id_kota = '';
			$minValue = '';
			$time_pickup_delivery = '';
			$paymentMethod = '';
			$city_id = '';
			$takeawayAvailability = 0;
			$deliveryAvailability = 0;
			$area_name = '';
			$city_name = '';
			$merchant_name = '';
			$estimasi_hari_pengiriman_min = '';
			$estimasi_hari_pengiriman_max = '';
			$url_area = URL_API.'merchantServiceVersion2/get_area';
			$url_city = URL_API.'merchantServiceVersion2/get_location';
			$_pm = array();
			foreach($merchant_infos->merchant as $merchant_info){
				if($merchant_info->merchant_id == $_merchant_id){
					$minimum_order = $merchant_info->minimum_order;
					
					$merchant_id_kota = $merchant_info->id_kota;
					$city_id = $merchant_id_kota;
					
					$takeawayAvailability = $merchant_info->take_away > 0 ? $merchant_info->take_away : 0;
					$deliveryAvailability = $merchant_info->delivery > 0 ? $merchant_info->delivery : 0;
					$area_name = '';
					
					$merchant_name = $merchant_info->merchant_name;
					$estimasi_hari_pengiriman_min = $merchant_info->min_delivery_time;
					$estimasi_hari_pengiriman_max = $merchant_info->max_delivery_time;
				}
				//error_log($minimum_order);
				
				
				foreach($merchant_info->payment_method as $pms){
					
					if($pms->merchant_id == $_merchant_id){
						array_push($_pm, $pms->id_type_payment);
					}
					
				}
				
			}
			
			$paymentMethod = implode(",", $_pm);
			$data_city = $this->send_api->post_data($url_city, array('last_update'=>'2015-09-09'));
			$dt_city = json_decode($data_city);
			foreach($dt_city->city as $dc){
				if($dc->id_kota == $city_id){
					$city_name = $dc->nama_kota;
				}
			}
			
			$data_area = $this->send_api->post_data($url_city, array('last_update'=>'2015-09-09'));
			$dt_area = json_decode($data_area);
			foreach($dt_area->area as $da){
				if($da->id_area == $_kode_pos){
					$area_name = $da->area_name;
				}
			}
			

			//error_log(serialize($paymentMethod));
			
			$menu_tambahan = array();
			$xy = 0;
			foreach($carts as $c){
				if($xy > 0){
					$input_deliveryFee = 0;
				}
					$details[] = array(
						'minimum_order'			=> $minimum_order,
						'maxValue'				=> $c['max_value'],
						'merchant_id_kota'		=> $merchant_id_kota,
						'minValue'				=> $c['min_value'],
						'delivery'				=> $delivery,
						'id_kelurahan'			=> $_id_kelurahan,
						'delivery_fee'			=> $input_deliveryFee,
						'merchant_food_name'	=> $c['name'],
						'merchant_id'			=> $_merchant_id,
						'amount'				=> $c['subtotal'],
						'merchant_food_id'		=> $c['food_id'],
						'merchant_name'			=> $merchant_name,
						'qty'					=> $c['qty'],
						'type_payment'			=> $type_payment,
						'kodepos'				=> $_kode_pos,
						'total_amount'			=> $c['subtotal'] + $input_deliveryFee,
						'time_pickup_delivery'	=> '',
						'paymentMethod'			=> $paymentMethod,
						'berat_per_porsi'		=> $c['weight'],
						'city_id'				=> $city_id,
						'takeawayAvailability'	=> $takeawayAvailability,
						'deliveryAvailability'	=> $deliveryAvailability,
						'area_name'				=> $area_name,
						'ppn'					=> 0,
						'city_name'				=> $city_name,
						'notes'					=> $c['notes']
					); 
					
					$menu_tambahan = array();
					$_menu_tambahans = array();
					if(!empty($c['menu_tambahan'])){
						foreach($c['menu_tambahan'] as $ic){
							$menu_tambahan[] = array(
								'merchant_food_id'	=> $c['food_id'],
								'menu_id_package'	=> $ic['menu_id_package'],
								'choice_id'			=> $ic['id_choices']
							);
						}
					}
					//$_menu_tambahans = json_encode($menu_tambahan);
					
					//$details += array('menu_tambahan' => $menu_tambahan);
					$xy++;
			}
			
		}
	//	$_detail = json_encode($details);
	    $cnt_details = count($details);
		for($i=0;$i<$cnt_details;$i++){
			$jk[] = $details[$i];
		}
		$_detail = json_encode($jk);
	
		$data_post += array(
			'kode_pos'			=> $_kode_pos,
			'alamat'			=> !empty($adr[3]) ? $adr[3] : '',
			'customer_id'		=> $user_id,
			'voucher_amount'	=> $voucher_amount > 0 ? $voucher_amount : '0',
			'type_voucher'		=> $type_voucher,
			//'note'				=> !empty($adr[4]) ? $adr[4] : '',
			'note'				=> isset($_POST['notes_alamat']) && !empty($_POST['notes_alamat']) ? $_POST['notes_alamat'] : '',
			'voucher_code'		=> $voucher_code,
			'id_kota'			=> $city_id,
			'id_kelurahan'		=> $_id_kelurahan,
			'id_kurir'			=> $nama_kurir,
			'id_service_kurir'	=> $ks[0],
			'payment_id'		=> $pm[0],
			'total_admin_fee'	=> $input_admFee,
			'estimasi_hari_pengiriman_min'	=> $estimasi_hari_pengiriman_min,
			'estimasi_hari_pengiriman_max' 	=> $estimasi_hari_pengiriman_max
		);
		$data_post += array('detail_order' => $_detail);
		$_menu_tambahan = json_encode($menu_tambahan);
		$data_post += array('menu_tambahan' => $_menu_tambahan);
		if(!empty($menu_tambahan)){
			$_menu_tambahan = json_encode($menu_tambahan);
			$data_post += array('menu_tambahan' => $_menu_tambahan);
		}else{
			$data_post += array('menu_tambahan' => '');
		}
		//error_log(serialize($data_post));
		$data_payment = $this->send_api->post_data($url_payment, $data_post);
		$data_api = json_decode($data_payment);
		//var_dump($data_api);
		$this->cart->destroy();
		$res_url = '';
		if($data_api->status > 0 ){
			if($type_payment == 1){
				$res_url = $data_api->url_mobile;
				echo $res_url;
			}
			if($type_payment == 2){
				$res_url = site_url('order');
				echo $res_url;
			}
			if($type_payment == 3){

				$res_url = site_url('payment/paymenttransfer');
				echo $res_url.'/'.$data_api->id_purchase.'/'.$grandTtl;
			}
		}
	}
	
	
	public function chk_promo(){
		$user_id = $this->session->userdata('user_id');
		//$user_id = 906;
		$kode_pos = $_POST['kode_pos'];
		$promo_code = $_POST['promo_code'];
		$data_post = array(
			'customer_id'	=> $user_id,
			'kode_pos'		=> $kode_pos
		);
		$url_promo = URL_API.'customerService/voucher';
		$data_promo = $this->send_api->post_data($url_promo, $data_post);
		$data_api = json_decode($data_promo);
		$dt_promo = array();
		$promo_dt = array();
		if(!empty($data_api->voucher)){
			foreach($data_api->voucher as $da){
				array_push($dt_promo, $da->voucher_code);
				$promo_dt[$da->voucher_code] = $da;
			}
		}
		
		$res = array();
		if(in_array($promo_code, $dt_promo)){
			$res = $promo_dt[$promo_code];
		}
		echo json_encode($res);
	}
	
}
