<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	function __construct() {
        parent::__construct();
    }
	
	public function index()
	{
		//error_log(serialize($_POST));
		$id_food = isset($_POST['id_food']) ? $_POST['id_food'] : 0;
		$food_name = $_POST['food_name'];
		$cart_merchant = $_POST['cart_merchant'];
		$price = $_POST['price'];
		$choices = $_POST['choices'];
		$qty = $_POST['qty'];
		//$notes = !empty($_POST['notes']) ? $_POST['notes'] : '-';
		$merchant_id = $_POST['merchant_id'];
		$merchant_name = $_POST['merchant_name'];
		$kode_pos = $_POST['kode_pos'];
		$id_kelurahan = $_POST['id_kelurahan'];
		$weight = $_POST['berat_porsi'];
		$max_value = $_POST['max_value'];
		$min_value = $_POST['min_value'];
		$submit_cart = isset($_POST['submit_cart']) ? $_POST['submit_cart'] : 0;
		$img = $_POST['my_img'];
		if((!empty($cart_merchant) || $cart_merchant != '') && $submit_cart > 0){
			if($cart_merchant != $merchant_id){
				$this->cart->destroy();
			}
		}
		$cart_awal = $this->cart->contents();
		$prices = 0;
		$id_choicess = '';
		$pricess = '';
		$menu_package = '';
		$data_cart = array();
		$generate_random = $this->generateRandomString(9, $id_food);
		$choices_id = array();
		$menu_tambahan = array();
		$menu_package_id = array();
		$list_choices =  array();
		for($i=0;$i<sizeof($choices);$i++){
			$choicess = '';
			$menu_package = '';
			$choices_name = '';
			if($choices[$i] > 0){
				$choicess = explode('Þ', $choices[$i]);
				$id_choicess = $choicess[0];
				$pricess = $choicess[1];
				$menu_package = $choicess[2];
				$choices_name = $choicess[3];
				$prices += $pricess;
				$menu_tambahan[] = array(
					'id_choices'		=> $id_choicess,
					'menu_id_package'	=> $menu_package
				);
				//array_push($choices_id, $id_choicess);
				//array_push($menu_package_id, $menu_package);
			}
			array_push($list_choices, $choices_name);
		}
		$lc = '';
		if(!empty($list_choices)){
			$lc = implode(",",$list_choices);
		}
		$lc .= ' Notes :';
		$notes = !empty($_POST['notes']) ? $lc.' '.$_POST['notes'] : $lc.' -';
		$data_cart += array('menu_tambahan' => $menu_tambahan);
		//$this->session->set_userdata('menu_tambahan_'.$generate_random, $menu_tambahan);
		
		$ttl_prices = $price + $prices;
		$data_cart += array(
			'id'     		=> $generate_random,
			'qty'     		=> $qty,
			'price'   		=> $ttl_prices,
			'name'    		=> $food_name,
			'food_id'		=> $id_food,
			'hrg_asli'		=> $price,
			'notes'			=> $notes,
			'merchant_id'	=> $merchant_id,
			'merchant_name'	=> $merchant_name,
			'kode_pos'		=> $kode_pos,
			'weight'		=> $weight,
			'id_kelurahan'	=> $id_kelurahan,
			'img'			=> $img,
			'max_value'		=> $max_value,
			'min_value'		=> $min_value
		);
		
		
		$upd = 0;
		$cnt_menutambahan = count($menu_tambahan);
		$id_mt_awal = array();
		$id_mt_new = array();
		$result = '';
		$data = array();
		error_log($id_food);
		error_log($submit_cart);
		if($id_food > 0 && $submit_cart > 0){
			if(!empty($cart_awal)){
				$trigger_mt = 0;
				foreach($cart_awal as $ca){
					if($id_food == $ca['food_id'] && $notes == $ca['notes']){
						$trigger_mt = 0;
						$id_mt_awal = array();
						$id_mt_new = array();
						$data = array();
						$result = '';
						if(!empty($ca['menu_tambahan']) && $cnt_menutambahan > 0){
							foreach($ca['menu_tambahan'] as $mt){
								array_push($id_mt_awal, $mt['id_choices']);
							}
							for($n=0;$n<$cnt_menutambahan;$n++){
								array_push($id_mt_new, $menu_tambahan[$n]['id_choices']);
							}
							$trigger_mt = 0;
							$result = array_diff($id_mt_new, $id_mt_awal);
							$trigger_mt = count($result);
						}
						//error_log($trigger_mt);
						$data = array();
						if($trigger_mt == 0){
							$ttl_qty = $ca['qty'] + $qty;
							$data = array(
								'rowid' => $ca['rowid'],
								'notes'	=> $notes,
								'qty'   => $ttl_qty
							);
							$trigger_mt = 0;
							$upd = $this->cart->update($data); 
						}
					}
				}
				if($upd == 0){
					$this->cart->insert($data_cart);
				}
			}else{
				$this->cart->insert($data_cart);
			}
		}
		
		$carts = $this->cart->contents();
		$data['id']	= $id_food;
		$data['carts'] = $carts;
			
		if(!empty($carts)){
			$cart_merchant = '';
			foreach($carts as $c){
				$cart_merchant = '';
				$merchant_id 	= $c['merchant_id'];
				$kode_pos		= $c['kode_pos'];
				$id_kelurahan	= $c['id_kelurahan'];
			}
		}
		
		$post_data2 = array(
			"merchant_id"	=> $merchant_id,
			"kode_pos"		=> $kode_pos,
			"id_kelurahan"	=> $id_kelurahan
		);
			
		$url2 = URL_API.'merchantServiceVersion2/merchant_info';
		$data_api2 = $this->send_api->post_data($url2, $post_data2);
		$data['merchant_info'] = json_decode($data_api2);
		
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$data['submit_cart'] = $submit_cart;
		$data['merchant_id'] = $merchant_id;
		$data['kode_pos'] = $kode_pos;
		$data['id_kelurahan'] = $id_kelurahan;
		$content = $this->load->view('transaction/cart', $data, TRUE);
		$this->template->load($setting, $content);
		
	}
	
	public function reorder($id_purchase){
		//$this->cart->destroy();
		$post_data = array(
			"id_purchase"		=> $id_purchase
		);
		$url = URL_API.'purchaseServiceVersion2/get_cart';
		$list_cart = $this->send_api->post_data($url, $post_data);
		$list_carts = json_decode($list_cart);
		$data_cart = array();
		$menu_tambahan = array();
		$coiches = '';
		$choices_id = '';
		$prices = 0;
		$ttl_prices = 0;
		$merchant_id = '';
		$merchant_name = '';
		$kode_pos = '';
		$id_kelurahan = '';
		
		$id_food = '';
		$qty = '';
		$price = '';
		$name = '';
		$food_id = '';
		$notes = '';
		$merchant_id = '';
		$merchant_name =  '';
		$kode_pos = '';
		$weight = '';
		$id_kelurahan = '';
		$img = '';
		$max_value = '';
		$min_value = '';
		
		if($list_carts->status == 1){
			foreach($list_carts->cart as $m){
				$merchant_id = $m->merchant_id;
				$merchant_name = $m->merchant_name;
				$kode_pos = $m->kode_pos;
				$id_kelurahan = $m->id_kelurahan;
				
				foreach($m->menu as $c){
					$id_food = $c->merchant_food_id;
					$qty = $c->qty;
					$price = $c->amount;
					$name = $c->merchant_food_name;
					$food_id = $c->merchant_food_id;
					$notes = $c->notes;
					$merchant_id = $merchant_id;
					$merchant_name =  $merchant_name;
					$kode_pos = $kode_pos;
					$weight = 1000;
					$id_kelurahan = $id_kelurahan;
					$hrg_asli = $c->amount;
					$img = 'http://idijakbar.org/wakuliner/images/merchant_food/sJscCmlHV3Kzyx4.jpg';
					$max_value = 10;
					$min_value = 1;
					
					$data_cart += array(
						'id'     		=> $id_food,
						'qty'     		=> $qty,
						'name'    		=> $name,
						'food_id'		=> $food_id,
						'notes'			=> $notes,
						'merchant_id'	=> $merchant_id,
						'merchant_name'	=> $merchant_name,
						'kode_pos'		=> $kode_pos,
						'weight'		=> $weight,
						'id_kelurahan'	=> $id_kelurahan,
						'img'			=> $img,
						'max_value'		=> $max_value,
						'hrg_asli'		=> $hrg_asli,
						'min_value'		=> $min_value
					);
					
					foreach($c->menu_package as $mp){
						$coiches = $mp->choices;
						$pricess = $coiches->price;
						$prices += $pricess;
						$menu_tambahan[] = array(
							'id_choices'		=> $coiches->choices_id,
							'menu_id_package'	=> $mp->menu_id_package
						);
					}
					$ttl_prices = $price + $prices;
					$data_cart += array(
						'price'   		=> $ttl_prices,
						'menu_tambahan'	=> $menu_tambahan
						);
					
				}
			}
			
		}
		$this->cart->insert($data_cart);
		$carts = $this->cart->contents();
		
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		
		$data['carts'] = $carts;
		$content = $this->load->view('transaction/cart', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	
	public function del_cart(){
		$rowid = $_POST['rowid'];
		$data = array(
			'rowid'   => $rowid,
			'notes'	  => '',
			'qty'     => 0
		);
		
		$this->cart->update($data); 
		echo 1;
	}
	
	public function view(){
		$carts = $this->cart->contents();
		$data['carts'] = $carts;
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$content = $this->load->view('transaction/cart', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	public function upd_cart(){
		$rowid = $_POST['rowid'];
		$qty = $_POST['qty'];
		$notes = !empty($_POST['notes']) ? $_POST['notes'] : '-';
		$data = array(
					'rowid' => $rowid,
					'notes'	=> $notes,
					'qty'   => $qty
			);
		
		$this->cart->update($data);
		//$carts = $this->cart->contents();
		//error_log(serialize($carts));
	}
	
	public function del_all(){
		$this->cart->destroy();
		echo 1;
	}
	
	function generateRandomString($length = 10, $id_food=0) {
		$characters = '0123uvwxyzABCDE456789abcdefghijklmnopqrstFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		$randomString .=$id_food;
		return $randomString;
	}
	
}
