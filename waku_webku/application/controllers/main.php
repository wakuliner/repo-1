<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
	   
		$btn_login = isset($_POST['btn_login']) ? $_POST['btn_login'] : 0;
		$log_username_sedaap = isset($_POST['email_login']) ? $_POST['email_login'] : '';
		$log_password_sedaap = isset($_POST['pass_login']) ? $_POST['pass_login'] : '';
		$url = '';
		$data_api = '';
		$msg = '';
		$carts = $this->cart->contents();
		$cnt_carts = count($carts);
		$lang = 0;
		$setting = '';
		$set_lang = '';
		if($log_username_sedaap != ""){
			$post_data = array(
				"log_username_sedaap"		=> $log_username_sedaap,
				"log_password_sedaap"		=> $log_password_sedaap,
				"app_id"					=> 1
			);
			$url = URL_API.'accountService/login';
			$res_api = $this->send_api->post_data($url, $post_data);
			$data_api = json_decode($res_api);
			
			if($data_api->status == 1){
			    $post_data2 = array(
					"user_id"		=> $data_api->user_id
				);
				$url_setting = URL_API.'customerService/setting';
				$data_setting = $this->send_api->post_data($url_setting, $post_data2);
				$setting = json_decode($data_setting);
				foreach($setting->SETTING as $s){
					$lang = $s->language;
				}
				$set_lang = '';
				if($lang == 1){
					$set_lang = 'bhs_indo';
				}
				if($lang == 2){
					$set_lang = 'bhs_ingg';
				}
				$this->session->set_userdata('role_id',$data_api->role_id);
				$this->session->set_userdata('first_time_login', $data_api->first_time_login);
				$this->session->set_userdata('user_id',$data_api->user_id);
				$this->session->set_userdata('user_email',$data_api->user_email);
				$this->session->set_userdata('name',$data_api->name);
				$this->session->set_userdata('handphone',$data_api->handphone);
				$this->session->set_userdata('login_user',true);
				$this->session->set_userdata('lang_user', $set_lang);
				/*if($cnt_carts > 0){
					redirect(site_url('/payment', 'refresh'));
				}else{
					redirect(site_url('/'));
				}*/
				
				$data_api->cart = $cnt_carts;
				echo json_encode($data_api);
			}else{
				$msg = $data_api->message;
				echo $res_api;
			}
		}
		#$data['msg'] = $msg;
		#$data['content_register'] = "hide";
		#$data['cnt_cart'] = $cnt_carts;
		#$content = $this->load->view('login', $data, TRUE);
		#$this->template->load($setting, $content);
	}

	public function register()
	{
		$data['content_login'] = "hide";
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$content = $this->load->view('login', $data, TRUE);
		$this->template->load($setting, $content);
	}
	
	public function reg(){
		$nama = isset($_POST['nama']) ? $_POST['nama'] : '';
		$email = isset($_POST['email_register']) ? $_POST['email_register'] : '';
		$hp = isset($_POST['hp']) ? $_POST['hp'] : '';
		$ulang_tahun = isset($_POST['ulang_tahun']) ? date("Y-m-d", strtotime($_POST['ulang_tahun'])) : '';
		$pass = isset($_POST['pass']) ? $_POST['pass'] : '';
		$jns_kelamin = isset($_POST['jns_kelamin']) ? $_POST['jns_kelamin'] : 0;
		$post_data = array(
			"email"		=> $email,
			"name"		=> $nama,
			"handphone"	=> $hp,
			"password"	=> $pass,
			"dob"		=> $ulang_tahun,
			"gender"	=> $jns_kelamin
		);
		$url = URL_API.'accountService/register';
		$reg_api = $this->send_api->post_data($url, $post_data);
		$data_api2 = json_decode($reg_api);
		if($data_api2->status > 0){
			$post_data = array(
				"log_username_sedaap"		=> $email,
				"log_password_sedaap"		=> $pass,
				"app_id"					=> 1
			);
			$url = URL_API.'accountService/login';
			$res_api = $this->send_api->post_data($url, $post_data);
			$data_api = json_decode($res_api);
			if($data_api->status == 1){
				$this->session->set_userdata('role_id',$data_api->role_id);
				$this->session->set_userdata('first_time_login', 1);
				$this->session->set_userdata('user_id',$data_api->user_id);
				$this->session->set_userdata('user_email',$data_api->user_email);
				$this->session->set_userdata('name',$data_api->name);
				$this->session->set_userdata('handphone',$data_api->handphone);
				$this->session->set_userdata('login_user',true);
				$this->session->set_userdata('lang_user', 'bhs_indo');
			}
		}
		echo $reg_api;
	}
	
	public function forget_pass(){
		$email = isset($_POST['email_forget']) ? $_POST['email_forget'] : '';
		//error_log($email);
		$post_data = array(
			"user_email"	=> $email
		);
		$url = URL_API.'accountService/resetPassword';
		$data_api = $this->send_api->post_data($url, $post_data);
		echo $data_api;
	}

	public function logout() {
        $this->session->sess_destroy();
        redirect('home');
    }
}
