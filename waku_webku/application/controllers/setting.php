<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends MY_Controller {

	function __construct() {
        parent::__construct();
    }
	
	public function index()
	{
		$LOGIN = $this->session->userdata('login_user');
		if($LOGIN != true){
			redirect('/login');
		}else{
			$user_id = $this->session->userdata('user_id');
			//$user_id = 47;
			$post_data = array(
					"user_id"		=> $user_id
				);
			$url = URL_API.'customerService/setting';
			$data_api = $this->send_api->post_data($url, $post_data);
			$data['setting'] = json_decode($data_api);
			
			$this->load->library('user_agent');
	    	$data['browser'] = $this->agent->browser();
			
			$content = $this->load->view('user/setting', $data, TRUE);
			
			$this->template->load($setting, $content);
		}
	}
	
	public function send_setting(){
		$lang = $_POST['lang'];
		$notif = $_POST['notif'];
		$user_id = $this->session->userdata('user_id');
		//$user_id = 47;
		$post_data = array(
				"user_id"		=> $user_id,
				 "language"		=> $lang,
				 "notification"	=> $notif
			);
		$set_lang = '';
		if($lang == 1){
			$set_lang = 'bhs_indo';
		}
		if($lang == 2){
			$set_lang = 'bhs_ingg';
		}
		$url = URL_API.'customerService/modify_setting';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data_api = json_decode($data_api);
		$status =  $data_api->status;
		if($status == 1){
			$this->session->set_userdata('lang_user', $set_lang);
		}
		echo $status;
	}
	
	public function change_pass(){
		$old = $_POST['old'];
		$new = $_POST['new_pass'];
		$user_email = $this->session->userdata('user_email');
		$post_data = array(
				"user_email"		=> $user_email,
				"old_password"		=> $old,
				"new_password"		=> $new
			);
		$url = URL_API.'accountService/changePassword';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data_api = json_decode($data_api);
		echo $data_api->status;
	}
	
	public function account()
	{
		$LOGIN = $this->session->userdata('login_user');
		if($LOGIN != true){
			redirect('/login');
		}else{
			$user_id = $this->session->userdata('user_id');
			//$user_id = 906;
			$post_data = array(
					"user_id"		=> $user_id
				);
			
			$url = URL_API.'customerService/profile';
			$data_api = $this->send_api->post_data($url, $post_data);
			$data['profile'] = json_decode($data_api);
			
			$url2 = URL_API.'customerService/detail_rating_user';
			$data_api2 = $this->send_api->post_data($url2, $post_data);
			$data['rating'] = json_decode($data_api2);
			
			$post_data3 = array('last_update'=>"2015-09-09");
		    $url3 = URL_API.'merchantService/get_location';
		    $data_api3 = $this->send_api->post_data($url3, $post_data3);
		    $data['city'] = json_decode($data_api3);
		    
		    $this->load->library('user_agent');
	    	$data['browser'] = $this->agent->browser();
		
			$content = $this->load->view('user/account', $data, TRUE);
			
			$this->template->load($setting, $content);
		}
	}
	
	public function get_area()
	{
		//$post_data = array('last_update'=>"2015-09-09","id_kota"=>$id_kota);
		$id_kota = !empty($_POST['id_kota']) ? $_POST['id_kota']: '';
		$selected_val = !empty($_POST['selected_val']) ? $_POST['selected_val']: '0';
		$url = URL_API.'merchantServiceVersion2/get_location/'.$id_kota;
		
		$data_api	= $this->send_api->send_data($url, '');
		ini_set('max_execution_time', 100000);
		$data		= json_decode($data_api);
		//error_log($url);
		//error_log(serialize($data->area));
		$html = '';
		$html = '<select class="form-control" name="id_area" id="id_areaa">
                  <option>Pilih Area</option>';
		foreach($data->area as $d):
			if($selected_val > 0  && $selected_val == $d->id_area){
				$html .= '<option value="'.$d->id_area.'Þ'.$d->kode_pos.'Þ'.$d->id_kelurahan.'" selected>'.$d->area_name.'</option>';
			}else{
				$html .= '<option value="'.$d->id_area.'Þ'.$d->kode_pos.'Þ'.$d->id_kelurahan.'">'.$d->area_name.'</option>';
			}
		endforeach;
		$html .= '</select>';
		
		echo $html;
	}
	
	public function save_edit_address(){
		$user_id = $this->session->userdata('user_id');
		$id_kota = isset($_POST['id_kota']) ? $_POST['id_kota'] : '';
		$dtku = isset($_POST['id_area']) ? $_POST['id_area'] : '';
		$address = isset($_POST['address']) ? $_POST['address'] : '';
		$id_address = isset($_POST['id_address']) ? $_POST['id_address'] : '0';
		$type_action = 1;
		if($id_address > 0){
			$type_action = 2;
		}
		$dt = explode('Þ',$dtku);
		$id_area = $dt[0];
		$kode_pos = $dt[1];
		$id_kelurahan = $dt[2];
		$post_data = array(
			"type_action" 	=> $type_action,
			"user_id"	  	=> $user_id,
			"address" 	  	=> $address,
			"city"		  	=> $id_kota,
			"kode_pos"		=> $id_area,
			"id_address"	=> $id_address,
			"detail"		=> '',
			"id_kelurahan"	=> $id_kelurahan
		);
		//error_log(serialize($post_data));
		$url = URL_API.'customerService/add_address';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data_api = json_decode($data_api);
		//if($data_api->status == 1){
			//echo $data_api->message;
		//}
		echo $data_api->status;
	}
	
	public function save_address(){
		$user_id = $this->session->userdata('user_id');
		$type_action = isset($_POST['type_action']) ? $_POST['type_action'] : '';
		$id_address = isset($_POST['id_address']) ? $_POST['id_address'] : '';
		//$user_id = 906;
		$post_data = array(
				"user_id"		=> $user_id,
				"type_action"	=> $type_action,
				"id_address"	=> $id_address
		);
		
		$url = URL_API.'customerService/add_address';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data_api = json_decode($data_api);
		//if($data_api->status == 1){
			//echo $data_api->message;
		//}
		echo $data_api->status;
	}
	
	public function edit_account(){
		$user_id = $this->session->userdata('user_id');
		$fullname = isset($_POST['fullname']) ? $_POST['fullname'] : '';
		$telp = isset($_POST['telp']) ? $_POST['telp'] : '';
		$hp = isset($_POST['hp']) ? $_POST['hp'] : '';
		$email_edit = isset($_POST['email_edit']) ? $_POST['email_edit'] : '';
		//$user_id = 906;
		$post_data = array(
				"user_id"		=> $user_id,
				"email"			=> $email_edit,
				"first_name"	=> $fullname,
				"telp"			=> $telp,
				"handphone"		=> $hp
		);
		
		$url = URL_API.'customerService/edit_profile';
		echo $this->send_api->post_data($url, $post_data);
		//$data_api = json_decode($data_api);
	}
	
}
