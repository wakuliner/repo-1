<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reward extends CI_Controller {

	function __construct() {
        parent::__construct();
    }
	
	public function index()
	{
		$customer_id = $this->session->userdata('user_id');
		//$customer_id = 1180;
		$url = URL_API.'customerService/referral';
		$post_data = array(
				"customer_id"		=> $customer_id
			);
		$data_referal = $this->send_api->post_data($url, $post_data);
		$data['data_referal'] = json_decode($data_referal);
		
		$post_reward = array(
				"user_id"		=> $customer_id
			);
		$url_rewards = URL_API.'accountService/sweeptakes';
		$data_rewards = $this->send_api->post_data($url_rewards, $post_reward);
		$data['data_rewards'] = json_decode($data_rewards);
		
		$url_reward2 = URL_API.'customerService/rewards';
		$data_reward2 = $this->send_api->post_data($url_reward2, $post_data);
		$data['data_reward2'] = json_decode($data_reward2);
		
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		
		$content = $this->load->view('user/reward', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	function submit_referal(){
		$customer_id = $this->session->userdata('user_id');
		$referral_code = $_POST['referral_code'];
		//$customer_id = 1189;
		$post_data = array(
				"customer_id"		=> $customer_id,
				"referral_code"		=> $referral_code
			);
		$url = URL_API.'customerService/enter_referral_code';
		$data_api = $this->send_api->post_data($url, $post_data);
		$res = json_decode($data_api);
		echo $res->message; 
	}
}
