<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

	function __construct() {
        parent::__construct();
    }
	
	public function index()
	{	
		
		$data['content_login'] = "hide";
		$data['content_register'] = "hide";
		
		$data['page'] = "home";
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$content = $this->load->view('home', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
}
