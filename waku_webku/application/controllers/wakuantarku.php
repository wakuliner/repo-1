<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wakuantarku extends CI_Controller {

	function __construct() {
        parent::__construct();
    }
	
	public function index($nama_merchant = '')
	{
		$post_data = array(
			'last_update'		=> '2015-09-09'
		);
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$res = '';
		$mn = '';
		$merchant_id = 0;
		if(!empty($nama_merchant) && $nama_merchant != 'static' && $nama_merchant != 'search'){
			$res = '';
			$mn = '';
			$merchant_id = 0;
			$mn = str_replace('-',' ', $nama_merchant);
			$this->db->select('merchant_id');
			$this->db->where('merchant_name', $nama_merchant);
			$res = $this->db->get(merchant)->row();
			$merchant_id = $res->merchant_id;
			$kode_pos = $this->session->userdata('kode_pos');
			$id_kelurahan = $this->session->userdata('id_kelurahan');
			$statusCari = '';
			$this->menu($merchant_id, $kode_pos, $id_kelurahan, $statusCari);
		}else{
			$content = $this->load->view('wakuantar/home', $data, TRUE);
			$setting['set_menu'] = array('mn_wakuantar' => 'active');
	
			$this->template->load($setting, $content);

		}
		//$url_city = URL_API.'merchantServiceVersion2/get_location';
		//$list_city = $this->send_api->send_data($url_city, $post_data);
		//$data['list_city'] = json_decode($list_city);
		
	}
	
	public function get_kota($a='', $b=''){
		$nama_kota = !empty($_POST['nama_kota']) ? $_POST['nama_kota']: '';
		$post_data = array(
			'last_update'		=> '2015-09-09'
		);
		$url_city = URL_API.'merchantServiceVersion2/get_location';
		$list_city = $this->send_api->send_data($url_city, $post_data);
		$list_cities = json_decode($list_city);
		$html = '';
		$test = '';
		$nama_kotaku = '';
		$res = array();
		if($list_cities->status == 1){
			foreach($list_cities->city as $lc){
				
				if(!empty($nama_kota)){
					if(strtolower($lc->nama_kota) == strtolower($nama_kota)){
						if($lc->top > 0){
							$res['others'][] = '';
							$res['top'][] = '<a href="#" class="list-group-item wk_city" onclick="showAreas('.$lc->id_kota.',\''.$lc->nama_kota.'\');"><span class="cityTeks">'.$lc->nama_kota.'</span></a>';
						}else{
							$res['top'][] = '';
							$res['others'][] = '<a href="#" class="list-group-item wk_city" onclick="showAreas('.$lc->id_kota.',\''.$lc->nama_kota.'\');"><span class="cityTeks">'.$lc->nama_kota.'</span></a>';
						}
					}else{
						$res['top'][] = '';
						$res['others'][] = '';
					}
				}else{
					if($lc->top > 0){
							$res['top'][] = '<a href="#" class="list-group-item wk_city" onclick="showAreas('.$lc->id_kota.',\''.$lc->nama_kota.'\');"><span class="cityTeks">'.$lc->nama_kota.'</span></a>';
						}else{
							$res['others'][] = '<a href="#" class="list-group-item wk_city" onclick="showAreas('.$lc->id_kota.',\''.$lc->nama_kota.'\');"><span class="cityTeks">'.$lc->nama_kota.'</span></a>';
						}
				}
				
			}
		}
		
		echo json_encode($res); 
	}
	
	public function get_area(){
		$id_kota = !empty($_POST['id_kota']) ? $_POST['id_kota']: '';
		$nama_area = !empty($_POST['nama_area']) ? $_POST['nama_area']: '';
		
		$url_area = URL_API.'merchantServiceVersion2/get_location/'.$id_kota;
		ini_set('max_execution_time', 100000);
		$list_area = $this->send_api->send_data($url_area, '');
		$list_areas = json_decode($list_area);
		$html = '';
		//if($id_kota > 0){
			if($list_areas->status == 1){
				foreach($list_areas->area as $lc){
					if(!empty($nama_area)){
						if(strtolower($nama_area) == strtolower($lc->area_name)){
							$html .= '<a href="#" class="list-group-item wk_city" onclick="selectAreas('.$lc->id_area.','.$lc->id_kelurahan.',\' '.$lc->area_name.'\');"><span class="areaTeks">'.$lc->area_name.'</span></a>';
						}
					}else{
						$html .= '<a href="#" class="list-group-item wk_city" onclick="selectAreas('.$lc->id_area.','.$lc->id_kelurahan.',\' '.$lc->area_name.'\');"><span class="areaTeks">'.$lc->area_name.'</span></a>';
					}
				}
			}
		//}
		echo $html;
		//print_r($list_areas);
	}
	
	public function search()
	{
        
		$kode_pos = isset($_POST['kode_pos']) ? $_POST['kode_pos'] : 0;
		$id_kelurahan = isset($_POST['id_kelurahan']) ? $_POST['id_kelurahan'] : 0;
		$location = isset($_POST['Input']) ? $_POST['Input'] : '';
		$kota = isset($_POST['my_city']) ? $_POST['my_city'] : '';
		$statuscari = isset($_POST['status']) ? $_POST['status'] : '';
		//$kode_pos =66;
		//$id_kelurahan = 74;
		$post_data = array(
			"kode_pos"		=> $kode_pos,
			"food_name"		=> '',
			"id_kelurahan"	=> $id_kelurahan
		);
		$this->session->set_userdata($post_data);
		$url = URL_API.'merchantService/get_tag';
		$data_api = $this->send_api->post_data($url, '');
		
		$data['filter_merchant'] = json_decode($data_api);
		$data['kode_pos'] = $kode_pos;
		$data['id_kelurahan'] = $id_kelurahan;
		$data['location'] = $location;
		$data['my_city'] = $kota;
		$data['statuscari'] = $statuscari;
        $setting['set_menu'] = array('mn_wakuantar' => 'active');
        
        $this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		
		$content = $this->load->view('wakuantar/list', $data, TRUE);
		
		$this->template->load($setting, $content);
	}

	
	public function load_merchants(){
		$kode_pos = isset($_POST['kode_pos']) ? $_POST['kode_pos'] : 0;
		$id_kelurahan = isset($_POST['id_kelurahan']) ? $_POST['id_kelurahan'] : 0;
		$food_name = isset($_POST['food_name']) ? $_POST['food_name'] : '';
		$tag = isset($_POST['tag']) ? $_POST['tag'] : '';
		$statuscari = isset($_POST['statuscari']) ? $_POST['statuscari'] : '';
		if(!empty($tag)){
			$tag = implode(',', $tag);
		}
		$post_data = array(
			"kode_pos"		=> $kode_pos,
			"tag"			=> $tag,
			"food_name"		=> $food_name,
			"id_kelurahan"	=> $id_kelurahan
		);
		
		//filtering URL_API
		if($statuscari == "1"){//siapsaji
			$url = URL_API.'merchantServiceVersion2/search_merchant';
			$data['statusCari'] = 1;
		}
		else{//snack
			$url = URL_API.'merchantServiceVersion2/search_merchant_new_list';
			$data['statusCari'] = 2;
		}
		$data_api = $this->send_api->post_data($url, $post_data);
		$data['list_merchant'] = json_decode($data_api);
		$data['kode_pos'] = $kode_pos;
		$data['id_kelurahan'] = $id_kelurahan;
		$this->load->view('wakuantar/load_merchants', $data);
	}
	
	public function menu($merchant_id = 0, $kode_pos = 0, $id_kelurahan = 0, $statusCari = 0){
		$post_data = array(
			"merchant_id"	=> $merchant_id,
			"kode_pos"		=> $kode_pos,
			"id_kelurahan"	=> $id_kelurahan,
			"statusCari"   => $statusCari
		);
		$url = URL_API.'merchantService/merchant_info';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data['merchant_info'] = json_decode($data_api);
		$url2 = URL_API.'merchantService/menu';
		$data_api2 = $this->send_api->post_data($url2, $post_data);
		$data['kode_pos'] = $kode_pos;
		$data['id_kelurahan'] = $id_kelurahan;
		$data['list_menu'] = json_decode($data_api2);
        $setting['set_menu'] = array('mn_wakuantar' => 'active');
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$data['statusCari'] = $statusCari;
		$content = $this->load->view('wakuantar/menu', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	public function menu_detail($merchant_id = 0, $kode_pos = 0, $id_kelurahan = 0, $statusCari = 0, $food_id = 0){
		$post_data = array(
			"merchant_id"	=> $merchant_id,
			"kode_pos"		=> $kode_pos,
			"id_kelurahan"	=> $id_kelurahan
		);
		$url = URL_API.'merchantService/merchant_info';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data['merchant_info'] = json_decode($data_api);
		$url2 = URL_API.'merchantServiceVersion2/menu';
		$data_api2 = $this->send_api->post_data($url2, $post_data);
		$data['kode_pos'] = $kode_pos;
		$data['id_kelurahan'] = $id_kelurahan;
		$list_menu = json_decode($data_api2);
		$food = array();
		foreach($list_menu->food as $f){
			if($f->merchant_food_id == $food_id){
				$food = $f;
			}
		}
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$data['list_menu'] = $food;
        $setting['set_menu'] = array('mn_wakuantar' => 'active');
    $data['statusCari'] = $statusCari;		
		
		$content = $this->load->view('wakuantar/menu_detail', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	public function get_area_ll(){
		$lat = $_POST['lat'];
		$long = $_POST['longitude'];
		$post_data = array(
			"long"	=> $long,
			"lat"	=> $lat
		);
		$url = URL_API.'merchantServiceVersion2/get_location_gps';
		$data_api = $this->send_api->post_data($url, $post_data);
		echo $data_api; 
	}

	
}
