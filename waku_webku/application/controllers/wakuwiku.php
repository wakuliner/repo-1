<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wakuwiku extends MY_Controller {

	function __construct() {
        parent::__construct();
    }
	
	public function index()
	{
		$url = URL_API.'merchantGotoService/get_kota';
		$data_api = $this->send_api->post_data($url, '');
		$data['kota'] = json_decode($data_api);
		
        $setting['set_menu'] = array('mn_wakuwiku' => 'active');
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$content = $this->load->view('wakuwiku/home', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	public function search()
	{
		$nama_kota = isset($_POST['kota']) ? $_POST['kota'] : 0; 
		#print_r($nama_kota);
		$id_kota = isset($_POST['id_kota']) ? $_POST['id_kota'] : 0;
		$input_area = isset($_POST['input_area']) ? $_POST['input_area'] : 0;
		$dt = explode('Þ', $input_area);
		$kode_pos = $dt[0];
		$id_kelurahan = $dt[1];
		$id_area = $dt[2];
		$kode_pos =66;
		$id_kelurahan = 73;
		$post_data = array(
			"kode_pos"		=> $kode_pos,
			"food_name"		=> '',
			"id_kelurahan"	=> $id_kelurahan
		);
		
		$url = URL_API.'merchantService/get_tag';
		$data_api = $this->send_api->post_data($url, '');
		
		$data['filter_merchant'] = json_decode($data_api);
		$data['nama_kota'] = $nama_kota;
		$data['id_kota'] = $id_kota;
		$data['kode_pos'] = $kode_pos;
		$data['id_kelurahan'] = $id_kelurahan;
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
        $setting['set_menu'] = array('mn_wakuwiku' => 'active');
		
		$content = $this->load->view('wakuwiku/list', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	public function load_merchants(){
		$sortlist = isset($_POST['sortlist']) ? $_POST['sortlist'] : '2';
		$merchant_name = isset($_POST['merchant_name']) ? $_POST['merchant_name'] : '';
		$tag = isset($_POST['tag']) ? $_POST['tag'] : '';
		$id_kota = isset($_POST['id_kota']) ? $_POST['id_kota'] : 0;
		
		if(!empty($tag)){
			$tag = implode(',', $tag);
		}
		$post_data = array(
			"merchant_name"	=> $merchant_name,
			"tag"			=> $tag,
			"sort_by"		=> $sortlist,
			"id_kota"		=> $id_kota
		);
		$url = URL_API.'merchantGotoService/search_merchant';
		$data_api = $this->send_api->post_data($url, $post_data);
		
		$data['list_merchant'] = json_decode($data_api);
		$this->load->view('wakuwiku/load_merchants', $data);
	}
	
	public function merchant($merchant_id = 0)
	{
		$post_data = array("merchant_id"	=> $merchant_id);
		$url = URL_API.'merchantGotoService/merchant_info';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data['merchant_info'] = json_decode($data_api);
		
		$post_data4= array("merchant_id"	=> $merchant_id);
		$url4 = URL_API."merchantGotoService/merchant_rating";
		$data_api4 = $this->send_api->post_data($url4, $post_data4);
		$data['merchant_rating'] = json_decode($data_api4);
		
        $setting['set_menu'] = array('mn_wakuwiku' => 'active');
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$data['merchant_id'] = $merchant_id;
		$content = $this->load->view('wakuwiku/merchant', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	public function merchant_menu($merchant_id = 0)
	{
		$post_data = array("merchant_id"	=> $merchant_id);
		$url = URL_API.'merchantGotoService/merchant_info';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data['merchant_info'] = json_decode($data_api);
		
		$post_data2= array("merchant_id"	=> $merchant_id);
		$url2 = URL_API."merchantGotoService/menu";
		$data_api2 = $this->send_api->post_data($url2, $post_data2);
		$data['merchant_menu'] = json_decode($data_api2);
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
        $setting['set_menu'] = array('mn_wakuwiku' => 'active');
		$data['merchant_id'] = $merchant_id;
		$content = $this->load->view('wakuwiku/merchant_menu', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	public function merchant_galeri($merchant_id = 0)
	{
		$post_data = array("merchant_id"	=> $merchant_id);
		$url = URL_API.'merchantGotoService/merchant_info';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data['merchant_info'] = json_decode($data_api);
		
		$post_data3= array("merchant_id"	=> $merchant_id);
		$url3 = URL_API."merchantGotoService/gallery";
		$data_api3 = $this->send_api->post_data($url3, $post_data3);
		$data['merchant_galery'] = json_decode($data_api3);
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
        $setting['set_menu'] = array('mn_wakuwiku' => 'active');
		$data['merchant_id'] = $merchant_id;
		$content = $this->load->view('wakuwiku/merchant_galeri', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	public function merchant_rating($merchant_id = 0)
	{
		$post_data = array("merchant_id"	=> $merchant_id);
		$url = URL_API.'merchantGotoService/merchant_info';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data['merchant_info'] = json_decode($data_api);
		
		$post_data4= array("merchant_id"	=> $merchant_id);
		$url4 = URL_API."merchantGotoService/merchant_rating";
		$data_api4 = $this->send_api->post_data($url4, $post_data4);
		$data['merchant_rating'] = json_decode($data_api4);
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
        $setting['set_menu'] = array('mn_wakuwiku' => 'active');
		$data['merchant_id'] = $merchant_id;
		$content = $this->load->view('wakuwiku/merchant_rating', $data, TRUE);
		
		$this->template->load($setting, $content);
	}
	
	public function send_rating()
	{
		$cust_id = $this->session->userdata('user_id');
		if($cust_id!=""):
			$merchant_id	= $_POST['merchant_id'];
			#$cust_id		= "958";
			$post_data = array("merchant_id"	=> $merchant_id,
							   "customer_id"	=> $cust_id,
							   "rating"			=> $_POST['rating'],
							   "review"			=> $_POST['review']);
			$url = URL_API.'merchantGotoService/add_review_rating';
			$data_api	= $this->send_api->post_data($url, $post_data);
			$data 		= json_decode($data_api);
			echo $data->message;
		else:
			echo "silahkan login terlebih dahulu";
		endif;
	}
	
	public function get_area_ll(){
		$lat = $_POST['lat'];
		$long = $_POST['longitude'];
		$post_data = array(
			"long"	=> $long,
			"lat"	=> $lat
		);
		$url = URL_API.'merchantGotoService/get_location_gps';
		$data_api = $this->send_api->post_data($url, $post_data);
		echo $data_api; 
	}
}
