<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Authentification extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function submit_login() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == TRUE && !$this->partner_id) {
            $post_data = array(
                'username' => $this->input->post('username'),
                'password' => $this->converter->encode($this->input->post('password')),
            );

            try {
                $query = $this->db->query("SELECT * FROM mstr_user WHERE LCASE(username) = '" . $this->db->escape_str($post_data['username']) . "' AND password = '" . $this->db->escape_str($post_data['password']) . "'")->result();

                if ($query) {
                    switch ($query[0]->status) {
                        case "off":
                            $result['status'] = false;
                            $result['code'] = '3';
                            $result['message'] = 'Silahkan hubungi pengelola toko, Akun anda tidak aktif.';
                            break;
                        case "on":
                            $this->session->unset_userdata('login_data');
							
                            $this->session->set_userdata('login_data', array('id' => $this->converter->encode($query[0]->user_id), 'detail' => (array) $query[0]));

                            $redirect = $this->input->post('redirect');

                            $result['status'] = true;
                            $result['code'] = '0';
                            $result['message'] = (($redirect) ? base_url() . $redirect : base_url('home'));
                            break;
                    }
                } else {
                    $result['status'] = false;
                    $result['code'] = '4';
                    $result['message'] = 'Login gagal. Username dan Password salah.';
                }
            } catch (Exception $e) {
                $result['status'] = false;
                $result['code'] = '2';
                $result['message'] = $e->getMessage();
            }
        } else {
            $result['status'] = false;
            $result['code'] = '1';
            $result['message'] = 'Silahkan cek kesalahan dalam pengisian form.';
            $result['error'] = array(
                'username' => form_error('username'),
                'password' => form_error('password'),
            );
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
    }
	
	public function submit_manager() {
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');
		
		if ($this->form_validation->run() == TRUE){
			if($this->input->post("password")=="123456"):
				$this->session->unset_userdata('login_data');
				$this->session->set_userdata('login_data', array('id' => $this->converter->encode("manager")));						
				$result['status'] = true;
				$result['code'] = '0';
				$result['message'] = (($redirect) ? base_url() . $redirect : base_url('manager/home'));
			else:
				$result['status'] = false;
				$result['code'] = '1';
				$result['message'] = 'Password salah.';	
				$result['error'] = array(
					'password' => "Password salah",
				);
			endif;
		} else {
            $result['status'] = false;
            $result['code'] = '1';
            $result['message'] = 'Silahkan cek kesalahan dalam pengisian form.';
            $result['error'] = array(
                'password' => form_error('password'),
            );
        }
		
		$this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
	}
}

/* End of file authentification.php */
/* Location: ./application/controller/authentification.php */
