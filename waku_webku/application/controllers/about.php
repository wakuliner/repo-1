<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	function __construct() {
        parent::__construct();
    }
	
	public function index($tipe)
	{
	    $this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$user_id = $this->session->userdata('user_id');
		//$user_id = 47;
		$post_data = array(
				"user_id"		=> $user_id
			);
		$url = URL_API.'customerService/setting';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data_api2 = json_decode($data_api);
		$lang = 0;
		$notif = 0;
		foreach($data_api2->SETTING as $s){
			$lang = $s->language;
			$notif = $s->notification;
		}
		$post_data2 = array(
			"app_id"	=> 2,
			"language"	=> $lang > 0 ? $lang : 1
		);
		$url2 = URL_API.'customerService/help';
		$data_apii = $this->send_api->post_data($url2, $post_data2);
		$data['list_about'] = json_decode($data_apii);
		$data['tipe']		= $tipe;
		
		$content = $this->load->view('about', $data, TRUE);
		$this->template->load($setting, $content);
	}
}
