<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	function __construct() {
        parent::__construct();
    }
	
	public function index()
	{
		$customer_id = $this->session->userdata('user_id');
		//$customer_id = 945;
		$post_data = array(
				"customer_id"		=> $customer_id
			);
		
		$url = URL_API.'customerService/OrderCustomer';
		$data_api = $this->send_api->post_data($url, $post_data);
		$data['list_order'] = json_decode($data_api);
		
		$url2 = URL_API.'customerService/get_rating_order_merchant';
		$data_api2 = $this->send_api->post_data($url2, $post_data);
		$data['get_rating_order_merchant'] = json_decode($data_api2); 
		
		$url3 = URL_API.'customerService/OrderPast';
		$data_api3 = $this->send_api->post_data($url3, $post_data);
		$data['order_past'] = json_decode($data_api3); 
		
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		
		$content = $this->load->view('transaction/order', $data, TRUE);
		$this->template->load($setting, $content);
	}
	
	public function received(){
		$merchant_id = $_POST['merchant_id'];
		$id_purchase = $_POST['id_purchase'];
		$data_post = array(
			'type'			=> 4,
			'merchant_id'	=> $merchant_id,
			'order_id'		=> $id_purchase
		);
		$url = URL_API.'merchantServiceVersion2/StageOrder';
		$data_api = $this->send_api->post_data($url, $data_post);
		$res = json_decode($data_api);
		echo $res->status; 
	}
	
	public function rate_merchant(){
		$merchant_id = $_POST['merchant_id'];
		$id_purchase = $_POST['id_purchase'];
		$rating = $_POST['rating'];
		$description = $_POST['description'];
		$customer_id = $this->session->userdata('user_id');
		//$customer_id = 1206;
		$data_post = array(
			'customer_id'	=> $customer_id,
			'rating'		=> $rating,
			'merchant_id'	=> $merchant_id,
			'id_purchase'	=> $id_purchase,
			'description'	=> $description
		);
		$url = URL_API.'customerService/rate_merchant';
		$data_api = $this->send_api->post_data($url, $data_post);
		$res = json_decode($data_api);
		echo $res->status; 
	}
	
	public function del_order(){
		$customer_id = $this->session->userdata('user_id');
		$order_id = $_POST['order_id'];
		//$customer_id = 945;
		$data_post = array(
			'customer_id'	=> $customer_id,
			'id_purchase'	=> $order_id
		);
		$url = URL_API.'customerService/delete_order';
		$data_api = $this->send_api->post_data($url, $data_post);
		$res = json_decode($data_api);
		echo $res->status; 
	}
}
