<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
	
if (!function_exists('static_file')) {

    function static_file() {
        $CI = & get_instance();

        return $CI->config->item('static_file');
    }

}

if (!function_exists('brand_url')) {

    function brand_url() {
        $CI = & get_instance();

        return $CI->config->item('brand_url');
    }

}

/* End of file common_helper.php */
/* Location: ./application/helpers/common_helper.php */