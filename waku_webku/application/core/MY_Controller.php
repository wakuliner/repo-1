<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	private $user_id = null;
    private $user_data = null;
	private $unit_kerja = null;

    function __construct() {
        parent::__construct();

        if ($this->session->userdata('login_data')) {
            $login_data = $this->session->userdata('login_data');

                $this->user_id = $login_data['id'];
                $this->user_data = $login_data['detail'];
				$this->unit_kerja = $login_data['detail']['kdUnitKerja'];
        }
    }

    function check_logged($login = false) {
        if ($this->user_id) {
            if ($login) {
                redirect('home', 'refresh');
            }
        } else {
            if (!$login) {
                $this->session->sess_destroy();

                redirect(base_url(), 'refresh');
            }
        }
    }

    function get_user_id() {
        return $this->user_id;
    }
	
	function get_unit_kerja() {
        return $this->unit_kerja;
    }

    function get_user_data() {
        return $this->user_data;
    }

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */